<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Document;
use App\Models\Event;
use App\Models\Occurrence;
use App\Models\Relase;
use App\Models\Survey;
use App\Models\Topic;
use App\Models\Dependent;
use App\Models\Employee;
use App\Models\Pet;
use App\Models\Reservation;
use App\Models\User;
use App\Models\Vehicle;
use App\Models\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;


class AccountController extends Controller
{

    protected $request;
    private $repository;
    
    public function __construct(Request $request, User $user)
    {
        
        $this->request = $request;
        $this->repository = $user;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function feed()
    {

        // UPDATE MESSAGE WELCOME
        User::where('id', Auth::id())->update(['welcome' => true]);

        // GET ALL DATA
        $relases = Relase::where('condominium', Auth::user()->condominium)->take(3)->get();
        $topics = Topic::where('condominium', Auth::user()->condominium)->where('status', 'Aprovado')->get()->take(2);
        $occurrences = Occurrence::where('condominium', Auth::user()->condominium)->where('status', 'Aprovada')->get()->take(2);
        $documents = Document::where('condominium', Auth::user()->condominium)->take(1)->get();
        $surveys = Survey::where('condominium', Auth::user()->condominium)->take(3)->get();
        $events = Event::where('condominium', Auth::user()->condominium)->take(2)->get();
        $albums = Album::where('condominium', Auth::user()->condominium)->take(2)->get();


        // RETURN VIEW WITH DATA
        return view('pages.index', [
            'relases' => $relases,
            'topics' => $topics,
            'occurrences' => $occurrences,
            'documents' => $documents,
            'surveys' => $surveys,
            'events' => $events,
            'albums' => $albums ,
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(in_array(Auth::user()->role, ['administrador', 'sindico'])){
            return redirect()->route('admin.index');
        }

        // GET DATA USER
        $users = Auth::user();

        // GET DATAS
        $myVehicles = Vehicle::orderBy('updated_at', 'DESC')->where('user', $users->id)->get();
        $myPets = Pet::orderBy('updated_at', 'DESC')->where('user', $users->id)->get();
        $myEmployees = Employee::orderBy('updated_at', 'DESC')->where('user', $users->id)->get();
        $myVisitors = Visitor::orderBy('updated_at', 'DESC')->where('user', $users->id)->get();
        $myDependents = Dependent::orderBy('updated_at', 'DESC')->where('user', $users->id)->get();
        $reservations = Reservation::all()->where('user', $users->id)->take(5);

        // YEARS OF USER
        $age = date_diff(date_create($users->birth), date_create(date("d-m-Y")));

        return view('pages.user.index')->with([
            'titlePage' => 'Minha conta',
            'user' => $users,
            'age' => $age->format("%y"),
            'vehicles' => $myVehicles,
            'pets' => $myPets,
            'employees' => $myEmployees,
            'visitors' => $myVisitors,
            'dependents' => $myDependents,
            'reservations' => $reservations,
        ]);

    }


    // DEFINE FOLDER ROUTE

    public function folderRoute()
    {

        return "condominios/" . Auth::user()->condominium . "/usuarios/" . Auth::user()->id . "/";

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

        // GET DATA USER
        $users = Auth::user();

        // YEARS OF USER
        $age = date_diff(date_create($users->birth), date_create(date("d-m-Y")));

        // CONVERT SELECTED OPTIONS IN TABLE TO STRING

        return view('pages.user.edit')->with([
            'titlePage' => 'Editar minha conta',
            'user' => $users,
            'age' => $age->format("%y"),
            'route' => route('account.update'),
        ]);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        if (time() < strtotime('+18 years', strtotime($request->birth))) {
            return redirect()
                ->back()
                ->with('message', 'Menores não podem usar o sistema.');
        }

        // GET USER DATA
        
        $user = $this->repository->find(auth()->user()->id);

        // GET FORM DATA

        $data = $request->all();       

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute() . $user->image)) {
                Storage::delete($this->folderRoute() . $user->image);
            }


            // CREATE NAME OF IMAGE AND STORE

            $nameFile = str_replace(' ', '_', $request->image->getClientOriginalName());
            $request->image->storeAs($this->folderRoute(), $nameFile);


            $data['image'] = $nameFile;

        }


        // STORING NEW DATA 

        $user->update($data);

        // REDIRECT AND MESSAGES
        
        return redirect()
                ->route('account.edit')
                ->with('message', 'Conta atualizada com sucesso.');

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function contacts(Request $request)
    {
        
        // GET USER DATA
        
        $user = $this->repository->find(auth()->user()->id);

        // GET FORM DATA

        $data = $request->all();

        // STORING NEW DATA 

        $user->update($data);

        // REDIRECT AND MESSAGES
        
        return redirect()
                ->route('account.edit')
                ->with('message', 'Contatos atualizados com sucesso.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
        // Desactive account
        User::where('id', Auth::user()->id)->update(['status' => 'desativado']);
        return redirect()
        ->route('logout')
        ->with('message', '<b>Solicitação enviada!</b><br>O prazo máximo para remoção dos dados da plataforma é de 24h (úteis).<br>
        A TH Condomínios aproveita essa oportunidade para desejar a você muito sucesso e felicidade na nova casa!');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function approval()
    {

        // GET ALL DATA
        $occurrences = Occurrence::where('user', Auth::id())->get();
        $topics = Topic::where('user', Auth::id())->get();

        // RETURN VIEW WITH DATA
        return view('pages.user.approval', [
            'occurrences' => $occurrences,
            'topics' => $topics,
        ]);

    }
}

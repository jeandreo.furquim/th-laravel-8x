<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\AlbumsPhotos;
use App\Models\Condominium;
use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Expr\Isset_;

class AlbumController extends Controller
{

    protected $request;
    private $repository;
    
    public function __construct(Request $request, Album $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    // DEFINE FOLDER ROUTE

    public function folderRoute($id)
    {
        return "condominios/" . session('sessionCondominium') . "/albuns/" . $id . "/";
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? Album::all()->where('condominium', session('sessionCondominium')) : Album::all();

        // RETURN VIEW WITH DATA
        return view('pages.album.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.album.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // GET FORM DATA

        $data = $request->all();

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Album::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE
            
            $data['image'] = $nameFile;

        }

        // STORING NEW DATA 

        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // STORE IMAGE

        if (isset($data['image'])) {
            $request->image->storeAs($this->folderRoute($insert->id), $nameFile);
        }

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 2, 0, $insert->name ?? $insert->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('albums.index')
                ->with('message', 'Álbum criado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
            return redirect()->back();

        // GET IMAGE
        $urlImage = $content->image ? url('storage/condominios/' . $content->condominium . "/albuns/" . $id . "/" .$content->image) : url('assets/images/default.jpg');
  
        // RETURN VIEW WITH DATA
        return view('pages.album.show', [
            'content' => $content,
            'urlImage' => $urlImage,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         // VERIFY IF EXISTS

         if(!$content = $this->repository->find($id))
         return redirect()->back();
 
         // RETURN VIEW WITH DATA
 
         return view('pages.album.edit', [
             'content' => $content,
         ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute($content->id) . $content->image)) {
                Storage::delete($this->folderRoute($content->id) . $content->image);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Album::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE

            $request->image->storeAs($this->folderRoute($content->id), $nameFile);
            $data['image'] = $nameFile;

        }

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 2, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('albums.edit', $id)
        ->with('message', 'Álbum atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute($content->id))) {
            Storage::deleteDirectory($this->folderRoute($content->id));
        }

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 2, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('albums.index')
                ->with('message', 'Álbum deletado com sucesso.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeGallery(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        /// GET FORM DATA

        $data = $request->all();


        foreach($data['image'] as $image){

            // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

            if ($image->isValid()) {

                // VERIFY IF THERE IS A FILE WITH THE SAME NAME

                $nameFile = $image->getClientOriginalName();
                $nameFile = str_replace(' ', '_', $nameFile);

                $exist = AlbumsPhotos::all()->where('image', '=', $nameFile)->count();

                if($exist != 0) {
                    $nameFile = rand(0 , 10000) . '-' . $nameFile;
                }

                // CREATE NAME OF IMAGE AND STORE
                
                $path = $this->folderRoute($content->id);
                
                // STORING NEW DATA
                $image->storeAs($path, $nameFile);
                $data['image'] = $nameFile;
                $data['album'] = $id;
                $data['condominium'] = session('sessionCondominium');
                $data['user'] = Auth::user()->id;

                AlbumsPhotos::create($data);

            }

        }

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('albums.show', $id)
                ->with('message', 'Imagens adicionadas com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeImage($id)
    {
        // VERIFY IF EXISTS

        if(!$content = AlbumsPhotos::find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute($content->album) . $content->image)) {
            Storage::delete($this->folderRoute($content->album) . $content->image);
        }

        // DELETE DATA 

        $content->delete();

        // REDIRECT AND MESSAGES

        return redirect()
            ->route('albums.show', $content->album)
            ->with('message', 'Imagem removida com sucesso.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function results($filter = null, $order = null)
    {

        // GET ALL DATA
         if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Album::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = Album::where('condominium', true);
        }

        // LOAD FILTER PARAMETERS
        include(app_path('/Utilities/FiltersResults.php'));

        // PAGINATE
        $contents = $contents->paginate(10);

        // TITLE OF CATEGORY
        $titleCategory = 'Álbuns';

        // ROUTE RESULTS
        $routeResults = 'albums.results';

        // ROUTE SHOW
        $routeShow = 'albums.show';

        // ROUTE EDIT
        $routeEdit = 'albums.edit';

        // ROUTE DELETE
        $routeDestroy = 'albums.destroy';

        // ROUTE PATH
        $routePath = 'albuns';

        // RETURN VIEW WITH DATA
        return view('pages.results', [
            'contents' => $contents,
            'titleCategory' => $titleCategory,
            'routeResults' => $routeResults,
            'routeShow' => $routeShow,
            'routeEdit' => $routeEdit,
            'routeDestroy' => $routeDestroy,
            'routePath' => $routePath,
            'subFolder' => true,
        ]);

    }

}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\Access;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {

        $request->authenticate();

        $request->session()->regenerate();
        
        $status = Auth::user()->status;

        if($status == 'aprovado'){
            
            // UPDATE LAST ACCESS ON USER
            User::where('id', Auth::user()->id)->update([
                'last_login_at' => Carbon::now()->toDateTimeString(),
                'last_login_ip' => $request->getClientIp()
            ]);

            // REGISTRATION IN ACCESS
            Access::create([
                'user' => Auth::id(),
                'condominium' => Auth::user()->condominium,
                'role' => Auth::user()->role,
                'last_login_ip' => $request->getClientIp()
            ]);
            
            // ACCESS
            if(in_array(Auth::user()->role, ['administrador'])){
                return redirect()->route('admin.index');
            } else {

                if(Auth::user()->welcome == null){ 
                    return redirect()->intended(RouteServiceProvider::HOME)
                    ->with('message-welcome', '-');
                } else {
                    return redirect()->intended(RouteServiceProvider::HOME);
                }

            }

        } else {

            $notify = Auth::user()->notify;

            Auth::guard('web')->logout();
            $request->session()->invalidate();
            $request->session()->regenerateToken();
            
            if($status == 'pendente') {
                return redirect()
                    ->back()
                    ->with('message', 'Seu cadastro ainda não foi aprovado pelo sindico, tente novemente mais tarde. :)');
            } elseif ($status == 'bloqueado') {
                return redirect()
                    ->route('notifications.show')
                    ->with('message', $notify);
            } else {
                return redirect()
                    ->back()
                    ->with('message', 'Sua conta encontra-se em processo de desativação.');
            }
        } 

    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Models\Condominium;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;

class RegisteredUserController extends Controller
{
    /**
     * Display the registration view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $contents = Condominium::all();

        return view('auth.register')->with(['contents' => $contents]);
    }

    /**
     * Handle an incoming registration request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {

        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'condominium' => ['required', 'string', 'max:255'],
            'subdivision' => ['required', 'string', 'max:255'],
            'house' => ['required', 'string', 'max:255'],
            'identifier' => ['required', 'string', 'max:255'],
            'sex' => ['required', 'string', 'max:255'],
            'birth' => ['required', 'string', 'max:255'],
            'cellphone' => ['required', 'string', 'max:255'],
            'owner' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
        ]);

        $user = User::create([
            'name' => $request->name,
            'condominium' => $request->condominium,
            'subdivision' => $request->subdivision,
            'house' => $request->house,
            'identifier' => $request->identifier,
            'sex' => $request->sex,
            'birth' => $request->birth,
            'cellphone' => $request->cellphone,
            'owner' => $request->owner,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        
        event(new Registered($user));

        return redirect()
            ->back()
            ->with('message', 'Sua solicitação de acesso foi enviada ao(à) síndico(a) do seu condomínio. Em até 24h, após validação, você receberá a confirmação de acesso no e-mail informado. Obrigada, TH Condomínios.');

    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BranchController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Branch $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? Branch::all()->where('condominium', session('sessionCondominium')) : Branch::all();

        // RETURN VIEW WITH DATA
        return view('pages.branch.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.branch.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        

        foreach($request->name as $key => $item){

            if(isset($request->branch[$key])){

                // STORING NEW DATA 
                $branch['condominium'] = session('sessionCondominium');
                $branch['user'] = Auth::user()->id;
                $branch['name'] = $request->name[$key];
                $branch['branch'] = $request->branch[$key];
                Branch::create($branch);

            }
        
        }

        // REGISTER IN UPDATES
        sendUpdate(0, 3, 0, 'Ramais adicionados');

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('branches.results', session('sessionCondominium'))
                ->with('message', 'Ramais criado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function results()
    {

        // GET ALL DATA
         if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Branch::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = Branch::where('condominium', true);
        }

        // PAGINATE
        $contents = $contents->paginate(10);

        // RETURN VIEW WITH DATA
        return view('pages.branch.show', [
            'content' => $contents,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         // VERIFY IF EXISTS

         if(!$content = $this->repository->find($id))
         return redirect()->back();
 
         // RETURN VIEW WITH DATA
 
         return view('pages.branch.edit', [
             'content' => $content,
         ]);
    }


    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();

        // STORING NEW DATA
        $content->where('id', $id)->update(['name' => $request->name[0], 'branch' => $request->branch[0]]);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 3, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('branches.edit', $id)
        ->with('message', 'Ramal atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 3, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('branches.index')
                ->with('message', 'Ramal deletado com sucesso.');
    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Comment $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function index($id = null)
    {


    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        


        // GET FORM DATA
        $data = $request->all();

        // STORING NEW DATA 
        $data['type'] = $request->type;
        $data['condominium'] = $request->condominium;
        $data['user'] = Auth::user()->id;

        $this->repository->create($data);

        // REDIRECT AND MESSAGES

        return true;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        // VERIFY IF EXISTS
        $comments = Comment::orderBy('created_at', 'DESC')->where('type', $request->type)->where('content', $request->content)->get();

        // RETURN VIEW WITH DATA
        return view('includes.comments', [
            'comments' => $comments,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.message.edit', [
            'content' => $content,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();

        // STORING NEW DATA
        $content->update($data);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('messages.edit', $id)
        ->with('message', 'Mensagem atualizado com sucesso.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // DELETE DATA 

        $content->delete();

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('messages.index')
                ->with('message', 'Mensagem deletado com sucesso.');

    }

}

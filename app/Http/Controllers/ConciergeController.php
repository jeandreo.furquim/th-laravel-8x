<?php

namespace App\Http\Controllers;

use App\Models\PortersInfos;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ConciergeController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, User $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? User::all()->where('condominium', session('sessionCondominium'))->where('role', 'portaria') : User::all()->where('role', 'portaria');

        // RETURN VIEW WITH DATA
        return view('pages.admin.concierge.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.admin.concierge.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(User::where('email', $request->email)->first())
            return redirect()->back()->with('message', 'Já existe um usuário com o email: <b style="color:red">'. $request->email . '</b>');

        // STORING NEW DATA 
        $create = $this->repository->create([
            'name' => $request->name,
            'email' => $request->email,
            'cellphone' => $request->cellphone,
            'status' => 'aprovado',
            'role' => 'portaria',
            'password' => $request->password,
            'condominium' => session('sessionCondominium'),
        ]);

        if($create){
            
            PortersInfos::create([
                'user' => $create->id,
                'name_concierge' => $request->name_concierge,
                'branch' => $request->branch,
                'start' => $request->start,
                'end' => $request->end,
                'company' => $request->company,
                'manager' => $request->manager,
                'contact_phone' => $request->contact_phone,
            ]);
        }

        // REGISTER IN UPDATES
        sendUpdate(0, 4, 0, 'Portaria adicionada');

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('admin.concierges.index')
                ->with('message', 'Portaria criada com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function show($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
            return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.admin.concierge.show', [
            'content' => $content,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.admin.concierge.edit', [
            'content' => $content,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();

        if($data['password'] == null) {
            unset($data['password']);
        } else {
            $data['password'];
        }

        // STORING NEW DATA
        $update = $content->update($data);

        if($update){
            PortersInfos::where('user', $id)
            ->update([
                'name_concierge' => $request->name_concierge,
                'branch' => $request->branch,
                'start' => $request->start,
                'end' => $request->end,
                'company' => $request->company,
                'manager' => $request->manager,
                'contact_phone' => $request->contact_phone,
            ]);
        }

        // REGISTER IN UPDATES
        sendUpdate($content->id, 4, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('admin.concierges.edit', $id)
        ->with('message', 'Portaria atualizada com sucesso.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 4, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('admin.concierges.index')
                ->with('message', 'Portaria deletada com sucesso.');

    }

}

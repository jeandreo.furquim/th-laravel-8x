<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUpdateCondominiumRequest;
use App\Models\Condominium;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class CondominiumController extends Controller
{

    protected $request;
    private $repository;
    
    public function __construct(Request $request, Condominium $condominium)
    {
        
        $this->request = $request;
        $this->repository = $condominium;

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function index()
    {
        
        // GET ALL DATA

        $condominiums = Condominium::all();

        // RETURN VIEW WITH DATA
        return view('pages.admin.condominium.index', [
            'condominiums' => $condominiums,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function create()
    {
        return view('pages.admin.condominium.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUpdateCondominiumRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUpdateCondominiumRequest $request)
    {

        // GET FORM DATA

        $data = $request->all();

         // CHECK IF THE USER SENT AN IMAGE AND STORE IN TABLE

        if ($request->hasFile('image') && $request->image->isValid()) {

            $nameFile = 'img-condominio.' . $request->image->getClientOriginalExtension();
            $data['image'] = $nameFile;

        };

        // STORING NEW DATA

        $query = $this->repository->create($data);

        // GET ID INSERT

        $idCond = $query->id;

        // CREATE FOLDER ROUTE

        $pathCond = 'condominios/'. $idCond;

        $diretory01 = $pathCond . '/albuns';
        $diretory02 = $pathCond . '/comunicados';
        $diretory03 = $pathCond . '/documentos';
        $diretory04 = $pathCond . '/enquetes';
        $diretory05 = $pathCond . '/espacos';
        $diretory06 = $pathCond . '/eventos';
        $diretory07 = $pathCond . '/fornecedores';
        $diretory08 = $pathCond . '/funcionarios';
        $diretory09 = $pathCond . '/obras';
        $diretory10 = $pathCond . '/ocorrencias';
        $diretory11 = $pathCond . '/topicos';
        $diretory12 = $pathCond . '/usuarios';

        // CREATE FOLDERS

        Storage::makeDirectory($pathCond);

        Storage::makeDirectory($diretory01);
        Storage::makeDirectory($diretory02);
        Storage::makeDirectory($diretory03);
        Storage::makeDirectory($diretory04);
        Storage::makeDirectory($diretory05);
        Storage::makeDirectory($diretory06);
        Storage::makeDirectory($diretory07);
        Storage::makeDirectory($diretory08);
        Storage::makeDirectory($diretory09);
        Storage::makeDirectory($diretory10);
        Storage::makeDirectory($diretory11);
        Storage::makeDirectory($diretory12);


        // STORE IMAGE IN FOLDER

        if ($request->hasFile('image') && $request->image->isValid()) {
            $request->image->storeAs($pathCond, $nameFile);
        }


        // REDIRECT AND MESSAGES

        return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Condomínio criado com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        // VERIFY IF EXISTS
        if(!$condominium = $this->repository->find($id))
            return redirect()->back();        

        // RETURN VIEW WITH DATA
        return view('pages.admin.condominium.show', [
            'condominium' => $condominium,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // VERIFY IF EXISTS

        if(!$condominium = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.admin.condominium.edit', [
            'condominium' => $condominium,
        ]);
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function phone($id)
    {

        // VERIFY IF EXISTS

        if(!$condominium = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.admin.condominium.edit', [
            'relase' => $condominium,
        ]);
        

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUpdateCondominiumRequest $request, $id)
    {
        // VERIFY IF EXISTS

        if(!$condominium = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CREATE NAME OF IMAGE AND STORE

            $nameFile = 'img-condominio.' . $request->image->getClientOriginalExtension();
            $pathCond = 'condominios/'. $id;
            $request->image->storeAs($pathCond, $nameFile);
            $data['image'] = $nameFile;

        }

        // STORING NEW DATA 

        $condominium->update($data);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('admin.condominiums.index')
        ->with('message', 'Condomínio atualizado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function session($id)
    {

        // VERIFY IF YOU WANT TO EXIT OR ACCESS
        
        if ($id === 'exit') {

            // DELETE SESSION CONDOMINIUM
            session()->forget('sessionCondominium');
            session()->forget('sessionName');

            // REDIRECT AND MESSAGES
            return redirect()
            ->route('admin.condominiums.index')
            ->with('message', 'Saiu do condomínio');

        } else {

            // GET DATA CONDOMINIUM
            $condominium = Condominium::find($id);

            // CREATE SESSION CONDOMINIUM;
            session(['sessionCondominium' => $id]);
            session(['sessionName' => $condominium->name]);

            // REDIRECT AND MESSAGES
            return redirect()
            ->route('admin.index');

        }

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Configuration;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ConfigurationsController extends Controller
{

    protected $request;
    private $repository;
    
    public function __construct(Request $request, Configuration $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }
    

    // DEFINE FOLDER ROUTE

    public function folderRoute()
    {
        return "configuracoes/";
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = Configuration::find(1);

        // RETURN VIEW WITH DATA
        return view('pages.admin.configuration.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // GET FORM DATA
        $data = $request->all();

        // STORING NEW DATA 
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // STORE IMAGE
        if (isset($data['image'])) {    
            $request->image->storeAs($this->folderRoute(), 'background_general.jpg');
        }

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 22, 2, 'Aparência do sistema atualizada');

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('admin.configurations.index')
                ->with('message', 'Aparência atualizada com sucesso.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find(1))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // STORING NEW DATA
        $content->update($data);

        // STORE IMAGE
        if (isset($data['image'])) {    
            $request->image->storeAs($this->folderRoute(), 'background_general.jpg');
        }

        // REGISTER IN UPDATES
        sendUpdate(1, 22, 2, 'Aparência do sistema atualizada');

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('admin.configurations.index')
                ->with('message', 'Aparência atualizada com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute($content->id))) {
            Storage::deleteDirectory($this->folderRoute($content->id));
        }

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 5, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('constructions.index')
                ->with('message', 'Obra deletada com sucesso.');
    }

}

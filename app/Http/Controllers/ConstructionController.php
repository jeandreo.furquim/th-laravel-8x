<?php

namespace App\Http\Controllers;

use App\Models\Construction;
use App\Models\GalleryConstruction;
use App\Models\Update;
use App\Models\UpdatesConstruction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ConstructionController extends Controller
{

    protected $request;
    private $repository;
    
    public function __construct(Request $request, Construction $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }
    

    // DEFINE FOLDER ROUTE

    public function folderRoute($id)
    {
        return "condominios/" . session('sessionCondominium') . "/obras/" . $id . "/";
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? Construction::all()->where('condominium', session('sessionCondominium')) : Construction::all();

        // RETURN VIEW WITH DATA
        return view('pages.construction.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.construction.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // GET FORM DATA

        $data = $request->all();

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Construction::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE
            
            $data['image'] = $nameFile;

        }

        // CHECK IF THE USER SENT AN DOCUMENT AND VALIDATE

        if ($request->hasFile('document') && $request->document->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->document->getClientOriginalName();
            $exist = Construction::all()->where('document', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF DOCUMENT
            $data['document'] = $nameFile;

        }

        // STORING NEW DATA 

        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // STORE IMAGE

        if (isset($data['image'])) {
            $request->image->storeAs($this->folderRoute($insert->id), $data['image']);
        }

        // STORE DOCUMENT

        if (isset($data['document'])) {
            $request->document->storeAs($this->folderRoute($insert->id), $data['document']);
        }

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 5, 0, $insert->name ?? $insert->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('constructions.index')
                ->with('message', 'Obra adicionada com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
            return redirect()->back();

            
        // GET IMAGE
        $urlImage = $content->image ? url('storage/condominios/' . $content->condominium . "/obras/" . $id . "/" .$content->image) : url('assets/images/default.jpg');

        // RETURN VIEW WITH DATA
        return view('pages.construction.show', [
            'content' => $content,
            'urlImage' => $urlImage,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         // VERIFY IF EXISTS

         if(!$content = $this->repository->find($id))
         return redirect()->back();
 
         // RETURN VIEW WITH DATA
 
         return view('pages.construction.edit', [
             'content' => $content,
         ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute($content->id) . $content->image)) {
                Storage::delete($this->folderRoute($content->id) . $content->image);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Construction::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE

            $request->image->storeAs($this->folderRoute($content->id), $nameFile);
            $data['image'] = $nameFile;

        }

        // CHECK IF THE USER SENT AN DOCUMENT AND VALIDATE

        if ($request->hasFile('document') && $request->document->isValid()) {

            // CHECK IF THE DOCUMENT ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute($content->id) . $content->document)) {
                Storage::delete($this->folderRoute($content->id) . $content->document);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->document->getClientOriginalName();
            $exist = Construction::all()->where('document', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF DOCUMENT AND STORE

            $request->document->storeAs($this->folderRoute($content->id), $nameFile);
            $data['document'] = $nameFile;

        }

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 5, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('constructions.edit', $id)
        ->with('message', 'Obra atualizada com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute($content->id))) {
            Storage::deleteDirectory($this->folderRoute($content->id));
        }

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 5, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('constructions.index')
                ->with('message', 'Obra deletada com sucesso.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function results($filter = null, $order = null)
    {

        // GET ALL DATA
         if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Construction::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = Construction::where('condominium', true);
        }

        // LOAD FILTER PARAMETERS
        include(app_path('/Utilities/FiltersResults.php'));

        // PAGINATE
        $contents = $contents->paginate(10);

        // TITLE OF CATEGORY
        $titleCategory = 'Obras';

        // ROUTE RESULTS
        $routeResults = 'constructions.results';

        // ROUTE SHOW
        $routeShow = 'constructions.show';

        // ROUTE EDIT
        $routeEdit = 'constructions.edit';

        // ROUTE DELETE
        $routeDestroy = 'constructions.destroy';

        // ROUTE PATH
        $routePath = 'obras';

        // RETURN VIEW WITH DATA
        return view('pages.results', [
            'contents' => $contents,
            'titleCategory' => $titleCategory,
            'routeResults' => $routeResults,
            'routeShow' => $routeShow,
            'routeEdit' => $routeEdit,
            'routeDestroy' => $routeDestroy,
            'routePath' => $routePath,
            'subFolder' => true,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeGallery(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        /// GET FORM DATA

        $data = $request->all();

        foreach($data['image'] as $image){

            // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

            if ($image->isValid()) {

                // VERIFY IF THERE IS A FILE WITH THE SAME NAME

                $nameFile = $image->getClientOriginalName();
                $nameFile = str_replace(' ', '_', $nameFile);

                $exist = GalleryConstruction::all()->where('image', '=', $nameFile)->count();

                if($exist != 0) {
                    $nameFile = rand(0 , 10000) . '-' . $nameFile;
                }

                // CREATE NAME OF IMAGE AND STORE
                
                $path = $this->folderRoute($content->id);
                
                // STORING NEW DATA
                $image->storeAs($path, $nameFile);
                $data['image'] = $nameFile;
                $data['construction'] = $id;
                $data['condominium'] = session('sessionCondominium');
                $data['user'] = Auth::user()->id;

                GalleryConstruction::create($data);

            }

        }

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('constructions.show', $id)
                ->with('message', 'Imagens adicionadas com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function removeImage($id)
    {
        // VERIFY IF EXISTS

        if(!$content = GalleryConstruction::find($id))
        return redirect()->back();

       // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute($content->construction) . $content->image)) {
            Storage::delete($this->folderRoute($content->construction) . $content->image);
        }

        // DELETE DATA 

        $content->delete();

        // REDIRECT AND MESSAGES

        return redirect()
            ->route('constructions.show', $content->construction)
            ->with('message', 'Imagem removida com sucesso.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeUpdate(Request $request, $id)
    {

        /// GET FORM DATA

        $data = $request->all();

        // STORING NEW DATA 

        $data['construction'] = $id;
        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        UpdatesConstruction::create($data);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('constructions.show', $id)
                ->with('message', 'Atualização enviaa com sucesso.');
    }
    
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyUpdate($id)
    {
        // VERIFY IF EXISTS

        if(!$content = UpdatesConstruction::find($id))
        return redirect()->back();

        // DELETE DATA 

        $content->delete();

        // REDIRECT AND MESSAGES

        return redirect()        
            ->route('constructions.show', $content->construction)
            ->with('message', 'Atualização excluida com sucesso.');
    }

}

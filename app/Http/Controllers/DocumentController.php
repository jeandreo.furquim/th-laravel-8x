<?php

namespace App\Http\Controllers;

use App\Models\Document;
use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Document $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }


    // DEFINE FOLDER ROUTE

    public function folderRoute()
    {

        return "condominios/" . session('sessionCondominium') . "/documentos/";

    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Document::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = Document::where('condominium', true);
        }
        
        // PAGINATE
        $contents = $contents->paginate(10);

        // RETURN VIEW WITH DATA
        return view('pages.document.index', [
            'contents' => $contents,
        ]);

    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.document.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        // GET FORM DATA

        $data = $request->all();

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE


        if ($request->hasFile('file') && $request->file->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->file->getClientOriginalName();
            $exist = Document::all()->where('file', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE
            
            $request->file->storeAs($this->folderRoute(), $nameFile);
            $data['file'] = $nameFile;

        }


        // STORING NEW DATA 

        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 7, 0, $insert->name ?? $insert->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('documents.index')
                ->with('message', 'Documento criado com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function show($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
            return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.document.show', [
            'content' => $content,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.document.edit', [
            'content' => $content,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('file') && $request->file->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute() . $content->file)) {
                Storage::delete($this->folderRoute() . $content->file);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->file->getClientOriginalName();
            $exist = Document::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE

            $request->file->storeAs($this->folderRoute(), $nameFile);
            $data['file'] = $nameFile;

        }

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 7, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('documents.edit', $id)
        ->with('message', 'Documento atualizado com sucesso.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute() . $content->file)) {
            Storage::delete($this->folderRoute() . $content->file);
        }

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 7, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('documents.index')
                ->with('message', 'Documento deletado com sucesso.');

    }

}

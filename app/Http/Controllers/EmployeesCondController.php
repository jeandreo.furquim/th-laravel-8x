<?php

namespace App\Http\Controllers;

use App\Models\EmployeeCond;
use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class EmployeesCondController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, EmployeeCond $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    // DEFINE FOLDER ROUTE

    public function folderRoute()
    {

        return "condominios/" . session('sessionCondominium') . "/funcionarios/";

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? EmployeeCond::all()->where('condominium', session('sessionCondominium')) : EmployeeCond::all();

        // RETURN VIEW WITH DATA
        return view('pages.employeescond.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.employeescond.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        /// GET FORM DATA

        $data = $request->all();

        if(Auth::user()->role == 'administrador'){
            $data['status'] = 'Aprovado';
        }

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = EmployeeCond::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE
            
            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }

        // STORING NEW DATA 

        $data['monday'] = isset($data['monday']) ? true : null;
        $data['tuesday'] = isset($data['tuesday']) ? true : null;
        $data['wednesday'] = isset($data['wednesday']) ? true : null;
        $data['thursday'] = isset($data['thursday']) ? true : null;
        $data['friday'] = isset($data['friday']) ? true : null;
        $data['saturday'] = isset($data['saturday']) ? true : null;
        $data['sunday'] = isset($data['sunday']) ? true : null;

        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 8, 0, $insert->name ?? $insert->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('employeescond.index')
                ->with('message', 'Funcionário criado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
            return redirect()->back();

        // GET IMAGE
        $urlImage = $content->image ? url('storage/condominios/' . $content->condominium . "/funcionarios/" .$content->image) : url('assets/images/default.jpg');

        // RETURN VIEW WITH DATA
        return view('pages.employeescond.show', [
            'content' => $content,
            'urlImage' => $urlImage,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         // VERIFY IF EXISTS

         if(!$content = $this->repository->find($id))
         return redirect()->back();
 
         // RETURN VIEW WITH DATA
 
         return view('pages.employeescond.edit', [
             'content' => $content,
         ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute() . $content->image)) {
                Storage::delete($this->folderRoute() . $content->image);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = EmployeeCond::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE

            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }

        // STORING NEW DATA
        
        $data['monday'] = isset($data['monday']) ? true : null;
        $data['tuesday'] = isset($data['tuesday']) ? true : null;
        $data['wednesday'] = isset($data['wednesday']) ? true : null;
        $data['thursday'] = isset($data['thursday']) ? true : null;
        $data['friday'] = isset($data['friday']) ? true : null;
        $data['saturday'] = isset($data['saturday']) ? true : null;
        $data['sunday'] = isset($data['sunday']) ? true : null;

        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 8, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('employeescond.edit', $id)
        ->with('message', 'Funcionário atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute() . $content->image)) {
            Storage::delete($this->folderRoute() . $content->image);
        }

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 8, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('employeescond.index')
                ->with('message', 'Funcionário deletado com sucesso.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function results($filter = null, $order = null)
    {

        // GET ALL DATA
         if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = EmployeeCond::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = EmployeeCond::where('condominium', true);
        }

        // LOAD FILTER PARAMETERS
        include(app_path('/Utilities/FiltersResults.php'));

        // PAGINATE
        $contents = $contents->paginate(10);

        // TITLE OF CATEGORY
        $titleCategory = 'Funcionários';

        // ROUTE RESULTS
        $routeResults = 'employeescond.results';

        // ROUTE SHOW
        $routeShow = 'employeescond.show';

        // ROUTE EDIT
        $routeEdit = 'employeescond.edit';

        // ROUTE DELETE
        $routeDestroy = 'employeescond.destroy';

        // ROUTE PATH
        $routePath = 'funcionarios';

        // RETURN VIEW WITH DATA
        return view('pages.results', [
            'contents' => $contents,
            'titleCategory' => $titleCategory,
            'routeResults' => $routeResults,
            'routeShow' => $routeShow,
            'routeEdit' => $routeEdit,
            'routeDestroy' => $routeDestroy,
            'routePath' => $routePath,
            'customStyle' => 'eventos',
            'disableActions' => true,
        ]);

    }


}

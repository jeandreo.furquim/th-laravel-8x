<?php

namespace App\Http\Controllers;

use App\Models\Maintenance;
use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MaintenanceController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Maintenance $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Maintenance::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = Maintenance::where('condominium', true);
        }

        // RETURN VIEW WITH DATA
        return view('pages.maintenance.index', [
            'contents' => $contents->get(),
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.maintenance.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /// GET FORM DATA

        $data = $request->all();


        // STORING NEW DATA 

        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 11, 0, $insert->name ?? $insert->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('maintenances.index')
                ->with('message', 'Manutenção criada com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
            return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.maintenance.show', [
            'content' => $content,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         // VERIFY IF EXISTS

         if(!$content = $this->repository->find($id))
         return redirect()->back();
 
         // RETURN VIEW WITH DATA
 
         return view('pages.maintenance.edit', [
             'content' => $content,
         ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 11, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('maintenances.edit', $id)
        ->with('message', 'Manutenção atualizada com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 11, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('maintenances.index')
                ->with('message', 'Manutenção deletada com sucesso.');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function results()
    {

        // GET ALL DATA
        if(Auth::user()->role == 'morador'){
            $contents = Maintenance::where('condominium', Auth::user()->condominium)->get()->toArray();
        } else {
            $contents = session('sessionCondominium') ? Maintenance::where('condominium', session('sessionCondominium'))->get()->toArray() : Maintenance::get()->toArray();
        }


        $events = [];

        foreach($contents as $content){
            $events[] = array('title' => "{$content['name']}", 'url' => route('maintenances.show', $content['id']), 'start' => $content['start'] . 'T' . $content['hour_start'] , 'end' => $content['end'] . 'T' . $content['hour_end'] );
        }


        // RETURN VIEW WITH DATA
        return view('pages.maintenance.results', [
            'events' => $events,
        ]);
        
    }

}

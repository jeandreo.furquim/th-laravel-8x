<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Message $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function index($id = null)
    {

        // GET ALL DATA
        if(Auth::user()->role == 'morador'){
            $peoples = User::where('condominium', Auth::user()->condominium)->where('id', '<>', Auth::id())->get();
        } else {
            $peoples = session('sessionCondominium') ? User::where('condominium', session('sessionCondominium'))->get() : User::get();
        }

        // DEFINE THAT YOU SEE MSGS
        Message::where('from', $id)->where('to', Auth::user()->id)->update(['see' => true]);

        $user = User::find($id);

        // RETURN VIEW WITH DATA
        return view('pages.message.index', [
            'peoples' => $peoples,
            'user' => $user,
        ]);

    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {        

        // GET FORM DATA
        $data = $request->all();


        // STORING NEW DATA 
        $data['to'] = $id;
        $data['condominium'] = Auth::user()->condominium;
        $data['from'] = Auth::user()->id;


        $this->repository->create($data);

        // REDIRECT AND MESSAGES

        return 'Mensagem enviada com sucesso';

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function show($id)
    {

        // VERIFY IF EXISTS

        if(!$content = Message::where('from', Auth::user()->id)->where('to', $id)->orWhere('from', $id)->where('to', Auth::user()->id)->get())
            return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.message.show', [
            'contents' => $content,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.message.edit', [
            'content' => $content,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();

        // STORING NEW DATA
        $content->update($data);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('messages.edit', $id)
        ->with('message', 'Mensagem atualizado com sucesso.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // DELETE DATA 

        $content->delete();

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('messages.index')
                ->with('message', 'Mensagem deletado com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function search($value)
    {

        if($value != 'all'){
            $users = User::where('name', 'LIKE', $value.'%')->where('id', '<>', Auth::id())->get();
        } else {
            // GET ALL DATA
            if(Auth::user()->role == 'morador'){
                $users = User::where('condominium', Auth::user()->condominium)->where('id', '<>', Auth::id())->get();
            } else {
                $users = session('sessionCondominium') ? User::all()->where('condominium', session('sessionCondominium')) : User::all();
            }
        }

        // RETURN VIEW WITH DATA
        return view('pages.message.search', [
            'contents' => $users,
        ]);


    }

}

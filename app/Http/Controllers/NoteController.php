<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Note $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.note.create');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? Note::all()->where('condominium', session('sessionCondominium')) : Note::all();

        // RETURN VIEW WITH DATA
        return view('pages.note.table', [
            'contents' => $contents,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // GET FORM DATA

        $data = $request->all();

        // STORING NEW DATA 
        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::id();
        $insert = $this->repository->create($data);

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 22, 1, $insert->name ?? $insert->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('notes.edit', $insert->id)
                ->with('message', 'Anotação criada com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function show($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
            return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.note.show', [
            'content' => $content,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.note.edit', [
            'content' => $content,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA
        $data = $request->all();     

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 22, 3, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('notes.edit', $id)
        ->with('message', 'Anotação atualizada com sucesso.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // DELETE DATA 
        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 22, 5, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES
        return redirect()
                ->route('notes.index')
                ->with('message', 'Anotação removida com sucesso.');

    }

}

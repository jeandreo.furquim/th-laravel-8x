<?php

namespace App\Http\Controllers;

use App\Models\Notify;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, User $content)
    {
        $this->request = $request;
        $this->repository = $content;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? User::all()->where('condominium', session('sessionCondominium'))->where('notify', true)->where('status', 'bloqueado') : User::all()->where('notify', true)->where('status', 'bloqueado');

        // RETURN VIEW WITH DATA
        return view('pages.admin.notify.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($user)
    {
        // RETURN VIEW
        return view('pages.admin.notify.create')->with('user', $user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $user)
    {

        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($user))
        return redirect()->back();

        // STORING NEW DATA 
        User::where('id', $user)->update(['status' => 'bloqueado', 'notify' => $request->notify]);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 21, 7, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES
        return redirect()
                ->route('admin.notifications.index')
                ->with('message', 'Usuário bloqueado e notificado com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */ 
    public function show(){

        // RETURN VIEW WITH DATA
        if(session('message')){
            return view('auth.notify');
        } else {
            return view('auth.login');
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF EXISTS
        if($content->notify == null)
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.admin.notify.edit', [
            'content' => $content,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA
        $data = $request->all();

        // STORING NEW DATA
        $content->update($data);

        // REDIRECT AND MESSAGES
        return redirect()
        ->route('admin.notifications.edit', $id)
        ->with('message', 'Motivo do bloqueio atualizado com sucesso.');
        
    }
}

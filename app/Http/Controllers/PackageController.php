<?php

namespace App\Http\Controllers;

use App\Models\Package;
use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PackageController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Package $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }


    // DEFINE FOLDER ROUTE

    public function folderRoute($id)
    {

        return "condominios/" . session('sessionCondominium') . "/usuarios/" . $id . "/";

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? Package::all()->where('condominium', session('sessionCondominium')) : Package::all();

        // RETURN VIEW WITH DATA
        return view('pages.package.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

        // RETURN VIEW

        return view('pages.package.create')->with('user', $id);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

        // GET FORM DATA

        $data = $request->all();

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Package::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE
            
            $request->image->storeAs($this->folderRoute($id), $nameFile);
            $data['image'] = $nameFile;

        }


        // STORING NEW DATA 

        $data['owner'] = $id;
        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 12, 0, $insert->name ?? $insert->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('packages.index')
                ->with('message', 'Aviso de encomenda criada com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function show($id)
    {

        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
            return redirect()->back();
            
        // GET IMAGE
        $urlImage = $content->image ? url('storage/condominios/'.$content->condominium.'/usuarios/'.$content->owner.'/'.$content->image) : url('assets/images/default.jpg');

        // RETURN VIEW WITH DATA
        return view('pages.package.show', [
            'content' => $content,
            'urlImage' => $urlImage,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.package.edit', [
            'content' => $content,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute($id) . $content->image)) {
                Storage::delete($this->folderRoute($id) . $content->image);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Package::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE

            $request->image->storeAs($this->folderRoute($id), $nameFile);
            $data['image'] = $nameFile;

        }

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 12, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('packages.edit', $id)
        ->with('message', 'Aviso de encomenda atualizado com sucesso.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute($id) . $content->image)) {
            Storage::delete($this->folderRoute($id) . $content->image);
        }

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 12, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('packages.index')
                ->with('message', 'Aviso de encomenda deletada com sucesso.');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus($id, $status)
    {
        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // STORING NEW DATA
        $content->update(['status' => $status]);


        if($status == 'retirado'){

            // REGISTER IN UPDATES
            sendUpdate($content->id, 12, 14, 'Pacote retirado');

        } else {

            // REGISTER IN UPDATES
            sendUpdate($content->id, 12, 14, 'Pacote não retirado');

        }

        return redirect()
                ->back()
                ->with('message', 'Status do pacote atualizado.');

    }

        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function results($filter = null, $order = null)
    {

        // GET ALL DATA
        if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Package::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = Package::where('condominium', true);
        }

        // LOAD FILTER PARAMETERS
        include(app_path('/Utilities/FiltersResults.php'));

        // PAGINATE
        $contents = $contents->paginate(10);

        // TITLE OF CATEGORY
        $titleCategory = 'Encomendas';

        // ROUTE RESULTS
        $routeResults = 'packages.results';

        // ROUTE SHOW
        $routeShow = 'packages.show';

        // ROUTE EDIT
        $routeEdit = 'packages.edit';

        // ROUTE DELETE
        $routeDestroy = 'packages.destroy';

        // ROUTE PATH
        $routePath = 'usuarios';
        
        // RETURN VIEW WITH DATA
        return view('pages.results', [
            'contents' => $contents,
            'titleCategory' => $titleCategory,
            'routeResults' => $routeResults,
            'routeShow' => $routeShow,
            'routeEdit' => $routeEdit,
            'routeDestroy' => $routeDestroy,
            'routePath' => $routePath,
            'customStyle' => 'encomendas',
            'disableActions' => true,
        ]);

    }

}

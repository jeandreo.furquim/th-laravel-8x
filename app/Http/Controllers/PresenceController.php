<?php

namespace App\Http\Controllers;

use App\Models\Presence;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PresenceController extends Controller
{

    protected $request;
    private $repository;
    
    public function __construct(Request $request, Presence $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // GET FORM DATA

        $data = $request->all();

        // STORING NEW DATA 

        $data['condominium'] = Auth::user()->condominium;
        $data['user'] = Auth::user()->id;
        $data['event'] = $id;
        $this->repository->create($data);


        // REDIRECT AND MESSAGES

        return redirect()
                ->route('events.show', $id)
                ->with('message', 'Opção registrada com sucesso.');
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Presence::where('event', $request->id)->where('user', Auth::user()->id)->update(['option' => $request->option]);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('events.show', $id)
                ->with('message', 'Opção atualizada com sucesso.');
    }

}

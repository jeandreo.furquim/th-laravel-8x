<?php

namespace App\Http\Controllers;

use App\Models\Provider;
use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProviderController extends Controller
{
   
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Provider $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }


    // DEFINE FOLDER ROUTE

    public function folderRoute()
    {

        return "condominios/" . session('sessionCondominium') . "/fornecedores/";

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? Provider::all()->where('condominium', session('sessionCondominium')) : Provider::all();

        // RETURN VIEW WITH DATA
        return view('pages.provider.table', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.provider.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        // GET FORM DATA

        $data = $request->all();

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Provider::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE
            
            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }

        // CHECK IF THE USER SENT AN DOCUMENT AND VALIDATE

        if ($request->hasFile('document') && $request->document->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->document->getClientOriginalName();
            $exist = Provider::all()->where('document', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF DOCUMENT AND STORE
            
            $request->document->storeAs($this->folderRoute(), $nameFile);
            $data['document'] = $nameFile;

        }


        // STORING NEW DATA 

        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 14, 0, $insert->name ?? $insert->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('providers.index')
                ->with('message', 'Fornecedor criado com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function show($id)
    {

        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
            return redirect()->back();

        // GET IMAGE
        $urlImage = $content->image ? url('storage/condominios/' . $content->condominium . "/fornecedores/" .$content->image) : url('assets/images/default.jpg');

        // RETURN VIEW WITH DATA
        return view('pages.provider.show', [
            'content' => $content,
            'urlImage' => $urlImage,

        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.provider.edit', [
            'content' => $content,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute() . $content->image)) {
                Storage::delete($this->folderRoute() . $content->image);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Provider::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE

            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }

        // CHECK IF THE USER SENT AN DOCUMENT AND VALIDATE

        if ($request->hasFile('document') && $request->document->isValid()) {

            // CHECK IF THE DOCUMENT ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute() . $content->document)) {
                Storage::delete($this->folderRoute() . $content->document);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->document->getClientOriginalName();
            $exist = Provider::all()->where('document', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF DOCUMENT AND STORE

            $request->document->storeAs($this->folderRoute(), $nameFile);
            $data['document'] = $nameFile;

        }

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 14, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('providers.edit', $id)
        ->with('message', 'Fornecedor atualizado com sucesso.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute() . $content->image)) {
            Storage::delete($this->folderRoute() . $content->image);
        }

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 14, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('providers.index')
                ->with('message', 'Fornecedor deletado com sucesso.');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function results($filter = null, $order = null)
    {

        // GET ALL DATA
        if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Provider::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = Provider::where('condominium', true);
        }

        // LOAD FILTER PARAMETERS
        include(app_path('/Utilities/FiltersResults.php'));

        // PAGINATE
        $contents = $contents->paginate(10);

        // TITLE OF CATEGORY
        $titleCategory = 'Fornecedores';

        // ROUTE RESULTS
        $routeResults = 'providers.results';

        // ROUTE SHOW
        $routeShow = 'providers.show';

        // ROUTE EDIT
        $routeEdit = 'providers.edit';

        // ROUTE DELETE
        $routeDestroy = 'providers.destroy';

        // ROUTE PATH
        $routePath = 'fornecedores';

        // RETURN VIEW WITH DATA
        return view('pages.results', [
            'contents' => $contents,
            'titleCategory' => $titleCategory,
            'routeResults' => $routeResults,
            'routeShow' => $routeShow,
            'routeEdit' => $routeEdit,
            'routeDestroy' => $routeDestroy,
            'routePath' => $routePath,
        ]);

    }

}

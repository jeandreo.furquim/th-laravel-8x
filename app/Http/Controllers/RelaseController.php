<?php

namespace App\Http\Controllers;

use App\Models\Relase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class RelaseController extends Controller
{

    protected $request;
    private $repository;
    
    public function __construct(Request $request, Relase $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }


    // DEFINE FOLDER ROUTE

    public function folderRoute()
    {

        return "condominios/" . session('sessionCondominium') . "/comunicados/";

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? Relase::all()->where('condominium', session('sessionCondominium')) : Relase::all();

        // RETURN VIEW WITH DATA
        return view('pages.relase.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.relase.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        // GET FORM DATA

        $data = $request->all();

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Relase::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE
            
            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }


        // STORING NEW DATA 

        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 0, 0, $insert->name ?? $insert->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('relases.show', $insert->id)
                ->with('message', 'Comunicado criado com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function show($id)
    {

        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
            return redirect()->back();

        // GET IMAGE
        $urlImage = $content->image ? url('storage/condominios/' . $content->condominium . "/comunicados/" .$content->image) : url('assets/images/default.jpg');

        // RETURN VIEW WITH DATA
        return view('pages.relase.show', [
            'content' => $content,
            'urlImage' => $urlImage,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.relase.edit', [
            'content' => $content,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute() . $content->image)) {
                Storage::delete($this->folderRoute() . $content->image);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Relase::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE

            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 0, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('relases.edit', $id)
        ->with('message', 'Comunicado atualizado com sucesso.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute() . $content->image)) {
            Storage::delete($this->folderRoute() . $content->image);
        }

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 0, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('relases.index')
                ->with('message', 'Comunicado deletado com sucesso.');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function results($filter = null, $order = null)
    {

        // GET ALL DATA
        if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Relase::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = Relase::where('condominium', true);
        }

        // LOAD FILTER PARAMETERS
        include(app_path('/Utilities/FiltersResults.php'));

        // PAGINATE
        $contents = $contents->paginate(10);

        // TITLE OF CATEGORY
        $titleCategory = 'Comunicados';

        // ROUTE RESULTS
        $routeResults = 'relases.results';

        // ROUTE SHOW
        $routeShow = 'relases.show';

        // ROUTE EDIT
        $routeEdit = 'relases.edit';

        // ROUTE DELETE
        $routeDestroy = 'relases.destroy';

        // ROUTE PATH
        $routePath = 'comunicados';

        // RETURN VIEW WITH DATA
        return view('pages.results', [
            'contents' => $contents,
            'titleCategory' => $titleCategory,
            'routeResults' => $routeResults,
            'routeShow' => $routeShow,
            'routeEdit' => $routeEdit,
            'routeDestroy' => $routeDestroy,
            'routePath' => $routePath,
        ]);

    }

}

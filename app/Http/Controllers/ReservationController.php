<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\Room;
use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReservationController extends Controller
{


    protected $request;
    private $repository;
    
    public function __construct(Request $request, Reservation $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        if(in_array(Auth::user()->role, ['administrador', 'sindico'])){
            $contents = session('sessionCondominium') ? Reservation::all()->where('condominium', session('sessionCondominium')) : Reservation::all();
        } else {
            $contents = Reservation::where('user', Auth::id())->get();
        }
        
        // RETURN VIEW WITH DATA
        return view('pages.room.reservations', [
            'contents' => $contents,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

        //RULES
        $rules = Room::find($id);

        // DATEZONE
        date_default_timezone_set('America/Sao_Paulo');

        // TODAY
        $today = date('Y-m-d'); 
        
        $content = $request->day;

        // DATE -> DATE CREATE
        $date01 = date_create($today);
        $date02 = date_create($content);

        // INTERVAL DAYS
        $interval = $date01->diff($date02);

        // DAYS PASSED
        if($interval->invert == true) {
            return redirect()
            ->route('rooms.show', $id)
            ->with('message', 'A data selecionada já passou!');
        }

        // IF EXCLUSIVE
        if($rules->exclusive){

        $verify = Reservation::all()->where('room', $id)->where('day', $content)->count();

            if($verify >= 1){
                return redirect()
                ->route('rooms.show', $id)
                ->with('message', 'Essa data já foi reservada por outro morador.');
            }

        }


        function intervalBetweenHours($startHour, $endHour, $hourSelected) {
            $startHourTimestamp = strtotime($startHour);
            $endHourTimestamp = strtotime($endHour);
            $hourSelectedTimestamp = strtotime($hourSelected);
            return (($hourSelectedTimestamp >= $startHourTimestamp) && ($hourSelectedTimestamp <= $endHourTimestamp));
        }

        // PARAMETERS
        $startHour = $rules->hour_start;
        $endHour = $rules->hour_end;
        $hourSelected = $request->hour;
        

        // VERIFY IF DATE IS VALID
        if(!intervalBetweenHours($startHour, $endHour, $hourSelected)){ 
            return redirect()
                ->route('rooms.show', $id)
                ->with('message', 'Você não pode agendar esse espaço as ' . $hourSelected . '.');
        }

        

        // DAYS OF WEEK
        $week = array('domingo', 'segunda', 'terca', 'quarta', 'quinta', 'sexta', 'sábado');

        // DATE FOR INTEGER OF WEEK
        $dayWeek = date('w', strtotime($content));

        $msg = 'Você não pode reservar esse espaço ' . $week[$dayWeek] . '.';

        if (!$rules->monday && $dayWeek == 0) {
            return redirect()
                ->route('rooms.show', $id)
                ->with('message', $msg);
        } elseif (!$rules->tuesday && $dayWeek == 1){
            return redirect()
                ->route('rooms.show', $id)
                ->with('message', $msg);
        } elseif (!$rules->wednesday && $dayWeek == 2){
            return redirect()
                ->route('rooms.show', $id)
                ->with('message', $msg);
        } elseif (!$rules->thursday && $dayWeek == 3){
            return redirect()
                ->route('rooms.show', $id)
                ->with('message', $msg);
        } elseif (!$rules->friday && $dayWeek == 4){
            return redirect()
                ->route('rooms.show', $id)
                ->with('message', $msg);
        } elseif (!$rules->saturday && $dayWeek == 5){
            return redirect()
                ->route('rooms.show', $id)
                ->with('message', $msg);
        } elseif (!$rules->sunday && $dayWeek == 6){
            return redirect()
                ->route('rooms.show', $id)
                ->with('message', $msg);
        }

        // GET FORM DATA

        $data = $request->all();


        // STORING NEW DATA 

        $data['day'] = $content;
        $data['hour'] = $request->hour;
        $data['room'] = $id;
        $data['condominium'] = $rules->condominium;
        $data['user'] = Auth::user()->id;

        // VERIFY IF NEED APPROVED
        if (!$rules->approval){
            $data['status'] = 'aprovado';
            $msg = 'Espaço reservado com sucesso.';
            $updateCond = 'Reservou o espaço ' . $rules->name;
            $insert = $this->repository->create($data);
            // REGISTER IN UPDATES
            sendUpdate($insert->id, 20, 9, $updateCond);
        } else {

            if($interval->invert == false && $interval->days <= $rules->antecedence){
                $msg = 'A reserva deste espaço requer antecedência mínima de ' . $rules->antecedence . ' dia(s) útil(úteis). Mas faremos o possível para garantir que esse espaço fique reservado a você no(s) dia(s)/hora informado(s). Se confirmada a sua reserva, essa estará disponível em até 24h na sua área do condômino. 
                <br>Se preferir, fale diretamente com a Síndica Thays Hoeffling ligando no número: (41) 9 9676-0610.';
            } else {
                $msg = 'Sua solicitação foi enviada! A confirmação da sua reserva estará disponível em até 24h.';
            }

            $updateCond = 'Solicitou reservar o espaço ' . $rules->name;
            $insert = $this->repository->create($data);
            // REGISTER IN UPDATES
            sendUpdate($insert->id, 20, 8, $updateCond);
        }
        
        // REDIRECT AND MESSAGES

        return redirect()
                ->route('rooms.show', $id)
                ->with('message', $msg);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, $status)
    {
        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // STORING NEW DATA
        $content->update(['status' => $status]);


        if($status == 'aprovado'){

            // REGISTER IN UPDATES
            sendUpdate($content->id, 20, 10, 'Reserva aprovada');

        } else {

            // REGISTER IN UPDATES
            sendUpdate($content->id, 20, 11, 'Reserva negada');

        }

        return redirect()
                ->back()
                ->with('message', 'Status reserva atualizada com sucesso.');

    }

}

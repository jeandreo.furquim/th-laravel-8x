<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\Room;
use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class RoomController extends Controller
{


    protected $request;
    private $repository;
    
    public function __construct(Request $request, room $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    // DEFINE FOLDER ROUTE

    public function folderRoute()
    {

        return "condominios/" . session('sessionCondominium') . "/espacos/";

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? Room::all()->where('condominium', session('sessionCondominium')) : Room::all();

        // RETURN VIEW WITH DATA
        return view('pages.room.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.room.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // GET FORM DATA

        $data = $request->all();

        $data['monday'] = isset($data['monday']) ? true : null;
        $data['tuesday'] = isset($data['tuesday']) ? true : null;
        $data['wednesday'] = isset($data['wednesday']) ? true : null;
        $data['thursday'] = isset($data['thursday']) ? true : null;
        $data['friday'] = isset($data['friday']) ? true : null;
        $data['saturday'] = isset($data['saturday']) ? true : null;
        $data['sunday'] = isset($data['sunday']) ? true : null;

        $data['exclusive'] = $data['exclusive'] == "sim" ? true : null;
        $data['approval'] = $data['approval'] == "sim" ? true : null;

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Room::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE
            
            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }


        // STORING NEW DATA 

        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 19, 0, $insert->name ?? $insert->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('rooms.create')
                ->with('message', 'Espaço criado com sucesso.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // GET DATA
        $content = Room::find($id);

        // EVENTS
        $events = Reservation::all()->where('room', $id)->toArray();

        // DISABLED DAYS
        $disabledDays = ['2023-02-01', '2023-02-11', '2023-02-14'];

        $newDays = [];

        if(isset($disabledDays)){

            foreach($disabledDays as $days){
                $dayExplode = explode('-', $days);
                $newDays[] = $dayExplode[0] . '-' . $dayExplode[1] . '-' . ($dayExplode[2] + 1);
            }

        }

        // VERIFY IF EXISTS
        if(!$content){
            return redirect()->back();
        }


        // GENERATES DISPLAY WITH DATA
        return view('pages.room.show')->with(['content' => $content, 'events' => $events, 'newDays' => $newDays]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.room.edit', [
            'content' => $content,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();

        $data['monday'] = isset($data['monday']) ? true : null;
        $data['tuesday'] = isset($data['tuesday']) ? true : null;
        $data['wednesday'] = isset($data['wednesday']) ? true : null;
        $data['thursday'] = isset($data['thursday']) ? true : null;
        $data['friday'] = isset($data['friday']) ? true : null;
        $data['saturday'] = isset($data['saturday']) ? true : null;
        $data['sunday'] = isset($data['sunday']) ? true : null;

        $data['exclusive'] = $data['exclusive'] == "sim" ? true : null;
        $data['approval'] = $data['approval'] == "sim" ? true : null;

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute() . $content->image)) {
                Storage::delete($this->folderRoute() . $content->image);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Room::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE

            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 19, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('rooms.edit', $id)
        ->with('message', 'Espaço atualizado com sucesso.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // STORING NEW DATA
        $content->update(['status' => 'desativado']);

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 19, 14, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('rooms.index')
                ->with('message', 'Espaço desativado com sucesso.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function results($filter = null, $order = null)
    {

        // GET ALL DATA
        if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Room::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = Room::where('condominium', true);
        }
        
        // LOAD FILTER PARAMETERS
        include(app_path('/Utilities/FiltersResults.php'));

        // PAGINATE
        $contents = $contents->paginate(10);

        // TITLE OF CATEGORY
        $titleCategory = 'Espaços';

        // ROUTE RESULTS
        $routeResults = 'rooms.results';

        // ROUTE SHOW
        $routeShow = 'rooms.show';

        // ROUTE EDIT
        $routeEdit = 'rooms.edit';

        // ROUTE DELETE
        $routeDestroy = 'rooms.destroy';

        // ROUTE PATH
        $routePath = 'espacos';
        // RETURN VIEW WITH DATA
        return view('pages.results', [
            'contents' => $contents,
            'titleCategory' => $titleCategory,
            'routeResults' => $routeResults,
            'routeShow' => $routeShow,
            'routeEdit' => $routeEdit,
            'routeDestroy' => $routeDestroy,
            'routePath' => $routePath,
            'customStyle' => 'espacos',
            'disableActions' => true,
        ]);

    }

}

<?php

namespace App\Http\Controllers;

use App\Models\Subdivision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubdivisionController extends Controller
{
    
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Subdivision $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
         if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Subdivision::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = Subdivision::where('condominium', true);
        }

        // PAGINATE
        $contents =  $contents->get();

        // RETURN VIEW WITH DATA
        return view('pages.admin.subdivision.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.admin.subdivision.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        // GET FORM DATA

        $data = $request->all();

        // STORING NEW DATA 
        
        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        $this->repository->create($data);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('admin.subdivisions.index')
                ->with('message', 'Subdivisão criada com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */ 
    public function show($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
            return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.admin.subdivision.show', [
            'content' => $content,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.admin.subdivision.edit', [
            'content' => $content,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();

        // STORING NEW DATA
        $content->update($data);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('subdivisions.edit', $id)
        ->with('message', 'Subdivisão atualizada com sucesso.');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // DELETE DATA 

        $content->delete();

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('subdivisions.index')
                ->with('message', 'Subdivisão deletada com sucesso.');

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
    */
    public function load($condominium)
    {

        // GET ALL DATA

        $contents = Subdivision::all()->where('condominium', $condominium);

        // RETURN VIEW WITH DATA
        return view('pages.admin.subdivision.list', [
            'contents' => $contents,
        ]);

    }

}

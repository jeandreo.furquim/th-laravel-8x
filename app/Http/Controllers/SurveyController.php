<?php

namespace App\Http\Controllers;

use App\Models\Condominium;
use App\Models\Survey;
use App\Models\Update;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class SurveyController extends Controller
{


    protected $request;
    private $repository;
    
    public function __construct(Request $request, Survey $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    // DEFINE FOLDER ROUTE

    public function folderRoute()
    {

        return "condominios/" . session('sessionCondominium') . "/enquetes/";

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $contents = session('sessionCondominium') ? Survey::all()->where('condominium', session('sessionCondominium')) : Survey::all();

        // RETURN VIEW WITH DATA
        return view('pages.survey.index', [
            'contents' => $contents,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

       // CHECK IF YOU ARE IN A CONDOMINIUM AND ARE AN ADMINISTRATOR
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium') || Auth::user()->role != 'administrador'){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.survey.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        // GET FORM DATA

        $data = $request->all();

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Survey::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE
            
            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }


        // STORING NEW DATA 

        $data['condominium'] = session('sessionCondominium');
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 15, 0, $insert->name ?? $insert->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('surveys.index')
                ->with('message', 'Enquete criada com sucesso.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        $users = User::all()->where('condominium', $content->condominium)->count();
        $votes = $content->votes()->count();
        $votes_01 = $content->votes()->where('option', 1)->count();
        $votes_02 = $content->votes()->where('option', 2)->count();
        $votes_03 = $content->votes()->where('option', 3)->count();
        $votes_04 = $content->votes()->where('option', 4)->count();
        $votes_05 = $content->votes()->where('option', 5)->count();

        function porcentagem_nx ($parcial, $total) {
            if($total != 0){
                $percentage = ($parcial / $total) * 100;
                return mb_strimwidth($percentage, 0, 4);
            } else  {
                return 0;
            }
        }

        $countVotes = porcentagem_nx($votes, $users);
        $countVotes_01 = porcentagem_nx($votes_01, $votes);
        $countVotes_02 = porcentagem_nx($votes_02, $votes);
        $countVotes_03 = porcentagem_nx($votes_03, $votes);
        $countVotes_04 = porcentagem_nx($votes_04, $votes);
        $countVotes_05 = porcentagem_nx($votes_05, $votes);
        
        // GET IMAGE
        $urlImage = $content->image ? url('storage/condominios/' . $content->condominium . "/enquetes/" .$content->image) : url('assets/images/default.jpg');

        // RETURN VIEW WITH DATA
        return view('pages.survey.show', [
            'content' => $content,
            'urlImage' => $urlImage,
            'countVotes' => $countVotes,
            'countVotes_01' => $countVotes_01,
            'countVotes_02' => $countVotes_02,
            'countVotes_03' => $countVotes_03,
            'countVotes_04' => $countVotes_04,
            'countVotes_05' => $countVotes_05,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // RETURN VIEW WITH DATA
        return view('pages.survey.edit', [
            'content' => $content,
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute() . $content->image)) {
                Storage::delete($this->folderRoute() . $content->image);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Survey::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE

            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 15, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('surveys.edit', $id)
        ->with('message', 'Enquete atualizado com sucesso.');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute() . $content->image)) {
            Storage::delete($this->folderRoute() . $content->image);
        }

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 15, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('surveys.index')
                ->with('message', 'Enquete deletado com sucesso.');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function results($filter = null, $order = null)
    {

        // GET ALL DATA
        if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Survey::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium);
        } else {
            $contents = Survey::where('condominium', true);
        }

        // LOAD FILTER PARAMETERS
        include(app_path('/Utilities/FiltersResults.php'));

        // PAGINATE
        $contents = $contents->paginate(10);

        // TITLE OF CATEGORY
        $titleCategory = 'Enquetes';

        // ROUTE RESULTS
        $routeResults = 'surveys.results';

        // ROUTE SHOW
        $routeShow = 'surveys.show';

        // ROUTE EDIT
        $routeEdit = 'surveys.edit';

        // ROUTE DELETE
        $routeDestroy = 'surveys.destroy';

        // ROUTE PATH
        $routePath = 'enquetes';

        // RETURN VIEW WITH DATA
        return view('pages.results', [
            'contents' => $contents,
            'titleCategory' => $titleCategory,
            'routeResults' => $routeResults,
            'routeShow' => $routeShow,
            'routeEdit' => $routeEdit,
            'routeDestroy' => $routeDestroy,
            'routePath' => $routePath,
        ]);

    }


}

<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class TopicController extends Controller
{

    protected $request;
    private $repository;
    
    public function __construct(Request $request, Topic $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    // DEFINE FOLDER ROUTE

    public function folderRoute()
    {

        return "condominios/" . session('sessionCondominium') . "/topicos/";

    }
    


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type = null)
    {

         // GET ALL DATA
         $contents = session('sessionCondominium') ? Topic::all()->where('condominium', session('sessionCondominium')) : Topic::all();

         // RETURN VIEW WITH DATA
         return view('pages.topic.index', [
             'contents' => $contents,
         ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        // CHECK IF YOU ARE IN A CONDOMINIUM
        if(Auth::user()->role == 'administrador' && !session('sessionCondominium')){
            return redirect()
                ->route('admin.condominiums.index')
                ->with('message', 'Selecione um condomínio.');
        }

        // RETURN VIEW
        return view('pages.topic.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        /// GET FORM DATA

        $data = $request->all();

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Topic::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE
            
            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }


        // STORING NEW DATA 

        $data['condominium'] = Auth::user()->role == 'administrador' ? session('sessionCondominium') : Auth::user()->condominium;
        $data['status'] = Auth::user()->role == 'administrador' ? 'Aprovado' : 'Pendente';
        $data['user'] = Auth::user()->id;
        $insert = $this->repository->create($data);

        // REGISTER IN UPDATES

        if(Auth::user()->role == 'administrador'){
            sendUpdate($insert->id, 16, 0, $insert->name ?? $insert->title);
            // REDIRECT AND MESSAGES
            return redirect()
                ->route('topics.index')
                ->with('message', 'Tópico adicionado ao mural com sucesso.');
        } else {
            sendUpdate($insert->id, 16, 12, $insert->name ?? $insert->title);
            // REDIRECT AND MESSAGES
            return redirect()
                ->route('account.approval')
                ->with('message', 'Solicitação de tópico criado.');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
            return redirect()->back();

        // GET IMAGE
        $urlImage = $content->image ? url('storage/condominios/' . $content->condominium . "/topicos/" .$content->image) : url('assets/images/default.jpg');

        // RETURN VIEW WITH DATA
        return view('pages.topic.show', [
            'content' => $content,
            'urlImage' => $urlImage,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         // VERIFY IF EXISTS

         if(!$content = $this->repository->find($id))
         return redirect()->back();
 
         // RETURN VIEW WITH DATA
 
         return view('pages.topic.edit', [
             'content' => $content,
         ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute() . $content->image)) {
                Storage::delete($this->folderRoute() . $content->image);
            }

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Topic::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE

            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 16, 2, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('topics.edit', $id)
        ->with('message', 'Tópico atualizado com sucesso.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute() . $content->image)) {
            Storage::delete($this->folderRoute() . $content->image);
        }

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 16, 4, $content->name ?? $content->title);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('topics.index')
                ->with('message', 'Tópico deletado com sucesso.');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function results($filter = null, $order = null, $type = null)
    {

        // GET ALL DATA
        if(Auth::user()->role == 'morador' || session('sessionCondominium')){
            $contents = Topic::where('condominium', session('sessionCondominium') ?? Auth::user()->condominium)->where('status', 'Aprovado');
        } else {
            $contents = Topic::where('condominium', true)->where('status', 'Aprovado');
        }

        // LOAD FILTER PARAMETERS
        include(app_path('/Utilities/FiltersResults.php'));

        // PAGINATE
        $contents = $contents->paginate(10);

        // TITLE OF CATEGORY
        $titleCategory = 'Tópicos';

        // ROUTE RESULTS
        $routeResults = 'topics.results';

        // ROUTE SHOW
        $routeShow = 'topics.show';

        // ROUTE EDIT
        $routeEdit = 'topics.edit';

        // ROUTE DELETE
        $routeDestroy = 'topics.destroy';

        // ROUTE PATH
        $routePath = 'topicos';

        // RETURN VIEW WITH DATA
        return view('pages.results', [
            'contents' => $contents,
            'titleCategory' => $titleCategory,
            'routeResults' => $routeResults,
            'routeShow' => $routeShow,
            'routeEdit' => $routeEdit,
            'routeDestroy' => $routeDestroy,
            'routePath' => $routePath,
            'typeSelected' => $typeSelected ?? null,
        ]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function toApprove($id)
    {

        // VERIFY IF EXISTS

        if(!$topic = $this->repository->find($id))
        return redirect()->back();

        // UPDATE DATA 
        if($topic->status == "Aprovado"){
            $topic->where('id', $id)->update(['status' => 'Bloqueado']);
        } else {
            $topic->where('id', $id)->update(['status' => 'Aprovado']);
        }

        // REDIRECT AND MESSAGES

        return redirect()
        ->route('topics.index')
        ->with('message', 'Status atualizado! :)');

    }   

}

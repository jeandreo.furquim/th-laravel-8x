<?php

namespace App\Http\Controllers;

use App\Models\Update;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdatesController extends Controller
{
    protected $request;
    private $repository;
    
    public function __construct(Request $request, Update $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updates()
    {

        // GET ALL DATA
        if(in_array(Auth::user()->role, ['administrador', 'sindico'])){
            $contents = session('sessionCondominium') ? Update::all()->where('condominium', session('sessionCondominium')) : Update::all();
        } else {
            $contents = Update::WhereIn('type', [0,1,2,3,4,5,7,8,10,11,14,15,16,19])->orderBy('id', 'DESC')->get();
        }
        


        // RETURN VIEW WITH DATA
        return view('pages.updates', [
            'contents' => $contents,
        ]);

    }

}

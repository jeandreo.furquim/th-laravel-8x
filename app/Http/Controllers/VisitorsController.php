<?php

namespace App\Http\Controllers;

use App\Models\Update;
use App\Models\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class VisitorsController extends Controller
{


    protected $request;
    private $repository;
    
    public function __construct(Request $request, Visitor $relase)
    {
        
        $this->request = $request;
        $this->repository = $relase;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        return view('pages.user._partials.add.visitors')->with('id', $id);
    }


    // DEFINE FOLDER ROUTE

    public function folderRoute()
    {

        return "condominios/" . Auth::user()->condominium . "/usuarios/" . Auth::user()->id . '/';

    }

     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA
        $data = session('sessionCondominium') ? Visitor::all()->where('condominium', session('sessionCondominium')) : Visitor::all();

        // RETURN VIEW WITH DATA
        return view('pages.admin.users.visitor', [
            'data' => $data,
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        
        
        // GET FORM DATA

        $data = $request->all();

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // VERIFY IF THERE IS A FILE WITH THE SAME NAME

            $nameFile = $request->image->getClientOriginalName();
            $exist = Visitor::all()->where('image', '=', $nameFile)->count();

            if($exist != 0) {
                $nameFile = rand(0 , 1000) . '-' . $nameFile;
            }

            // CREATE NAME OF IMAGE AND STORE
            
            $request->image->storeAs($this->folderRoute(), $nameFile);
            $data['image'] = $nameFile;

        }

        // VERIFY IF ADM INSERT IN USER
        $data['user'] = Auth::user()->role == 'administrador' ? $data['user'] = $data['user'] = $id :  Auth::id();

        // STORING NEW DATA 

        $data['condominium'] = Auth::user()->condominium;
        $insert = $this->repository->create($data);

        // REGISTER IN UPDATES
        sendUpdate($insert->id, 18, 0, $insert->name ?? $insert->title, $data['user']);


        // REDIRECT AND MESSAGES

        if(Auth::user()->role == 'administrador'){
            return redirect()
                ->route('admin.users.show', $id)
                ->with('message', 'Visitante adicionado com sucesso.');
        } else {
            return redirect()
                ->route('account.index')
                ->with('message', 'Aguardar o prazo de 24h para que estas informações sejam repassadas ao operacional do condomínio.<br>Se a liberação desejada for anterior a este prazo, favor contatar a portaria do condomínio DIRETAMENTE.');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // GET DATA
        $content = DB::table('visitors')->where([
            ['id', $id],
        ])->first();


        // VERIFY IF EXISTS
        if(!$content){
            return redirect()->back();
        }


        // GENERATES DISPLAY WITH DATA
        return view('pages.user._partials.see.visitors')->with(['visitor' => $content]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // GET DATA
        $content = DB::table('visitors')->where([
            ['id', $id],
        ])->first();


        // VERIFY IF EXISTS
        if(!$content){
            return redirect()->back();
        }

        // GENERATES DISPLAY WITH DATA
        return view('pages.user._partials.edit.visitors')->with(['visitor' => $content]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // GET FORM DATA

        $data = $request->all();     
        
        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

        // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

        if (Storage::exists($this->folderRoute() . $content->image)) {
            Storage::delete($this->folderRoute() . $content->image);
        }

        // VERIFY IF THERE IS A FILE WITH THE SAME NAME

        $nameFile = $request->image->getClientOriginalName();
        $exist = Visitor::all()->where('image', '=', $nameFile)->count();

        if($exist != 0) {
            $nameFile = rand(0 , 1000) . '-' . $nameFile;
        }

        // CREATE NAME OF IMAGE AND STORE

        $request->image->storeAs($this->folderRoute(), $nameFile);
        $data['image'] = $nameFile;

        }

        // STORING NEW DATA
        $content->update($data);

        // REGISTER IN UPDATES
        sendUpdate($content->id, 18, 2, $content->name ?? $content->title, $content->user);

        // REDIRECT AND MESSAGES

        if(Auth::user()->role == 'administrador'){
            return redirect()
                ->route('admin.users.show', $content->user)
                ->with('message', 'Visitante atualizado com sucesso.');
        } else {
            return redirect()
                ->route('account.index')
                ->with('message', 'Visitante atualizado com sucesso.');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // VERIFY IF EXISTS

        if(!$content = $this->repository->find($id))
        return redirect()->back();

        // VERIFY IF IMAGE EXISTS AND DELETE

        if (Storage::exists($this->folderRoute() . $content->image)) {
            Storage::delete($this->folderRoute() . $content->image);
        }

        // DELETE DATA 

        $content->delete();

        // REGISTER IN UPDATES
        sendUpdate($content->id, 18, 4, $content->name ?? $content->title, $content->user);

        // REDIRECT AND MESSAGES

        if(Auth::user()->role == 'administrador'){
            return redirect()
                ->route('admin.users.show', $content->user)
                ->with('message', 'Visitante deletado com sucesso.');
        } else {
            return redirect()
                ->route('account.index')
                ->with('message', 'Visitante deletado com sucesso.');
        }

    }

}

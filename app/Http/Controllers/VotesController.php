<?php

namespace App\Http\Controllers;

use App\Models\Vote;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VotesController extends Controller
{


    protected $request;
    private $repository;
    
    public function __construct(Request $request, Vote $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {

        // GET FORM DATA

        $data = $request->all();

        // STORING NEW DATA 

        $data['condominium'] = Auth::user()->condominium;
        $data['user'] = Auth::user()->id;
        $data['survey'] = $id;
        $this->repository->create($data);


        // REDIRECT AND MESSAGES

        return redirect()
                ->route('surveys.show', $id)
                ->with('message', 'Voto registrado com sucesso.');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        Vote::where('survey', $request->id)->where('user', Auth::user()->id)->update(['option' => $request->option]);

        // REDIRECT AND MESSAGES

        return redirect()
                ->route('surveys.show', $id)
                ->with('message', 'Voto atualizado com sucesso.');
    }

}

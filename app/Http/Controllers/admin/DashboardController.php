<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Condominium;
use App\Models\Construction;
use App\Models\Event;
use App\Models\Maintenance;
use App\Models\Message;
use App\Models\Note;
use App\Models\Relase;
use App\Models\Reservation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        
        // RETURN VIEW WITH DATA

        $notes          = Note::orderBy('created_at', 'DESC');
        $messages       = Message::orderBy('created_at', 'DESC');
        $users          = User::orderBy('created_at', 'DESC')->where('status', 'pendente');
        $reservations   = Reservation::orderBy('created_at', 'DESC')->where('status', 'pendente');
        $maintenances   = Maintenance::orderBy('created_at', 'DESC');
        $condominiums   = Condominium::orderBy('created_at', 'DESC');
        $notes          = Note::orderBy('created_at', 'DESC');
        $constructions  = Construction::orderBy('created_at', 'DESC');
        $events         = Event::orderBy('created_at', 'DESC');

        // FILTER IF YOU ARE VIEWING A CONDOMINIUM

        if(session('sessionCondominium')){

                // VARIABLE CONDOMINIUM
                $condominium  =  session('sessionCondominium');

                // QUERY
                $notes          = $notes->where('condominium', $condominium);
                $messages       = $messages->where('condominium', $condominium);
                $users          = $users->where('condominium', $condominium);
                $reservations   = $reservations->where('condominium', $condominium);
                $maintenances   = $maintenances->where('condominium', $condominium);
                $notes          = $notes->where('condominium', $condominium);
                $constructions  = $constructions->where('condominium', $condominium);
                $events         = $events->where('condominium', $condominium);

        }

        
        // CALENDAR
        foreach($notes->get() as $notes){
                $datesCalendar[] = array(
                        'title' => "{$notes['title']}", 
                        'url' => route('notes.edit', $notes['id']), 
                        'start' => $notes['day'] . 'T' .$notes['hour_start'], 
                        'end' => $notes['day'] . 'T'. $notes['hour_end'],
                        'color' => 'green'
                );
        }

        foreach($constructions->get() as $construction){
                $datesCalendar[] = array(
                        'title' => "{$construction['name']}", 
                        'url' => route('constructions.show', $construction['id']), 
                        'start' => $construction['start'].'T08:30:00', 
                        'end' => $construction['end'].'T09:30:00', 
                );
        }

        foreach($events->get() as $event){
                $datesCalendar[] = array(
                        'title' => "{$event['title']}", 
                        'url' => route('events.show', $event['id']), 
                        'start' => $event['start'].'T'.$event['hour_start'],
                        'end' => $event['end'].'T'.$event['hour_end'],
                        'color' => '#ff3364',
                );
        }

        return view('pages.admin.index', [
            'messages'      => $messages->get()->unique('from')->take(5),
            'users'         => $users->get()->take(5),
            'reservations'  => $reservations->get()->take(5),
            'maintenances'  => $maintenances->get()->take(5),
            'condominiums'  => $condominiums->get()->take(5),
            'notes'         => $notes->get()->take(5),
            'datesCalendar' => $datesCalendar ?? null,
        ]);

    }

}

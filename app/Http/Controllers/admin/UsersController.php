<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Condominium;
use App\Models\Dependent;
use App\Models\Employee;
use App\Models\Pet;
use App\Models\Reservation;
use App\Models\User;
use App\Models\Vehicle;
use App\Models\Visitor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class UsersController extends Controller
{

    protected $request;
    private $repository;
    
    public function __construct(Request $request, User $content)
    {
        
        $this->request = $request;
        $this->repository = $content;

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // GET ALL DATA

        $users = User::where('role', '!=', 'portaria')->get();

        // RETURN VIEW WITH DATA
        return view('pages.admin.users.index', [
            'users' => $users,
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // GET DATA USER
        $users = User::find($id);

        // GET DATAS
        $myVehicles = Vehicle::orderBy('updated_at', 'DESC')->where('user', $id)->get();
        $myPets = Pet::orderBy('updated_at', 'DESC')->where('user', $id)->get();
        $myEmployees = Employee::orderBy('updated_at', 'DESC')->where('user', $id)->get();
        $myVisitors = Visitor::orderBy('updated_at', 'DESC')->where('user', $id)->get();
        $myDependents = Dependent::orderBy('updated_at', 'DESC')->where('user', $id)->get();
        $reservations = Reservation::all()->where('user', $id)->take(5);

        // YEARS OF USER
        $age = date_diff(date_create($users->birth), date_create(date("d-m-Y")));

        return view('pages.user.index')->with([
            'titlePage' => 'Visualizando usuário',
            'user' => $users,
            'age' => $age->format("%y"),
            'vehicles' => $myVehicles,
            'pets' => $myPets,
            'employees' => $myEmployees,
            'visitors' => $myVisitors,
            'dependents' => $myDependents,
            'reservations' => $reservations,
        ]);
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        // GET DATA USER
        $users = User::find($id);

        // YEARS OF USER
        $age = date_diff(date_create($users->birth), date_create(date("d-m-Y")));

        // CONVERT SELECTED OPTIONS IN TABLE TO STRING

        return view('pages.user.edit')->with([
            'titlePage' => 'Editando usuário',
            'user' => $users,
            'age' => $age->format("%y"),
            'route' => route('admin.users.update', $users->id),
        ]);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        if (time() < strtotime('+18 years', strtotime($request->birth))) {
            return redirect()
                ->back()
                ->with('message', 'Menores não podem usar o sistema.');
        }

        // GET USER DATA
        
        $user = $this->repository->find($id);

        // GET FORM DATA

        $data = $request->all();       

        // CHECK IF THE USER SENT AN IMAGE AND VALIDATE

        if ($request->hasFile('image') && $request->image->isValid()) {

            // CHECK IF THE IMAGE ALREADY EXISTS, IF YES, DELETE AND SEND THE NEW

            if (Storage::exists($this->folderRoute() . $user->image)) {
                Storage::delete($this->folderRoute() . $user->image);
            }


            // CREATE NAME OF IMAGE AND STORE

            $nameFile = str_replace(' ', '_', $request->image->getClientOriginalName());
            $request->image->storeAs($this->folderRoute(), $nameFile);


            $data['image'] = $nameFile;

        }
    
        // STORING NEW DATA 

        $user->update($data);

        // REDIRECT AND MESSAGES
        
        return redirect()
                ->route('admin.users.show', $id)
                ->with('message', 'Usuário atualizado com sucesso.');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function approve($id, $status)
    {
        // VERIFY IF EXISTS
        if(!$content = $this->repository->find($id))
        return redirect()->back();

        if($status == 'aprovado'){
            // STORING NEW DATA
            $content->update(['status' => $status]);

            // REGISTER IN UPDATES
            sendUpdate($content->id, 21, 6, $content->name ?? $content->title);

            return redirect()
                    ->route('admin.index')
                    ->with('message', 'Usuário aprovado com sucesso.');
        } else {
            // STORING NEW DATA
            $content->update(['status' => $status]);

            // REGISTER IN UPDATES
            sendUpdate($content->id, 21, 7, $content->name ?? $content->title);

            return redirect()
                    ->route('admin.index')
                    ->with('message', 'Usuário bloqueado com sucesso.');
        }

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function condominium()
    {

        // VERIFY IF EXISTS

        if(!$condominium = Condominium::find(Auth::user()->condominium))
            return redirect()->back();


        // RETURN VIEW WITH DATA
        return view('pages.admin.condominium.show', [
            'condominium' => $condominium,
        ]);

    }


}

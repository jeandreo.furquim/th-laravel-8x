<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Access
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @param  string  $role
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, $role)
    {

        // Admin -> Syndic -> Concierges -> Resident

        if($role == 'admin' && Auth::user()->role == 'administrador'){

            return $next($request);

        } elseif ($role == 'syndic' && in_array(Auth::user()->role, ['administrador', 'sindico'])){

            return $next($request);

        } elseif ($role == 'concierges' && in_array(Auth::user()->role, ['administrador', 'sindico', 'portaria'])){

            return $next($request);

        } else {
            return redirect()
            ->back()
            ->with('message', 'Eii, você não pode acessar essa página!');
        }
           

    }
}

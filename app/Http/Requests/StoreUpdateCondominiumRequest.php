<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateCondominiumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'max:255',
            ],
            'syndic' => 'required|max:255',
            'cnpj' => 'required|max:255',
            'zone' => 'required|max:255',
            'cep' => 'required|max:255',
            'address' => 'required|max:255',
            'city' => 'required|max:255',
            'build' => 'required|max:255',
            'image' => 'nullable|image',
        ];
    }
}

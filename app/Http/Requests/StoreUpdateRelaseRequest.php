<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateRelaseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $id = $this->segment(3);

        return [
            'title' => [
                'required',
                'min:3',
                'max:255',
                "unique:relases,title,{$id},id"
            ],
            'description' => 'required|min:3|max:10000',
            'image' => 'nullable|file|max:20000|mimes:png,jpg,jpeg,pdf,msword,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        ];
    }

    public function messages()
    {
        return [
            'title.unique' => 'Já existe um comunicado com o mesmo nome',
            'title.required' => 'Título é obrigatório',
            'title.min' => 'O título muito curto',
            'title.max' => 'Título muito longo',
            'description.required' => 'A descrição é obrigatória',
            'description.min' => 'O descrição muito curta',
            'description.max' => 'Descrição muito longa',
        ];
    }
}

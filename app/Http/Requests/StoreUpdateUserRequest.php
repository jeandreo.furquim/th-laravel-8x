<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        $identifier = auth()->user()->identifier;
        $email = auth()->user()->email;

        return [
            'name' => [
                'required',
                'min:10',
                'max:255',
            ],

            'email' => [
                'required',
                "unique:users,email,{$email},email",
                'min:5',
                'max:255',
            ],

            'identifier' => [
                'required',
                "unique:users,identifier,{$identifier},identifier",
                'min:3',
                'max:3',
            ],
            'sex' => 'required|max:10',
            'house' => 'required|max:10',
            'cellphone' => 'required|max:16',
            'birth' => 'required|min:3|max:255',
            'image' => 'nullable|image',
        ];
    }
}

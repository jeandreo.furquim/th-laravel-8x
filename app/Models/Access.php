<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Access extends Model
{
    use HasFactory;
    protected $fillable = ['user', 'condominium', 'role', 'last_login_at', 'last_login_ip'];
    protected $table = 'access';
}

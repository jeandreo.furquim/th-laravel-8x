<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'name', 'image', 'description'];
    
    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

    public function gallery()
    {
        return $this->hasMany(AlbumsPhotos::class, 'album', 'id');
    }

}
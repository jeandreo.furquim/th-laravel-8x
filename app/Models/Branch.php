<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'name', 'branch'];

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConciergeInfos extends Model
{
    use HasFactory;
    protected $fillable = [
        'user',
        'name_concierge',
        'branch',
        'start',
        'end',
        'company',
        'manager',
        'contact_phone',
    ];
}

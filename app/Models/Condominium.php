<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Condominium extends Model
{   
    use HasFactory;
    protected $fillable = ['name', 'syndic', 'type', 'cnpj', 'zone', 'cep', 'address', 'city', 'build', 'image', 'phone', 'attorney_name', 'attorney_office', 'attorney_phone', 'attorney_email', 'attorney_address', 'administrator_company', 'administrator_address', 'administrator_name_01', 'administrator_phone_01', 'administrator_email_01', 'administrator_name_02', 'administrator_phone_02', 'administrator_email_02', 'insurance_company', 'insurance_number', 'insurance_phone', 'insurance_email', 'insurance_agent_name', 'insurance_agent_phone', 'insurance_agent_email', 'insurance_agent_address', 'engineering_company', 'engineering_name', 'engineering_phone', 'engineering_email', 'engineering_address', 'construction_company', 'construction_developer', 'construction_developer_cnpj', 'construction_name', 'construction_cnpj', 'construction_phone', 'construction_email', 'construction_address', ];
    protected $table = 'condominiums';

    public function branches()
    {
        return $this->hasMany(Branch::class, 'condominium', 'id');
    }

    public function usersCondominium()
    {
        return $this->hasMany(User::class, 'condominium', 'id');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Configuration extends Model
{
    use HasFactory;
    protected $table = 'configurations';
    protected $fillable = [
        'user',
        'sidebar',
        'sidebar_text',
        'sidebar_icon', 
        'sidebar_top',
        'sidebar_icon_active',
        'buttons', 
        'buttons_hover', 
        'titles_color', 
        'p_color', 
        'link_color', 
        'dark_color', 
        'muted_color', 
    ];
}
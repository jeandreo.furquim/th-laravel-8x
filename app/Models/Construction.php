<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Construction extends Model
{
    
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'image', 'document', 'name', 'start', 'end', 'cost', 'description'];

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

    public function gallery()
    {
        return $this->hasMany(GalleryConstruction::class, 'construction', 'id');
    }

    public function updates()
    {
        return $this->hasMany(UpdatesConstruction::class, 'construction', 'id');
    }

}

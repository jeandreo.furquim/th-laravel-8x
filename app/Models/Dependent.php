<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dependent extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'image', 'name', 'identifier', 'birth', 'observation'];

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'name', 'file', 'url', 'description'];

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }
}
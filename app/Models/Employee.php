<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'image', 'name', 'identifier', 'office', 'phone', 'cellphone', 'from', 'until', 'week', 'observation'];

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }
}

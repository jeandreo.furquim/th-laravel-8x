<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmployeeCond extends Model
{
    use HasFactory;

    protected $fillable = ['condominium', 'user', 'image', 'name', 'office', 'phone', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'hour_start', 'hour_end', 'observation'];
    protected $table = 'employees_condominiums';

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

    public function cond()
    {
        return $this->hasOne(Condominium::class, 'id', 'condominium');
    }
}

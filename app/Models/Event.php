<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'image', 'document', 'title', 'place', 'start', 'end', 'hour_start', 'hour_end', 'description'];

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

    public function presence()
    {
        return $this->hasMany(Presence::class, 'event', 'id');
    }

}

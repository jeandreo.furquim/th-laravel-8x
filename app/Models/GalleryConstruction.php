<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GalleryConstruction extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'image', 'construction'];
    protected $table = 'constructions_gallery';
}

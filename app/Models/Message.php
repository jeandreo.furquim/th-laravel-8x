<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'from', 'to', 'message', 'see'];

    public function from()
    {
        return $this->hasOne(User::class, 'id', 'from');
    }
    
}

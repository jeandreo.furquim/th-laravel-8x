<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'title', 'day', 'hour_start', 'hour_end', 'description', 'status'];

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

}

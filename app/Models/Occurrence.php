<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Occurrence extends Model
{

    use HasFactory;
    protected $fillable = ['condominium', 'user', 'image', 'document', 'title', 'type', 'status', 'condition', 'description'];

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'owner', 'image', 'title', 'sender', 'observation', 'status', 'received'];

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

}

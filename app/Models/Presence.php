<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Presence extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'event', 'option'];

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }
}

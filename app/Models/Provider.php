<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Provider extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'name', 'image', 'document', 'phone', 'email', 'cnpj', 'website', 'start', 'end', 'description'];
    
    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }
}
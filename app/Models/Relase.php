<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relase extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'image', 'title', 'description'];

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }
}
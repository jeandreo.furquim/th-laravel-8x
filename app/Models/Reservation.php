<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;

    protected $fillable = ['condominium', 'user', 'room', 'hour', 'day', 'time', 'status'];
    
    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

    public function room()
    {
        return $this->hasOne(Room::class, 'id', 'room');
    }

    public function cond()
    {
        return $this->hasOne(Condominium::class, 'id', 'condominium');
    }

}

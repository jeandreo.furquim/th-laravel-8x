<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class room extends Model
{
    use HasFactory;

    protected $fillable = ['condominium', 'user', 'image', 'name', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday', 'hour_start', 'hour_end', 'capacity', 'tax_money', 'antecedence', 'cancellation', 'exclusive', 'approval', 'infrastructure', 'rules'];
    
    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

    public function cond()
    {
        return $this->hasOne(Condominium::class, 'id', 'condominium');
    }

}

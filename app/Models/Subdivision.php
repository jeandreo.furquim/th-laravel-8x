<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subdivision extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'name'];

    public function cond()
    {
        return $this->hasOne(Condominium::class, 'id', 'condominium');
    }
    
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'image', 'title', 'start', 'end', 'description', 'option_01', 'option_02', 'option_03', 'option_04', 'option_05', 'show', 'overdue'];

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

    public function usersCond()
    {
        return $this->hasMany(User::class, 'condominium', 'id');
    }

    public function votes()
    {
        return $this->hasMany(Vote::class, 'survey', 'id');
    }
    
}

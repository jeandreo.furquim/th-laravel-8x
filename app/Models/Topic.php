<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'image', 'title', 'type', 'status', 'description'];

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Update extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'content', 'type', 'action', 'title'];

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }
}

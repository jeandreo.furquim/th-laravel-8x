<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'condominium',
        'subdivision',
        'house',
        'identifier',
        'sex',
        'birth',
        'cellphone',
        'contacts',
        'owner',
        'status',
        'role',
        'email',
        'password',
        'image',
        'notify',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function cond()
    {
        return $this->hasOne(Condominium::class, 'id', 'condominium');
    }

    public function subd()
    {
        return $this->hasOne(Subdivision::class, 'id', 'subdivision');
    }

    public function msgsUser()
    {
        return $this->hasMany(Message::class, 'from', 'id');
    }

    public function portersInfos()
    {
        return $this->hasOne(PortersInfos::class, 'user', 'id');
    }

}

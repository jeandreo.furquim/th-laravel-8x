<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'image', 'type', 'brand', 'board', 'year', 'color', 'observation'];

    public function owner()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }
}



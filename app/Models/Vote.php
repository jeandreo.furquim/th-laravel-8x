<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    use HasFactory;
    protected $fillable = ['condominium', 'user', 'survey', 'option'];
    protected $table = 'votes';

    public function author()
    {
        return $this->hasOne(User::class, 'id', 'user');
    }

    

}

<?php 
// GET OPTIONS FROM URL
$urlOptions = request()->segments();

// FILTER BY PERIOD
if(in_array($filter, ['semana', 'mes', 'ano'])){
    switch ($filter) {
        case 'semana':
                $start = date('Y-m-d', strtotime('-7 days'));
                $end = date('Y-m-d');
            break;
        case 'mes':
                $start = date('Y-m-d', strtotime('-30 days'));
                $end = date('Y-m-t');
                break;
        case 'ano':
                $start = date('Y-01-01');
                $end = date('Y-12-t');
            break;
    }
    if(isset($filter) && $filter != 'all'){
        $contents = $contents->whereRaw('date(created_at) >= ? AND date(created_at) <= ?',[
            date('Y-m-d',strtotime($start)),
            date('Y-m-d',strtotime($end)),
        ]);
    }
}

// ORDER BY
if(in_array(end($urlOptions), ['recentes', 'antigos'])){
    $contents = end($urlOptions) == 'antigos' ? $contents->orderBy('id', 'ASC') : $contents->orderBy('id', 'DESC'); 
}


// FILTER TYPES
if(request()->segments()[0] == 'topicos'){
    $topicsCategorys = ['Assembleias', 'Achados e perdidos', 'Eu recomendo', 'Carona', 'Eu Preciso de', 'Vagas de garagem', 'AdCircus'];
} else {
    $topicsCategorys = ['Dúvida', 'Manutenção', 'Sugestão', 'Solicitação', 'Reclamação', 'Elogio', 'Outros'];
}

// VERIFY IF TYPE
if(array_intersect($urlOptions, $topicsCategorys) != []){
    // FIND ARRAY CATEGORY
    $typeSelected = implode(array_intersect($urlOptions, $topicsCategorys));
    $contents = $contents->where('type', $typeSelected);
}
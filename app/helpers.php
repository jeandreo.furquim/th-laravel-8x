<?php

use App\Models\Configuration;
use App\Models\Update;
use App\Models\User;
use Illuminate\Support\Facades\Auth;


    // LOAD CUSTOM STYLE
    function customStyle(){

        $configurations = Configuration::find(1);

        return $configurations;

    }

        
    // VERIFY IF THE USER HAS A PROFILE IMAGE

    function imageUser($id){

        $user = User::find($id);

        if ($user->image){
            $imgProfile = url("storage/condominios/" . $user->condominium . "/usuarios/" . $user->id . "/" . $user->image);
        } else {
            $imgProfile = url("assets/images/blank.png");
        }
        return $imgProfile;

    }

    // GET IMAGE FROM USER

    function itemImageUser($array){

        if ($array->image) {
            $url = url('storage/condominios/' . $array->condominium . '/usuarios/' . $array->user . '/' . $array->image);
        } else {
            $url = url('assets/images/default.jpg');
        }
        return $url;

    }

    // MAKE YOUR DATES MORE FRIENDLY

    function days($date){

        $today = new DateTime();
        $received = new DateTime($date);
        $interval = $today->diff($received);

        switch ($interval->d) {
            case 0:
                $msg = 'Hoje';
                break;
            case 1:
                $msg = 'Ontem';
                break;
            default:
                $msg = $interval->d . ' dias';
                break;
        }

        return $msg;

    }

    // REMOVE ALL SPECIAL CHARACTERS

    function clean($string) {
        $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/ ', '', $string); // Removes special chars.
    }

    ////////////////////////////////////
    /////// UPDATES /// UPDATES ////////
    ////////////////////////////////////

    // [0] Comunicado
    // [1] Chamados 
    // [2] Albúm
    // [3] Ramal
    // [4] Portaria
    // [5] Construção
    // [6] Morador
    // [7] Documento
    // [8] Funcionário condomínio
    // [9] Funcionário
    // [10] Evento
    // [11] Manutenção
    // [12] Pacote
    // [13] Pet
    // [14] Fornecedor
    // [15] Enquete
    // [16] Tópicos [Mural]
    // [17] Veículos
    // [18] Visitantes
    // [19] Espaços
    // [20] Reservas
    // [21] Usuário
    // [22] Anotações
    // [23] Configurações

    // GET ALL UPDATES

    function updatesCondominiuns($qtd){

        if(session('sessionCondominium')){
            $updates = Update::where('condominium', session('sessionCondominium'))->orderBy('id', 'DESC')->take($qtd)->get();
        } else {
            $updates = Update::orderBy('id', 'DESC')->take($qtd)->get();
        }
        return $updates;

    }

    function updatesCondominiunsHeader($qtd){

        $updates = Update::WhereIn('type', [0,1,2,3,4,5,7,8,10,11,14,15,16,19])->orderBy('id', 'DESC')->take($qtd)->get();
        return $updates;

    }

    function urlUpdates($type, $content, $user){

        switch ($type) {
            case 0:
                $url = route('relases.show', $content);
                break;
            case 1:
                $url = route('occurrences.show', $content);
                break;
            case 2:
                $url = route('albums.show', $content);
                break;
            case 3:
                $url = route('branches.results', $content);
                break;
            case 4:
                $url = '#';
                break;
            case 5:
                $url = route('constructions.show', $content);
                break;
            case 6:
                $url = route('admin.users.show', $content);
                break;
            case 7:
                $url = route('documents.show', $content);
                break;
            case 8:
                $url = route('employeescond.show', $content);
                break;
            case 9:
                $url = route('admin.users.show', $user);
                break;
            case 10:
                $url = route('events.show', $content);
                break;
            case 11:
                $url = route('maintenances.show', $content);
                break;
            case 12:
                $url = route('packages.show', $content);
                break;
            case 13:
                $url = route('admin.users.show', $user);
                break;
            case 14:
                $url = route('providers.show', $content);
                break;
            case 15:
                $url = route('surveys.show', $content);
                break;
            case 16:
                $url = route('topics.show', $content);
                break;
            case 17:
                $url = route('admin.users.show', $user);
                break;
            case 18:
                $url = route('admin.users.show', $user);
                break;
            case 19:
                $url = route('rooms.show', $content);
                break;
            case 20:
                $url = route('rooms.show', $content);
                break;
            default:
                $url = '#';
                break;
        }

        return $url;

    }

    // REGISTER IN UPDATES
    function sendUpdate($id, $type, $action, $title, $idUser = null){

        $user = $idUser == null ? Auth::user()->id : $idUser;

        Update::create([
            'condominium' => session('sessionCondominium') ?? Auth::user()->condominium,
            'user' => $user,
            'content' => $id,
            'type' => $type,
            'action' => $action,
            'title' => $title,
        ]);
    }

    // RENDER MESSAGES IN VIEW

    function msgUpdate($a, $b){
        
        // CONTENT TYPE
        $type = ['Comunicado', 'Chamado', 'Albúm', 'Números', 'Portaria', 'Construção', 'Morador', 'Documento', 'Funcionário do condomínio', 
            'Funcionário', 'Evento', 'Manutenção', 'Pacote', 'Pet', 'Fornecedor', 'Enquete', 'Tópico', 'Veículo', 'Visitante', 'Espaço', 'Reserva', 'Usuário', 'Anotação', 'Configuração'];

        // ACTION EXECUTED
        $action = [
        'adicionado(s)', 'adicionada(s)', 'atualizado(s)', 
        'atualizada(s)', 'excluído(s)',   'excluída(s)', 
        'aprovado',      'bloqueado',     'solicitada', 
        'reservou',      'aprovada',      'negada',
        'solicitado',    'Desativado',    'retirado',
        'não retirado'
    ];

        // GENERATE UPDATE MESSAGE
        return $type[$a] . ' ' . $action[$b] . '.';

    }
<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class RelaseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user' => '1',
            'title' => $this->faker->unique()->word,
            'description' => $this->faker->sentence(),
            'zone' => '1',
        ];
    }
}

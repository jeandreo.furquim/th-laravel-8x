<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        
        return [
            'name' => $this->faker->name(),
            'image' => '1.jpg',
            'condominium' => '1',
            'subdivision' => $this->faker->numberBetween(1,3),
            'house' => $this->faker->unique()->numberBetween(1,15000),
            'identifier' => $this->faker->numberBetween(1,999),
            'sex' => $this->faker->randomElement(['masculino', 'feminino']),
            'birth' => $this->faker->dateTimeBetween(),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'cellphone' => '(41) 9 9999-9999',
            'owner' => $this->faker->randomElement(['Proprietário(a)', 'Morador(a)', 'Inquilino(a)', 'Locatário(a)', 'Representante/Procurador']),
            'status' => $this->faker->randomElement(['aprovado', 'aprovado', 'aprovado', 'bloqueado', 'pendente']),
            'role' => 'morador',
            'password' => bcrypt('LIFEword1243'), // password
            'remember_token' => Str::random(10),
            'last_login_at' => $this->faker->dateTimeBetween('-2 months', '+0 day'),
        ];
    }
    
    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}

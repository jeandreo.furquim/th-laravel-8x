<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image')->nullable();
            $table->integer('condominium');
            $table->integer('subdivision')->nullable();
            $table->string('house')->nullable();
            $table->string('identifier', 3)->nullable();
            $table->enum('sex', ['masculino', 'feminino']);
            $table->date('birth');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('cellphone')->nullable();
            $table->json('contacts')->nullable();
            $table->enum('owner', ['Proprietário(a)', 'Morador(a)', 'Inquilino(a)', 'Locatário(a)', 'Representante/Procurador']);
            $table->enum('status', ['pendente', 'aprovado', 'bloqueado', 'desativado'])->default('pendente');
            $table->enum('role', ['morador', 'administrador', 'sindico', 'portaria']);
            $table->text('notify')->nullable();
            $table->integer('welcome')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->dateTime('last_login_at')->nullable();
            $table->string('last_login_ip')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

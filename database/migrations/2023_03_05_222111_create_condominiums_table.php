<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCondominiumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condominiums', function (Blueprint $table) {
            $table->id();
            $table->string('image')->nullable();
            $table->string('name');
            $table->string('syndic');
            $table->string('type');
            $table->string('cnpj');
            $table->string('zone');
            $table->string('cep');
            $table->string('address');
            $table->string('city');
            $table->date('build');
            $table->string('phone')->nullable();
            $table->string('attorney_name')->nullable();
            $table->string('attorney_office')->nullable();
            $table->string('attorney_phone')->nullable();
            $table->string('attorney_phone_fixe')->nullable();
            $table->string('attorney_email')->nullable();
            $table->string('attorney_address')->nullable();
            $table->string('administrator_company')->nullable();
            $table->string('administrator_address')->nullable();
            $table->string('administrator_name_01')->nullable();
            $table->string('administrator_phone_01')->nullable();
            $table->string('administrator_email_01')->nullable();
            $table->string('administrator_name_02')->nullable();
            $table->string('administrator_phone_02')->nullable();
            $table->string('administrator_email_02')->nullable();
            $table->string('insurance_company')->nullable();
            $table->string('insurance_number')->nullable();
            $table->string('insurance_phone')->nullable();
            $table->string('insurance_email')->nullable();
            $table->string('insurance_agent_name')->nullable();
            $table->string('insurance_agent_phone')->nullable();
            $table->string('insurance_agent_email')->nullable();
            $table->string('insurance_agent_address')->nullable();
            $table->string('engineering_company')->nullable();
            $table->string('engineering_name')->nullable();
            $table->string('engineering_phone')->nullable();
            $table->string('engineering_email')->nullable();
            $table->string('engineering_address')->nullable();
            $table->string('construction_company')->nullable();
            $table->string('construction_developer')->nullable();
            $table->string('construction_name')->nullable();
            $table->string('construction_phone')->nullable();
            $table->string('construction_email')->nullable();
            $table->string('construction_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('condominiums');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
            $table->integer('condominium');
            $table->integer('user');
            $table->string('image', 255)->nullable();
            $table->enum('status', ['ativo', 'desativado'])->default('ativo');
            $table->string('name', 100);
            $table->integer('monday')->default(0);
            $table->integer('tuesday')->default(0);
            $table->integer('wednesday')->default(0);
            $table->integer('thursday')->default(0);
            $table->integer('friday')->default(0);
            $table->integer('saturday')->default(0);
            $table->integer('sunday')->default(0);
            $table->string('hour_start')->default('8:00');
            $table->string('hour_end')->default('20:00');
            $table->string('capacity');
            $table->string('tax_money');
            $table->string('antecedence');
            $table->string('cancellation');
            $table->string('exclusive')->nullable();
            $table->string('approval')->nullable();
            $table->text('infrastructure')->nullable();
            $table->text('rules')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}

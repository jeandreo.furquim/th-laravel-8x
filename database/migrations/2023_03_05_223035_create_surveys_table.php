<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surveys', function (Blueprint $table) {
            $table->id();
            $table->integer('condominium');
            $table->integer('user');
            $table->string('image', 255)->nullable();
            $table->string('title', 100)->unique();
            $table->date('start');
            $table->date('end');
            $table->text('description');
            $table->string('option_01', 100)->nullable();
            $table->string('option_02', 100)->nullable();
            $table->string('option_03', 100)->nullable();
            $table->string('option_04', 100)->nullable();
            $table->string('option_05', 100)->nullable();
            $table->string('show', 100);
            $table->string('overdue', 100);            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surveys');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopicsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topics', function (Blueprint $table) {
            $table->id();
            $table->integer('condominium');
            $table->integer('user');
            $table->string('image', 255)->nullable();
            $table->string('title', 100);
            $table->enum('type', ['Assembleias', 'Achados e perdidos', 'Eu recomendo', 'Carona', 'Eu Preciso de', 'Vagas de garagem', 'AdCircus']);
            $table->enum('status', ['Aprovado', 'Bloqueado', 'Pendente'])->default('Pendente');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topics');
    }
}

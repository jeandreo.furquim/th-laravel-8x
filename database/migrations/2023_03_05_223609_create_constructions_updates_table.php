<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConstructionsUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('constructions_updates', function (Blueprint $table) {
            $table->id();
            $table->integer('condominium');
            $table->integer('user');
            $table->string('title', 255);
            $table->text('description')->nullable();
            $table->integer('construction');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructions_updates');
    }
}

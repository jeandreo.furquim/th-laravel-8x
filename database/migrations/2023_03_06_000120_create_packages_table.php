<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->integer('condominium');
            $table->integer('user');
            $table->integer('owner');
            $table->string('image', 255)->nullable();
            $table->string('title');
            $table->string('sender')->nullable();
            $table->text('observation')->nullable();
            $table->enum('status', ['aguardando', 'não retirado', 'retirado'])->default('aguardando');
            $table->dateTime('received')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}

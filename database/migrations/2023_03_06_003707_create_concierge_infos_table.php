<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConciergeInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concierge_infos', function (Blueprint $table) {
            $table->id();
            $table->integer('user');
            $table->string('name_concierge')->nullable();
            $table->string('branch')->nullable();
            $table->string('start');
            $table->string('end');
            $table->string('company')->nullable();
            $table->string('manager')->nullable();
            $table->string('contact_phone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('concierge_infos');
    }
}

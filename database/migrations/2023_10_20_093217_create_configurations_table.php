<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configurations', function (Blueprint $table) {
            $table->id();
            $table->integer('user')->nullable();
            $table->string('sidebar')->nullable();
            $table->string('sidebar_text')->nullable();
            $table->string('sidebar_icon')->nullable();
            $table->string('sidebar_top')->nullable();
            $table->string('sidebar_icon_active')->nullable();
            $table->string('buttons')->nullable();
            $table->string('buttons_hover')->nullable();
            $table->string('titles_color')->nullable();
            $table->string('p_color')->nullable();
            $table->string('link_color')->nullable();
            $table->string('dark_color')->nullable();
            $table->string('muted_color')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configurations');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCnpjInCondominiumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('condominiums', function (Blueprint $table) {
            $table->string('construction_cnpj')->nullable()->after('construction_name');
            $table->string('construction_developer_cnpj')->nullable()->after('construction_developer');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('condominiums', function (Blueprint $table) {
            $table->dropColumn(['construction_cnpj', 'construction_developer_cnpj']);
        });
    }
}

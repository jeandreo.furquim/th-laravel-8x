<?php

namespace Database\Seeders;

use App\Models\Album;
use App\Models\Branch;
use App\Models\ConciergeInfos;
use App\Models\Condominium;
use App\Models\Construction;
use App\Models\EmployeeCond;
use App\Models\Event;
use App\Models\GalleryConstruction;
use App\Models\Note;
use App\Models\Occurrence;
use App\Models\Package;
use App\Models\Provider;
use App\Models\Relase;
use App\Models\Room;
use App\Models\Subdivision;
use App\Models\Survey;
use App\Models\Topic;
use App\Models\UpdatesConstruction;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

use function PHPSTORM_META\map;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Thays Hoeffling',
            'image' => '1.jpg',
            'condominium' => 1,
            'subdivision' => null,
            'house' => '123456',
            'identifier' => '111',
            'sex' => 'feminino',
            'birth' => '1995-01-01',
            'email' => 'administracao@thcondominios.com',
            'email_verified_at' => now(),
            'cellphone' => '(41) 9 9999-9999',
            'owner' => 'Proprietário(a)',
            'status' => 'aprovado',
            'role' => 'administrador',
            'password' => bcrypt('LIFEword1243'),
            'remember_token' => Str::random(10),
            'last_login_at' => now(),
        ]);

        User::create([
            'name' => 'Eduarda Cruz',
            'image' => '1.jpg',
            'condominium' => 1,
            'subdivision' => 1,
            'house' => '1048',
            'identifier' => '106',
            'sex' => 'feminino',
            'birth' => '2002-01-01',
            'email' => 'eduarda@gmail.com',
            'email_verified_at' => now(),
            'cellphone' => '(41) 9 9999-9999',
            'owner' => 'Proprietário(a)',
            'status' => 'aprovado',
            'role' => 'morador',
            'password' => bcrypt('LIFEword1243'),
            'remember_token' => Str::random(10),
            'last_login_at' => now(),
        ]);

        User::create([
            'name' => 'Rafael Dias',
            'email' => 'portaria@toronto.com',
            'condominium' => 1,
            'cellphone' => '(99) 9 9999-9999',
            'birth' => '1995-01-01',
            'role' => 'portaria',
            'status' => 'aprovado',
            'password' => bcrypt('LIFEword1243'),
            'remember_token' => Str::random(10),
            'last_login_at' => now(),
        ]);

        ConciergeInfos::create([
            'user' => 3,
            'name_concierge' => 'Portaria Entrada Sul',
            'start' => '8:30',
            'end' => '19:30',
            'branch' => 1025,
            'company' => 'SULATENDIMENTO PORTARIAS',
            'manager' => 'Pamela(gerente)',
            'contact_phone' => '(99) 9 9999-9999',
        ]);

        
        User::factory(10)->create();

        Condominium::create([
            'name' => 'Condomínio Toronto',
            'image' => '1.jpg',
            'syndic' => 'Thays Hoeffling',
            'type' => 'Horizontal',
            'cnpj' => '00.000.000/0001-00',
            'zone' => 'Alphaville',
            'cep' => '13403-151',
            'address' => 'Av. dos Marins, 1550 - Jardim Parque Jupia Piracicaba - SP',
            'city' => 'Curitiba',
            'build' => '2000-01-01',
            'phone' => '(41) 9 9999-9999',
            'created_at' => now(),
        ]);

        Condominium::create([
            'name' => 'Condomínio Green Village',
            'image' => true,
            'syndic' => 'Thays Hoeffling',
            'type' => 'Horizontal',
            'cnpj' => '00.000.000/0001-00',
            'zone' => 'Cascavel',
            'cep' => '83413-200',
            'address' => 'R. Estr. da Graciosa, 3642 - Santa Rita, Pinhais - PR',
            'city' => 'Colombo',
            'build' => '2000-01-01',
            'created_at' => now(),
        ]);
        
        Subdivision::create([
            'condominium' => 1,
            'user' => 1,
            'name' => 'Bloco A',
            'created_at' => now(),
        ]);
        
        Subdivision::create([
            'condominium' => 1,
            'user' => 1,
            'name' => 'Bloco B',
            'created_at' => now(),
        ]);

        Subdivision::create([
            'condominium' => 1,
            'user' => 1,
            'name' => 'Bloco C',
            'created_at' => now(),
        ]);
        
        Relase::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'portaria.jpg',
            'user' => 1,
            'title' => 'Horário da portaria atualizado',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since.',
        ]);
        
        Survey::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'identidade.jpg',
            'title' => 'Nova identidade visual para nosso condomínio',
            'start' => '2022-08-30',
            'end' => '2022-12-25',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'option_01' => 'Sim, apoio essa decisão.',
            'option_02' => 'Não concordo com isso.',
            'option_03' => 'Não me importo.',
            'show' => 'sim',
            'overdue' => 'não',
            'created_at' => now(),
        ]);

        Survey::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'fachada.jpg',
            'title' => 'Reforma da fachada',
            'start' => '2021-03-07',
            'end' => '2021-03-31',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'option_01' => 'Prefiro a empresa A',
            'option_02' => 'Prefiro a empresa B',
            'option_03' => 'Prefiro a empresa C',
            'show' => 'sim',
            'overdue' => 'não',
            'created_at' => now(),
        ]);

        Survey::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'festival.jpg',
            'title' => 'Festival de ano',
            'start' => '2023-03-07',
            'end' => '2023-03-31',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'option_01' => 'Prefiro que a festa inicie as 08:00',
            'option_02' => 'Prefiro que a festa inicie as 14:00',
            'show' => 'sim',
            'overdue' => 'não',
            'created_at' => now(),
        ]);

        Event::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'virada.jpg',
            'document' => 'pdfdeexemplo.pdf',
            'title' => 'Virada do Ano',
            'place' => 'Salão de festas',
            'start' => '2022-12-31',
            'end' => '2023-01-01',
            'hour_start' => '22:30:00',
            'hour_end' => '01:00:00',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'created_at' => now(),
        ]);

        Event::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'juninta.jpg',
            'document' => null,
            'title' => 'Festa Junina 2022',
            'place' => 'Quadra de esportes',
            'start' => '2022-10-11',
            'end' => '2022-10-11',
            'hour_start' => '08:30:00',
            'hour_end' => '19:00:00',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'created_at' => now(),
        ]);

        Construction::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'basquete.jpg',
            'document' => null,
            'name' => 'Criação da Quadra de Basquete',
            'start' => '2022-10-11',
            'end' => '2022-10-11',
            'cost' => 'R$ 12.500,00',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'created_at' => now(),
        ]);

        
        UpdatesConstruction::create([
            'condominium' => 1,
            'user' => 1,
            'title' => 'Obra em fase final!',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'construction' => 1,
            'created_at' => now()
        ]);

        GalleryConstruction::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'img_02.jpg',
            'construction' => 1,
        ]);

        GalleryConstruction::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'img_02.jpg',
            'construction' => 1,
        ]);

        GalleryConstruction::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'img_02.jpg',
            'construction' => 1,
        ]);

        GalleryConstruction::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'img_02.jpg',
            'construction' => 1,
        ]);

        GalleryConstruction::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'img_02.jpg',
            'construction' => 1,
        ]);
        
        GalleryConstruction::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'img_02.jpg',
            'construction' => 1,
        ]);

        Room::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'salao.jpg',
            'name' => 'Salão de festas',
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'hour_start' => '08:00',
            'hour_end' => '23:00',
            'capacity' => 80,
            'tax_money' => 'R$ 320,00',
            'antecedence' => 15,
            'cancellation' => 10,
            'exclusive' => 1,
            'approval' => 1,
            'infrastructure' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'rules' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'created_at' => now(),
        ]);

        Room::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'academia.jpeg',
            'name' => 'Academia Grow',
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'hour_start' => '06:00',
            'hour_end' => '22:00',
            'capacity' => 30,
            'tax_money' => 'R$ 230,00',
            'antecedence' => 2,
            'cancellation' => 0,
            'exclusive' => 0,
            'approval' => 0,
            'infrastructure' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'rules' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'created_at' => now(),
        ]);

        Room::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'biblioteca.jpg',
            'name' => 'Biblioteca',
            'thursday' => 1,
            'friday' => 1,
            'saturday' => 1,
            'sunday' => 1,
            'hour_start' => '08:00',
            'hour_end' => '20:00',
            'capacity' => 10,
            'tax_money' => 'R$ 0',
            'antecedence' => 0,
            'cancellation' => 0,
            'exclusive' => 0,
            'approval' => 0,
            'infrastructure' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'rules' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry"s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
            'created_at' => now(),
        ]);

        Occurrence::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'jovens.jpg',
            'document' => 'pdf_exemplo.pdf',
            'title' => 'Cursos para jovens',
            'type' => 'Sugestão',
            'status' => 'Aprovada',
            'condition' => 'Em andamento',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since.',
            'created_at' => now(),
        ]);

        Occurrence::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'internet.jpg',
            'document' => null,
            'title' => 'Temos outra opção de internet?',
            'type' => 'Dúvida',
            'status' => 'Aprovada',
            'condition' => 'Encerrada',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since.',
            'created_at' => now(),
        ]);

        Topic::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'chave.jpeg',
            'title' => 'Encontrado chave de BMW',
            'type' => 'Achados e perdidos',
            'status' => 'Aprovado',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since.',
            'created_at' => now(),
        ]);

        Topic::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'vaga.webp',
            'title' => 'Vaga Extra disponível',
            'type' => 'Vagas de garagem',
            'status' => 'Aprovado',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since.',
            'created_at' => now(),
        ]);

        EmployeeCond::create([
            'condominium' => 1,
            'user' => 1,
            'image' => 'julia.jpg',
            'name' => 'Julia Aguiar',
            'office' => 'Zeladora',
            'phone' => '(41) 9 9999-9999',
            'monday' => 1,
            'tuesday' => 1,
            'wednesday' => 1,
            'thursday' => 1,
            'friday' => 1,
            'hour_start' => '07:00',
            'hour_end' => '17:00',
            'observation' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since.',
            'created_at' => now(),
        ]);

        Branch::create([
            'condominium' => 1,
            'user' => 1,
            'name' => 'Portaria',
            'branch' => '123-456',
            'created_at' => now(),
        ]);

        Branch::create([
            'condominium' => 1,
            'user' => 1,
            'name' => 'Portaria 2',
            'branch' => '123-321',
            'created_at' => now(),
        ]);

        Branch::create([
            'condominium' => 1,
            'user' => 1,
            'name' => 'Central de Segurança',
            'branch' => '(41) 9 9999-9999',
            'created_at' => now(),
        ]);

        Branch::create([
            'condominium' => 1,
            'user' => 1,
            'name' => 'Provedor de internet',
            'branch' => '(41) 9 9999-9998',
            'created_at' => now(),
        ]);

        Branch::create([
            'condominium' => 1,
            'user' => 1,
            'name' => 'TH Condomínios - Suporte',
            'branch' => '(41) 9 9999-9998',
            'created_at' => now(),
        ]);

        Provider::create([
            'condominium' => 1,
            'user' => 1,
            'name' => 'Empresa de Faxinas',
            'image' => 'reformas.jpg',
            'document' => 'contrato.pdf',
            'phone' => '(41) 9 9999-9998',
            'email' => 'email@email.com.br',
            'cnpj' => '99.999.999/9999-99',
            'website' => 'www.reformas.com.br',
            'start' => '2022-08-30',
            'end' => '2022-12-25',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since.',
            'created_at' => now(),
        ]);

        Album::create([
            'condominium' => 1,
            'user' => 1,
            'name' => 'Festa de Outono',
            'image' => 'outono.jpg',
            'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since.',
            'created_at' => now(),
        ]);
        
        Package::create([
            'condominium' => 1,
            'user' => 1,
            'owner' => 2,
            'image' => 'caixa.webp',
            'title' => 'Encomenda grande do Submarino',
            'sender' => 'Submarino',
            'observation' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since.',
        ]);

        Note::create([
            'condominium' => 1,
            'user' => 1,
            'title' => 'Visitar Morador do 3B',
            'day' => '2022-12-16',
            'hour_start' => '09:00:00',
            'hour_end' => '12:00:00',
            'description' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged."
        ]);

    }
}

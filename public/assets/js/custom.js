// CNPJ
Inputmask({
    clearIncomplete: true,
    "mask" : "99.999.999/9999-99"
}).mask(".input-cnpj");

// CEP
Inputmask({
    clearIncomplete: true,
    "mask" : "99999-999"
}).mask(".input-cep");

// PHONE
Inputmask({
    clearIncomplete: true,
    "mask" : ["(99) 9999-9999", "(99) 9 9999-9999", "9999 999 99999"]
}).mask(".input-phone");

// Calendar DataPicker
$(".flatpickr").flatpickr({
    allowInput: true,
    altInput: true,
    altFormat: "d/m/Y",
    dateFormat: "Y-m-d",
    "locale": "pt",
});
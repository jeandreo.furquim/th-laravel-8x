<!DOCTYPE html>
<html lang="pt-BR">
	<!--begin::Head-->
	<head>
        @section('title', 'Acessar meu condomínio')
        @include('layouts.head')
    </head>
    <body id="kt_body" class="bg-body" style="background: url('{{ asset('assets/images/bd-login.jpg') }}');">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Authentication - Sign-in -->
			<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
				<!--begin::Content-->
				<div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
					<!--begin::Wrapper-->
					<div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                        <div class="w-100 d-flex align-items-center justify-content-center">
                            <img alt="Logo" src="{{ asset('assets/images/logo_th.png') }}" class="h-70px" />
                        </div>
                        @if(session('message'))
                        <h5 class="modal-title text-center mt-8">Atenção: <span class="fw-bold fs-6" style="color: red;">{{ session('message')}}</span></h5>
                        @endif
						<!--begin::Form-->
						<form class="form w-100"  method="POST" action="{{ route('login') }}">
                            @csrf
                            <!--begin::Heading-->
                            <div class="text-center mb-10">
                                <!--end::Link-->
                                <!-- Session Status -->
                                <x-auth-session-status class="mb-4" :status="session('status')" />

                                <!-- Validation Errors -->
                                <x-auth-validation-errors class="mb-4" :errors="$errors" />
                            </div>
                            <!--begin::Heading-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-10">
                                <!--begin::Label-->
                                <label class="form-label fs-6 fw-bolder text-dark">E-mail</label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input class="form-control form-control-lg form-control-solid" type="email" name="email" value="{{ old('email') }}" required autofocus autocomplete="off" />
                                <!--end::Input-->
                            </div>
                            <!--end::Input group-->
                            <!--begin::Input group-->
                            <div class="fv-row mb-3">
                                <!--begin::Wrapper-->
                                <div class="d-flex flex-stack mb-2">
                                    <!--begin::Label-->
                                    <label class="form-label fw-bolder text-dark fs-6 mb-0">Senha</label>
                                    <!--end::Label-->
                                    <!--begin::Link-->
                                    <a href="#" class="link-primary fs-6 fw-bolder">Esqueceu a senha?</a>
                                    {{-- {{ route('password.request') }} --}}
                                    <!--end::Link-->
                                </div>
                                <!--end::Wrapper-->
                                <!--begin::Input-->
                                @if (Route::has('password.request'))
                                    <!--begin::Main wrapper-->
                                    <div class="fv-row" data-kt-password-meter="true">
                                        <!--begin::Wrapper-->
                                        <div class="mb-1">
                                            <!--begin::Input wrapper-->
                                            <div class="position-relative mb-3">
                                                <input class="form-control form-control-lg form-control-solid" type="password" name="password" autocomplete="off" required/>
                                                <!--begin::Visibility toggle-->
                                                <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                                                    <i class="bi bi-eye-slash fs-2"></i>
                                                    <i class="bi bi-eye fs-2 d-none"></i>
                                                </span>
                                                <!--end::Visibility toggle-->
                                            </div>
                                            <!--end::Input wrapper-->
                                        </div>
                                    </div>
                                    <!--end::Main wrapper-->
                                @endif
                                
                                <!--end::Input-->
                                <label class="form-check form-switch form-check-solid mt-6 mb-6">
                                    <input class="form-check-input" type="checkbox" name="remenber" style="margin-top:-5px;">
                                    <span class="form-check-label fw-bold text-muted">{{ __('Mantenha-me conectado') }}</span>
                                </label>
                            </div>
                            <!--end::Input group-->
                            <!--begin::Actions-->
                            <div class="text-center">
                                <!--begin::Submit button-->
                                <button type="submit" class="btn btn-lg btn-primary w-100 mb-5">
                                    <span class="indicator-label">{{ __('Acessar seu condomínio') }}</span>
                                </button>
                                <!--end::Submit button-->
                            </div>
                            <!--end::Actions-->
                            <span class="form-check-label fw-bold text-muted">Primeiro acesso? <a href="{{ route('register') }}">Clique aqui</a>.</span>
                        </form>
						<!--end::Form-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Content-->
			</div>
			<!--end::Authentication - Sign-in-->
		</div>
        <!--end::Main-->
        <script>var hostUrl = "assets/";</script>
        <!--begin::Javascript-->
        <!--begin::Global Javascript Bundle(used by all pages)-->
        <script src={{asset('assets/plugins/global/plugins.bundle.js')}}></script>
        <script src={{asset('assets/js/scripts.bundle.js')}}></script>
        <!--end::Global Javascript Bundle-->
        <!--begin::Page Custom Javascript(used by this page)-->
        <script src={{asset('assets/js/custom/authentication/sign-in/general.js')}}></script>
        <!--end::Page Custom Javascript-->
        <!--end::Javascript-->
    </body>
    <!--end::Body-->
</html>

<!DOCTYPE html>
<html lang="pt-BR">
	<!--begin::Head-->
	<head>
        @section('title', 'Acessar meu condomínio')
        @include('layouts.head')
    </head>
    <body id="kt_body" class="bg-body" style="background: url('{{ asset('assets/images/bd-login.jpg') }}');">
		<!--begin::Main-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Authentication - Sign-in -->
			<div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed">
				<!--begin::Content-->
				<div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
					<!--begin::Wrapper-->
					<div class="w-lg-600px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                        <div class="w-100 d-flex align-items-center justify-content-center">
                            <img alt="Logo" src="{{ asset('assets/images/logo_th.png') }}" class="h-70px" />
                        </div>
                        <h4 class="modal-title text-center mt-8">Atenção: <span class="fw-bold h4" style="color: red;">Seu cadastro não foi aprovado</span></h4>
                        @if(session('message'))
                        <h5 class="modal-title mt-3 text-justify">Motivo: <span class="fw-bold fs-6 text-muted">{{ session('message')}}</span></h5>
                        @endif
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Content-->
			</div>
			<!--end::Authentication - Sign-in-->
		</div>
        <!--end::Main-->
        <script>var hostUrl = "assets/";</script>
        <!--begin::Javascript-->
        <!--begin::Global Javascript Bundle(used by all pages)-->
        <script src={{asset('assets/plugins/global/plugins.bundle.js')}}></script>
        <script src={{asset('assets/js/scripts.bundle.js')}}></script>
        <!--end::Global Javascript Bundle-->
        <!--begin::Page Custom Javascript(used by this page)-->
        <script src={{asset('assets/js/custom/authentication/sign-in/general.js')}}></script>
        <!--end::Page Custom Javascript-->
        <!--end::Javascript-->
    </body>
    <!--end::Body-->
</html>

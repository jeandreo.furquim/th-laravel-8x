<!DOCTYPE html>
<html lang="pt-BR">
	<!--begin::Head-->
	<head>
        @section('title', 'Registrar-se')
		@include('layouts.head')
    </head>
        <body id="kt_body" class="bg-body" style="background: url('{{ asset('assets/images/bd-login.jpg') }}');">
            <!--begin::Main-->
            <div class="d-flex flex-column flex-root">
                <!--begin::Authentication - Sign-up -->
                <div class="d-flex flex-column flex-lg-row flex-column-fluid">
                    <!--begin::Body-->
                    <div class="d-flex flex-column flex-lg-row-fluid py-10" style="box-shadow: 0px 0px 30px #2e345424;">
                        <!--begin::Content-->
                        <div class="d-flex flex-center flex-column flex-column-fluid">
                            <!--begin::Wrapper-->
                            <div class="w-lg-900px p-10 p-lg-15 mx-auto bg-body rounded shadow-sm">
                                <!--begin::Form-->
                                <form class="form w-100" method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <!--begin::Heading-->
                                    <div class="mb-10 text-center">
                                        <!--begin::Title-->
                                        <h1 class="text-dark mb-3">Criar uma conta</h1>
                                        <!--end::Title-->
                                        <!--begin::Link-->
                                        <div class="text-gray-400 fw-bold fs-4">Ja possuí uma conta?
                                        <a href="{{ route('login') }}" class="link-primary fw-bolder">Acesse aqui</a></div>
                                        <!--end::Link-->

                                        @if(session('message'))
                                        <h5 class="modal-title text-center">Atenção: <span class="fw-bold fs-6" style="color: red;">{{ session('message')}}</span></h5>
                                        @endif
                                        <h5 class="modal-title text-center of-age-of-age" style="display: none;">Atenção: <span class="fw-bold fs-6" style="color: red;">Para receber a liberação de acesso á plataforma. Você deverá ter no mínimo 18 anos e/ou pleno gozo de sua capacidade civil.</span></h5>
                                        <x-auth-validation-errors class="mb-4" style="color: red" :errors="$errors" />
                                    </div>
                                    <!--end::Heading-->
                                    <!--begin::Input group-->
                                    <div class="row fv-row">
                                        <!--begin::Col-->
                                        <div class="col-md-4 mb-4">
                                            <label class="form-label fw-bolder text-dark fs-6">Nome Completo</label>
                                            <input class="form-control form-control-lg form-control-solid" type="text" name="name" placeholder="Nome e sobrenome" value="{{ old('name') }}" required autocomplete="off" />
                                        </div>
                                        <!--end::Col-->
                                        <!--begin::Col-->
                                        <div class="col-md-4 mb-4">
                                            <label class="form-label fw-bolder text-dark fs-6">Sexo</label>
                                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" name="sex" data-placeholder="Selecione uma opção" required>
                                                <option value="">Selecione uma opção...</option>
                                                <option value="masculino" @if ( old('sex') == 'masculino') selected @endif>Masculino</option>
                                                <option value="feminino" @if ( old('sex') == 'feminino') selected @endif>Feminino</option>
                                            </select> 
                                        </div>
                                        <!--end::Col-->
                                        
                                        <!--begin::Col-->
                                        <div class="col-md-4 mb-4">
                                            <label class="form-label fw-bolder text-dark fs-6">Data de nascimento</label>
                                            <span class="ms-1" type="button" data-bs-toggle="tooltip" title="Você deve ter no mínimo 18 anos.">
                                                <i class="fas fa-exclamation-circle fs-7" style="color: #a1a5b7;"></i>
                                            </span>
                                            <!--begin::Icon-->
                                            <!--begin::Input-->
                                            <div class="position-relative d-flex align-items-center">
                                                <!--begin::Icon-->
                                                <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                                <span class="svg-icon svg-icon-2 position-absolute mx-4">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="black" />
                                                        <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="black" />
                                                        <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="black" />
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->
                                                <!--end::Icon-->
                                                <!--begin::Datepicker-->
                                                <input class="form-control form-control-solid ps-12 flatpickr birth-date" required placeholder="Selecione uma data" name="birth" id="calendarInput"/>
                                                <!--end::Datepicker-->
                                            </div> 
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="row fv-row">
                                        <!--begin::Col-->
                                        <div class="col-md-4 mb-4">
                                            <label class="form-label fw-bolder text-dark fs-6">Email</label>
                                            <input class="form-control form-control-lg form-control-solid" type="email" name="email" placeholder="usuario@exemplo.com.br" value="{{ old('email') }}" required autocomplete="off" />
                                        </div>
                                        <!--end::Col-->
                                        <!--begin::Col-->
                                        <div class="col-md-4 mb-4">
                                            <label class="form-label fw-bolder text-dark fs-6">CPF (últimos 3 digitos)</label>
                                            <input class="form-control form-control-lg form-control-solid" type="text" id="identifier" name="identifier" placeholder="XXX" value="{{ old('identifier') }}" maxlength="3" required autocomplete="off" />
                                        </div>
                                        <!--end::Col-->
                                        <!--begin::Col-->
                                        <div class="col-md-4 mb-4">
                                            <label class="form-label fw-bolder text-dark fs-6">Celular</label>
                                            <input class="form-control form-control-lg form-control-solid" type="text" id="cellphone" name="cellphone" placeholder="(XX) XXXX-XXXX" value="{{ old('cellphone') }}" required autocomplete="off" />
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="row fv-row">
                                        <!--begin::Col-->
                                        <div class="col-md-6 mb-4">
                                            <label class="form-label fw-bolder text-dark fs-6">Selecione seu condomínio</label>
                                            <select class="form-select form-select-solid" id="condominium" data-control="select2" data-hide-search="true" name="condominium" data-placeholder="Selecione uma opção" required>
                                                <option value="">Selecione uma opção</option>
                                                @foreach ($contents as $content)
                                                <option value="{{ $content->id }}">{{ $content->name }}</option>
                                                @endforeach
                                            </select> 
                                        </div>
                                        <!--end::Col-->
                                         <!--begin::Col-->
                                         <div class="col-md-6 mb-4">
                                            <label class="form-label fw-bolder text-dark fs-6">Selecione sua subdivisão</label>
                                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" id="subdivisions" name="subdivision" data-placeholder="Selecione uma opção" required>
                                                <option value="">Selecione uma opção</option>
                                            </select> 
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="row fv-row">
                                        <!--begin::Col-->
                                        <div class="col-md-6 mb-4">
                                            <label class="form-label fw-bolder text-dark fs-6">Número da unidade</label>
                                            <input class="form-control form-control-lg form-control-solid" type="text" name="house" value="{{ old('house') }}" placeholder="Número da unidade" maxlength="10" required autocomplete="off" />
                                        </div>
                                        <!--end::Col-->
                                        <!--begin::Col-->
                                        <div class="col-md-6 mb-4">
                                            <label class="form-label fw-bolder text-dark fs-6">Qual a sua relação com o imóvel?</label>
                                            <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" name="owner" data-placeholder="Selecione uma opção" required>
                                                <option value="">Selecione uma opção...</option>
                                                <option value="Proprietário(a)" @if ( old('owner') == 'Proprietário(a)') selected @endif>Proprietário(a)</option>
                                                <option value="Morador(a)" @if ( old('owner') == 'Morador(a)') selected @endif>Morador(a)</option>
                                                <option value="Inquilino(a)" @if ( old('owner') == 'Inquilino(a)') selected @endif>Inquilino(a)</option>
                                                <option value="Locatário(a)" @if ( old('owner') == 'Locatário(a)') selected @endif>Locatário(a)</option>
                                                <option value="Representante/Procurador" @if ( old('owner') == 'Representante/Procurador') selected @endif>Representante/Procurador</option>
                                            </select> 
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Input group-->
                                    <div class="row fv-row">
                                        <!--begin::Col-->
                                        <div class="col-md-6 mb-4" data-kt-password-meter="true">
                                            <!--begin::Wrapper-->
                                            <div class="mb-1">
                                                <!--begin::Label-->
                                                <label class="form-label fw-bolder text-dark fs-6">Crie Sua Senha</label>
                                                <!--end::Label-->
                                                <!--begin::Input wrapper-->
                                                <div class="position-relative mb-3">
                                                    <input class="form-control form-control-lg form-control-solid passwords-verify password-input" type="password" name="password" autocomplete="new-password" required />
                                                    <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                                                        <i class="bi bi-eye-slash fs-2"></i>
                                                        <i class="bi bi-eye fs-2 d-none"></i>
                                                    </span>
                                                </div>
                                                <!--end::Input wrapper-->
                                                <!--begin::Meter-->
                                                <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                                                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                                    <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                                                </div>
                                                <!--end::Meter-->
                                            </div>
                                            <!--end::Wrapper-->
                                        </div>
                                        <!--end::Col-->
                                        <!--begin::Col-->
                                        <div class="col-md-6 mb-4" data-kt-password-meter="true">
                                            <!--begin::Wrapper-->
                                            <div class="mb-1">
                                                <!--begin::Label-->
                                                <label class="form-label fw-bolder text-dark fs-6">Confirme a Senha</label>
                                                <!--end::Label-->
                                                <!--begin::Input wrapper-->
                                                <div class="position-relative mb-3">
                                                    <input class="form-control form-control-lg form-control-solid passwords-verify password-input-verify" type="password" name="password_confirmation" autocomplete="off" required />
                                                    <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                                                        <i class="bi bi-eye-slash fs-2"></i>
                                                        <i class="bi bi-eye fs-2 d-none"></i>
                                                    </span>
                                                </div>
                                                <!--end::Input wrapper-->
                                            </div>
                                            <!--end::Wrapper-->
                                        </div>
                                        <!--end::Col-->
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Hint-->
                                    <p class="mb-3 mt-n2 fw-bolder alert-verify-password" style="display: none;">Atenção: <span class="fw-bold fs-6" style="color: red;">Suas senhas não coincidem.</span></h5>
                                    <div class="text-muted mb-6 mt-n2">Use 8 caracteres ou mais e varie entre letras, números &amp; símbolos.</div>
                                    <!--end::Hint-->
                                    <!--begin::Input group-->
                                    <div class="fv-row mb-10">
                                        <label class="form-check form-check-custom form-check-solid form-check-inline">
                                            <input class="form-check-input" type="checkbox" name="toc" value="1" required/>
                                            <span class="form-check-label fw-bold text-gray-800 fs-6">Li e aceito os <a href="https://thcondominios.com/termos-de-uso/" target="_blank" class="ms-1 link-primary">termos e condições</a> do Meu Condomínio.</span>
                                        </label>
                                    </div>
                                    <!--end::Input group-->
                                    <!--begin::Actions-->
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-lg btn-primary btn-register">
                                            <span class="indicator-label">{{ __('Cadastrar-se') }}</span>
                                        </button>
                                    </div>
                                    <!--end::Actions-->
                                </form>
                                <!--end::Form-->
                            </div>
                            <!--end::Wrapper-->
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Authentication - Sign-up-->
            </div>
            <!--end::Main-->
        <script>var hostUrl = "assets/";</script>
        <!--begin::Javascript-->
        <!--begin::Global Javascript Bundle(used by all pages)-->
        <script src={{asset('assets/plugins/global/plugins.bundle.js')}}></script>
        <script src={{asset('assets/js/scripts.bundle.js')}}></script>
        <!--end::Global Javascript Bundle-->
        <!--begin::Page Custom Javascript(used by this page)-->
        <script src={{asset('assets/js/custom/authentication/sign-in/general.js')}}></script>
        <!--end::Page Custom Javascript-->

        <!-- Calendar DataPicker -->
        <script src="https://npmcdn.com/flatpickr/dist/l10n/pt.js"></script>
        <script type="text/javascript">

            // Phone
            Inputmask({
                clearIncomplete: true,
                "mask" : ["(99) 9999-9999", "(99) 9 9999-9999"],
            }).mask("#cellphone");

            // Identifier
            Inputmask({
                clearIncomplete: true,
                "mask" : "999"
            }).mask("#identifier");
            
            // CALENDAR DATAPICKER
            $(".flatpickr").flatpickr({
                altInput: true,
                altFormat: "d/m/Y",
                dateFormat: "Y-m-d",
                locale: "pt",
                allowInput: true,
                disableMobile: true,
            });

            
            // VERIFY AGE OF RESIDENT
            $(".birth-date").change(function() {

                // GET VALUE OF INPUT
                var dateBirth = $(this).val();

                // MAKE MOMENT FROM DATE AND NOW
                var birthDate = moment(dateBirth);
                var currentDate = moment();
                var age = currentDate.diff(birthDate, 'years');

                console.log(birthDate, currentDate, age);

                // OF AGE OF AGE
                if (age >= 18) {
                    $('.of-age-of-age').hide();
                } else {
                    $('.of-age-of-age').show();
                }

            });

            // VERIFY PASSWORD AND CONFIRM PASSWORD
            $('.passwords-verify').keyup(function(){
                var password = $('.password-input').val();
                var confirmPassword = $('.password-input-verify').val()
                
                // IF EQUALS
                if(password === confirmPassword){
                    $('.alert-verify-password').hide();
                    $('.btn-register').attr('disabled', false);
                } else {
                    $('.alert-verify-password').show();
                    $('.btn-register').attr('disabled', true);
                }

            });

            // LOAD SUBDIVISIONS
            $("#condominium").change(function(e) {
                var selected=$(this).val();
                var url = "{{ route('subdivisions.load', ['condominium' => ':condominium']) }}";
                url = url.replace(':condominium', selected);
                $.ajax({
                    url: url,
                    method: 'GET',
                    data: {
                        selected: selected,
                    },
                }).done(function(response){
                    $('#subdivisions').html(response);
                });
                
            });

        </script>
        <!--end::Javascript-->
    </body>
    <!--end::Body-->
</html>

@if(session('message'))
    <button type="button" class="d-none" id="btn-alert" data-bs-toggle="modal" data-bs-target="#modal_alerts"></button>
    <div class="modal fade" tabindex="-1" id="modal_alerts">
        <div class="modal-dialog h-100 d-flex align-items-center mx-auto m-0">
            <div class="modal-content">
                <div class="modal-header" style="border: none !important">
                    <h5 class="modal-title text-center">Alerta:</h5><span class="text-muted fw-bold fs-6">{!! session('message') !!}</span>
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-light ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </div>
                    <!--end::Close-->
                </div>
            </div>
        </div>
    </div>
@endif

@if(session('message-welcome'))
    <button type="button" class="d-none" id="btn-alert" data-bs-toggle="modal" data-bs-target="#modal_alerts"></button>
    <div class="modal fade" tabindex="-1" id="modal_alerts">
        <div class="modal-dialog h-100 d-flex align-items-center mx-auto m-0">
            <div class="modal-content">
                <div class="modal-header d-flex align-items-end justify-content-end pb-0" style="border: none !important; margin-bottom: -15px; z-index: 999">
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-light ms-2" data-bs-dismiss="modal" aria-label="Close">
                        <i class="fas fa-times"></i>
                    </div>
                    <!--end::Close-->
                </div>
                <div class="modal-body">
                    <h2 class="text-center mb-7 fs-1 text-uppercase" style="font-weight: 900 !important;">Seja muito bem-vindo(a) {{ Auth::user()->name }}!</h2>
                    <p class="text-justify text-muted fs-5" style="line-height: 1.8;">
                        Este ambiente on-line foi cuidadosamente desenvolvido pela TH Condomínios para ser uma plataforma de proximidade entre você e a síndica Thays Hoeffling na gestão do seu condomínio.
                        Todas as informações e necessidades do condomínio são centralizadas nesta única plataforma que poderá ser acessada a qualquer tempo, de qualquer lugar. Chamados e solicitações devem ser realizados prioritariamente aqui e, SEM EXCEÇÃO, serão respondidos em até 24h.
                        Por favor, em caso de urgência ou emergência, não hesite em me ligar. A TH Condomínios estará disponível 24h/dia no número: (41) 9 9676-0610.
                    </p>  
                    <p class="text-right fw-bolder fs-5">
                        Atenciosamente,<br>
                        Thays Hoeffling<br>
                        Síndica  
                    </p>                
                </div>
            </div>
        </div>
    </div>
@endif

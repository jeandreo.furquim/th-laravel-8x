@if($comments->count())
    @foreach ($comments as $comment)
    <div class="mb-13">
        <!--begin::Header-->
        <div class="d-flex align-items-center mb-3">
            <!--begin::User-->
            <div class="d-flex align-items-center flex-grow-1">
                <!--begin::Avatar-->
                <div class="symbol symbol-45px me-5">
                    <img src="{{ imageUser($comment->author()->first()->id) }}" alt="">
                </div>
                <!--end::Avatar-->
                <!--begin::Info-->
                <div class="d-flex flex-column">
                    <a href="#" class="text-gray-800 text-hover-primary fs-6 fw-bolder">{{ $comment->author()->first()->name }}</a>
                    <span class="text-gray-400 fw-bold">{{ $comment->created_at->format('d/m/Y H:i') }}</span>
                </div>
                <!--end::Info-->
            </div>
            <!--end::User-->
            <!--begin::Menu-->
            {{-- <div class="my-0">
                <button type="button" class="btn btn-sm btn-icon btn-color-primary btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                    <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000"></rect>
                                <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                            </g>
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </button>
                <!--begin::Menu 2-->
                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg-light-primary fw-bold w-200px" data-kt-menu="true">
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <div class="menu-content fs-6 text-dark fw-bolder px-3 py-4">Quick Actions</div>
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu separator-->
                    <div class="separator mb-3 opacity-75"></div>
                    <!--end::Menu separator-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">New Ticket</a>
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">New Customer</a>
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3" data-kt-menu-trigger="hover" data-kt-menu-placement="right-start">
                        <!--begin::Menu item-->
                        <a href="#" class="menu-link px-3">
                            <span class="menu-title">New Group</span>
                            <span class="menu-arrow"></span>
                        </a>
                        <!--end::Menu item-->
                        <!--begin::Menu sub-->
                        <div class="menu-sub menu-sub-dropdown w-175px py-4">
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3">Admin Group</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3">Staff Group</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="#" class="menu-link px-3">Member Group</a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu sub-->
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <a href="#" class="menu-link px-3">New Contact</a>
                    </div>
                    <!--end::Menu item-->
                    <!--begin::Menu separator-->
                    <div class="separator mt-3 opacity-75"></div>
                    <!--end::Menu separator-->
                    <!--begin::Menu item-->
                    <div class="menu-item px-3">
                        <div class="menu-content px-3 py-3">
                            <a class="btn btn-primary btn-sm px-4" href="#">Generate Reports</a>
                        </div>
                    </div>
                    <!--end::Menu item-->
                </div>
                <!--end::Menu 2-->
            </div> --}}
            <!--end::Menu-->
        </div>
        <!--end::Header-->
        <!--begin::Post-->
        <div class="mb-7">
            <!--begin::Text-->
            <div class="text-gray-800 mb-5">{{ $comment->comment }}</div>
            <!--end::Text-->
            <!--begin::Toolbar-->
            {{-- <div class="d-flex align-items-center mb-5">
                <a href="#" class="btn btn-sm btn-light btn-color-muted btn-active-light-success px-4 py-2 me-4">
                <!--begin::Svg Icon | path: icons/duotune/communication/com012.svg-->
                <span class="svg-icon svg-icon-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path opacity="0.3" d="M20 3H4C2.89543 3 2 3.89543 2 5V16C2 17.1046 2.89543 18 4 18H4.5C5.05228 18 5.5 18.4477 5.5 19V21.5052C5.5 22.1441 6.21212 22.5253 6.74376 22.1708L11.4885 19.0077C12.4741 18.3506 13.6321 18 14.8167 18H20C21.1046 18 22 17.1046 22 16V5C22 3.89543 21.1046 3 20 3Z" fill="black"></path>
                        <rect x="6" y="12" width="7" height="2" rx="1" fill="black"></rect>
                        <rect x="6" y="7" width="12" height="2" rx="1" fill="black"></rect>
                    </svg>
                </span>
                <!--end::Svg Icon-->12</a>
                <a href="#" class="btn btn-sm btn-light btn-color-muted btn-active-light-danger px-4 py-2">
                <!--begin::Svg Icon | path: icons/duotune/general/gen030.svg-->
                <span class="svg-icon svg-icon-2">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path d="M18.3721 4.65439C17.6415 4.23815 16.8052 4 15.9142 4C14.3444 4 12.9339 4.73924 12.003 5.89633C11.0657 4.73913 9.66 4 8.08626 4C7.19611 4 6.35789 4.23746 5.62804 4.65439C4.06148 5.54462 3 7.26056 3 9.24232C3 9.81001 3.08941 10.3491 3.25153 10.8593C4.12155 14.9013 9.69287 20 12.0034 20C14.2502 20 19.875 14.9013 20.7488 10.8593C20.9109 10.3491 21 9.81001 21 9.24232C21.0007 7.26056 19.9383 5.54462 18.3721 4.65439Z" fill="black"></path>
                    </svg>
                </span>
                <!--end::Svg Icon-->150</a>
            </div> --}}
            <!--end::Toolbar-->
        </div>
        <!--end::Post-->
        <!--begin::Replies-->
        {{-- <div class="mb-7 ps-10">
            <!--begin::Reply-->
            <div class="d-flex mb-5">
                <!--begin::Avatar-->
                <div class="symbol symbol-45px me-5">
                    <img src="{{ asset('assets/media/avatars/150-11.jpg') }}" alt="">
                </div>
                <!--end::Avatar-->
                <!--begin::Info-->
                <div class="d-flex flex-column flex-row-fluid">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center flex-wrap mb-1">
                        <a href="#" class="text-gray-800 text-hover-primary fw-bolder me-2">Pamela dias</a>
                        <span class="text-gray-400 fw-bold fs-7">1 dia</span>
                        <a href="#" class="ms-auto text-gray-400 text-hover-primary fw-bold fs-7">Responder</a>
                    </div>
                    <!--end::Info-->
                    <!--begin::Post-->
                    <span class="text-gray-800 fs-7 fw-normal pt-1">Long before you sit dow to put digital pen to paper you need to make sure you have to sit down and write.</span>
                    <!--end::Post-->
                </div>
                <!--end::Info-->
            </div>
            <!--end::Reply-->
            <!--begin::Reply-->
            <div class="d-flex">
                <!--begin::Avatar-->
                <div class="symbol symbol-45px me-5">
                    <img src="{{ asset('assets/media/avatars/150-8.jpg') }}" alt="">
                </div>
                <!--end::Avatar-->
                <!--begin::Info-->
                <div class="d-flex flex-column flex-row-fluid">
                    <!--begin::Info-->
                    <div class="d-flex align-items-center flex-wrap mb-1">
                        <a href="#" class="text-gray-800 text-hover-primary fw-bolder me-2">David Nogueira</a>
                        <span class="text-gray-400 fw-bold fs-7">2 dias</span>
                        <a href="#" class="ms-auto text-gray-400 text-hover-primary fw-bold fs-7">Responder</a>
                    </div>
                    <!--end::Info-->
                    <!--begin::Post-->
                    <span class="text-gray-800 fs-7 fw-normal pt-1">Outlines keep you honest. They stop you from indulging in poorly</span>
                    <!--end::Post-->
                </div>
                <!--end::Info-->
            </div>
            <!--end::Reply-->
        </div> --}}
        <!--end::Replies-->
    </div>
    @endforeach
@else 
<div class="w-100 d-block rounded align-items-center justify-content-center bg-light mb-5 p-10">
    <!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com003.svg-->
    <p class="text-center svg-icon svg-icon-muted svg-icon-3hx mb-2"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
    <path opacity="0.3" d="M2 4V16C2 16.6 2.4 17 3 17H13L16.6 20.6C17.1 21.1 18 20.8 18 20V17H21C21.6 17 22 16.6 22 16V4C22 3.4 21.6 3 21 3H3C2.4 3 2 3.4 2 4Z" fill="black"/>
    <path d="M18 9H6C5.4 9 5 8.6 5 8C5 7.4 5.4 7 6 7H18C18.6 7 19 7.4 19 8C19 8.6 18.6 9 18 9ZM16 12C16 11.4 15.6 11 15 11H6C5.4 11 5 11.4 5 12C5 12.6 5.4 13 6 13H15C15.6 13 16 12.6 16 12Z" fill="black"/>
    </svg></p>
    <!--end::Svg Icon-->
    <p class="text-center text-muted fw-bold fs-5 ml-3 mb-0">Seja o primeiro a deixar um comentário!</p>
</div>
@endif
<!--begin::Separator-->
<div class="separator mb-4"></div>
<!--end::Separator-->
<!--begin::Comment input-->
<form class="position-relative mb-6" action="" id="form-comments">
    @csrf
    <input type="text" class="form-control border-0 p-0 pe-10 resize-none min-h-25px" id="comment-input" data-kt-autosize="true" rows="1" placeholder="Responder.." style="overflow: hidden; overflow-wrap: break-word; height: 25px;" name="comment"></input>
    <div class="position-absolute top-0 end-0 me-n5">
        <button type="submit" class="btn p-1">
            <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen016.svg-->
            <span class="svg-icon svg-icon-3 mb-3 mr-4"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <path d="M15.43 8.56949L10.744 15.1395C10.6422 15.282 10.5804 15.4492 10.5651 15.6236C10.5498 15.7981 10.5815 15.9734 10.657 16.1315L13.194 21.4425C13.2737 21.6097 13.3991 21.751 13.5557 21.8499C13.7123 21.9488 13.8938 22.0014 14.079 22.0015H14.117C14.3087 21.9941 14.4941 21.9307 14.6502 21.8191C14.8062 21.7075 14.9261 21.5526 14.995 21.3735L21.933 3.33649C22.0011 3.15918 22.0164 2.96594 21.977 2.78013C21.9376 2.59432 21.8452 2.4239 21.711 2.28949L15.43 8.56949Z" fill="black"/>
            <path opacity="0.3" d="M20.664 2.06648L2.62602 9.00148C2.44768 9.07085 2.29348 9.19082 2.1824 9.34663C2.07131 9.50244 2.00818 9.68731 2.00074 9.87853C1.99331 10.0697 2.04189 10.259 2.14054 10.4229C2.23919 10.5869 2.38359 10.7185 2.55601 10.8015L7.86601 13.3365C8.02383 13.4126 8.19925 13.4448 8.37382 13.4297C8.54839 13.4145 8.71565 13.3526 8.85801 13.2505L15.43 8.56548L21.711 2.28448C21.5762 2.15096 21.4055 2.05932 21.2198 2.02064C21.034 1.98196 20.8409 1.99788 20.664 2.06648Z" fill="black"/>
            </svg></span>
            <!--end::Svg Icon-->
        </button>
    </div>
</form>
<!--end::Comment input-->
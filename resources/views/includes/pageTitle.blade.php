<!-- PAGE TITLE -->
<div class="container-xxl mb-7">
    <div class="container p-0">
        <div class="row">
            <div class="col-10">
                <h1>@yield('title-post')</h1>
            </div>
            <div class="col-2 mt-auto mb-auto text-right">
                <a href="@yield('link-back', route("feed"))"><span>Voltar</span></a>
            </div>
        </div>
    </div>
</div>
<!-- PAGE TITLE -->
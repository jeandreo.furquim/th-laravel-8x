<div class="footer py-4 d-flex flex-lg-column" id="kt_footer">
    <!--begin::Container-->
    <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
        <!--begin::Copyright-->
        <div class="text-dark order-2 order-md-1">
            <span class="text-muted fw-bold me-1">2023©</span>
            <a href="https://dreamake.com.br" target="_blank" class="text-gray-800 text-hover-primary">Dreamake Marketing Digital</a>
        </div>
        <!--end::Copyright-->
        <!--begin::Menu-->
        <ul class="menu menu-gray-600 menu-hover-primary fw-bold order-1">
            <li class="menu-item d-none">
                <a href="#" target="_blank" class="menu-link px-2">Perguntas frequentes</a>
            </li>
            <li class="menu-item d-none">
                <a href="#" target="_blank" class="menu-link px-2">Suporte</a>
            </li>
            <li class="menu-item d-none">
                <a href="#" target="_blank" class="menu-link px-2">Denunciar Atividade</a>
            </li>
            @if(!in_array(Auth::user()->role, ['administrador']))
            <li class="menu-item">
                <a href="{{ route('account.edit') }}#desativar" class="menu-link px-2">Solicitar desativação de conta</a>
            </li>
            @endif
        </ul>
        <!--end::Menu-->
    </div>
    <!--end::Container-->
</div>
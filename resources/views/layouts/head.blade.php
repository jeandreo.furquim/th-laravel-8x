<title>@yield('title') - TH Condomínios</title>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta property="og:locale" content="pt_BR" />
<meta property="og:type" content="website" />
<meta property="og:title" content="TH Condomínios" />
<meta property="og:url" content="https://thcondominios.com/" />
<meta property="og:site_name" content="TH condomínios" />
<link rel="canonical" href="https://thcondominios.com/" />
<link rel="shortcut icon" href="https://thcondominios.com/wp-content/uploads/2021/08/b4.png" />
<!--begin::Fonts-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
<!--end::Fonts-->
<!-- begin::Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!-- end::Bootstrap -->
<!--begin::Global Stylesheets Bundle(used by all pages)-->
<link href={{asset('assets/plugins/global/plugins.bundle.css')}} rel="stylesheet" type="text/css" />
<link href={{asset('assets/css/style.bundle.css')}} rel="stylesheet" type="text/css" />
<!--end::Global Stylesheets Bundle-->
<!--begin::Custom Stylesheets (Dreamake)-->
<link href={{asset('assets/css/custom.style.css')}} rel="stylesheet" type="text/css" />
<!--end::Custom Stylesheets-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/js/all.min.js" integrity="sha512-6PM0qYu5KExuNcKt5bURAoT6KCThUmHRewN3zUFNaoI6Di7XJPTMoT6K0nsagZKk2OB4L7E3q1uQKHNHd4stIQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
@yield('custom-head')

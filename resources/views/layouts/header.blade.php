<div id="kt_header" style="" class="header align-items-stretch">
    <!--begin::Container-->
    <div class="container-fluid d-flex align-items-stretch justify-content-between">
        <!--begin::Aside mobile toggle-->
        <div class="d-flex align-items-center d-lg-none ms-n3 me-1" title="Show aside menu">
            <div class="btn btn-icon btn-active-light-primary w-30px h-30px w-md-40px h-md-40px" id="kt_aside_mobile_toggle">
                <!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
                <span class="svg-icon svg-icon-2x mt-1">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
                        <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
                    </svg>
                </span>
                <!--end::Svg Icon-->
            </div>
        </div>
        <!--end::Aside mobile toggle-->
        <!--begin::Mobile logo-->
        <div class="d-flex align-items-center flex-grow-1 flex-lg-grow-0">
            <a href="@if((in_array(Auth::user()->role, ['administrador', 'portaria']))){{ route('admin.index') }}@else{{ url('') }}@endif">
                <svg xmlns="http://www.w3.org/2000/svg" class="logo-home logo-header-top logo-home-mob" xml:space="preserve" width="89.2636mm" height="21.3615mm" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 10280 2460" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003">
                <g id="Camada_x0020_1">
                <metadata id="CorelCorpID_0Corel-Layer"/>
                <path class="fil0" d="M4930 1406c-41,0 -79,-8 -116,-24 -36,-15 -67,-37 -94,-65 -27,-28 -48,-60 -63,-96 -15,-37 -22,-76 -21,-117 2,-76 29,-141 81,-196 52,-55 116,-86 191,-92 67,-5 128,10 184,47 3,2 3,5 1,8l-15 23c-2,3 -4,4 -8,2 -48,-31 -101,-45 -159,-40 -66,5 -122,32 -167,81 -46,49 -69,106 -70,172 0,35 6,69 19,101 13,31 31,59 55,83 24,24 51,43 82,56 32,14 65,21 100,21 51,0 97,-14 139,-41 3,-2 6,-2 8,2l16 23c2,3 2,6 -2,8 -49,32 -103,48 -161,48zm-1885 -1154c11,12 10,31 -2,43 -12,11 -31,10 -43,-2l-211 -232 -2187 0 -532 579 1661 1747 416 -439c11,-12 31,-13 43,-1 12,11 13,31 1,43 -153,161 -282,307 -438,463 -12,11 -31,11 -43,-1l-1701 -1789c-11,-11 -12,-30 -1,-42l558 -608c6,-7 14,-11 23,-11l2213 0 0 0c8,0 16,3 22,10l220 242zm1085 1633c0,8 -3,15 -10,22 -7,7 -14,10 -22,10 -9,0 -17,-3 -24,-9 -7,-6 -10,-14 -10,-23l0 -627 -937 0 0 627c0,21 -10,32 -31,32 -10,0 -17,-3 -23,-10 -6,-7 -8,-14 -8,-22l0 -1536c0,-8 3,-15 9,-21 6,-6 13,-9 22,-9 8,0 16,3 22,9 6,6 9,13 9,21l0 653 937 0 0 -653c0,-9 4,-17 11,-22 7,-5 15,-8 23,-8 8,0 15,3 22,9 7,6 10,13 10,21l0 1536zm-66 -689l0 -136 -933 0 -2 136 935 0zm-1313 -846c0,8 -3,16 -9,22 -6,6 -13,9 -21,9l-983 0c-8,0 -15,-3 -21,-9 -6,-6 -9,-13 -9,-22 0,-8 3,-16 9,-22 6,-6 13,-9 21,-9l983 0c8,0 15,3 21,9 6,6 9,13 9,22zm8 193c0,8 -3,15 -9,22 -6,7 -13,10 -21,10l-463 0 0 1313c0,8 -3,15 -10,21 -6,6 -14,9 -21,9 -8,0 -15,-3 -21,-8 -6,-5 -10,-13 -10,-22l0 -1313 -465 0c-8,0 -15,-3 -21,-10 -6,-7 -9,-14 -9,-22 0,-8 3,-16 8,-23 5,-7 13,-11 22,-11l991 0c9,0 17,4 22,11 5,7 8,15 8,23zm2681 313c-35,0 -68,7 -100,20 -31,13 -58,31 -81,55 -24,24 -42,51 -55,81 -13,32 -20,65 -20,100 0,35 7,68 20,100 13,31 31,58 55,81 24,24 51,42 81,55 32,13 65,20 100,20 35,0 68,-7 100,-20 31,-13 58,-31 81,-55 24,-24 42,-51 55,-81 13,-32 20,-65 20,-100 0,-35 -7,-68 -20,-100 -13,-31 -31,-58 -55,-81 -24,-24 -51,-42 -81,-55 -32,-13 -65,-20 -100,-20zm0 551c-40,0 -78,-8 -115,-23 -35,-15 -66,-36 -94,-63 -27,-28 -48,-59 -63,-94 -15,-37 -23,-75 -23,-115 0,-40 8,-78 23,-115 15,-35 36,-66 63,-94 28,-27 59,-48 94,-63 37,-16 75,-23 115,-23 40,0 78,8 115,23 35,15 66,36 94,63 27,27 48,59 63,94 16,37 23,75 23,115 0,40 -8,78 -23,115 -15,35 -36,66 -63,94 -27,27 -59,48 -94,63 -37,15 -75,23 -115,23zm814 -6c0,4 -2,6 -6,6l-3 0c-2,0 -4,-1 -4,-3l-362 -483 0 479c0,4 -2,6 -6,6l-27 0c-4,0 -6,-2 -6,-6l0 -577c0,-4 2,-6 6,-6l1 0c2,0 3,1 5,2l363 486 0 -482c0,-4 2,-6 6,-6l28 0c4,0 6,2 6,6l0 578zm193 -33l146 0c35,0 68,-7 100,-20 30,-13 58,-31 81,-55 24,-24 42,-51 55,-81 13,-32 20,-65 20,-100 0,-35 -7,-68 -20,-100 -13,-31 -31,-58 -55,-81 -24,-24 -51,-42 -81,-55 -32,-13 -65,-20 -100,-20l-146 0 0 512zm-33 39c-4,0 -6,-2 -6,-6l0 -579c0,-4 2,-6 6,-6l179 0c40,0 78,7 114,23 35,14 66,35 93,62 27,27 48,57 64,92 16,36 24,73 24,113 1,41 -7,80 -22,117 -15,36 -37,68 -65,96 -28,28 -60,49 -96,64 -37,16 -76,24 -117,24l-174 0zm876 -551c-35,0 -68,7 -100,20 -31,13 -58,31 -81,55 -24,24 -42,51 -55,81 -13,32 -20,65 -20,100 0,35 7,68 20,100 13,31 31,58 55,81 24,24 51,42 81,55 32,13 65,20 100,20 35,0 68,-7 100,-20 31,-13 58,-31 81,-55 24,-24 42,-51 55,-81 13,-32 20,-65 20,-100 0,-35 -7,-68 -20,-100 -13,-31 -31,-58 -55,-81 -24,-24 -51,-42 -81,-55 -32,-13 -65,-20 -100,-20zm0 551c-40,0 -78,-8 -115,-23 -35,-15 -66,-36 -94,-63 -27,-28 -48,-59 -63,-94 -15,-37 -23,-75 -23,-115 0,-40 8,-78 23,-115 15,-35 36,-66 63,-94 28,-27 59,-48 94,-63 37,-16 75,-23 115,-23 40,0 78,8 115,23 35,15 66,36 94,63 27,27 48,59 63,94 16,37 23,75 23,115 0,40 -8,78 -23,115 -15,35 -36,66 -63,94 -27,27 -59,48 -94,63 -37,15 -75,23 -115,23zm638 -336l-225 -251c-1,-1 -3,-2 -4,-2l-3 0c-4,0 -6,2 -6,6l0 578c0,4 2,6 6,6l28 0c4,0 5,-2 5,-6l0 -494 195 217c3,3 6,3 9,0l195 -218 0 494c0,4 2,6 5,6l28 0c4,0 6,-2 6,-6l0 -578c0,-4 -2,-6 -6,-6l-3 0c-2,0 -3,1 -4,2l-225 251zm396 336c-4,0 -6,-2 -6,-6l0 -578c0,-4 2,-6 6,-6l28 0c4,0 6,2 6,6l0 578c0,4 -2,6 -6,6l-28 0zm-68 -638l168 -25c4,-1 5,-3 5,-7l-4 -27c-1,-4 -3,-5 -7,-5l-168 25c-4,1 -5,3 -5,7l4 27c1,4 3,5 7,5zm682 633c0,4 -2,6 -6,6l-3 0c-2,0 -4,-1 -4,-3l-362 -483 0 479c0,4 -2,6 -6,6l-27 0c-4,0 -6,-2 -6,-6l0 -577c0,-4 2,-6 6,-6l1 0c2,0 3,1 4,2l363 486 0 -482c0,-4 2,-6 6,-6l28 0c4,0 6,2 6,6l0 578zm160 6c-4,0 -6,-2 -6,-6l0 -578c0,-4 2,-6 6,-6l28 0c4,0 6,2 6,6l0 578c0,4 -2,6 -6,6l-28 0zm448 -551c-35,0 -68,7 -100,20 -31,13 -58,31 -81,55 -24,24 -42,51 -55,81 -13,32 -20,65 -20,100 0,35 7,68 20,100 13,31 31,58 55,81 24,24 51,42 81,55 32,13 65,20 100,20 35,0 68,-7 100,-20 31,-13 58,-31 81,-55 24,-24 42,-51 55,-81 13,-32 20,-65 20,-100 0,-35 -7,-68 -20,-100 -13,-31 -31,-58 -55,-81 -24,-24 -51,-42 -81,-55 -32,-13 -65,-20 -100,-20zm0 551c-40,0 -78,-8 -115,-23 -35,-15 -66,-36 -94,-63 -27,-28 -48,-59 -63,-94 -15,-37 -23,-75 -23,-115 0,-40 8,-78 23,-115 15,-35 36,-66 63,-94 28,-27 59,-48 94,-63 37,-16 75,-23 115,-23 40,0 78,8 115,23 35,15 66,36 94,63 27,27 48,59 63,94 16,37 23,75 23,115 0,40 -8,78 -23,115 -15,35 -36,66 -63,94 -27,27 -59,48 -94,63 -37,15 -75,23 -115,23zm699 -505c-3,3 -5,3 -7,0 -2,-2 -4,-3 -6,-5 -3,-2 -6,-4 -9,-6 -7,-5 -15,-10 -21,-14 -17,-9 -35,-16 -54,-20 -14,-3 -26,-4 -35,-5l-10 0 -10 0c-3,0 -7,0 -11,1l-5 0 -5 0c-16,2 -29,5 -40,10 -14,5 -26,13 -35,23 -10,10 -17,22 -21,37l-1 6c-1,2 -1,4 -1,6 -1,5 -1,9 -1,12l0 6 0 0c0,0 0,0 0,0l0 -1 0 1 0 2 0 3 0 7 1 5 0 3 1 3 2 5c0,1 1,3 2,5l1 2 1 3 3 5 3 5 2 3 2 2c1,2 3,4 4,5 10,11 24,22 41,32 13,7 30,16 51,25l7 3 7 3 13 6 27 12c22,10 40,19 55,29 20,13 37,27 49,41 15,18 25,38 29,60l2 8c1,3 1,6 1,8l0 8 0 4 0 1 0 0 0 1 0 2 0 7 0 3c0,2 0,3 0,4l-1 8 -1 8c-4,22 -13,41 -26,57 -12,16 -28,29 -46,38 -15,8 -32,14 -52,18l-6 1 -7 1c-5,1 -9,2 -12,2l-12 1 -12 0 -6 0 -3 0 -2 0 -2 0 -11 0c-3,0 -7,0 -11,-1l-6 0 -6 -1c-13,-1 -26,-4 -41,-8 -22,-6 -42,-15 -62,-26 -7,-4 -14,-9 -21,-13 -5,-4 -10,-7 -15,-11 -3,-2 -6,-5 -8,-7 -3,-3 -3,-5 0,-8l20 -19c3,-2 5,-2 8,0 2,1 3,3 5,4 4,3 8,6 12,9 6,4 12,8 18,12 17,10 35,17 53,22 13,4 24,6 35,7l5 0 4 0c4,1 7,1 10,1l11 0 1 0 1 0 3 0 5 0 10 0 11 -1c3,0 7,-1 11,-1l5 -1 6 -1c16,-4 30,-8 42,-14 14,-7 25,-16 34,-28 9,-12 16,-26 19,-42l1 -5 1 -6c0,-1 0,-2 0,-3l0 -3 0 -7 0 -2 0 -1 0 0c0,0 0,1 0,1l0 0 0 -3 0 -6c0,-2 0,-4 0,-6l-1 -3 0 -3c-3,-16 -10,-30 -21,-43 -17,-20 -48,-40 -91,-60l-28 -12 -13 -7 -6 -3 -7 -3c-22,-10 -41,-19 -55,-27 -21,-12 -37,-26 -50,-40 -2,-2 -4,-4 -5,-6l-2 -3 -3 -3c-4,-6 -7,-11 -9,-14l-2 -4 -2 -4c-1,-3 -2,-6 -3,-8l-3 -8 -1 -5 -1 -4 -2 -9 0 -7 0 -3 0 -2 0 -1 0 -1c0,0 0,0 0,0l0 0 0 -8c0,-4 1,-9 2,-16 0,-2 1,-5 1,-7l2 -7c5,-21 15,-39 30,-54 13,-13 29,-23 48,-31 14,-5 30,-9 49,-12l6 0 6 0c4,-1 8,-1 11,-1l11 0 11 0c12,0 25,2 41,5 22,5 43,13 63,23 8,4 15,8 21,13 5,3 10,7 15,11 2,2 4,4 8,7 3,3 3,5 0,8l-17 17z"/>
                </g>
                </svg>
            </a>
        </div>
        <!--end::Mobile logo-->
        <!--begin::Wrapper-->
        <div class="d-flex align-items-stretch justify-content-between flex-lg-grow-1">
            <!--begin::Navbar-->
            <div class="d-flex align-items-stretch" id="kt_header_nav">
                <!--begin::Menu wrapper-->
                <div class="header-menu align-items-stretch" data-kt-drawer="true" data-kt-drawer-name="header-menu" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="end" data-kt-drawer-toggle="#kt_header_menu_mobile_toggle" data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_body', lg: '#kt_header_nav'}">
                    <!--begin::Menu-->
                    <div class="menu menu-lg-rounded menu-column menu-lg-row menu-state-bg menu-title-gray-700 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-400 fw-bold my-5 my-lg-0 align-items-stretch" id="#kt_header_menu" data-kt-menu="true" title="Menu Principal">
                        <div class="py-2">
                            <a href="@if((in_array(Auth::user()->role, ['administrador', 'portaria']))){{ route('admin.index') }}@else{{ url('') }}@endif">
                                <svg xmlns="http://www.w3.org/2000/svg" class="logo-home logo-header-top" xml:space="preserve" width="89.2636mm" height="21.3615mm" version="1.1" style="shape-rendering:geometricPrecision; text-rendering:geometricPrecision; image-rendering:optimizeQuality; fill-rule:evenodd; clip-rule:evenodd" viewBox="0 0 10280 2460" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xodm="http://www.corel.com/coreldraw/odm/2003">
                                <g id="Camada_x0020_1">
                                <metadata id="CorelCorpID_0Corel-Layer"/>
                                <path class="fil0" d="M4930 1406c-41,0 -79,-8 -116,-24 -36,-15 -67,-37 -94,-65 -27,-28 -48,-60 -63,-96 -15,-37 -22,-76 -21,-117 2,-76 29,-141 81,-196 52,-55 116,-86 191,-92 67,-5 128,10 184,47 3,2 3,5 1,8l-15 23c-2,3 -4,4 -8,2 -48,-31 -101,-45 -159,-40 -66,5 -122,32 -167,81 -46,49 -69,106 -70,172 0,35 6,69 19,101 13,31 31,59 55,83 24,24 51,43 82,56 32,14 65,21 100,21 51,0 97,-14 139,-41 3,-2 6,-2 8,2l16 23c2,3 2,6 -2,8 -49,32 -103,48 -161,48zm-1885 -1154c11,12 10,31 -2,43 -12,11 -31,10 -43,-2l-211 -232 -2187 0 -532 579 1661 1747 416 -439c11,-12 31,-13 43,-1 12,11 13,31 1,43 -153,161 -282,307 -438,463 -12,11 -31,11 -43,-1l-1701 -1789c-11,-11 -12,-30 -1,-42l558 -608c6,-7 14,-11 23,-11l2213 0 0 0c8,0 16,3 22,10l220 242zm1085 1633c0,8 -3,15 -10,22 -7,7 -14,10 -22,10 -9,0 -17,-3 -24,-9 -7,-6 -10,-14 -10,-23l0 -627 -937 0 0 627c0,21 -10,32 -31,32 -10,0 -17,-3 -23,-10 -6,-7 -8,-14 -8,-22l0 -1536c0,-8 3,-15 9,-21 6,-6 13,-9 22,-9 8,0 16,3 22,9 6,6 9,13 9,21l0 653 937 0 0 -653c0,-9 4,-17 11,-22 7,-5 15,-8 23,-8 8,0 15,3 22,9 7,6 10,13 10,21l0 1536zm-66 -689l0 -136 -933 0 -2 136 935 0zm-1313 -846c0,8 -3,16 -9,22 -6,6 -13,9 -21,9l-983 0c-8,0 -15,-3 -21,-9 -6,-6 -9,-13 -9,-22 0,-8 3,-16 9,-22 6,-6 13,-9 21,-9l983 0c8,0 15,3 21,9 6,6 9,13 9,22zm8 193c0,8 -3,15 -9,22 -6,7 -13,10 -21,10l-463 0 0 1313c0,8 -3,15 -10,21 -6,6 -14,9 -21,9 -8,0 -15,-3 -21,-8 -6,-5 -10,-13 -10,-22l0 -1313 -465 0c-8,0 -15,-3 -21,-10 -6,-7 -9,-14 -9,-22 0,-8 3,-16 8,-23 5,-7 13,-11 22,-11l991 0c9,0 17,4 22,11 5,7 8,15 8,23zm2681 313c-35,0 -68,7 -100,20 -31,13 -58,31 -81,55 -24,24 -42,51 -55,81 -13,32 -20,65 -20,100 0,35 7,68 20,100 13,31 31,58 55,81 24,24 51,42 81,55 32,13 65,20 100,20 35,0 68,-7 100,-20 31,-13 58,-31 81,-55 24,-24 42,-51 55,-81 13,-32 20,-65 20,-100 0,-35 -7,-68 -20,-100 -13,-31 -31,-58 -55,-81 -24,-24 -51,-42 -81,-55 -32,-13 -65,-20 -100,-20zm0 551c-40,0 -78,-8 -115,-23 -35,-15 -66,-36 -94,-63 -27,-28 -48,-59 -63,-94 -15,-37 -23,-75 -23,-115 0,-40 8,-78 23,-115 15,-35 36,-66 63,-94 28,-27 59,-48 94,-63 37,-16 75,-23 115,-23 40,0 78,8 115,23 35,15 66,36 94,63 27,27 48,59 63,94 16,37 23,75 23,115 0,40 -8,78 -23,115 -15,35 -36,66 -63,94 -27,27 -59,48 -94,63 -37,15 -75,23 -115,23zm814 -6c0,4 -2,6 -6,6l-3 0c-2,0 -4,-1 -4,-3l-362 -483 0 479c0,4 -2,6 -6,6l-27 0c-4,0 -6,-2 -6,-6l0 -577c0,-4 2,-6 6,-6l1 0c2,0 3,1 5,2l363 486 0 -482c0,-4 2,-6 6,-6l28 0c4,0 6,2 6,6l0 578zm193 -33l146 0c35,0 68,-7 100,-20 30,-13 58,-31 81,-55 24,-24 42,-51 55,-81 13,-32 20,-65 20,-100 0,-35 -7,-68 -20,-100 -13,-31 -31,-58 -55,-81 -24,-24 -51,-42 -81,-55 -32,-13 -65,-20 -100,-20l-146 0 0 512zm-33 39c-4,0 -6,-2 -6,-6l0 -579c0,-4 2,-6 6,-6l179 0c40,0 78,7 114,23 35,14 66,35 93,62 27,27 48,57 64,92 16,36 24,73 24,113 1,41 -7,80 -22,117 -15,36 -37,68 -65,96 -28,28 -60,49 -96,64 -37,16 -76,24 -117,24l-174 0zm876 -551c-35,0 -68,7 -100,20 -31,13 -58,31 -81,55 -24,24 -42,51 -55,81 -13,32 -20,65 -20,100 0,35 7,68 20,100 13,31 31,58 55,81 24,24 51,42 81,55 32,13 65,20 100,20 35,0 68,-7 100,-20 31,-13 58,-31 81,-55 24,-24 42,-51 55,-81 13,-32 20,-65 20,-100 0,-35 -7,-68 -20,-100 -13,-31 -31,-58 -55,-81 -24,-24 -51,-42 -81,-55 -32,-13 -65,-20 -100,-20zm0 551c-40,0 -78,-8 -115,-23 -35,-15 -66,-36 -94,-63 -27,-28 -48,-59 -63,-94 -15,-37 -23,-75 -23,-115 0,-40 8,-78 23,-115 15,-35 36,-66 63,-94 28,-27 59,-48 94,-63 37,-16 75,-23 115,-23 40,0 78,8 115,23 35,15 66,36 94,63 27,27 48,59 63,94 16,37 23,75 23,115 0,40 -8,78 -23,115 -15,35 -36,66 -63,94 -27,27 -59,48 -94,63 -37,15 -75,23 -115,23zm638 -336l-225 -251c-1,-1 -3,-2 -4,-2l-3 0c-4,0 -6,2 -6,6l0 578c0,4 2,6 6,6l28 0c4,0 5,-2 5,-6l0 -494 195 217c3,3 6,3 9,0l195 -218 0 494c0,4 2,6 5,6l28 0c4,0 6,-2 6,-6l0 -578c0,-4 -2,-6 -6,-6l-3 0c-2,0 -3,1 -4,2l-225 251zm396 336c-4,0 -6,-2 -6,-6l0 -578c0,-4 2,-6 6,-6l28 0c4,0 6,2 6,6l0 578c0,4 -2,6 -6,6l-28 0zm-68 -638l168 -25c4,-1 5,-3 5,-7l-4 -27c-1,-4 -3,-5 -7,-5l-168 25c-4,1 -5,3 -5,7l4 27c1,4 3,5 7,5zm682 633c0,4 -2,6 -6,6l-3 0c-2,0 -4,-1 -4,-3l-362 -483 0 479c0,4 -2,6 -6,6l-27 0c-4,0 -6,-2 -6,-6l0 -577c0,-4 2,-6 6,-6l1 0c2,0 3,1 4,2l363 486 0 -482c0,-4 2,-6 6,-6l28 0c4,0 6,2 6,6l0 578zm160 6c-4,0 -6,-2 -6,-6l0 -578c0,-4 2,-6 6,-6l28 0c4,0 6,2 6,6l0 578c0,4 -2,6 -6,6l-28 0zm448 -551c-35,0 -68,7 -100,20 -31,13 -58,31 -81,55 -24,24 -42,51 -55,81 -13,32 -20,65 -20,100 0,35 7,68 20,100 13,31 31,58 55,81 24,24 51,42 81,55 32,13 65,20 100,20 35,0 68,-7 100,-20 31,-13 58,-31 81,-55 24,-24 42,-51 55,-81 13,-32 20,-65 20,-100 0,-35 -7,-68 -20,-100 -13,-31 -31,-58 -55,-81 -24,-24 -51,-42 -81,-55 -32,-13 -65,-20 -100,-20zm0 551c-40,0 -78,-8 -115,-23 -35,-15 -66,-36 -94,-63 -27,-28 -48,-59 -63,-94 -15,-37 -23,-75 -23,-115 0,-40 8,-78 23,-115 15,-35 36,-66 63,-94 28,-27 59,-48 94,-63 37,-16 75,-23 115,-23 40,0 78,8 115,23 35,15 66,36 94,63 27,27 48,59 63,94 16,37 23,75 23,115 0,40 -8,78 -23,115 -15,35 -36,66 -63,94 -27,27 -59,48 -94,63 -37,15 -75,23 -115,23zm699 -505c-3,3 -5,3 -7,0 -2,-2 -4,-3 -6,-5 -3,-2 -6,-4 -9,-6 -7,-5 -15,-10 -21,-14 -17,-9 -35,-16 -54,-20 -14,-3 -26,-4 -35,-5l-10 0 -10 0c-3,0 -7,0 -11,1l-5 0 -5 0c-16,2 -29,5 -40,10 -14,5 -26,13 -35,23 -10,10 -17,22 -21,37l-1 6c-1,2 -1,4 -1,6 -1,5 -1,9 -1,12l0 6 0 0c0,0 0,0 0,0l0 -1 0 1 0 2 0 3 0 7 1 5 0 3 1 3 2 5c0,1 1,3 2,5l1 2 1 3 3 5 3 5 2 3 2 2c1,2 3,4 4,5 10,11 24,22 41,32 13,7 30,16 51,25l7 3 7 3 13 6 27 12c22,10 40,19 55,29 20,13 37,27 49,41 15,18 25,38 29,60l2 8c1,3 1,6 1,8l0 8 0 4 0 1 0 0 0 1 0 2 0 7 0 3c0,2 0,3 0,4l-1 8 -1 8c-4,22 -13,41 -26,57 -12,16 -28,29 -46,38 -15,8 -32,14 -52,18l-6 1 -7 1c-5,1 -9,2 -12,2l-12 1 -12 0 -6 0 -3 0 -2 0 -2 0 -11 0c-3,0 -7,0 -11,-1l-6 0 -6 -1c-13,-1 -26,-4 -41,-8 -22,-6 -42,-15 -62,-26 -7,-4 -14,-9 -21,-13 -5,-4 -10,-7 -15,-11 -3,-2 -6,-5 -8,-7 -3,-3 -3,-5 0,-8l20 -19c3,-2 5,-2 8,0 2,1 3,3 5,4 4,3 8,6 12,9 6,4 12,8 18,12 17,10 35,17 53,22 13,4 24,6 35,7l5 0 4 0c4,1 7,1 10,1l11 0 1 0 1 0 3 0 5 0 10 0 11 -1c3,0 7,-1 11,-1l5 -1 6 -1c16,-4 30,-8 42,-14 14,-7 25,-16 34,-28 9,-12 16,-26 19,-42l1 -5 1 -6c0,-1 0,-2 0,-3l0 -3 0 -7 0 -2 0 -1 0 0c0,0 0,1 0,1l0 0 0 -3 0 -6c0,-2 0,-4 0,-6l-1 -3 0 -3c-3,-16 -10,-30 -21,-43 -17,-20 -48,-40 -91,-60l-28 -12 -13 -7 -6 -3 -7 -3c-22,-10 -41,-19 -55,-27 -21,-12 -37,-26 -50,-40 -2,-2 -4,-4 -5,-6l-2 -3 -3 -3c-4,-6 -7,-11 -9,-14l-2 -4 -2 -4c-1,-3 -2,-6 -3,-8l-3 -8 -1 -5 -1 -4 -2 -9 0 -7 0 -3 0 -2 0 -1 0 -1c0,0 0,0 0,0l0 0 0 -8c0,-4 1,-9 2,-16 0,-2 1,-5 1,-7l2 -7c5,-21 15,-39 30,-54 13,-13 29,-23 48,-31 14,-5 30,-9 49,-12l6 0 6 0c4,-1 8,-1 11,-1l11 0 11 0c12,0 25,2 41,5 22,5 43,13 63,23 8,4 15,8 21,13 5,3 10,7 15,11 2,2 4,4 8,7 3,3 3,5 0,8l-17 17z"/>
                                </g>
                                </svg>
                            </a>
                        </div>
                        <div data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start" class="menu-item menu-lg-down-accordion me-lg-1">
                            <span class="menu-link p-0 btn-menus">
                                <div class="btn btn-icon w-30px h-30px w-md-40px h-md-40px show menu-dropdown" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen025.svg-->
                                    <span class="svg-icon svg-icon-1">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect x="2" y="2" width="9" height="9" rx="2" fill="black"></rect>
                                            <rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="black"></rect>
                                            <rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="black"></rect>
                                            <rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="black"></rect>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </div>
                                <span class="menu-arrow d-lg-none"></span>
                            </span>
                            <div class="menu-sub menu-sub-lg-down-accordion menu-sub-lg-dropdown w-100 mega-menu-lg p-7 p-lg-7">
                                <!--begin:Row-->
                                <div class="row" data-kt-menu-dismiss="true">
                                    <!--begin:Col-->
                                    <div class="col border-left-lg-1">
                                        <div class="menu-inline menu-column menu-active-bg">
                                            <div class="menu-item">
                                                <a href="{{ route('relases.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title title-menu-dropdown">Comunicados</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('relases.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Comunicados</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('messages.index') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Mensagens Privadas</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('documents.index') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Documentos</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('surveys.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Enquetes</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('events.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Eventos</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end:Col-->
                                    <!--begin:Col-->
                                    <div class="col border-left-lg-1">
                                        <div class="menu-inline menu-column menu-active-bg">
                                            <div class="menu-item">
                                                <a href="{{ route('occurrences.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title title-menu-dropdown">Chamados</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('occurrences.results', 'Manutenção') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Manutenção</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('occurrences.results', 'Sugestão') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Sugestão</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('occurrences.results', 'Solicitação') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Solicitação</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('occurrences.results', 'Reclamação') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Reclamações</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('occurrences.results', 'Dúvida') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Dúvidas</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('occurrences.results', 'Elogio') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Elogios</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('occurrences.results', 'Outros') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Outros</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end:Col-->
                                     <!--begin:Col-->
                                     <div class="col border-left-lg-1">
                                        <div class="menu-inline menu-column menu-active-bg">
                                            <div class="menu-item">
                                                <a href="{{ route('topics.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title title-menu-dropdown">Mural</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('topics.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Todos</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('topics.results', 'Eu recomendo') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Eu recomendo</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('topics.results', 'Eu Preciso de') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Eu preciso de...</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('topics.results', 'Carona') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Carona</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('topics.results', 'Achados e perdidos') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Achados e perdidos</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('topics.results', 'Vagas de garagem') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Vagas de garagem</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end:Col-->
                                     <!--begin:Col-->
                                     <div class="col border-left-lg-1">
                                        <div class="menu-inline menu-column menu-active-bg">
                                            <div class="menu-item">
                                                <a href="{{ route('admin.condominiums.show', Auth::user()->condominium) }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title title-menu-dropdown">Condomínio</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('employeescond.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Funcionários</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('providers.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Fornecedores</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('albums.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Álbum de fotos</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('branches.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Telefones e ramais</span>
                                                </a>
                                            </div>
                                            @if(!in_array(Auth::user()->role, ['administrador', 'portaria']))
                                            <div class="menu-item">
                                                <a href="{{ route('condominium') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Ficha técnica</span>
                                                </a>
                                            </div>
                                            @else
                                            <div class="menu-item">
                                                @if(session('sessionCondominium'))
                                                <a href="{{ route('admin.condominiums.show', session('sessionCondominium')) }}" class="menu-link">
                                                @else
                                                <a href="{{ route('admin.condominiums.index') }}" class="menu-link">
                                                @endif
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Ficha técnica</span>
                                                </a>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <!--end:Col-->
                                     <!--begin:Col-->
                                     <div class="col border-left-lg-1">
                                        <div class="menu-inline menu-column menu-active-bg">
                                            <div class="menu-item">
                                                <a href="{{ route('reservations.index') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title title-menu-dropdown">Reservas</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('rooms.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Espaços</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('reservations.index') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Minhas reservas</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('messages.index') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title title-menu-dropdown">Portaria</span>
                                                </a>
                                            </div>
                                            <div class="menu-item">
                                                <a href="{{ route('packages.results') }}" class="menu-link">
                                                    <span class="menu-bullet">
                                                        <span class="bullet bullet-dot"></span>
                                                    </span>
                                                    <span class="menu-title">Minhas encomendas</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!--end:Col-->
                                </div>
                                <!--end:Row-->
                            </div>
                        </div>
                    </div>
                    <!--end::Menu-->
                </div>
                <!--end::Menu wrapper-->
            </div>
            <!--end::Navbar-->
            <div class="h-100 d-flex justify-content-center align-items-center condominiums-header d-none d-md-flex">
                @if(Auth::user()->role == 'administrador')
                    @if(session('sessionCondominium'))
                        <a href="{{ route('admin.index') }}">
                            <span class="menu-title text-danger text-uppercase fw-bolder fs-6">{{ 'VENDO ' . session('sessionName') }}</span>
                        </a>
                    @else
                        <a href="{{ route('admin.condominiums.index') }}">
                            <span class="menu-title text-primary text-uppercase fw-bolder fs-6">TODOS CONDOMÍNIOS</span>
                        </a>
                    @endif
                @endif
            </div>
            <!--begin::Topbar-->
            <div class="d-flex align-items-stretch flex-shrink-0">
                <!--begin::Toolbar wrapper-->
                <div class="d-flex align-items-stretch flex-shrink-0">
                    <!--begin::Chat-->
                    <div class="d-flex align-items-center ms-1 ms-lg-3">
                        <!--begin::Menu wrapper-->
                        <a href="{{ route('feed') }}" class="btn btn-icon position-relative w-30px h-30px w-md-40px h-md-40px btn-menus" title="Início">
                            <!--begin::Svg Icon | path: icons/duotune/communication/com012.svg-->
                            <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M11 2.375L2 9.575V20.575C2 21.175 2.4 21.575 3 21.575H9C9.6 21.575 10 21.175 10 20.575V14.575C10 13.975 10.4 13.575 11 13.575H13C13.6 13.575 14 13.975 14 14.575V20.575C14 21.175 14.4 21.575 15 21.575H21C21.6 21.575 22 21.175 22 20.575V9.575L13 2.375C12.4 1.875 11.6 1.875 11 2.375Z" fill="black"/>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </a>
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::Chat-->
                    <!--begin::Chat-->
                    <div class="d-flex align-items-center ms-1 ms-lg-3">
                        <!--begin::Menu wrapper-->
                        <a href="{{ route('messages.index') }}" class="btn btn-icon position-relative w-30px h-30px w-md-40px h-md-40px btn-menus" title="Chat">
                            <!--begin::Svg Icon | path: icons/duotune/communication/com012.svg-->
                            <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M20 3H4C2.89543 3 2 3.89543 2 5V16C2 17.1046 2.89543 18 4 18H4.5C5.05228 18 5.5 18.4477 5.5 19V21.5052C5.5 22.1441 6.21212 22.5253 6.74376 22.1708L11.4885 19.0077C12.4741 18.3506 13.6321 18 14.8167 18H20C21.1046 18 22 17.1046 22 16V5C22 3.89543 21.1046 3 20 3Z" fill="black" />
                                    <rect x="6" y="12" width="7" height="2" rx="1" fill="black" />
                                    <rect x="6" y="7" width="12" height="2" rx="1" fill="black" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                            {{-- <span class="bullet bullet-dot bg-success h-6px w-6px position-absolute translate-middle top-0 start-50 animation-blink"></span> --}}
                        </a>
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::Chat-->
                    <!--begin::Notifications-->
                    <div class="d-flex align-items-center ms-1 ms-lg-3">
                        <!--begin::Menu- wrapper-->
                        <div class="btn btn-icon position-relative w-30px h-30px w-md-40px h-md-40px btn-menus" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end" title="Atualizações">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen022.svg-->
                            <span class="svg-icon svg-icon-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M11.2929 2.70711C11.6834 2.31658 12.3166 2.31658 12.7071 2.70711L15.2929 5.29289C15.6834 5.68342 15.6834 6.31658 15.2929 6.70711L12.7071 9.29289C12.3166 9.68342 11.6834 9.68342 11.2929 9.29289L8.70711 6.70711C8.31658 6.31658 8.31658 5.68342 8.70711 5.29289L11.2929 2.70711Z" fill="black" />
                                    <path d="M11.2929 14.7071C11.6834 14.3166 12.3166 14.3166 12.7071 14.7071L15.2929 17.2929C15.6834 17.6834 15.6834 18.3166 15.2929 18.7071L12.7071 21.2929C12.3166 21.6834 11.6834 21.6834 11.2929 21.2929L8.70711 18.7071C8.31658 18.3166 8.31658 17.6834 8.70711 17.2929L11.2929 14.7071Z" fill="black" />
                                    <path opacity="0.3" d="M5.29289 8.70711C5.68342 8.31658 6.31658 8.31658 6.70711 8.70711L9.29289 11.2929C9.68342 11.6834 9.68342 12.3166 9.29289 12.7071L6.70711 15.2929C6.31658 15.6834 5.68342 15.6834 5.29289 15.2929L2.70711 12.7071C2.31658 12.3166 2.31658 11.6834 2.70711 11.2929L5.29289 8.70711Z" fill="black" />
                                    <path opacity="0.3" d="M17.2929 8.70711C17.6834 8.31658 18.3166 8.31658 18.7071 8.70711L21.2929 11.2929C21.6834 11.6834 21.6834 12.3166 21.2929 12.7071L18.7071 15.2929C18.3166 15.6834 17.6834 15.6834 17.2929 15.2929L14.7071 12.7071C14.3166 12.3166 14.3166 11.6834 14.7071 11.2929L17.2929 8.70711Z" fill="black" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </div>
                        <!--begin::Menu-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column w-350px w-lg-375px" data-kt-menu="true">
                            <!--begin::Heading-->
                            <div class="d-flex flex-column bgi-no-repeat rounded-top bg-dark">
                                <!--begin::Title-->
                                <h3 class="text-white fw-bold px-9 mt-10 mb-6">Notificações
                                {{-- <span class="fs-8 opacity-75 ps-3">24 atividades</span></h3> --}}
                                {{-- <!--end::Title-->
                                <!--begin::Tabs-->
                                <ul class="nav nav-line-tabs nav-line-tabs-2x nav-stretch fw-bold px-9">
                                    <li class="nav-item">
                                        <a class="nav-link text-white opacity-75 opacity-state-100 pb-4 active" data-bs-toggle="tab" href="#kt_topbar_notifications_2">Atualizações</a>
                                    </li>
                                </ul>
                                <!--end::Tabs--> --}}
                            </div>
                            <!--end::Heading-->
                            <!--begin::Tab content-->
                            <div class="tab-content">
                                <!--begin::Tab panel-->
                                <div class="tab-pane fade show active" id="kt_topbar_notifications_2" role="tabpanel">
                                    <!--begin::Items-->
                                    <div class="scroll-y mh-325px my-5 px-8">
                                        @if(updatesCondominiunsHeader(5)->count())
                                        @foreach (updatesCondominiunsHeader(5) as $update)
                                        <a href="{{urlUpdates($update->type, $update->content, $update->user)}}">
                                        <!--begin::Item-->
                                        <div class="d-flex flex-stack py-4">
                                            <!--begin::Section-->
                                            <div class="d-flex align-items-center">
                                                <!--begin::Symbol-->
                                                <div class="symbol symbol-35px me-4">
                                                    <div class="cursor-pointer symbol symbol-30px symbol-md-40px" title="{{ $update->author->name }}">
                                                        <img src="{{ imageUser($update->author->id) }}" alt="user">
                                                    </div>
                                                </div>
                                                <!--end::Symbol-->
                                                <!--begin::Title-->
                                                <div class="mb-0 me-2">
                                                    <span class="fs-6 text-gray-800 text-hover-primary fw-bolder">{!! mb_strimwidth($update->title, 0, 28, '...') !!}</span>
                                                    <div class="text-gray-400 fs-7">{!! msgUpdate($update->type, $update->action) !!}</div>
                                                </div>
                                                <!--end::Title-->
                                            </div>
                                            <!--end::Section-->
                                            <!--begin::Label-->
                                            <span class="badge badge-light fs-8">{{days($update->created_at)}}</span>
                                            <!--end::Label-->
                                        </div>
                                        <!--end::Item-->
                                        </a>
                                        @endforeach
                                        @else
                                        <div data-kt-search-element="empty" class="text-center">
                                            <!--begin::Icon-->
                                            <div class="pt-10 pb-5">
                                                <!--begin::Svg Icon | path: icons/duotune/files/fil024.svg-->
                                                <span class="svg-icon svg-icon-4x opacity-50">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path opacity="0.3" d="M14 2H6C4.89543 2 4 2.89543 4 4V20C4 21.1046 4.89543 22 6 22H18C19.1046 22 20 21.1046 20 20V8L14 2Z" fill="black"></path>
                                                        <path d="M20 8L14 2V6C14 7.10457 14.8954 8 16 8H20Z" fill="black"></path>
                                                        <rect x="13.6993" y="13.6656" width="4.42828" height="1.73089" rx="0.865447" transform="rotate(45 13.6993 13.6656)" fill="black"></rect>
                                                        <path d="M15 12C15 14.2 13.2 16 11 16C8.8 16 7 14.2 7 12C7 9.8 8.8 8 11 8C13.2 8 15 9.8 15 12ZM11 9.6C9.68 9.6 8.6 10.68 8.6 12C8.6 13.32 9.68 14.4 11 14.4C12.32 14.4 13.4 13.32 13.4 12C13.4 10.68 12.32 9.6 11 9.6Z" fill="black"></path>
                                                    </svg>
                                                </span>
                                                <!--end::Svg Icon-->
                                            </div>
                                            <!--end::Icon-->
                                            <!--begin::Message-->
                                            <div class="pb-15 fw-bold">
                                                <h3 class="text-gray-800 fs-5 mb-2">Nenhuma atualização</h3>
                                                <div class="text-muted fs-7">Verifique novamente em instantes.</div>
                                            </div>
                                            <!--end::Message-->
                                        </div>
                                        @endif
                                    </div>
                                    <!--end::Items-->
                                    <!--begin::View more-->
                                    <div class="py-3 text-center border-top">
                                        <a href="{{ route('updates') }}" class="btn btn-color-gray-600 btn-active-color-primary">Ver todas
                                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                        <span class="svg-icon svg-icon-5">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="black" />
                                                <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="black" />
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon--></a>
                                    </div>
                                    <!--end::View more-->
                                </div>
                                <!--end::Tab panel-->
                            </div>
                            <!--end::Tab content-->
                        </div>
                        <!--end::Menu-->
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::Notifications-->
                    <!--end::Quick links-->
                    <!--begin::User-->
                    <div class="d-flex align-items-center ms-1 ms-lg-5" id="kt_header_user_menu_toggle">
                        <!--begin::Menu wrapper-->
                        <div class="cursor-pointer symbol symbol-30px symbol-md-40px object-cover" data-kt-menu-trigger="click" data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
                            <img src="{{ imageUser(Auth::id()) }}" alt="user" class="rounded-circle" />
                        </div>
                        <!--begin::Menu--> 
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-275px" data-kt-menu="true">
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <div class="menu-content d-flex align-items-center px-3">
                                    <!--begin::Avatar-->
                                    <div class="symbol symbol-50px me-5 object-cover">
                                        <img alt="Logo" src="{{ imageUser(Auth::id()) }}" />
                                    </div>
                                    <!--end::Avatar-->
                                    <!--begin::Username-->
                                    <div class="d-flex flex-column">
                                        <div class="fw-bolder d-flex align-items-center fs-5">{{Str::limit(Auth::user()->name, 12)}}
                                        <span class="badge badge-light-success fw-bolder fs-8 px-2 py-1 ms-2">#{{ str_pad(Auth::id() , 3 , '0' , STR_PAD_LEFT) }}</span></div>
                                        <a href="#" class="fw-bold text-muted text-hover-primary fs-7">{{Str::limit(Auth::user()->email, 20)}}</a>
                                    </div>
                                    <!--end::Username-->
                                </div>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu separator-->
                            <div class="separator my-2"></div>
                            <!--end::Menu separator-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-5">
                                @if(!in_array(Auth::user()->role, ['administrador', 'portaria']))
                                <a href="{{ route('account.index') }}" class="menu-link px-5">Meu Perfil</a>
                                @else 
                                <a href="{{ route('admin.index') }}" class="menu-link px-5">Painel de administrador</a>
                                @endif
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-5 my-1">
                                <a href="{{ route('account.edit') }}" class="menu-link px-5">Editar meus dados</a>
                            </div>
                            <!--end::Menu item-->
                            @if(!in_array(Auth::user()->role, ['administrador', 'portaria']))
                            <!--begin::Menu item-->
                            <div class="menu-item px-5 my-1">
                                <a href="{{ route('account.approval') }}" class="menu-link px-5">Publicações em aprovação</a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-5 my-1">
                                <a href="{{ route('condominium') }}" class="menu-link px-5">Meu condomínio</a>
                            </div>
                            <!--end::Menu item-->
                            @endif
                            <!--begin::Menu item-->
                            <div class="menu-item px-5 my-1">
                                <a href="{{ route('logout') }}" class="menu-link px-5">Sair</a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu-->
                        <!--end::Menu wrapper-->
                    </div>
                    <!--end::User -->
                </div>
                <!--end::Toolbar wrapper-->
            </div>
            <!--end::Topbar-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Container-->
</div>
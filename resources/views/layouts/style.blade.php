@if(customStyle())
<style>

    .btn-primary {
        color: #fff;
        border-color: {{ customStyle()->buttons }} !important;
        background-color: {{ customStyle()->buttons }} !important;
    }

    .btn-check:active+.btn.btn-primary, .btn-check:checked+.btn.btn-primary, .btn.btn-primary.active, .btn.btn-primary.show, .btn.btn-primary:active:not(.btn-active), .btn.btn-primary:focus:not(.btn-active), .btn.btn-primary:hover:not(.btn-active), .show>.btn.btn-primary {
        color: #fff;
        border-color: {{ customStyle()->buttons_hover }} !important;
        background-color: {{ customStyle()->buttons_hover }} !important;
    }

    .aside.aside-dark {
        background-color: {{ customStyle()->sidebar }} !important;
    }

    .aside.aside-dark .aside-logo {
        background-color: {{ customStyle()->sidebar_top }} !important;
    }
        
    .aside-dark .menu .menu-item .menu-link:hover:not(.disabled):not(.active), .aside-dark .menu .menu-item.hover>.menu-link:not(.disabled):not(.active) {
        background-color: {{ customStyle()->sidebar }} !important;
    }

    .aside-dark .menu .menu-item .menu-link .menu-title {
        color: {{ customStyle()->sidebar_text }} !important;
    }

    .aside-dark .menu .menu-item .menu-link .menu-icon .svg-icon svg [fill]:not(.permanent):not(g) {
        fill: {{ customStyle()->sidebar_icon }} !important;
    }

    .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
        color: {{ customStyle()->titles_color }} !important;
    }

    p {
        color: {{ customStyle()->p_color }} !important;
    }

    a {
        color: {{ customStyle()->link_color }} !important;
    }

    .text-muted {
        color: {{ customStyle()->muted_color }} !important;
    }

    .text-dark {
        color: {{ customStyle()->dark_color }} !important;
    }
    
    .aside-dark .menu .menu-item .menu-link.active .menu-icon .svg-icon svg [fill]:not(.permanent):not(g){
        fill: {{ customStyle()->sidebar_icon_active }} !important;
    }

</style>
@endif
@csrf      
<!--begin::Input group-->
<div class="d-flex flex-column mb-8 fv-row">
    <!--begin::Label-->
    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Nome do álbum</span>
    </label>
    <!--end::Label-->
    <input type="text" class="form-control form-control-solid" required placeholder="Ex.: Reforma do salão de festas, construção quadra, etc." name="name" value="{{ $content->name ?? old('name') }}" maxlength="255"  />
</div>
<div class="d-flex flex-column mb-8">
    <label class="fs-6 fw-bold mb-2">Descrição</label>
    <textarea class="form-control form-control-solid" rows="3" name="description" placeholder="Coloque o conteúdo aqui.">{{ $content->description ?? old('description') }}</textarea>
</div>
<div class="row mb-12">
    <div class="col">
        <!--begin::Label-->
        <div class="me-5 mb-2">
            <label class="fs-6 fw-bold">Capa do álbum</label>
        </div>
        <!--end::Label-->
        <!--begin::Switch-->
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input class="form-control form-control-solid" name="image" type="file" id="formFile" accept=".png, .jpg, .jpeg, .webp">
        </label>
        <!--end::Switch-->
    </div>
</div>
<!--end::Input group-->
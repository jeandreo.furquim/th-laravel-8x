@csrf
                
<div class="row mb-3">
    <!--begin::Input group-->
    <div class="col mb-3">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Nome</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" required placeholder="Ex.: Portaria!" name="name[]" value="{{ $content->name ?? old('name') }}" maxlength="255"  required/>
    </div>
    <div class="col mb-3">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Ramal</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" required placeholder="Ex.: 365" name="branch[]" value="{{ $content->branch ?? old('branch') }}" maxlength="255"  required/>
    </div>
</div>

<div class="row mb-3 hide-col">
    <!--begin::Input group-->
    <div class="col mb-3">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="">Nome</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" placeholder="Ex.: Portaria! (opcional)" name="name[]" value="{{ $content->name ?? old('name') }}" maxlength="255"  />
    </div>
    <div class="col mb-3">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="">Ramal</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" placeholder="Ex.: 365 (opcional)" name="branch[]" value="{{ $content->branch ?? old('branch') }}" maxlength="255"  />
    </div>
</div>

<div class="row mb-3 hide-col">
    <!--begin::Input group-->
    <div class="col mb-3">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="">Nome</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" placeholder="Ex.: Portaria! (opcional)" name="name[]" value="{{ $content->name ?? old('name') }}" maxlength="255"  />
    </div>
    <div class="col mb-3">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="">Ramal</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" placeholder="Ex.: 365 (opcional)" name="branch[]" value="{{ $content->branch ?? old('branch') }}" maxlength="255"  />
    </div>
</div>

<div class="row mb-3 hide-col">
    <!--begin::Input group-->
    <div class="col mb-3">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="">Nome</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" placeholder="Ex.: Portaria! (opcional)" name="name[]" value="{{ $content->name ?? old('name') }}" maxlength="255"  />
    </div>
    <div class="col mb-3">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="">Ramal</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" placeholder="Ex.: 365 (opcional)" name="branch[]" value="{{ $content->branch ?? old('branch') }}" maxlength="255"  />
    </div>
</div>

<div class="row mb-9 hide-col">
    <!--begin::Input group-->
    <div class="col mb-3">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="">Nome</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" placeholder="Ex.: Portaria! (opcional)" name="name[]" value="{{ $content->name ?? old('name') }}" maxlength="255"  />
    </div>
    <div class="col mb-3">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="">Ramal</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" placeholder="Ex.: 365 (opcional)" name="branch[]" value="{{ $content->branch ?? old('branch') }}" maxlength="255"  />
    </div>
</div>

@csrf

<!--begin::Input group-->
<div class="row fv-row mb-7">
    <!--begin::Col-->
    <div class="col-xl-3 mb-4">
        <label class="form-label fw-bolder text-dark fs-6 required">Nome da portaria</label>
        <input class="form-control form-control-lg form-control-solid" required type="text" name="name_concierge" placeholder="Portaria principal, Portaria entrada garagem, etc." value="@if(isset($content)){{ $content->portersInfos()->first()->name_concierge }}@else{{ old('name_concierge') }} @endif" required autocomplete="off" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-3 mb-4">
        <label class="form-label fw-bolder text-dark fs-6 required">Nome do porteiro</label>
        <input class="form-control form-control-lg form-control-solid" required type="text" name="name" placeholder="Rafael, Juca, Angela, Marcelo, etc." value="@if(isset($content)){{ $content->name }}@else{{ old('name') }} @endif" required autocomplete="off" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-3 mb-4">
        <label class="form-label fw-bolder text-dark fs-6 required">Telefone do porteiro</label>
        <input class="form-control form-control-lg form-control-solid" required type="text" name="cellphone" placeholder="(00) 0 0000-0000" value="@if(isset($content)){{ $content->cellphone }}@else{{ old('cellphone') }} @endif" required autocomplete="off" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-3 mb-4">
        <label class="form-label fw-bolder text-dark fs-6 required">Data de nascimento</label>
        <input class="form-control form-control-lg form-control-solid flatpickr" required type="text" name="birth" placeholder="Data de nascimento" value="@if(isset($content)){{ $content->birth }}@else{{ old('birth') }} @endif" required autocomplete="off" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-4 mb-4">
        <label class="form-label fw-bolder text-dark fs-6 required">Ramal</label>
        <input class="form-control form-control-lg form-control-solid" required type="text" name="branch" placeholder="0000000" value="@if(isset($content)){{ $content->portersInfos()->first()->branch }}@else{{ old('branch') }} @endif" autocomplete="off" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-4 mb-4">
        <label class="form-label fw-bolder text-dark fs-6 required">Inicio do turno</label>
        <input class="form-control form-control-lg form-control-solid hour-picker" required type="text" name="start" placeholder="00:00" value="@if(isset($content)){{ $content->portersInfos()->first()->start }}@else{{ old('start') }} @endif" autocomplete="off" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-4 mb-4">
        <label class="form-label fw-bolder text-dark fs-6 required">Final do turno</label>
        <input class="form-control form-control-lg form-control-solid hour-picker" required type="text" name="end" placeholder="00:00" value="@if(isset($content)){{ $content->portersInfos()->first()->end }}@else{{ old('end') }} @endif" autocomplete="off" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-4 mb-4">
        <label class="form-label fw-bolder text-dark fs-6 required">Empresa responsável</label>
        <input class="form-control form-control-lg form-control-solid" required type="text" name="company" placeholder="Alcatraz, Poliserviços, Portaria Ágil, etc." value="@if(isset($content)){{ $content->portersInfos()->first()->company }}@else{{ old('company') }} @endif" required autocomplete="off" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-4 mb-4">
        <label class="form-label fw-bolder text-dark fs-6 required">Supervisão</label>
        <input class="form-control form-control-lg form-control-solid" required type="text" name="manager" placeholder="Brenda, João, Angelo, etc." value="@if(isset($content)){{ $content->portersInfos()->first()->manager }}@else{{ old('manager') }} @endif" required autocomplete="off" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-4 mb-4">
        <label class="form-label fw-bolder text-dark fs-6 required">Telefone de contato</label>
        <input class="form-control form-control-lg form-control-solid phone-all" required type="company_phone" name="contact_phone" placeholder="(00) 0 0000-0000" value="@if(isset($content)){{ $content->portersInfos()->first()->contact_phone }}@else{{ old('contact_phone') }} @endif" required autocomplete="off" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-4 mb-4">
        <label class="form-label fw-bolder text-dark fs-6 required">Email de Acesso</label>
        <input class="form-control form-control-lg form-control-solid" required type="email" name="email" placeholder="email@email.com.br" value="@if(isset($content)){{ $content->email }}@else{{ old('email') }} @endif" required autocomplete="off" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-4 mb-4" data-kt-password-meter="true">
        <!--begin::Wrapper-->
        <div class="mb-1">
            <!--begin::Label-->
            <label class="form-label fw-bolder text-dark fs-6">Crie Sua Senha</label>
            <!--end::Label-->
            <!--begin::Input wrapper-->
            <div class="position-relative mb-3">
                <input class="form-control form-control-lg form-control-solid" type="password" name="password" autocomplete="new-password" @if(!isset($content)) required @endif />
                <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                    <i class="bi bi-eye-slash fs-2"></i>
                    <i class="bi bi-eye fs-2 d-none"></i>
                </span>
            </div>
            <!--end::Input wrapper-->
            <!--begin::Meter-->
            <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
            </div>
            <!--end::Meter-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-4 mb-4" data-kt-password-meter="true">
        <!--begin::Wrapper-->
        <div class="mb-1">
            <!--begin::Label-->
            <label class="form-label fw-bolder text-dark fs-6">Confirme a Senha</label>
            <!--end::Label-->
            <!--begin::Input wrapper-->
            <div class="position-relative mb-3">
                <input class="form-control form-control-lg form-control-solid" type="password" name="password_confirmation" autocomplete="off" @if(!isset($content)) required @endif />
                <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                    <i class="bi bi-eye-slash fs-2"></i>
                    <i class="bi bi-eye fs-2 d-none"></i>
                </span>
            </div>
            <!--end::Input wrapper-->
        </div>
        <!--end::Wrapper-->
    </div>
    <!--end::Col-->
</div>
<!--end::Input group-->
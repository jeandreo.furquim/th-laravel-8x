@csrf

<!--end::Input group-->
<div class="row g-9 mb-8">
    <!--begin::Col-->
    <div class="col-md-4 fv-row">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
            <span class="required">Nome do condomínio</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" required placeholder="Ex.: Condomínio Toronto" name="name" value="{{ $condominium->name ?? old('name') }}" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-md-4 fv-row">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
            <span class="required">Nome do síndico</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" required placeholder="Ex.: Thays Hoeffling" name="syndic" value="{{ $condominium->name ?? old('syndic') }}" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-md-4 fv-row">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
            <span class="required">Tipo de condomínio</span>
        </label>
        <!--end::Label-->
        <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" name="type" data-placeholder="Selecione uma opção" required>
            <option value="">Selecione uma opção...</option>
            <option value="Horizontal" @if (isset($condominium) && $condominium->type == 'Horizontal') selected @endif>Horizontal</option>
            <option value="Vertical" @if (isset($condominium) && $condominium->type == 'Vertical') selected @endif>Vertical</option>
            <option value="Comercial" @if (isset($condominium) && $condominium->type == 'Comercial') selected @endif>Comercial</option>
            <option value="Misto" @if (isset($condominium) && $condominium->type == 'Misto') selected @endif>Misto</option>
            <option value="associacao" @if (isset($condominium) && $condominium->type == 'associacao') selected @endif>Associação</option>
            <option value="Hotelaria" @if (isset($condominium) && $condominium->type == 'Hotelaria') selected @endif>Hotelaria e/ou curta temporada</option>
            <option value="Multipropriedade" @if (isset($condominium) && $condominium->type == 'Multipropriedade') selected @endif>Multipropriedade</option>
        </select> 
    </div>
    <!--end::Col-->
</div>

<!--end::Input group-->
<div class="row g-9 mb-8">
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
            <span class="required">CNPJ</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid input-cnpj" required placeholder="Ex.: 00.000.000/0000-00" minlength="18" name="cnpj" value="{{ $condominium->cnpj ?? old('cnpj') }}" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
            <span class="required">Bairro</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" required placeholder="Ex.: Santa Felicidade" name="zone" value="{{ $condominium->zone ?? old('zone') }}" />
    </div>
    <!--end::Col-->
</div>

<!--end::Input group-->
<div class="row g-9 mb-8">
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
            <span class="required">CEP</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid input-cep" required placeholder="Ex.: 00000-000" name="cep" value="{{ $condominium->cep ?? old('cep') }}" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
            <span class="required">Endereço</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" required placeholder="Ex.: Rua Manoel Ribas, 12345." name="address" value="{{ $condominium->address ?? old('address') }}" />
    </div>
    <!--end::Col-->
</div>


<!--end::Input group-->
<div class="row g-9 mb-8">
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
        <!--begin::Label-->
    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Cidade</span>
    </label>
    <!--end::Label-->
    <input type="text" class="form-control form-control-solid" required placeholder="Curitiba, São Paulo, Belo Horizonte." name="city" value="{{ $condominium->city ?? old('city') }}" />
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
        <label class="required fs-6 fw-bold mb-2">Construído em:</label>
        <!--begin::Input-->
        <div class="position-relative d-flex align-items-center">
            <!--begin::Icon-->
            <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
            <span class="svg-icon svg-icon-2 position-absolute mx-4">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="black"></path>
                    <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="black"></path>
                    <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="black"></path>
                </svg>
            </span>
            <!--end::Svg Icon-->
            <!--end::Icon-->
            <!--begin::Datepicker-->
            <input class="form-control form-control-solid ps-12 flatpickr-input flatpickr" required placeholder="Selecione uma data" name="build" value="{{ $condominium->build ?? old('build') }}" type="text" readonly="readonly">
            <!--end::Datepicker-->
        </div> 
        <!--end::Input-->
    </div>
    <!--end::Col-->
</div>
<!--begin::Input group-->

<!--end::Input group-->
<div class="row g-9 mb-8">
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
        <!--begin::Label-->
        <div class="me-5 mb-2">
            <label class="fs-6 fw-bold">Whatsapp 2ª via</label>
        </div>
        <!--end::Label-->
        <!--begin::Switch-->
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input type="text" class="form-control form-control-solid input-phone" placeholder="(00) 0 0000-0000" name="phone" id="phone" value="{{ $condominium->phone ?? old('phone') }}" />
        </label>
        <!--end::Switch-->    
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-md-6 fv-row">
        <!--begin::Label-->
        <div class="me-5 mb-2">
            <label class="fs-6 fw-bold">Imagem do condomínio</label>
        </div>
        <!--end::Label-->
        <!--begin::Switch-->
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input class="form-control form-control-solid" name="image" type="file" id="formFile" accept=".png, .jpg, .jpeg, .webp" >
        </label>
        <!--end::Switch-->
    </div>
    <!--end::Col-->
</div>
<!--begin::Input group-->

<!--begin::Accordion-->
<div class="accordion mb-7" id="kt_accordion_1">
    <div class="accordion-item">
        <h2 class="accordion-header" id="kt_accordion_1_header_1">
            <button class="accordion-button fs-4 fw-bold py-4 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_body_1" aria-expanded="false" aria-controls="kt_accordion_1_body_1">
                Jurídico
            </button>
        </h2>
        <div id="kt_accordion_1_body_1" class="accordion-collapse collapse" aria-labelledby="kt_accordion_1_header_1" data-bs-parent="#kt_accordion_1">
            <div class="accordion-body">
                <!--end::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-6 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Jurídico</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir nome advogado(a)" name="attorney_name" value="{{ $condominium->attorney_name ?? old('attorney_name') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-6 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Escritório ou Sindicato</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir escritório ou sindicado" name="attorney_office" value="{{ $condominium->attorney_office ?? old('attorney_office') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                </div>
                <!--begin::Input group-->

                <!--end::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Telefone</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid input-phone" placeholder="(00) 0 0000-0000" name="attorney_phone" value="{{ $condominium->attorney_phone ?? old('attorney_phone') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Telefone Fixo</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid input-phone" placeholder="(00) 0 0000-0000" name="attorney_phone_fixe" value="{{ $condominium->attorney_phone_fixe ?? old('attorney_phone_fixe') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Email</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="email" class="form-control form-control-solid" placeholder="Inserir email " name="attorney_email" value="{{ $condominium->attorney_email ?? old('attorney_email') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Endereço</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir endereço " name="attorney_address" value="{{ $condominium->attorney_address ?? old('attorney_address') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                </div>
                <!--begin::Input group-->
            </div>
        </div>
    </div>

    <div class="accordion-item">
        <h2 class="accordion-header" id="kt_accordion_1_header_2">
            <button class="accordion-button fs-4 fw-bold py-4 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_body_2" aria-expanded="false" aria-controls="kt_accordion_1_body_2">
                Administradora
            </button>
        </h2>
        <div id="kt_accordion_1_body_2" class="accordion-collapse collapse" aria-labelledby="kt_accordion_1_header_2" data-bs-parent="#kt_accordion_1">
            <div class="accordion-body">
                <!--end::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-6 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Administradora</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir empresa" name="administrator_company" value="{{ $condominium->administrator_company ?? old('administrator_company') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-6 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Endereço</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir endereço" name="administrator_address" value="{{ $condominium->administrator_address ?? old('administrator_address') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                </div>
                <!--begin::Input group-->

                <!--end::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-4 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Nome do contato 01</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir nome " name="administrator_name_01" value="{{ $condominium->administrator_name_01 ?? old('administrator_name_01') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Telefone 01</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid input-phone" placeholder="(00) 0 0000-0000" name="administrator_phone_01" value="{{ $condominium->administrator_phone_01 ?? old('administrator_phone_01') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Email 01</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="email" class="form-control form-control-solid" placeholder="Inserir email " name="administrator_email_01" value="{{ $condominium->administrator_email_01 ?? old('administrator_email_01') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                </div>
                <!--begin::Input group-->

                <!--end::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-4 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Nome do contato 02</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir nome" name="administrator_name_02" value="{{ $condominium->administrator_name_02 ?? old('administrator_name_02') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Telefone 02</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid input-phone" placeholder="(00) 0 0000-0000" name="administrator_phone_02" value="{{ $condominium->administrator_phone_02 ?? old('administrator_phone_02') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-4 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Email 02</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="email" class="form-control form-control-solid" placeholder="Inserir email " name="administrator_email_02" value="{{ $condominium->administrator_email_02 ?? old('administrator_email_02') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                </div>
                <!--begin::Input group-->
            </div>
        </div>
    </div>

    <div class="accordion-item">
        <h2 class="accordion-header" id="kt_accordion_1_header_3">
            <button class="accordion-button fs-4 fw-bold py-4 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_body_3" aria-expanded="false" aria-controls="kt_accordion_1_body_3">
                Seguradora
            </button>
        </h2>
        <div id="kt_accordion_1_body_3" class="accordion-collapse collapse" aria-labelledby="kt_accordion_1_header_3" data-bs-parent="#kt_accordion_1">
            <div class="accordion-body">
               <!--end::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Seguradora</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir empresa" name="insurance_company" value="{{ $condominium->insurance_company ?? old('insurance_company') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Nº de apólice</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir Nº" name="insurance_number" value="{{ $condominium->insurance_number ?? old('insurance_number') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Telefone</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid phone-all" placeholder="(00) 0 0000-0000" name="insurance_phone" value="{{ $condominium->insurance_phone ?? old('insurance_phone') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Email</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="email" class="form-control form-control-solid" placeholder="Inserir email" name="insurance_email" value="{{ $condominium->insurance_email ?? old('insurance_email') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                </div>
                <!--begin::Input group-->

                <!--end::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Contato</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Nome do corretor" name="insurance_agent_name" value="{{ $condominium->insurance_agent_name ?? old('insurance_agent_name') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Telefone</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid input-phone" placeholder="(00) 0 0000-0000" name="insurance_agent_phone" value="{{ $condominium->insurance_agent_phone ?? old('insurance_agent_phone') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Email</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="email" class="form-control form-control-solid" placeholder="Inserir email " name="insurance_agent_email" value="{{ $condominium->insurance_agent_email ?? old('insurance_agent_email') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Endereço</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir endereço" name="insurance_agent_address" value="{{ $condominium->insurance_agent_address ?? old('insurance_agent_address') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                </div>
                <!--begin::Input group-->
            </div>
        </div>
    </div>

    <div class="accordion-item">
        <h2 class="accordion-header" id="kt_accordion_1_header_4">
            <button class="accordion-button fs-4 fw-bold py-4 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_body_4" aria-expanded="false" aria-controls="kt_accordion_1_body_4">
                Engenharia
            </button>
        </h2>
        <div id="kt_accordion_1_body_4" class="accordion-collapse collapse" aria-labelledby="kt_accordion_1_header_3" data-bs-parent="#kt_accordion_1">
            <div class="accordion-body">
                <!--end::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-12 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Engenharia</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir empresa" name="engineering_company" value="{{ $condominium->engineering_company ?? old('engineering_company') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                </div>
                <!--begin::Input group-->

                <!--end::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Contato</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir nome" name="engineering_name" value="{{ $condominium->engineering_name ?? old('engineering_name') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Telefone</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid input-phone" placeholder="(00) 0 0000-0000" name="engineering_phone" value="{{ $condominium->engineering_phone ?? old('engineering_phone') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Email</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="email" class="form-control form-control-solid" placeholder="Inserir email " name="engineering_email" value="{{ $condominium->engineering_email ?? old('engineering_email') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Endereço</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir endereço" name="engineering_address" value="{{ $condominium->engineering_address ?? old('engineering_address') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                </div>
                <!--begin::Input group-->
            </div>
        </div>
    </div>
    <div class="accordion-item">
        <h2 class="accordion-header" id="kt_accordion_1_header_5">
            <button class="accordion-button fs-4 fw-bold py-4 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#kt_accordion_1_body_5" aria-expanded="false" aria-controls="kt_accordion_1_body_5">
                Construtora
            </button>
        </h2>
        <div id="kt_accordion_1_body_5" class="accordion-collapse collapse" aria-labelledby="kt_accordion_1_header_3" data-bs-parent="#kt_accordion_1">
            <div class="accordion-body">
                <!--end::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Construtora</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir empresa" name="construction_company" value="{{ $condominium->construction_company ?? old('construction_company') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">CNPJ Construtora</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid input-cnpj" placeholder="00.000.000/0000-00" name="construction_cnpj" value="{{ $condominium->construction_cnpj ?? old('construction_cnpj') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Incorporadora</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir empresa" name="construction_developer" value="{{ $condominium->construction_developer ?? old('construction_developer') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">CNPJ Incorporadora</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid input-cnpj" placeholder="00.000.000/0000-00" name="construction_developer_cnpj" value="{{ $condominium->construction_developer_cnpj ?? old('construction_developer_cnpj') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                </div>
                <!--begin::Input group-->

                <!--end::Input group-->
                <div class="row g-9 mb-8">
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Contato</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir nome" name="construction_name" value="{{ $condominium->construction_name ?? old('construction_name') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Telefone</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid input-phone" placeholder="(00) 0 0000-0000" name="construction_phone" value="{{ $condominium->construction_phone ?? old('construction_phone') }}" />
                        </label>
                        <!--end::Switch-->    
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Email</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="email" class="form-control form-control-solid" placeholder="Inserir email " name="construction_email" value="{{ $condominium->construction_email ?? old('construction_email') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-md-3 fv-row">
                        <!--begin::Label-->
                        <div class="me-5 mb-2">
                            <label class="fs-6 fw-bold">Endereço</label>
                        </div>
                        <!--end::Label-->
                        <!--begin::Switch-->
                        <label class="form-check form-switch form-check-custom form-check-solid">
                            <input type="text" class="form-control form-control-solid" placeholder="Inserir endereço" name="construction_address" value="{{ $condominium->construction_address ?? old('construction_address') }}" />
                        </label>
                        <!--end::Switch-->  
                    </div>
                    <!--end::Col-->
                </div>
                <!--begin::Input group-->
            </div>
        </div>
    </div>
</div>
<!--end::Accordion-->
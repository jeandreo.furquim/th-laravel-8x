@csrf
<div class="row mb-12">
    <!--begin::Input group-->
    <div class="col-3 mb-7">
        <img src="{{ asset('assets/images/orientacao.png') }}" class="rounded shadow w-100" alt="">
    </div>
    <!--end::Input group-->
    <!--begin::Input group-->
    <div class="col-9 mb-7 d-flex align-items-center">
        <div class="row px-12">
            <div class="col-md-4 mb-7">
                <!--begin::Label-->
                <label class="d-flex align-items-center fs-6 mb-2">
                    <span class="required">Fundo Topo</span>
                </label>
                <!--end::Label-->
                <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="sidebar_top" value="{{ $contents->sidebar_top ?? '#1a1a27' }}" maxlength="255"  />
            </div>
            <div class="col-md-4 mb-7">
                <!--begin::Label-->
                <label class="d-flex align-items-center fs-6 mb-2">
                    <span class="required">Fundo Barra</span>
                </label>
                <!--end::Label-->
                <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="sidebar" value="{{ $contents->sidebar ?? '#1e1e2d' }}" maxlength="255"  />
            </div>
            <div class="col-md-4 mb-7">
                <!--begin::Label-->
                <label class="d-flex align-items-center fs-6 mb-2">
                    <span class="required">Cor Textos</span>
                </label>
                <!--end::Label-->
                <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="sidebar_text" value="{{ $contents->sidebar_text ?? '#9899ac' }}" maxlength="255"  />
            </div>
            <div class="col-md-3 mb-7">
                <!--begin::Label-->
                <label class="d-flex align-items-center fs-6 mb-2">
                    <span class="required">Cor Ícones</span>
                </label>
                <!--end::Label-->
                <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="sidebar_icon" value="{{ $contents->sidebar_icon ?? '#b5bac5' }}" maxlength="255"  />
            </div>
            <div class="col-md-3 mb-7">
                <!--begin::Label-->
                <label class="d-flex align-items-center fs-6 mb-2">
                    <span class="required">Cor Ícones Ativo</span>
                </label>
                <!--end::Label-->
                <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="sidebar_icon_active" value="{{ $contents->sidebar_icon_active ?? '#009ef7' }}" maxlength="255"  />
            </div>
            <div class="col-md-3 mb-7">
                <!--begin::Label-->
                <label class="d-flex align-items-center fs-6 mb-2">
                    <span class="required">Cor Botões (Geral)</span>
                </label>
                <!--end::Label-->
                <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="buttons" value="{{ $contents->buttons ?? '#2e3454' }}" maxlength="255"  />
            </div>
            <div class="col-md-3 mb-7">
                <!--begin::Label-->
                <label class="d-flex align-items-center fs-6 mb-2">
                    <span class="required">Cor Botões (Geral) :hover</span>
                </label>
                <!--end::Label-->
                <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="buttons_hover" value="{{ $contents->buttons_hover ?? '#dba71c' }}" maxlength="255"  />
            </div>
        </div>
    </div>
    <!--end::Input group-->
    <div class="separator my-5"></div>
    <h1 class="mb-0 text-center mb-8">Títulos e Parágrafos</h1>,
    <div class="row">
        <div class="col-md-6 mb-7">
            <!--begin::Label-->
            <div class="d-flex align-items-center fs-6 mb-2">
                <h1 class="mb-0">Títulos - </h1>
                <div class="d-flex align-items-baseline">
                    <h1 class="me-2 mb-0">h1</h1>
                    <h2 class="me-2 mb-0">h2</h2>
                    <h3 class="me-2 mb-0">h3</h3>
                    <h4 class="me-2 mb-0">h4</h4>
                    <h5 class="me-2 mb-0">h5</h5>
                    <h6 class="me-2 mb-0">h6</h6>
                </div>
            </div>
            <!--end::Label-->
            <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="titles_color" value="{{ $contents->titles_color ?? '#181c32' }}" maxlength="255"  />
        </div>
        <div class="col-md-6 mb-7">
            <!--begin::Label-->
            <div class="d-flex align-items-center fs-6 mb-4">
                <p class="m-0">Parágrafo <\p></p>
            </div>
            <!--end::Label-->
            <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="p_color" value="{{ $contents->p_color ?? '#181c32' }}" maxlength="255"  />
        </div>
        <div class="col-md-4 mb-7">
            <!--begin::Label-->
            <div class="d-flex align-items-center fs-6 mb-4">
                <a href="#">Link <\a></a>
            </div>
            <!--end::Label-->
            <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="link_color" value="{{ $contents->link_color ?? '#181c32' }}" maxlength="255"  />
        </div>
        <div class="col-md-4 mb-7">
            <!--begin::Label-->
            <div class="d-flex align-items-center fs-6 mb-2">
                <p class="text-dark mb-0">.text-dark</span>
            </div>
            <!--end::Label-->
            <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="dark_color" value="{{ $contents->dark_color ?? '#181c32' }}" maxlength="255"  />
        </div>
        
        <div class="col-md-4 mb-7">
            <!--begin::Label-->
            <div class="d-flex align-items-center fs-6 mb-2">
                <p class="text-muted mb-0">.text-muted</span>
            </div>
            <!--end::Label-->
            <input type="color" class="form-control form-control-solid h-50px rounded" required placeholder="Ex.: #35153" name="muted_color" value="{{ $contents->muted_color ?? '#878a99' }}" maxlength="255"  />
        </div>
    </div>
    <div class="separator my-5"></div>
    <div class="col-12 mb-7">
        <!--begin::Label-->
        <div class="me-5 mb-2">
            <label class="fs-6 fw-bold">Fundo Cabeçalho Páginas</label>
        </div>
        <!--end::Label-->
        <!--begin::Switch-->
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input class="form-control form-control-solid" name="image" type="file" accept=".png, .jpg, .jpeg, .webp">
        </label>
        <!--end::Switch-->
    </div>
</div>
<!--end::Input group-->
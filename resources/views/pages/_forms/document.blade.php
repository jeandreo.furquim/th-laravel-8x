@csrf
             
<div class="row">
    <div class="col">
        <!--begin::Input group-->
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Nome</span>
            </label>
            <!--end::Label-->
            <input type="text" class="form-control form-control-solid" placeholder="Ex.: Novas normas, lista de parceiros, etc." name="name" value="{{ $content->name ?? old('name') }}" maxlength="255" required />
        </div>
        <!--end::Input group-->
    </div>
    <div class="col">
        <!--begin::Input group-->
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="">Link</span>
            </label>
            <!--end::Label-->
            <input type="text" class="form-control form-control-solid" placeholder="Ex.: Drive, onedrive, youtube, etc." name="url" value="{{ $content->link ?? old('link') }}" maxlength="255"  />
        </div>
        <!--end::Input group-->
    </div>
</div>

<!--end::Input group-->
<!--begin::Input group-->
<div class="d-flex flex-column mb-8">
    <label class="fs-6 fw-bold mb-2">Descrição</label>
    <textarea class="form-control form-control-solid" rows="3" name="description" placeholder="Coloque o conteúdo aqui.">{{ $content->description ?? old('description') }}</textarea>
</div>
<!--end::Input group-->
<!--begin::Input group-->
<div class="flex-stack mb-12">
    <!--begin::Label-->
    <div class="me-5 mb-2">
        <label class="fs-6 fw-bold">Anexe seu arquivo</label>
        <div class="fs-7 fw-bold text-muted">Selecione um arquivo para anexar.</div>
    </div>
    <!--end::Label-->
    <!--begin::Switch-->
    <label class="form-check form-switch form-check-custom form-check-solid">
        <input class="form-control form-control-solid" name="file" type="file" id="formFile" accept=".png, .jpg, .jpeg, .webp, .pdf, .doc, .docx, .xls, .xlsx">
    </label>
    <!--end::Switch-->
</div>
<!--end::Input group-->
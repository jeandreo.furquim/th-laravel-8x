@csrf
                
<!--begin::Input group-->
<div class="row mb-8">
    <!--begin::Input group-->
    <div class="col">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Nome</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" required placeholder="Inserir nome e sobrenome." name="name" value="{{ $content->name ?? old('name') }}" maxlength="255"  />
    </div>
    <div class="col">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Cargo</span>
        </label>
        <!--end::Label-->
        <input type="text" class="form-control form-control-solid" required placeholder="Ex.: faxineira, jardineiro, etc." name="office" value="{{ $content->office ?? old('office') }}" maxlength="255"  />
    </div>
    <div class="col">
        <!--begin::Label-->
        <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Contato</span>
        </label>
        <!--end::Label-->
        <input class="form-control form-control-lg form-control-solid maskphone" required type="text" name="phone" placeholder="(00) 0 0000-0000" value="{{ $content->phone ?? old('phone') }}" required="">
    </div>
    <!--end::Input group-->
</div>
<div class="row">
    <div class="col">
        <!--begin::Input group-->
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Horários de entrada</span>
            </label>
            <!--end::Label-->
            <div class="d-flex">
                <input type="time" class="form-control form-control-solid" required name="hour_start" value="{{ $content->hour_start ?? old('hour_start') }}"/>
            </div>
        </div>
        <!--end::Input group-->
    </div>
    <div class="col">
        <!--begin::Input group-->
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Horário de saída</span>
            </label>
            <!--end::Label-->
            <div class="d-flex">
                <input type="time" class="form-control form-control-solid" required name="hour_end" value="{{ $content->hour_end ?? old('hour_end') }}"/>
            </div>
        </div>
        <!--end::Input group-->
    </div>
</div>
<!--begin::Input group-->
<div class="d-flex flex-column mb-8">
    <label class="fs-6 fw-bold mb-2">Observação</label>
    <textarea class="form-control form-control-solid" rows="3" name="observation" placeholder="Coloque o conteúdo aqui.">{{ $content->observation ?? old('observation') }}</textarea>
</div>
<div class="row">
    <!--begin::Input group-->
    <div class="col">
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Dias liberados</span>
            </label>
            <!--end::Label-->
            <!--end::Label-->
            <div class="d-flex mt-3">
                <div class="form-check form-check-custom form-check-solid">
                    <input name="monday" id="monday" class="form-check-input" type="checkbox" @if(isset($content) && $content->monday == true || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="monday">
                        Segunda
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="tuesday" id="tuesday" class="form-check-input" type="checkbox" @if(isset($content) && $content->tuesday == true || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="tuesday">
                        Terça
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="wednesday" id="wednesday" class="form-check-input" type="checkbox" @if(isset($content) && $content->wednesday == true || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="wednesday">
                        Quarta
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="thursday" id="thursday" class="form-check-input" type="checkbox" @if(isset($content) && $content->thursday == true || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="thursday">
                        Quinta
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="friday" id="friday" class="form-check-input" type="checkbox" @if(isset($content) && $content->friday == true || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="friday">
                        Sexta
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="saturday" id="saturday" class="form-check-input" type="checkbox" @if(isset($content) && $content->saturday == true) checked="" @endif/>
                    <label class="form-check-label" for="saturday">
                        Sábado
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="sunday" id="sunday" class="form-check-input" type="checkbox" @if(isset($content) && $content->sunday == true) checked="" @endif/>
                    <label class="form-check-label" for="sunday">
                        Domingo
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mb-12">
    <div class="col">
        <!--begin::Label-->
        <div class="me-5 mb-2">
            <label class="fs-6 fw-bold">Foto do funcionário</label>
        </div>
        <!--end::Label-->
        <!--begin::Switch-->
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input class="form-control form-control-solid" name="image" type="file" id="formFile" accept=".png, .jpg, .jpeg, .webp, .pdf, .doc, .docx, .xls, .xlsx">
        </label>
        <!--end::Switch-->
    </div>
</div>
<!--end::Input group-->

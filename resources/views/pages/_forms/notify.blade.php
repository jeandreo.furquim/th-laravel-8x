@csrf
<!--end::Input group-->
<!--begin::Input group-->
<div class="d-flex flex-column mb-8">
    <label class="fs-6 fw-bold mb-2">Descreva o motivo do bloqueio:</label>
    <textarea class="form-control form-control-solid" rows="3" name="notify" placeholder="Coloque o conteúdo aqui." required>{{ $content->notify ?? old('notify') }}</textarea>
</div>
<!--end::Input group-->
@csrf
                
<!--begin::Input group-->
<div class="d-flex flex-column mb-8 fv-row">
    <!--begin::Label-->
    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Título</span>
    </label>
    <!--end::Label-->
    <input type="text" class="form-control form-control-solid" required placeholder="Ex.: Festa de fim de ano." name="title" value="{{ $content->title ?? old('title') }}" maxlength="255"  />
</div>
<!--end::Input group-->
<!--begin::Input group-->
<div class="d-flex flex-column mb-8">
    <label class="fs-6 fw-bold mb-2 required">Descrição</label>
    <textarea class="form-control form-control-solid" rows="3" name="description" placeholder="Coloque o conteúdo aqui." required>{{ $content->description ?? old('description') }}</textarea>
</div>
<div class="row">
    <!--begin::Input group-->
    <div class="col">
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Tipo de chamado</span>
            </label>
            <!--end::Label-->
            <div class="d-md-flex mt-3">
                <div class="form-check form-check-custom form-check-solid">
                    <input name="type" value="Dúvida" id="Dúvida" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Dúvida' || !isset($content)) checked @endif>
                    <label class="form-check-label" for="Dúvida">
                        Dúvida
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="Manutenção" id="Manutenção" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Manutenção') checked @endif>
                    <label class="form-check-label" for="Manutenção">
                        Manutenção
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="Sugestão" id="Sugestão" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Sugestão') checked @endif>
                    <label class="form-check-label" for="Sugestão">
                        Sugestão
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="Solicitação" id="Solicitação" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Solicitação') checked @endif>
                    <label class="form-check-label" for="Solicitação">
                        Solicitação
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="Reclamação" id="Reclamação" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Reclamação') checked @endif> 
                    <label class="form-check-label" for="Reclamação">
                        Reclamação
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="Elogio" id="Elogio" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Elogio') checked @endif> 
                    <label class="form-check-label" for="Elogio">
                        Elogio
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="Outros" id="Outros" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Outros') checked @endif> 
                    <label class="form-check-label" for="Outros">
                        Outros
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mb-12">
    <div class="col">
        <!--begin::Label-->
        <div class="me-5 mb-2">
            <label class="fs-6 fw-bold">Anexar imagem ao chamado</label>
        </div>
        <!--end::Label-->
        <!--begin::Switch-->
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input class="form-control form-control-solid" name="image" type="file" id="formFile" accept=".png, .jpg, .jpeg, .webp">
        </label>
        <!--end::Switch-->
    </div>
    <div class="col">
        <!--begin::Label-->
        <div class="me-5 mb-2">
            <label class="fs-6 fw-bold">Anexar arquivo ao chamado</label>
        </div>
        <!--end::Label-->
        <!--begin::Switch-->
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input class="form-control form-control-solid" name="document" type="file" id="formFile" accept=".png, .jpg, .jpeg, .webp, .pdf, .doc, .docx, .xls, .xlsx">
        </label>
        <!--end::Switch-->
    </div>
</div>
<!--end::Input group-->
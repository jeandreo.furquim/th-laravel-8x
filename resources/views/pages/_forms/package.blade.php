@csrf
                
<!--begin::Input group-->
<div class="d-flex flex-column mb-8 fv-row">
    <!--begin::Label-->
    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Título</span>
    </label>
    <!--end::Label-->
    <input type="text" class="form-control form-control-solid" required placeholder="Ex.: Entrega do Mercado Livre." name="title" value="{{ $content->title ?? old('title') }}" maxlength="255"  />
</div>
<!--end::Input group-->

<!--begin::Input group-->
<div class="d-flex flex-column mb-8 fv-row">
    <!--begin::Label-->
    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Remetente</span>
    </label>
    <!--end::Label-->
    <input type="text" class="form-control form-control-solid" required placeholder="Ex.: Entrega do Mercado Livre." name="sender" value="{{ $content->sender ?? old('sender') }}" maxlength="255"  />
</div>
<!--end::Input group-->

<!--end::Input group-->
<!--begin::Input group-->
<div class="d-flex flex-column mb-8">
    <label class="fs-6 fw-bold mb-2">Observações</label>
    <textarea class="form-control form-control-solid" rows="3" name="observation" placeholder="Coloque o conteúdo aqui.">{{ $content->observation ?? old('observation') }}</textarea>
</div>
<!--end::Input group-->
<!--begin::Input group-->
<div class="flex-stack mb-12">
    <!--begin::Label-->
    <div class="me-5 mb-2">
        <label class="fs-6 fw-bold">Imagem da encomenda</label>
        <div class="fs-7 fw-bold text-muted">Selecione um arquivo para anexar a este aviso.</div>
    </div>
    <!--end::Label-->
    <!--begin::Switch-->
    <label class="form-check form-switch form-check-custom form-check-solid">
        <input class="form-control form-control-solid" name="image" type="file" id="formFile" accept=".png, .jpg, .jpeg, .webp">
    </label>
    <!--end::Switch-->
</div>
<!--end::Input group-->
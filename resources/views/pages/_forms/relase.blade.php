@csrf
                
<!--begin::Input group-->
<div class="d-flex flex-column mb-8 fv-row">
    <!--begin::Label-->
    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Título</span>
    </label>
    <!--end::Label-->
    <input type="text" class="form-control form-control-solid" required placeholder="Escreva um título para que as pessoas identifiquem seu comunicado." name="title" value="{{ $content->title ?? old('title') }}" maxlength="255"  />
</div>
<!--end::Input group-->

<!--end::Input group-->
<!--begin::Input group-->
<div class="d-flex flex-column mb-8">
    <label class="fs-6 fw-bold mb-2 required">Descrição</label>
    <textarea class="form-control form-control-solid" rows="3" name="description" placeholder="Coloque o conteúdo aqui." required>{{ $content->description ?? old('description') }}</textarea>
</div>
<!--end::Input group-->
<!--begin::Input group-->
<div class="flex-stack mb-12">
    <!--begin::Label-->
    <div class="me-5 mb-2">
        <label class="fs-6 fw-bold">Anexar imagem ao comunicado</label>
    </div>
    <!--end::Label-->
    <!--begin::Switch-->
    <label class="form-check form-switch form-check-custom form-check-solid">
        <input class="form-control form-control-solid" name="image" type="file" id="formFile" accept=".png, .jpg, .jpeg, .webp">
    </label>
    <!--end::Switch-->
</div>
<!--end::Input group-->
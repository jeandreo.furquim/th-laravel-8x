@csrf
                
<!--begin::Input group-->
<div class="d-flex flex-column mb-8 fv-row">
    <!--begin::Label-->
    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Nome do espaço</span>
    </label>
    <!--end::Label-->
    <input type="text" class="form-control form-control-solid" required placeholder="Ex: Academia, Piscina, Lan House, Salão de festas, etc." name="name" value="{{ $content->name ?? old('name') }}" maxlength="255"  />
</div>
<!--end::Input group-->

<div class="row">
    <div class="col">
        <!--begin::Input group-->
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Dias de funcionamento</span>
            </label>
            
            <!--end::Label-->
            <div class="d-flex mt-3">
                <div class="form-check form-check-custom form-check-solid">
                    <input name="monday" id="monday" class="form-check-input" type="checkbox" @if(isset($content) && $content->monday == true || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="monday">
                        Segunda
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="tuesday" id="tuesday" class="form-check-input" type="checkbox" @if(isset($content) && $content->tuesday == true || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="tuesday">
                        Terça
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="wednesday" id="wednesday" class="form-check-input" type="checkbox" @if(isset($content) && $content->wednesday == true || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="wednesday">
                        Quarta
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="thursday" id="thursday" class="form-check-input" type="checkbox" @if(isset($content) && $content->thursday == true || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="thursday">
                        Quinta
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="friday" id="friday" class="form-check-input" type="checkbox" @if(isset($content) && $content->friday == true || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="friday">
                        Sexta
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="saturday" id="saturday" class="form-check-input" type="checkbox" @if(isset($content) && $content->saturday == true) checked="" @endif/>
                    <label class="form-check-label" for="saturday">
                        Sábado
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="sunday" id="sunday" class="form-check-input" type="checkbox" @if(isset($content) && $content->sunday == true) checked="" @endif/>
                    <label class="form-check-label" for="sunday">
                        Domingo
                    </label>
                </div>
            </div>
        </div>
        <!--end::Input group-->
    </div>
    <div class="col">
        <!--begin::Input group-->
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Horários de funcionamento</span>
            </label>
            <!--end::Label-->
            <div class="d-flex">
                <input type="time" class="form-control form-control-solid" required name="hour_start" value="{{ $content->hour_start ?? old('hour_start') }}"/>
            </div>
        </div>
        <!--end::Input group-->
    </div>
    <div class="col">
        <!--begin::Input group-->
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Horário de fechamento</span>
            </label>
            <!--end::Label-->
            <div class="d-flex">
                <input type="time" class="form-control form-control-solid" required name="hour_end" value="{{ $content->hour_end ?? old('hour_end') }}"/>
            </div>
        </div>
        <!--end::Input group-->
    </div>
</div>

<div class="row">
    <!--begin::Input group-->
    <div class="col">
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Capacidade - Número máximo de pessoas</span>
            </label>
            <!--end::Label-->
            <input type="number" class="form-control form-control-solid" required name="capacity" value="{{ $content->capacity ?? old('capacity') }}"/>
        </div>
    </div>
    <div class="col">
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Valor para reserva em R$</span>
            </label>
            <!--end::Label-->
            <input type="text" class="form-control form-control-solid" required name="tax_money" value="{{ $content->tax_money ?? old('tax_money') }}" id="money"/>
        </div>
    </div>
    <!--end::Input group-->
</div>

<div class="row">
    <!--begin::Input group-->
    <div class="col">
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Antecedência mínima para reserva</span>
            </label>
            <!--end::Label-->
            <input type="number" class="form-control form-control-solid" required name="antecedence" value="{{ $content->antecedence ?? old('antecedence') }}"/>
        </div>
    </div>
    <div class="col">
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Antecedência mínima para cancelamento</span>
            </label>
            <!--end::Label-->
            <input type="number" class="form-control form-control-solid" required name="cancellation" value="{{ $content->cancellation ?? old('cancellation') }}"/>
        </div>
    </div>
    <!--end::Input group-->
</div>

<div class="row">
    <!--begin::Input group-->
    <div class="col">
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Reserva exclusiva?</span>
                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Ao reservar este espaço, outra pessoa fica impedida de reservar esse espaço no mesmo dia ou período?"></i>
            </label>
            <!--end::Label-->
            <div class="d-flex mt-3">
                <div class="form-check form-check-custom form-check-solid">
                    <input name="exclusive" id="esim" value="não" class="form-check-input" type="radio" @if(isset($content) && $content->exclusive == 0 || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="esim">
                        Mais de uma pessoa pode reservar por vez
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="exclusive" id="enao" value="sim" class="form-check-input" type="radio" @if(isset($content->exclusive) && $content->exclusive == 1) checked="" @endif/>
                    <label class="form-check-label" for="enao">
                        Apenas uma reservar por vez
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Necessita de aprovação?</span>
                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Marque como sim se você quer liberar as reservas para esse espaço manualmente."></i>
            </label>
            <!--end::Label-->
            <div class="d-flex mt-3">
                <div class="form-check form-check-custom form-check-solid">
                    <input name="approval" id="asim" value="sim" class="form-check-input" type="radio"  @if(isset($content->approval) && $content->approval == 1 || !isset($content)) checked="" @endif/>
                    <label class="form-check-label" for="asim">
                        Sim
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-3">
                    <input name="approval" id="anao" value="não" class="form-check-input" type="radio" @if(isset($content) && $content->approval == 0) checked="" @endif/>
                    <label class="form-check-label" for="anao">
                        Não
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>

<!--end::Input group-->
<!--begin::Input group-->
<div class="d-flex flex-column mb-8">
    <label class="fs-6 fw-bold mb-2 required">Infraestrutura - Descreva a infraestrutura do espaço</label>
    <textarea class="form-control form-control-solid" required rows="3" name="infrastructure" placeholder="Coloque o conteúdo aqui.">{{ $content->infrastructure ?? old('infrastructure') }}</textarea>
</div>
<!--end::Input group-->
<!--begin::Input group-->
<div class="d-flex flex-column mb-8">
    <label class="fs-6 fw-bold mb-2 required">Regras de uso</label>
    <textarea class="form-control form-control-solid" required rows="3" name="rules" placeholder="Coloque o conteúdo aqui.">{{ $content->rules ?? old('rules') }}</textarea>
</div>
<!--end::Input group-->
<!--begin::Input group-->
<div class="flex-stack mb-12">
    <!--begin::Label-->
    <div class="me-5 mb-2">
        <label class="fs-6 fw-bold">Foto do espaço</label>
        <div class="fs-7 fw-bold text-muted">Selecione um arquivo para anexar ao seu espaço.</div>
    </div>
    <!--end::Label-->
    <!--begin::Switch-->
    <label class="form-check form-switch form-check-custom form-check-solid">
        <input class="form-control form-control-solid" name="image" type="file" id="formFile" accept=".png, .jpg, .jpeg, .webp">
    </label>
    <!--end::Switch-->
</div>
<!--end::Input group-->
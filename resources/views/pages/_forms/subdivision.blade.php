@csrf

<!--begin::Input group-->
<div class="d-flex flex-column mb-8 fv-row">
    <div class="row">
        <div class="col">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Subdivisão</span>
            </label>
            <!--end::Label-->
            <input type="text" class="form-control form-control-solid" required placeholder="Ex.: Subdivisão 01, Subdivisão 02." name="name" value="{{ $content->name ?? old('name') }}" maxlength="255"  />
        </div>
    </div>
</div>
<!--end::Input group-->
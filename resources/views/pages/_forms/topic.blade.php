@csrf
                
<!--begin::Input group-->
<div class="d-flex flex-column mb-8 fv-row">
    <!--begin::Label-->
    <label class="d-flex align-items-center fs-6 fw-bold mb-2">
        <span class="required">Título</span>
    </label>
    <!--end::Label-->
    <input type="text" class="form-control form-control-solid" required placeholder="Ex.: Festa de fim de ano!" name="title" value="{{ $content->title ?? old('title') }}" maxlength="255"  />
</div>
<!--end::Input group-->
<!--begin::Input group-->
<div class="d-flex flex-column mb-8">
    <label class="fs-6 fw-bold mb-2 required">Descrição</label>
    <textarea class="form-control form-control-solid" required rows="3" name="description" placeholder="Coloque o conteúdo aqui.">{{ $content->description ?? old('description') }}</textarea>
</div>
<div class="row">
    <!--begin::Input group-->
    <div class="col">
        <div class="d-flex flex-column mb-8 fv-row">
            <!--begin::Label-->
            <label class="d-flex align-items-center fs-6 fw-bold mb-2">
                <span class="required">Tipo de tópico</span>
            </label>    
            <!--end::Label-->
            <div class="d-md-flex mt-3">
                <div class="form-check form-check-custom form-check-solid">
                    <input name="type" value="Assembleias" id="Assembleias" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Assembleias' || !isset($content)) checked @endif>
                    <label class="form-check-label" for="Assembleias">
                        Assembleias
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="Achados e perdidos" id="Achados" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Achados e perdidos') checked @endif>
                    <label class="form-check-label" for="Achados">
                        Achados e perdidos
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="Eu recomendo" id="recomendo" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Eu recomendo') checked @endif>
                    <label class="form-check-label" for="recomendo">
                        Eu recomendo
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="Carona" id="Carona" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Carona') checked @endif>
                    <label class="form-check-label" for="Carona">
                        Carona
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="Eu Preciso de" id="Preciso" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Eu Preciso de') checked @endif> 
                    <label class="form-check-label" for="Preciso">
                        Eu Preciso de
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="Vagas de garagem" id="garagem" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'Vagas de garagem') checked @endif> 
                    <label class="form-check-label" for="garagem">
                        Vagas de garagem
                    </label>
                </div>
                <div class="form-check form-check-custom form-check-solid ml-md-3 mt-3 mt-md-0">
                    <input name="type" value="AdCircus" id="AdCircus" class="form-check-input" type="radio" @if(isset($content) && $content->type == 'AdCircus') checked @endif> 
                    <label class="form-check-label" for="AdCircus">
                        AdCircus
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row mb-12">
    <div class="col">
        <!--begin::Label-->
        <div class="me-5 mb-2">
            <label class="fs-6 fw-bold">Capa do tópico</label>
        </div>
        <!--end::Label-->
        <!--begin::Switch-->
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input class="form-control form-control-solid" name="image" type="file" id="formFile" accept=".png, .jpg, .jpeg, .webp">
        </label>
        <!--end::Switch-->
    </div>
</div>
<!--end::Input group-->
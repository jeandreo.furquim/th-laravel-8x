@extends('layouts.app')

@section('title', 'Adicionar portaria')

@section('custom-head')
   
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0" id="kt_content">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card p-5">
                <div class="card-body">
                    <!--begin::Heading-->
                    <div class="mb-8 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Adicionar nova portaria</h1>
                        <!-- end::Title -->
                    </div>
                    <!--end::Heading-->
                    <form action="{{ route('admin.concierges.store') }}" class="form" method="post" enctype="multipart/form-data">
                       @include('pages._forms.concierge')
                        <!--begin::Actions-->
                        <div class="text-center mt-9">
                            <a href="{{ route('admin.concierges.index') }}">
                                <label class="btn btn-light me-3 mb-0">Voltar</label>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label" >Adicionar portaria</span>
                            </button>
                        </div>
                        <!--end::Actions-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom-js')
<!-- Calendar DataPicker -->
<script src="https://npmcdn.com/flatpickr/dist/l10n/pt.js"></script>
<script>

    // CALENDAR HOUR
    $(".hour-picker").flatpickr({
        enableTime: true,
        noCalendar: true,
        dateFormat: "H:i",
        time_24hr: true,
    });

    // CALENDAR
    $(".flatpickr").flatpickr({
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        "locale": "pt",
        allowInput: true, // prevent "readonly" prop
        disableMobile: true,
    });

    // PHONE
    Inputmask({
        clearIncomplete: true,
        "mask" : ["(99) 9999-9999", "(99) 9 9999-9999"]
    }).mask(".phone-all");

</script>
@endsection

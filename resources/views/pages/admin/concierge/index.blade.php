@extends('layouts.app')

@section('title', 'Portarias')

@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">
                <div class="card-body">
                    <!--begin::Heading-->
                    <div class="mb-8 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Portarias</h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Heading-->
                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>Nome</th>
                                <th>Condomínio</th>
                                <th>Tipo de morador</th>
                                <th>Telefone</th>
                                <th>Ramal</th>
                                <th>Status</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $content)
                            <tr>
                                <td>{{ $content->name }}</td>
                                <td><span class="text-muted fs-7 fw-bolder">{{ $content->cond()->first()->name }} @if(isset($content->subdivision)) / {{ $content->subd()->first()->name }} @endif</span></td>
                                @if($content->role == 'administrador')
                                <td><span class="badge badge-light-danger fs-8 fw-bolder">Administrador</span></td>
                                @elseif($content->role == 'portaria')
                                <td><span class="badge badge-light-success fs-8 fw-bolder">Portaria</span></td>
                                @else
                                <td><span class="badge badge-light-primary fs-8 fw-bolder">Morador</span></td>
                                @endif
                                <td><span class="fw-bold text-gray-800">@if($content->role == 'portaria') <span class="fw-bolder"></span> @endif{{ $content->cellphone }}</span></td>
                                <td><span class="fw-bold text-gray-800">@if($content->role == 'portaria') <span class="fw-bolder">RAMAL:</span> @endif{{ $content->portersInfos()->first()->branch }}</span></td>
                                @if($content->status == 'pendente')
                                <td><span class="badge badge-light-warning fs-8 fw-bolder">Pendente</span></td>
                                @elseif($content->status == 'aprovado')
                                <td><span class="badge badge-light-success fs-8 fw-bolder">Aprovado</span></td>
                                @else
                                <td><span class="badge badge-light-danger fs-8 fw-bolder">Bloqueado</span></td>
                                @endif
                                <td class="actions">
                                    <a href="{{ route('messages.index', $content->id) }}"><i class="fas fa-comment-dots" title="Mensagem"></i></a>
                                    @if(session('sessionCondominium'))
                                    <a href="{{ route('admin.concierges.edit', $content->id) }}"><i class="fas fa-pencil-alt" title="Editar"></i></a>
                                    <a href="{{ route('admin.concierges.destroy', $content->id) }}"><i class="fas fa-trash" title="Excluir"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
@extends('layouts.app')

@section('title', 'Editar condominio')

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card p-5">
                <div class="card-body">
                    <div class="mb-8 text-center">
                        <h1 class="mb-0">Editar condomínio: {{ $condominium->title }}</h1>
                    </div>
                    <form action="{{ route('admin.condominiums.update', $condominium->id) }}" class="form" method="post" enctype="multipart/form-data">
                        @method('PUT')
                        @include('pages._forms.condominium')
                        <div class="text-center">
                            <a href="{{ route('admin.condominiums.index') }}">
                                <label class="btn btn-light me-3 mb-0">Voltar</label>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label" >Editar condomínio</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
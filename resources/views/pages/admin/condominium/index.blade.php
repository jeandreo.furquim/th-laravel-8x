@extends('layouts.app')

@section('title', 'Condomínios')

@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-8 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Condomínios</h1>
                        <!--end::Title-->
                        <!--begin::Description-->
                        <div class="text-muted fw-bold fs-5">Condomínios registrados no sistema.</div>
                        <!--end::Description-->
                        
                    </div>
                    <!--end::Heading-->

                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>ID</th>
                                <th>Condomínio</th>
                                <th>Data</th>
                                <th>Síndico/Gestor</th>
                                <th>Tipo de condomínio</th>
                                <th>Usuários</th>
                                <th>Bairro</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($condominiums as $condominium)
                                <tr>
                                    <td>{{ $condominium->id }}</td>
                                    <td>{{ $condominium->name }}</td>
                                    <td>{{ $condominium->created_at->format('d/m/Y') }}</td>
                                    <td>{{ $condominium->syndic }}</td>
                                    <td>{{ $condominium->type }}</td>
                                    <td>{{ $condominium->usersCondominium->count() }}</td>
                                    <td>{{ $condominium->zone }}</td>
                                    <td class="p-0">
                                        <a href="{{ route('admin.condominiums.session', $condominium->id) }}" class="btn btn-sm btn-light btn-active-primary" style="margin: 5px;" title="Visualizar">Acessar</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <a href="{{ route('admin.condominiums.create') }}" class="btn btn-primary w-auto">
                <span class="indicator-label" >Criar novo condomínio</span>
            </a>
            
            @if (session('sessionCondominium'))
            <a href="{{ route('admin.condominiums.session', 'exit') }}" class="btn btn-danger w-auto ms-4">
                <span class="indicator-label" >Sair do condomínio</span>
            </a>
            @endif

        </div>
    </div>
</div>


@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
@extends('layouts.app')

@section('title', 'Configurações')

@section('custom-head')
   
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0" id="kt_content">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card p-5">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-8 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Configurações Barra e Botões</h1>
                        <!-- end::Title -->
                    </div>
                    <!--end::Heading-->

                    @if (customStyle() == null)
                    <form action="{{ route('admin.configurations.store') }}" class="form" method="post" enctype="multipart/form-data">
                    @else
                    <form action="{{ route('admin.configurations.update') }}" class="form" method="post" enctype="multipart/form-data">
                    @method('PUT')
                    @endif
                
                       @include('pages._forms.configuration')

                        <!--begin::Actions-->
                        <div class="text-center">
                            <a href="{{ route('admin.subdivisions.index') }}">
                                <label class="btn btn-light me-3 mb-0">Voltar</label>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label" >Atualizar aparência</span>
                            </button>
                        </div>
                        <!--end::Actions-->

                    </form>
                    
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
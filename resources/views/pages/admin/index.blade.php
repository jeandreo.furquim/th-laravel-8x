@extends('layouts.app')

@section('title', 'Administração')

@section('custom-head')
<style>
    .table.table-row-dashed tr {
        border-bottom-width: 1px;
    }

    .custom-border {
        border-bottom: dashed 1px #eff2f5;
    }
    
    .table.table-row-gray-200 tr {
        border-bottom-color: #e3e3e3;
    }

    .header-fixed .header {
        position: absolute;
    }

    .header {
        background: transparent !important  ;
    }
    .btn-check:active+.btn.btn-active-light-primary .svg-icon svg [fill]:not(.permanent):not(g), .btn-check:checked+.btn.btn-active-light-primary .svg-icon svg [fill]:not(.permanent):not(g), .btn.btn-active-light-primary.active .svg-icon svg [fill]:not(.permanent):not(g), .btn.btn-active-light-primary.show .svg-icon svg [fill]:not(.permanent):not(g), .btn.btn-active-light-primary:active:not(.btn-active) .svg-icon svg [fill]:not(.permanent):not(g), .btn.btn-active-light-primary:focus:not(.btn-active) .svg-icon svg [fill]:not(.permanent):not(g), .btn.btn-active-light-primary:hover:not(.btn-active) .svg-icon svg [fill]:not(.permanent):not(g), .show>.btn.btn-active-light-primary .svg-icon svg [fill]:not(.permanent):not(g) {
        fill: white;
    }
    .condominiums-header {
        display: none !important;
    }

    .menu-state-bg .menu-item.here>.menu-link, .menu-state-bg .menu-item.show>.menu-link, .btn-menus:hover {
        background-color: #181c3288 !important;
        color: white !important;
    }

    .height-cards-occurrence {
        height: 319px;
    }

    .logo-home {
        fill: white !important;
    }

</style>
<link href="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
<script src="{{ asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
@endsection

@section('content')
<div class="content d-flex justify-content-center align-items-center" style="background: url('{{ Storage::disk('local')->exists("public/configuracoes/background_general.jpg") ? url("storage/configuracoes/background_general.jpg") : asset('assets/images/background-adm-2.jpg') }}'); background-size:cover;background-position:center;height: 230px;margin-top: -70px;padding-top: 70px;">
    <div class="row">
        <div class="col">
            <p class="text-center m-0 h1 text-light">
                <a href="{{ route('admin.condominiums.index') }}" class="text-light">
                    @if (session('sessionCondominium'))
                        <span class="fs-6">{{'VISUALIZANDO:'}}</span><br>
                        <span class="text-uppercase text-center fw-bolder">{{ session('sessionName') }}</span>
                        <p class="text-center text-white m-0 fw-bold fs-7">
                            <a href="{{ route('admin.condominiums.show', session('sessionCondominium')) }}" class="text-white">Ficha técnica</a> / <a href="{{ route('admin.condominiums.edit', session('sessionCondominium')) }}" class="text-white">Editar</a> / <a href="{{ route('admin.condominiums.session', 'exit') }}" class="text-white">Sair do condomínio</a>
                        </p>
                    @else
                        <span class="">NENHUM CONDOMÍNIO SELECIONADO</span><br>
                        <button class="btn btn-white text-dark mt-4">Selecionar condomínio</button>
                    @endif
                </a>
            </p>
        </div>
    </div>
</div>
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0" id="kt_content">
    <div id="content_container" class="container-xxl">
        <!--begin::Row-->
        <div class="row gy-5 g-xl-8">            
            <!--begin::Col-->
            <div class="col-xxl-12">
                <!--begin::Tables Widget 5-->
                <div class="card card-xxl-stretch mb-4">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-7">
                        <div>
                            <p class="card-label m-0 fw-bolder fs-3">Últimas atualizações</p>
                            <p class="text-muted m-0 fw-bold fs-7">Confira as últimas atividades desse condomínio.</p>
                        </div>
                        <div class="card-toolbar">
                            <a href="{{ route('updates') }}" class="btn btn-sm btn-light">Histórico</a>
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body py-3">
                        <div class="tab-content">
                            <!--begin::Tap pane-->
                            <div class="tab-pane fade show active" id="kt_table_widget_5_tab_1">
                                <!--begin::Table container-->
                                <div class="table-responsive">
                                    <!--begin::Table-->
                                    <table class="table table-row-dashed table-row-gray-200 align-middle gs-0 gy-4">
                                        <!--begin::Table head-->
                                        <thead>
                                            <tr class="border-0">
                                                <th class="p-0 w-50px" style="border: transparent"></th>
                                                <th class="p-0 min-w-150px" style="border: transparent"></th>
                                                <th class="p-0 min-w-140px" style="border: transparent"></th>
                                                <th class="p-0 min-w-110px" style="border: transparent"></th>
                                                <th class="p-0 min-w-50px" style="border: transparent"></th>
                                            </tr>
                                        </thead>
                                        <!--end::Table head-->
                                        <!--begin::Table body-->
                                        <tbody>
                                            @if(updatesCondominiuns(5)->count())
                                            @foreach (updatesCondominiuns(5) as $update)
                                            <tr>
                                                <td>
                                                    <a href="{{ route('admin.users.show', $update->user) }}">
                                                        <div class="symbol symbol-45px me-2">
                                                            <img src="{{ imageUser($update->user) }}" class="object-cover align-self-center" title="{{ $update->author()->first()->name }}" />
                                                        </div>
                                                    </a>
                                                </td>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div>
                                                            <a href="{{urlUpdates($update->type, $update->content, $update->user)}}" class="text-dark fw-bolder mt-2">{{ mb_strimwidth($update->title, 0, 95, '...') }}</a>
                                                            <span class="text-muted fw-bold d-block">{{ msgUpdate($update->type, $update->action) }}</span>
                                                            </div>
                                                    </div>
                                                </td>
                                                <td class="text-end text-muted fw-bold">
                                                    <span class="badge badge-light fs-8 fw-bolder" title="{{ $update->created_at->format('d/m/Y H:i') }}">{{days($update->created_at)}}</span>
                                                </td>
                                                <td class="text-end">
                                                    @if($update->action == 0 && $update->type != 20)
                                                        <span class="badge badge-light-success">Adicionado</span>
                                                    @elseif($update->action == 1)
                                                        <span class="badge badge-light-warning">Alterado</span>
                                                    @elseif($update->action == 2)
                                                        <span class="badge badge-light-danger">Excluído</span>
                                                    @elseif($update->type == 21)
                                                        <span class="badge badge-light-info">Status Usuário</span>
                                                    @elseif($update->type == 20)
                                                        <span class="badge badge-light-dark">Reserva</span>
                                                    @else
                                                        <span class="badge badge-light-primary">Sistema</span>
                                                    @endif
                                                </td>
                                                <td class="text-end">
                                                    <a href="{{urlUpdates($update->type, $update->content, $update->user)}}" target="_blank">
                                                        <span class="btn btn-sm btn-icon btn-bg-light btn-active-color-primary">
                                                            <!--begin::Svg Icon | path: icons/duotune/arrows/arr064.svg-->
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <rect opacity="0.5" x="18" y="13" width="13" height="2" rx="1" transform="rotate(-180 18 13)" fill="black" />
                                                                    <path d="M15.4343 12.5657L11.25 16.75C10.8358 17.1642 10.8358 17.8358 11.25 18.25C11.6642 18.6642 12.3358 18.6642 12.75 18.25L18.2929 12.7071C18.6834 12.3166 18.6834 11.6834 18.2929 11.2929L12.75 5.75C12.3358 5.33579 11.6642 5.33579 11.25 5.75C10.8358 6.16421 10.8358 6.83579 11.25 7.25L15.4343 11.4343C15.7467 11.7467 15.7467 12.2533 15.4343 12.5657Z" fill="black" />
                                                                </svg>
                                                            </span>
                                                            <!--end::Svg Icon-->
                                                        </span>
                                                    </a>
                                                </td>
                                            </tr>
                                            @endforeach
                                            @else
                                            <div class="height-cards-occurrence">
                                                <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                                                    <div class="">
                                                        <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen004.svg-->
                                                        <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z" fill="black"></path>
                                                        <path opacity="0.3" d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z" fill="black"></path>
                                                        </svg></p>
                                                        <!--end::Svg Icon-->
                                                        <span class="text-muted fw-bold fs-5 ml-2">Não existem atualizações a serem exibidas</span>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </tbody>
                                        <!--end::Table body-->
                                    </table>
                                </div>
                                <!--end::Table-->
                            </div>
                            <!--end::Tap pane-->
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Tables Widget 5-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-lg-4 mt-0">
                <!--begin::Tables Widget 5-->
                <div class="card card-xxl-stretch mb-4">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-7">
                        <div>
                            <p class="card-label m-0 fw-bolder fs-3">Anotações</p>
                            <p class="text-muted m-0 fw-bold fs-7">Seus últimos registros.</p>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Menu-->
                            <a href="{{ route('notes.create') }}" class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" title="Adicionar anotação" style="margin-right: -15px; z-index: 9">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen035.svg-->
                                <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black"/>
                                <rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black"/>
                                <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black"/>
                                </svg></span>
                                <!--end::Svg Icon-->
                            </a>
                            <!--begin::Menu-->
                            <!--begin::Menu-->
                            <a href="{{ route('notes.index') }}" class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" title="Todas anotações">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                                <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="currentColor"></rect>
                                        <rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </a>
                            <!--begin::Menu-->
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body py-3">
                        <div class="tab-content">
                            @if($notes->count())
                            @foreach ($notes as $note)
                            <!--begin::Tap pane-->
                            <div class="d-flex flex-stack py-4 custom-border">
                                <!--begin::Details-->
                                <div class="d-flex align-items-center">
                                    <!--begin::Details-->
                                    <div class="ms-0 me-2">
                                        <a href="{{ route('notes.edit', $note->id) }}" >
                                            <p class="fs-5 fw-bolder text-gray-800 text-hover-primary mb-0">{{ mb_strimwidth($note->title, 0, 26, '...') }}</p>
                                            <p class="fw-bold text-muted">{{ mb_strimwidth($note->description, 0, 75, '...') }}</p>
                                        </a>
                                    </div>
                                    <!--end::Details-->
                                </div>
                                <!--end::Details-->
                                <!--begin::Lat seen-->
                                <div class="d-flex flex-column align-items-end ms-2">
                                    <a href="{{ route('notes.edit', $note->id) }}" class="text-muted fs-7 mb-1" title="{{ $note->created_at->format('d/m/Y H:i') }}">{{ date('d/m/Y', strtotime($note->day)) }}</a>
                                </div>
                                <!--end::Lat seen-->
                            </div>
                            <!--end::Tap pane-->
                            @endforeach
                            @else
                            <div class="height-cards-occurrence">
                                <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                                    <div class="">
                                        <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen043.svg-->
                                        <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                        <path d="M10.4343 12.4343L8.75 10.75C8.33579 10.3358 7.66421 10.3358 7.25 10.75C6.83579 11.1642 6.83579 11.8358 7.25 12.25L10.2929 15.2929C10.6834 15.6834 11.3166 15.6834 11.7071 15.2929L17.25 9.75C17.6642 9.33579 17.6642 8.66421 17.25 8.25C16.8358 7.83579 16.1642 7.83579 15.75 8.25L11.5657 12.4343C11.2533 12.7467 10.7467 12.7467 10.4343 12.4343Z" fill="black"/>
                                        </svg></p>
                                        <!--end::Svg Icon-->
                                        <span class="text-muted fw-bold fs-5 ml-2">Nenhuma anotação</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Tables Widget 5-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-lg-4 mt-0">
                <!--begin::Tables Widget 5-->
                <div class="card card-xxl-stretch mb-4">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-7">
                        <div>
                            <p class="card-label m-0 fw-bolder fs-3">Mensagens não lidas</p>
                            <p class="text-muted m-0 fw-bold fs-7">Última mensagem hoje.</p>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Menu-->
                            <a href="{{ route('messages.index') }}" class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" title="Todas mensagens">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                                <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="currentColor"></rect>
                                        <rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </a>
                            <!--begin::Menu-->
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body py-3">
                        <div class="tab-content">
                            @if($messages->count())
                            @foreach ($messages as $msgs)
                            <!--begin::Tap pane-->
                            <div class="d-flex flex-stack py-4 custom-border">
                                <!--begin::Details-->
                                <div class="d-flex align-items-center">
                                    <!--begin::Avatar-->
                                    <a href="{{ route('admin.users.show', $msgs->from) }}">
                                        <div class="symbol symbol-45px">
                                            <img src="{{ imageUser($msgs->from) }}" class=" align-self-center" title="{{ $msgs->from()->first()->name }}" />
                                        </div>
                                    </a>
                                    <!--end::Avatar-->
                                    <!--begin::Details-->
                                    <div class="ms-5">
                                        <a href="{{ route('messages.index', $msgs->from) }}" class="fs-5 fw-bolder text-gray-800 text-hover-primary mb-2">{{ mb_strimwidth($msgs->from()->first()->name, 0, 26, '...') }}</a>
                                        <p class="m-0"><a href="{{ route('messages.index', $msgs->from) }}" class="fw-bold text-muted">{{ mb_strimwidth($msgs->message, 0, 28, '...') }}</a></p>
                                    </div>
                                    <!--end::Details-->
                                </div>
                                <!--end::Details-->
                                <!--begin::Lat seen-->
                                <div class="d-flex flex-column align-items-end ms-2">
                                    <span class="text-muted fs-7 mb-1" title="{{ $msgs->created_at->format('d/m/Y H:i') }}">{{days($msgs->created_at)}}</span>
                                </div>
                                <!--end::Lat seen-->
                            </div>
                            <!--end::Tap pane-->
                            @endforeach
                            @else
                            <div class="height-cards-occurrence">
                                <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                                    <div class="">
                                        <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen004.svg-->
                                        <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3" d="M20 3H4C2.89543 3 2 3.89543 2 5V16C2 17.1046 2.89543 18 4 18H4.5C5.05228 18 5.5 18.4477 5.5 19V21.5052C5.5 22.1441 6.21212 22.5253 6.74376 22.1708L11.4885 19.0077C12.4741 18.3506 13.6321 18 14.8167 18H20C21.1046 18 22 17.1046 22 16V5C22 3.89543 21.1046 3 20 3Z" fill="black" />
                                            <rect x="6" y="12" width="7" height="2" rx="1" fill="black" />
                                            <rect x="6" y="7" width="12" height="2" rx="1" fill="black" />
                                        </svg></p>
                                        <!--end::Svg Icon-->
                                        <span class="text-muted fw-bold fs-5 ml-2">Nenhuma mensagem</span>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Tables Widget 5-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-lg-4 mt-0">
                <!--begin::Tables Widget 5-->
                <div class="card card-xxl-stretch mb-4">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-7">
                        <div>
                            <p class="card-label m-0 fw-bolder fs-3">Reservas pendentes</p>
                            <p class="text-muted m-0 fw-bold fs-7">Solicitações para espaços.</p>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Menu-->
                            <a href="{{ route('reservations.index') }}" class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" title="Todas reservas">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                                <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="currentColor"></rect>
                                        <rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </a>
                            <!--begin::Menu-->
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body py-3" style="min-height: 330px;">
                    @if($reservations->count())
                       @foreach($reservations as $reservation)
                       <div class="tab-content">
                            <!--begin::Tap pane-->
                            <div class="d-flex flex-stack py-4 custom-border">
                                <!--begin::Details-->
                                <div class="d-flex align-items-center">
                                    <!--begin::Avatar-->
                                    <a href="{{ route('admin.users.show', $reservation->user) }}">
                                        <div class="symbol symbol-45px me-2">
                                            <img src="{{ imageUser($reservation->user) }}" class=" align-self-center" title="{{ $reservation->author()->first()->name }}" />
                                        </div>
                                    </a>
                                    <!--end::Avatar-->
                                    <!--begin::Details-->
                                    <div class="ms-5">
                                        <a href="#" class="fs-5 fw-bolder text-gray-800 text-hover-primary mb-2">{{ mb_strimwidth($reservation->room()->first()->name, 0, 22, '...') }}</a>
                                        <div class="fw-bold text-muted">Solicitado: {{days($reservation->created_at)}}</div>
                                    </div>
                                    <!--end::Details-->
                                </div>
                                <!--end::Details-->
                                <!--begin::Lat seen-->
                                <div class="d-flex align-items-end ms-2">
                                    <div>
                                        <div>
                                            <a class="badge badge-sm badge-light-primary text-primary my-1 w-100" href="{{ route('rooms.show', $reservation->room()->first()->id) }}" style="color: #009ef7 !important"  title="Visualizar espaço" target="_blank"><i class="fas fa-eye"></i></a>
                                        </div>
                                        <div>
                                            <a class="badge badge-sm badge-light-success text-success my-1" href="{{ route('reservations.update', ['id' => $reservation->id, 'status' => 'aprovado']) }}" title="Aprovar"><i class="fas fa-check-circle"></i></a>    
                                            <a class="badge badge-sm badge-light-danger text-danger my-1" href="{{ route('reservations.update', ['id' => $reservation->id, 'status' => 'recusado']) }}" title="Recusar"><i class="fas fa-times-circle"></i></a>
                                        </div>  
                                    </div>  
                                </div>
                                <!--end::Lat seen-->
                            </div>
                            <!--end::Tap pane-->
                        </div>
                       @endforeach
                       @else
                       <div class="height-cards-occurrence">
                           <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                               <div class="">
                                   <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen004.svg-->
                                   <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="black"/>
                                    <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="black"/>
                                    <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="black"/>
                                    </svg></p>
                                   <!--end::Svg Icon-->
                                   <span class="text-muted fw-bold fs-5 ml-2">Sem solicitações</span>
                               </div>
                           </div>
                       </div>
                       @endif
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Tables Widget 5-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-lg-4 mt-0">
                <!--begin::Tables Widget 5-->
                <div class="card card-xxl-stretch mb-4">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-7">
                        <div>
                            <p class="card-label m-0 fw-bolder fs-3">Próximas manutenções</p>
                            <p class="text-muted m-0 fw-bold fs-7">Confira o calendário.</p>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Menu-->
                            <a href="{{ route('maintenances.index') }}" title="Todas manutenções" class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                                <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="currentColor"></rect>
                                        <rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </a>
                            <!--begin::Menu-->
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body py-3" style="min-height: 330px;">
                    @if($maintenances->count())
                       @foreach($maintenances as $maintenance)
                       <div class="tab-content">
                            <!--begin::Tap pane-->
                            <div class="d-flex flex-stack py-4 custom-border">
                                <!--begin::Details-->
                                <div class="d-flex align-items-center">
                                    <!--begin::Avatar-->
                                    <a href="{{ route('admin.users.show', $maintenance->user) }}">
                                        <div class="symbol symbol-45px me-2">
                                            <img src="{{ imageUser($maintenance->user) }}" class=" align-self-center" title="{{ $maintenance->author()->first()->name }}" />
                                        </div>
                                    </a>
                                    <!--end::Avatar-->
                                    <!--begin::Details-->
                                    <div class="ms-5">
                                        <a href="#" class="fs-5 fw-bolder text-gray-800 text-hover-primary mb-2">{{ mb_strimwidth($maintenance->name, 0, 22, '...') }}</a>
                                        <div class="fw-bold text-muted">{{ mb_strimwidth($maintenance->author()->first()->name, 0, 26, '...') }}</div>
                                    </div>
                                    <!--end::Details-->
                                </div>
                                <!--end::Details-->
                                <!--begin::Lat seen-->
                                <div class="d-flex flex-column align-items-end ms-2">
                                    <span class="badge text-muted fs-8 fw-bolder mb-0" href="#">{{ date_create($maintenance->start)->format('d/m/Y') }}</span>
                                    <span class="badge badge-light fs-8 fw-bolder" href="#">{{ date_create($maintenance->end)->format('d/m/Y') }}</span>
                                </div>
                                <!--end::Lat seen-->
                            </div>
                            <!--end::Tap pane-->
                        </div>
                        @endforeach
                        @else
                        <div class="height-cards-occurrence">
                            <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                                <div class="">
                                    <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen004.svg-->
                                    <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path opacity="0.3" d="M20.859 12.596L17.736 13.596L10.388 20.944C10.2915 21.0406 10.1769 21.1172 10.0508 21.1695C9.9247 21.2218 9.78953 21.2486 9.65302 21.2486C9.5165 21.2486 9.3813 21.2218 9.25519 21.1695C9.12907 21.1172 9.01449 21.0406 8.918 20.944L2.29999 14.3229C2.10543 14.1278 1.99619 13.8635 1.99619 13.588C1.99619 13.3124 2.10543 13.0481 2.29999 12.853L11.853 3.29999C11.9495 3.20341 12.0641 3.12679 12.1902 3.07452C12.3163 3.02225 12.4515 2.9953 12.588 2.9953C12.7245 2.9953 12.8597 3.02225 12.9858 3.07452C13.1119 3.12679 13.2265 3.20341 13.323 3.29999L21.199 11.176C21.3036 11.2791 21.3797 11.4075 21.4201 11.5486C21.4605 11.6898 21.4637 11.8391 21.4295 11.9819C21.3953 12.1247 21.3249 12.2562 21.2249 12.3638C21.125 12.4714 20.9989 12.5514 20.859 12.596Z" fill="black"/>
                                        <path d="M14.8 10.184C14.7447 10.1843 14.6895 10.1796 14.635 10.1699L5.816 8.69997C5.55436 8.65634 5.32077 8.51055 5.16661 8.29469C5.01246 8.07884 4.95035 7.8106 4.99397 7.54897C5.0376 7.28733 5.18339 7.05371 5.39925 6.89955C5.6151 6.7454 5.88334 6.68332 6.14498 6.72694L14.963 8.19692C15.2112 8.23733 15.435 8.36982 15.59 8.56789C15.7449 8.76596 15.8195 9.01502 15.7989 9.26564C15.7784 9.51626 15.6642 9.75001 15.479 9.92018C15.2939 10.0904 15.0514 10.1846 14.8 10.184ZM17 18.6229C17 19.0281 17.0985 19.4272 17.287 19.7859C17.4755 20.1446 17.7484 20.4521 18.0821 20.6819C18.4158 20.9117 18.8004 21.0571 19.2027 21.1052C19.605 21.1534 20.0131 21.103 20.3916 20.9585C20.7702 20.814 21.1079 20.5797 21.3758 20.2757C21.6437 19.9716 21.8336 19.607 21.9293 19.2133C22.025 18.8195 22.0235 18.4085 21.925 18.0154C21.8266 17.6223 21.634 17.259 21.364 16.9569L19.843 15.257C19.7999 15.2085 19.7471 15.1697 19.688 15.1432C19.6289 15.1167 19.5648 15.1029 19.5 15.1029C19.4352 15.1029 19.3711 15.1167 19.312 15.1432C19.2529 15.1697 19.2001 15.2085 19.157 15.257L17.636 16.9569C17.2254 17.4146 16.9988 18.0081 17 18.6229ZM10.388 20.9409L17.736 13.5929H1.99999C1.99921 13.7291 2.02532 13.8643 2.0768 13.9904C2.12828 14.1165 2.2041 14.2311 2.29997 14.3279L8.91399 20.9409C9.01055 21.0381 9.12539 21.1152 9.25188 21.1679C9.37836 21.2205 9.51399 21.2476 9.65099 21.2476C9.78798 21.2476 9.92361 21.2205 10.0501 21.1679C10.1766 21.1152 10.2914 21.0381 10.388 20.9409Z" fill="black"/>
                                        </svg></p>
                                    <!--end::Svg Icon-->
                                    <span class="text-muted fw-bold fs-5 ml-2">Sem manutenções</span>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Tables Widget 5-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-lg-4 mt-0">
                <!--begin::Tables Widget 5-->
                <div class="card card-xxl-stretch mb-4 pb-3">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-7">
                        <div>
                            <p class="card-label m-0 fw-bolder fs-3">Solicitações de acesso</p>
                            <p class="text-muted m-0 fw-bold fs-7">Novos moradores.</p>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Menu-->
                            <a href="{{ route('admin.notifications.index') }}" class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" title="Todos usuários bloqueados">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                                <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="currentColor"></rect>
                                        <rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </a>
                            <!--begin::Menu-->
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body py-3" style="min-height: 330px;">
                       @if($users->count())
                       @foreach($users as $user)
                       <div class="tab-content">
                            <!--begin::Tap pane-->
                            <div class="d-flex flex-stack py-4 custom-border">
                                <!--begin::Details-->
                                <div class="d-flex align-items-center">
                                    <!--begin::Avatar-->
                                    <a href="{{ route('admin.users.show', $user->id) }}">
                                        <div class="symbol symbol-45px me-2">
                                            <img src="{{ imageUser($user->id) }}" class=" align-self-center" title="{{ $user->name }}" />
                                        </div>
                                    </a>
                                    <!--end::Avatar-->
                                    <!--begin::Details-->
                                    <div class="ms-5">
                                        <a href="#" class="fs-5 fw-bolder text-gray-800 text-hover-primary mb-2">{{ mb_strimwidth($user->name, 0, 25, '...') }}</a>
                                        <div class="fw-bold text-muted">{{ $user->cond()->first()->name }} - {{days($user->created_at)}}</div>
                                    </div>
                                    <!--end::Details-->
                                </div>
                                <!--end::Details-->
                                <!--begin::Lat seen-->
                                <div class="d-flex flex-column align-items-end ms-2">
                                    <a class="badge badge-sm badge-light-success text-success mb-2" href="{{ route('admin.users.approve', ['id' => $user->id, 'status' => 'aprovado']) }}">Aprovar</a>
                                    <a class="badge badge-sm badge-light-danger text-danger" href="{{ route('admin.notifications.create', $user->id) }}">Bloquear</a>
                                </div>
                                <!--end::Lat seen-->
                            </div>
                            <!--end::Tap pane-->
                        </div>
                       @endforeach
                       @else
                        <div class="height-cards-occurrence">
                            <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                                <div class="">
                                    <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen004.svg-->
                                    <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z" fill="black"/>
                                        <rect opacity="0.3" x="14" y="4" width="4" height="4" rx="2" fill="black"/>
                                        <path d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z" fill="black"/>
                                        <rect opacity="0.3" x="6" y="5" width="6" height="6" rx="3" fill="black"/>
                                        </svg></p>
                                    <!--end::Svg Icon-->
                                    <span class="text-muted fw-bold fs-5 ml-2">Sem usuários para aprovação</span>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Tables Widget 5-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-lg-4 mt-0">
                <!--begin::Tables Widget 5-->
                <div class="card card-xxl-stretch mb-4 pb-3">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-7">
                        <div>
                            <p class="card-label m-0 fw-bolder fs-3">Meus condomínios</p>
                            <p class="text-muted m-0 fw-bold fs-7">Visualize seus condomínios.</p>
                        </div>
                        <div class="card-toolbar">
                            <!--begin::Menu-->
                            <a href="{{ route('admin.users.index') }}" class="btn btn-icon btn-color-gray-400 btn-active-color-primary justify-content-end" title="Todos condomínios">
                                <!--begin::Svg Icon | path: icons/duotune/general/gen023.svg-->
                                <span class="svg-icon svg-icon-1 svg-icon-gray-300 me-n1">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="4" fill="currentColor"></rect>
                                        <rect x="11" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="15" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                        <rect x="7" y="11" width="2.6" height="2.6" rx="1.3" fill="currentColor"></rect>
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </a>
                            <!--begin::Menu-->
                        </div>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body py-3" style="min-height: 330px;">
                       @if($condominiums->count())
                       @foreach($condominiums as $condominiums)
                       <div class="tab-content">
                            <!--begin::Tap pane-->
                            <div class="d-flex flex-stack py-4 custom-border">
                                <!--begin::Details-->
                                <div class="d-flex align-items-center">
                                    <!--begin::Details-->
                                    <div class="ms-5">
                                        <a href="#" class="fs-5 fw-bolder text-gray-800 text-hover-primary mb-2">{{ mb_strimwidth($condominiums->name, 0, 33, '...') }}</a>
                                    </div>
                                    <!--end::Details-->
                                </div>
                                <!--end::Details-->
                                <!--begin::Lat seen-->
                                <div class="d-flex flex-column align-items-end ms-2">
                                    <a class="badge badge-sm badge-light-primary text-primary mb-2" href="{{ route('admin.condominiums.session', $condominiums->id) }}">Acessar</a>
                                </div>
                                <!--end::Lat seen-->
                            </div>
                            <!--end::Tap pane-->
                        </div>
                       @endforeach
                       @else
                        <div class="height-cards-occurrence">
                            <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                                <div class="">
                                    <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen004.svg-->
                                    <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M16.0173 9H15.3945C14.2833 9 13.263 9.61425 12.7431 10.5963L12.154 11.7091C12.0645 11.8781 12.1072 12.0868 12.2559 12.2071L12.6402 12.5183C13.2631 13.0225 13.7556 13.6691 14.0764 14.4035L14.2321 14.7601C14.2957 14.9058 14.4396 15 14.5987 15H18.6747C19.7297 15 20.4057 13.8774 19.912 12.945L18.6686 10.5963C18.1487 9.61425 17.1285 9 16.0173 9Z" fill="black"/>
                                        <rect opacity="0.3" x="14" y="4" width="4" height="4" rx="2" fill="black"/>
                                        <path d="M4.65486 14.8559C5.40389 13.1224 7.11161 12 9 12C10.8884 12 12.5961 13.1224 13.3451 14.8559L14.793 18.2067C15.3636 19.5271 14.3955 21 12.9571 21H5.04292C3.60453 21 2.63644 19.5271 3.20698 18.2067L4.65486 14.8559Z" fill="black"/>
                                        <rect opacity="0.3" x="6" y="5" width="6" height="6" rx="3" fill="black"/>
                                        </svg></p>
                                    <!--end::Svg Icon-->
                                    <span class="text-muted fw-bold fs-5 ml-2">Sem usuários para aprovação</span>
                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Tables Widget 5-->
            </div>
            <!--begin::Col-->
            <div class="col-xl-12 mt-0">
                <!--begin::List Widget 5-->
                <div class="card p-4">
                    <!--begin::Body-->
                    <div class="card-body p-5 posts-cards">
                        <div id='calendar'></div>
                    </div>
                    <!--end: Card Body-->
                </div>
                <!--end: List Widget 5-->
            </div>
            <!--end::Col-->
            <!--end::Col-->
            <div class="col-xl-12" style="display: none">
                <!--begin::Charts Widget 3-->
                <div class="card card-xl-stretch mb-xl-8">
                    <!--begin::Header-->
                    <div class="card-header border-0 pt-7">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bolder fs-3 mb-1">Acessos por mês</span>
                            <span class="text-muted fw-bold fs-7">Histórico do condomínio.</span>
                        </h3>
                    </div>
                    <!--end::Header-->
                    <!--begin::Body-->
                    <div class="card-body">
                        <!--begin::Chart-->
                        <div id="chard_01" style="height: 350px"></div>
                        <!--end::Chart-->
                    </div>
                    <!--end::Body-->
                </div>
                <!--end::Charts Widget 3-->
            </div>
        </div>
        <!--end::Row-->
        </div>
</div>


@endsection

@section('custom-js')

<script>
    var options = {
        series: [
          {
            name: "Moradores",
            data: [0, 0, 0, 0,0, 0,0, 0,0, 0,0, 0]
          },
          {
            name: "Portarias",
            data: [0, 0, 0, 0,0, 0,0, 0,0, 0,0, 0   ]
          }
        ],
          chart: {
          height: 350,
          type: 'area',
          zoom: {
            enabled: false
          }
        },
        fill: {
          type: 'gradient',
          gradient: {
            shadeIntensity: 1,
            inverseColors: false,
            opacityFrom: 0.5,
            opacityTo: 0,
            stops: [0, 90, 100]
          },
        },
        dataLabels: {
          enabled: false
        },
        stroke: {
          curve: 'straight'
        },
        grid: {
          row: {
            colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
            opacity: 0.5
          },
        },
        xaxis: {
          categories: ['Jan', 'Feb', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
        }
        };

        var chart = new ApexCharts(document.querySelector("#chard_01"), options);
        chart.render();
    </script>
    {{-- {{dd($datesCalendar)}} --}}
    <script>
        const element = document.getElementById("calendar");
    
        var todayDate = moment().startOf("day");
        var YM = todayDate.format("YYYY-MM");
        var YESTERDAY = todayDate.clone().subtract(1, "day").format("YYYY-MM-DD");
        var TODAY = todayDate.format("YYYY-MM-DD");
        var TOMORROW = todayDate.clone().add(1, "day").format("YYYY-MM-DD");
    
        var calendarEl = document.getElementById("calendar");
        var calendar = new FullCalendar.Calendar(calendarEl, {
            
            locale: 'pt-br',
            headerToolbar: {
                left: "prev,next today",
                center: "title",
                right: "dayGridMonth,timeGridWeek,timeGridDay,listMonth"
            },
    
            height: 800,
            contentHeight: 780,
            aspectRatio: 3,
    
            nowIndicator: true,
            now: TODAY + "T09:25:00",
    
            eventDurationEditable: false,
            eventStartEditable: false, 
    
            views: {
                dayGridMonth: { buttonText: "mês" },
                timeGridWeek: { buttonText: "semana" },
                timeGridDay: { buttonText: "dia" }
            },
    
            initialView: "dayGridMonth",
            initialDate: TODAY,
    
            editable: true,
            dayMaxEvents: true,
            navLinks: true,
            events: @json($datesCalendar),
        });
    
        calendar.render();
    </script>
@endsection
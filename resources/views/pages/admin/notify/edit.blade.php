@extends('layouts.app')

@section('title', 'Editar motivo do bloqueio')

@section('custom-head')

@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card p-5">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-8 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Editar motivo de bloqueio: {{ $content->name }}</h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Heading-->

                    <form action="{{ route('admin.notifications.update', $content->id) }}" class="form" method="post" enctype="multipart/form-data">
                
                        @method('PUT')
                        @include('pages._forms.notify')

                        <!--begin::Actions-->
                        <div class="text-center mt-9">
                            <a href="{{ route('admin.index') }}">
                                <label class="btn btn-light me-3 mb-0">Voltar</label>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label" >Editar motivo</span>
                            </button>
                        </div>
                        <!--end::Actions-->

                    </form>
                    
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
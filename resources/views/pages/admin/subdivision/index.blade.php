@extends('layouts.app')

@section('title', 'Condomínios')

@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-8 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Subdivisões</h1>
                        <!--end::Title-->
                        
                    </div>
                    <!--end::Heading-->

                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Condomínio</th>
                                <th>Data</th>
                                <th>Modificado</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $content)
                                <tr>
                                    <td>{{ $content->id }}</td>
                                    <td>{{ $content->name }}</td>
                                    <td>{{ $content->cond()->first()->name }}</td>
                                    <td>{{ $content->created_at->format('d/m/Y H:i') }}</td>
                                    <td>{{ $content->updated_at->format('d/m/Y H:i') }}</td>
                                    <td>
                                        <a href="{{ route('admin.subdivisions.edit', $content->id) }}"><i class="fas fa-pencil-alt" title="Editar"></i></a>
                                        <a href="{{ route('admin.subdivisions.destroy', $content->id) }}"><i class="fas fa-trash" title="Visualizar"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @if(session('sessionCondominium'))
            <a href="{{ route('admin.subdivisions.create') }}">
                <button type="submit" class="btn btn-primary w-auto">
                    <span class="indicator-label" >Criar nova subdivisão</span>
                </button>
            </a>
            @endif
        </div>
    </div>
</div>


@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
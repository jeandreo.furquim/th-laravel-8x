@extends('layouts.app')


@section('title', 'Todos os usuários')


@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!-- Font Awesome Icons -->
	<link rel="stylesheet" href="{{ url('https://pro.fontawesome.com/releases/v5.10.0/css/all.css') }}" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
@endsection


@section('content')
   
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-7 mt-7 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Todos os usuários</h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Heading-->

                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded table-custom-dm">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>#</th>
                                <th>-</th>
                                <th>Nome</th>
                                <th>Condomínio</th>
                                <th>Tipo de morador</th>
                                <th>Status</th>
                                <th>Últ. Acesso</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <th>#{{ $user->id }}</th>
                                <th class="p-2">
                                    <div class="symbol symbol-30px symbol-circle"">
                                        <img alt="Pic" src="{{ imageUser($user->id) }}">
                                    </div>
                                </th>
                                <td>{{ $user->name }}</td>
                                <td>
                                    <span class="badge badge-light fs-8 fw-bolder">
                                        @if ($user->cond)
                                        {{ $user->cond()->first()->name }}
                                        @else
                                        Sem condomínio
                                        @endif
                                    </span>
                                </td>
                                @if($user->role == 'administrador')
                                <td><span class="badge badge-light-danger fs-8 fw-bolder">Administrador</span></td>
                                @elseif($user->role == 'portaria')
                                <td><span class="badge badge-light-success fs-8 fw-bolder">Portaria</span></td>
                                @else
                                <td><span class="badge badge-light-primary fs-8 fw-bolder">Morador</span></td>
                                @endif
                                @if($user->status == 'pendente')
                                <td><span class="badge badge-light-warning fs-8 fw-bolder">Pendente</span></td>
                                @elseif($user->status == 'aprovado')
                                <td><span class="badge badge-light-success fs-8 fw-bolder">Aprovado</span></td>
                                @else
                                <td><span class="badge badge-light-danger fs-8 fw-bolder">Bloqueado</span></td>
                                @endif
                                <td>{{ days($user->last_login_at) }}</td>
                                <td class="actions">
                                    <a href="{{ route('admin.users.show', $user->id) }}"><i class="fas fa-eye" title="Visualizar"></i></a>
                                    <a href="{{ route('packages.create', $user->id) }}"><i class="fas fa-box" title="Encomenda"></i></a>
                                    <a href="{{ route('admin.notifications.create', $user->id) }}"><i class="fas fa-ban" title="Bloquear"></i></a>
                                    <a href="{{ route('messages.index', $user->id) }}"><i class="fas fa-comment-dots" title="Mensagem"></i></a>
                                </td>
                            </tr>
                            @endforeach                            
                        </tbody>
                    </table>
                    
                </div>

            </div>
        </div>
    </div>
</div>
        

@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
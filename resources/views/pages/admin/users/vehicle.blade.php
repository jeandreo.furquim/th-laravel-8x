@extends('layouts.app')


@section('title', 'Todos os veículos')


@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
    <!-- Font Awesome Icons -->
	<link rel="stylesheet" href="{{ url('https://pro.fontawesome.com/releases/v5.10.0/css/all.css') }}" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
@endsection


@section('content')
   
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-7 mt-7 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Todos os veículos</h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Heading-->

                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded table-custom-dm">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th style="width: 30px">#id</th>
                                <th>Marca/Modelo</th>
                                <th>Tipo</th>
                                <th>Placa</th>
                                <th>Cor</th>
                                <th>Casa</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $vehicle)
                            <tr>
                                <td>{{ $vehicle->id }}</td>
                                <td>{{ $vehicle->brand }}</td>
                                <td>{{ $vehicle->type }}</td>
                                <td>{{ $vehicle->board }}</td>
                                <td>{{ $vehicle->color }}</td>
                                <td>{{ $vehicle->owner()->first()->house }}</td>
                                <td class="actions">
                                    <a href="{{ route('admin.users.show', $vehicle->owner()->first()->id) }}"><i class="fas fa-eye"></i></a>
                                    <i class="fas fa-comment-dots"></i>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                    
                </div>

            </div>
        </div>
    </div>
</div>
        

@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
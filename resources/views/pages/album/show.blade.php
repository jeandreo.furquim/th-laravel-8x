@extends('layouts.app')

{{-- TITLE OF PAGE --}}
@section('title', $content->name)

{{-- TITLE OF POST --}}
@section('title-post', $content->name) 

{{-- LINK FOR RETURN --}}
@section('link-back', route('albums.index'))

{{-- CONTENT --}}
@section('content')
<!-- POST -->
<div class="content flex-column-fluid">
	@include('includes.pageTitle')
	<div class="container">
		<div class="row gy-5 g-xl-8">
			<!-- CONTENT PAGE -->
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						@include('includes.imagePost')
						@include('includes.infosPost')
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						<p class="text-justify text-muted mb-0 description-post">
							{!! nl2br(e($content->description)) !!}
						</p>
					</div>
				</div>
			</div>
			<!-- GALLERY -->
			@if($content->gallery()->count())
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<h2 class="fw-bolder">Galeria</h2>
						<div class="row">
							@foreach ($content->gallery()->get() as $file)
							<div class="col-lg-3">
								<a href={{ url("storage/condominios/$content->condominium/albuns/$content->id/$file->image") }}>
									<img src={{ url("storage/condominios/$content->condominium/albuns/$content->id/$file->image") }} class="w-100 shadow-1-strong rounded mb-4"/>
								</a>
								@if($content->user == Auth::user()->id)
								<a href="{{route('albums.removeImage', $file->id)}}">
									<div class="remove-image">
										<span class="letter-close text-primary">X</span>
									</div>
								</a>
								@endif
							</div>
							@endforeach
						</div>
					</div>
				</div>
			</div>
			@endif
			<!-- GALLERY -->
			@if(Auth::user()->role == 'administrador')
			<!-- SEND PHOTOS -->
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<h2 class="fw-bolder">Enviar imagens</h2>
						<!--begin::Label-->
						<form action="{{ route('albums.storeGallery', $content->id) }}" class="form" method="post" enctype="multipart/form-data">
							@csrf
							<div class="me-5 mb-2">
								<div class="fs-7 fw-bold text-muted">Selecione uma imagens para inserir na galeria dessa obra.</div>
							</div>
							<!--end::Label-->
							<!--begin::Switch-->
							<label class="form-check form-switch form-check-custom form-check-solid">
								<input class="form-control form-control-solid" name="image[]" type="file" required multiple id="formFile" accept=".png, .jpg, .jpeg">
							</label>
							<!--end::Switch-->
							<button type="submit" class="btn btn-primary mt-5">
								<span class="indicator-label" >Enviar imagens</span>
							</button>
						</form>
					</div>
				</div>
			</div>
			<!-- SEND PHOTOS -->
			@endif
			<!-- COMENTS -->
			<div class="col-xxl-12">
				<!--begin::Feeds Widget 3-->
				<div class="card mb-5 mb-xxl-8">
					<!--begin::Body-->
					<div class="card-body pb-0">
						<h2 class="fw-bolder mb-5">Comentários</h2>
						<div id="load-comments">
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
						</div>
					</div>
					<!--end::Body-->
				</div>
				<!--end::Feeds Widget 3-->
			</div>
			<!-- COMENTS -->
			<!-- CONTENT PAGE -->
		</div>
	</div>
</div>
<!-- POST -->
@endsection

@section('custom-js')
<script>
	// LOAD AJAX
	sistemAjax(1, {{ $content->id }}, {{ $content->condominium }});
</script>
@endsection
@extends('layouts.app')

{{-- TITLE OF PAGE --}}
@section('title', 'Ramais')

{{-- TITLE OF POST --}}
@section('title-post', 'Ramais') 

{{-- Content --}}
@section('content')	
<!--begin::Post-->
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
	
	<!-- INFOS-POST -->
	@include('includes.infosPost')
	<!-- INFOS-POST -->

	<div id="kt_content_container" class="container-xxl">
		<div class="row gy-5 g-xl-8">
			<!--begin::Col-->
			<div class="col-xxl-12">
				@if($content->count())
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<table class="table table-striped mb-6">
									<thead>
										<tr>
											<th scope="col "><span class="fs-5 fw-bolder">Nome</span></th>
											<th scope="col "><span class="fs-5 fw-bolder">Ramal</span></th>
										</tr>
									</thead>
									<tbody>
										@foreach ($content as $branch)
										<tr>
											<td class="px-5">{{ $branch->name }}</td>
											<td class="px-5">{{ $branch->branch }}</td>
										</tr>	
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				@else
                <div class="row mb-9">
                    <div class="col-lg-12">
                        <!--begin::List Widget 5-->
                        <div class="card p-10 d-flex align-items-center justify-content-center">
                            <i class="las la-sad-cry fs-5x text-danger mb-5"></i>
                            <h3 class="m-0">Nenhum resultado encontrado</h3>
                        </div>
                        <!--end::List Widget 5-->
                    </div>
                </div>
                @endif
			</div>
			<!--end::Col-->
		</div>
	</div>
</div>
<!--end::Post-->

@endsection
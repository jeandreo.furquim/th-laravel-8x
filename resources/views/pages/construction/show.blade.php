@extends('layouts.app')

{{-- TITLE OF PAGE --}}
@section('title', $content->name)

{{-- TITLE OF POST --}}
@section('title-post', $content->name) 

{{-- LINK FOR RETURN --}}
@section('link-back', route('constructions.index'))

{{-- CONTENT --}}
@section('content')
<!-- POST -->
<div class="content flex-column-fluid">
	@include('includes.pageTitle')
	<div class="container">
		<div class="row gy-5 g-xl-8">
			<!-- CONTENT PAGE -->
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						@include('includes.imagePost')
						@include('includes.infosPost')
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						<div class="row">
							<div class="col mb-5">
								<b class="text-dark">Início:</b>
								<p class="mb-0 text-muted">{{ date_create($content->start)->format('d/m/Y') }}</p>
							</div>
							<div class="col mb-5">
								<b class="text-dark">Encerramento:</b>
								<p class="mb-0 text-muted">{{ date_create($content->end)->format('d/m/Y') }}</p>
							</div>
							<div class="col mb-5">
								<b class="text-dark">Previsão de custos:</b>
								<p class="mb-0 text-muted">{{ $content->cost }}</p>
							</div>
							<div class="col"></div>
							<div class="col"></div>
						</div>
						<p class="text-justify text-muted mb-0 description-post">
							{!! nl2br(e($content->description)) !!}
						</p>
					</div>
				</div>
			</div>
			<!-- CONTENT PAGE -->
			<!-- UPDATES -->
			@if($content->updates()->count())
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col">
								<h2 class="fw-bolder">Atualizações</h2>
								<div>
									@foreach ($content->updates()->get() as $update)
									<div class="card-header p-0" style="min-height: 0;">
										<h3 class="card-title align-items-start flex-column">
											<span class="mb-0 text-gray-800 text-hover-primary fs-4 fw-bolder me-1">{{ $update->title }}</span>
										</h3>
										<div class="card-toolbar">
											<span class="text-muted">{{ $content->created_at->format('d/m/Y') }}</span>
											<!--begin::Menu-->
											<button type="button" class="btn btn-sm btn-icon btn-color-muted btn-options-header" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
												<!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
												<span class="svg-icon svg-icon-2">
													<svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
														<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
															<rect x="5" y="5" width="5" height="5" rx="1" fill="#000000"></rect>
															<rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
															<rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
															<rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
														</g>
													</svg>
												</span>
												<!--end::Svg Icon-->
											</button>
												<!--begin::Menu 1-->
												<div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true">
												<!--begin::Form-->
												<div class="px-7 py-5">
													<!--begin::Input group-->
													<div class="">
														<!--begin::Label-->
														<label class="form-label fw-bold mb-5">O que deseja fazer com esse item?</label>
														<!--end::Label-->
														<!--begin::Input-->
														<div class="d-flex justify-content-around">
															{{-- <a href="#" class="btn btn-sm btn-light-success">Editar Dados</a> --}}
															<a href="{{  route('constructions.destroyUpdate', $update->id)  }}" class="btn btn-sm btn-light-danger">Deletar</a>
														</div>
														<!--end::Input-->
													</div>
													<!--end::Input group-->
												</div>
												<!--end::Form-->
											</div>
											<!--end::Menu 1-->
											<!--end::Menu-->
										</div>
									</div>
								<div class="card-body px-0 py-4">
									<p class="text-justify text-muted mb-0 description-post">{{ $update->description }}</p>
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
			</div>
			@endif
			<!-- UPDATES -->
			<!-- GALLERY -->
			@if($content->gallery()->count())
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col">
								<h2 class="fw-bolder">Galeria</h2>
								<div class="row">
									@foreach ($content->gallery()->get() as $file)
									<div class="col-lg-3">
										<a href={{ url("storage/condominios/$content->condominium/obras/$content->id/$file->image") }}>
											<img src={{ url("storage/condominios/$content->condominium/obras/$content->id/$file->image") }} class="w-100 shadow-1-strong rounded mb-4"/>
										</a>
										@if($content->user == Auth::user()->id)
										<a href="{{route('constructions.removeImage', $file->id)}}">
											<div class="remove-image">
												<span class="letter-close text-primary">X</span>
											</div>
										</a>
										@endif
									</div>
									@endforeach
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endif
			<!-- GALLERY -->
			<!-- COMENTS -->
			<div class="col-xxl-12">
				<!--begin::Feeds Widget 3-->
				<div class="card mb-5 mb-xxl-8">
					<!--begin::Body-->
					<div class="card-body pb-0">
						<h2 class="fw-bolder mb-5">Comentários</h2>
						<div id="load-comments">
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
						</div>
					</div>
					<!--end::Body-->
				</div>
				<!--end::Feeds Widget 3-->
			</div>
			<!-- COMENTS -->
		</div>
		<!-- SEND UPDATES/PHOTOS -->
		@if(in_array(Auth::user()->role, ['administrador']))
		<div class="d-flex">
			<button data-bs-toggle="modal" data-bs-target="#kt_modal_1" class="btn btn-danger text-white mr-4">Nova Atualização</button>
			<button data-bs-toggle="modal" data-bs-target="#kt_modal_2"  class="btn btn-danger text-white">Adicionar fotos</button>
		</div>
		<div class="modal fade" tabindex="-1" id="kt_modal_1">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Enviar atualização</h5>
						<!--begin::Close-->
						<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
							<span class="svg-icon svg-icon-2x"></span>
						</div>
						<!--end::Close-->
					</div>
					<div class="modal-body">
						<form action="{{ route('constructions.storeUpdate', $content->id) }}" class="form" method="post" enctype="multipart/form-data">
							@csrf
							<div class="row mb-8">
								<!--begin::Input group-->
								<div class="col">
									<!--begin::Label-->
									<label class="d-flex align-items-center fs-6 fw-bold mb-2">
									<span class="required">Título</span>
									</label>
									<!--end::Label-->
									<input type="text" class="form-control form-control-solid" placeholder="Pausa na obra por conta das fortes chuvas" name="title" maxlength="255"  />
								</div>
								<!--end::Input group-->
							</div>
							<div class="d-flex flex-column mb-8">
								<label class="fs-6 fw-bold mb-2">Descrição</label>
								<textarea class="form-control form-control-solid" rows="3" name="description" placeholder="Coloque o conteúdo aqui."></textarea>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-light" data-bs-dismiss="modal">Fechar</button>
							<button type="submit" class="btn btn-primary">
								<span class="indicator-label" >Enviar atualização</span>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="modal fade" tabindex="-1" id="kt_modal_2">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Enviar imagens</h5>
						<!--begin::Close-->
						<div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
							<span class="svg-icon svg-icon-2x"></span>
						</div>
						<!--end::Close-->
					</div>
					<div class="modal-body">
						<form action="{{ route('constructions.storeGallery', $content->id) }}" class="form" method="post" enctype="multipart/form-data">
							@csrf
							<div class="me-5 mb-2">
								<div class="fs-7 fw-bold text-muted">Selecione uma imagens para inserir na galeria dessa obra.</div>
							</div>
							<!--end::Label-->
							<!--begin::Switch-->
							<label class="form-check form-switch form-check-custom form-check-solid">
								<input class="form-control form-control-solid" name="image[]" type="file" multiple id="formFile" accept=".png, .jpg, .jpeg">
							</label>
							<!--end::Switch-->
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-light" data-bs-dismiss="modal">Fechar</button>
							<button type="submit" class="btn btn-primary">
								<span class="indicator-label" >Enviar imagens</span>
							</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		@endif
		<!-- SEND UPDATES/PHOTOS -->
	</div>
</div>
<!-- POST -->
@endsection

@section('custom-js')
<script>
	// LOAD AJAX
	sistemAjax(2, {{ $content->id }}, {{ $content->condominium }});
</script>
@endsection
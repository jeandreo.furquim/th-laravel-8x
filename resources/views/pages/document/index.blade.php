@extends('layouts.app')

@section('title', 'Visualizar Documentos')

@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">

                <div class="card-body">
                    <!--begin::Heading-->
                    <div class="mb-8 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-4">Visualizar Documentos</h1>
                        <!--end::Title-->
                        <div class="d-flex align-items-center justify-content-center">
                            <div class="">
                                <p class="text-muted fs-6 fw-bold text-center">
                                    Todos os documentos relativos à gestão do seu condomínio você encontra aqui. Desde regramentos internos, como Convenção Condominial e Regimento Interno, até Editais de Convocação, Contratos, Atas de Assembleias, Apólice de Seguro, Certificados, Relatórios etc..
                                    <b>Você pode apenas visualizá-los ou baixar/imprimir cópia.</b>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end::Heading-->
                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Link</th>
                                <th>Data de criação</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $content)
                                <tr>
                                    <td>{{ $content->id }}</td>
                                    <td>{{ $content->name }}</td>
                                    <td>
                                        @if($content->url)
                                        {{ $content->url }}
                                        @else
                                        -
                                        @endif
                                    </td>
                                    <td>{{ $content->created_at->format('d/m/Y H:i') }}</td>
                                    <td class="actions">
                                        <a href="{{ route('documents.show', $content->id) }}" class="document-ajax" data-bs-toggle="modal" data-bs-target="#modal_details"><i class="fas fa-eye" title="Visualizar"></i></a>
                                        @if(session('sessionCondominium'))
                                        <a href="{{ route('documents.edit', $content->id) }}"><i class="fas fa-pencil-alt" title="Editar"></i></a>
                                        <a href="{{ route('documents.destroy', $content->id) }}"><i class="fas fa-trash" title="Excluir"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @if(session('sessionCondominium'))
            <a href="{{ route('documents.create') }}">
                <button type="submit" class="btn btn-primary w-auto">
                    <span class="indicator-label" >Adicionar novo documento</span>
                </button>
            </a>
            @endif
            <!-- MODAL DE BUSCA -->
            <div class="modal fade" tabindex="-1" id="modal_details">  
                <div class="modal-dialog">
                    <div class="modal-content" id="modal-content">
                        
                    </div>
                </div>
            </div>
            <!-- END MODAL DE BUSCA -->
        </div>
    </div>
</div>


@endsection

@section('custom-js')
    @include('layouts.tables')
    <script>
        $('.document-ajax').click(function(e){
            e.preventDefault();
            let href = $(this).attr('href');
            $.get(href, function(response) {
                $('#modal-content').html(response); //Local em que vai ser aparecer
            });
        });
    </script>
@endsection
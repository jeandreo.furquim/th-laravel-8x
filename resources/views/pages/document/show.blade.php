{{-- TITLE OF PAGE --}}
@section('title', $content->name)

<!--begin::Post-->
<div class="row gy-5 g-xl-8">
	<!--begin::Col-->
	<div class="col-xxl-12">
		<!--begin::List Widget 5-->
		<div class="card card-xxl p-10">

			<!-- INFOS-POST -->
			@include('includes.infosPost')
			<!-- INFOS-POST -->

			<p class="text-justify text-muted mb-0 description-post">
				<h1 class="text-left">{{ $content->name }}</h1>
				<p class="text-justify text-muted mb-0 description-post">
					{!! nl2br(e($content->description)) !!}
				</p>

				@if($content->url)
				<div class="mt-3">
					<b>Link:</b> <a href={{ $content->url }} target=_blank>{{ $content->url }}</a>
				</div>
				@endif
				@if($content->file)
				<div class="mt-3">
					<b>Arquivo:</b> <a href={{ url('storage/condominios/' . $content->condominium . '/documentos/' . $content->file) }} target=_blank>{{ $content->file }}</a>
				</div>
				@endif
				<div class="mt-4">
					<a class="btn btn-info" href="{{ url("storage/condominios/$content->condominium/documentos/$content->file") }}" target="_blank">Baixar arquivo</a>
					<a class="btn btn-primary" href="{{ route('documents.edit', $content->id) }}">Editar</a>
					<a class="btn btn-danger" href="{{ route('documents.destroy', $content->id) }}">Deletar</a>
				</div>
			</p>
		</div>
		<!--end: List Widget 5-->
	</div>
	<!--end::Col-->
</div>
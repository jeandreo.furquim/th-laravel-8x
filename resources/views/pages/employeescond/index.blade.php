@extends('layouts.app')

@section('title', 'Todos funcionários')

@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-7 mt-7 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Todos os funcionários</h1>
                        <!--end::Title-->
                        
                    </div>
                    <!--end::Heading-->

                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Cargo</th>
                                <th>Horário</th>
                                <th>Dias liberados</th>
                                <th>Data de criação</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $content)
                                <tr>
                                    <td>{{ $content->id }}</td>
                                    <td>{{ $content->name }}</td>
                                    <td><b>{{ $content->office }}</b></td>
                                    <td>{{ $content->hour_start . ' - ' . $content->hour_end }}</td>
                                    <td>
                                        <div class="mt-1 d-flex">
                                            <div class="div-day-week {{ $content->monday ? "div-day-active" : "div-day-not-active" }}">
                                                <span class="day-week">S</span>
                                            </div>
                                            <div class="div-day-week {{ $content->tuesday ? "div-day-active" : "div-day-not-active" }}">
                                                <span class="day-week">T</span>
                                            </div>
                                            <div class="div-day-week {{ $content->wednesday ? "div-day-active" : "div-day-not-active" }}">
                                                <span class="day-week">Q</span>
                                            </div>
                                            <div class="div-day-week {{ $content->thursday ? "div-day-active" : "div-day-not-active" }}">
                                                <span class="day-week">Q</span>
                                            </div>
                                            <div class="div-day-week {{ $content->friday ? "div-day-active" : "div-day-not-active" }}">
                                                <span class="day-week">S</span>
                                            </div>
                                            <div class="div-day-week {{ $content->saturday ? "div-day-active" : "div-day-not-active" }}">
                                                <span class="day-week">S</span>
                                            </div>
                                            <div class="div-day-week {{ $content->sunday ? "div-day-active" : "div-day-not-active" }}">
                                                <span class="day-week">D</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>{{ $content->created_at->format('d/m/Y H:i') }}</td>
                                    <td class="actions">
                                        <a href="{{ route('employeescond.show', $content->id) }}"><i class="fas fa-eye" title="Visualizar"></i></a>
                                        @if(session('sessionCondominium'))
                                        <a href="{{ route('employeescond.edit', $content->id) }}"><i class="fas fa-pencil-alt" title="Editar"></i></a>
                                        <a href="{{ route('employeescond.destroy', $content->id) }}"><i class="fas fa-trash" title="Excluir"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @if(session('sessionCondominium'))
            <a href="{{ route('employeescond.create') }}">
                <button type="submit" class="btn btn-primary w-auto">
                    <span class="indicator-label" >Adicionar funcionário</span>
                </button>
            </a>
            @endif
        </div>
    </div>
</div>


@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
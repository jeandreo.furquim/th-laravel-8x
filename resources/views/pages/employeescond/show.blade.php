@extends('layouts.app')

{{-- TITLE OF PAGE --}}
@section('title', 'Funcionários')

{{-- TITLE OF POST --}}
@section('title-post', $content->name) 

{{-- LINK FOR RETURN --}}
@section('link-back', route('employeescond.index'))

{{-- CONTENT --}}
@section('content')
<!-- POST -->
<div class="content flex-column-fluid">
	@include('includes.pageTitle')
	<div class="container">
		<div class="row gy-5 g-xl-8">
			<!-- CONTENT PAGE -->
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<!-- IMAGE-POST -->
						@include('includes.imagePost')
						<!-- IMAGE-POST -->
						<p class="text-justify text-muted mb-0 description-post">
							<div class="row">
								<div class="col mb-5">
									<b class="text-dark">Nome:</b>
									<p class="mb-0 text-muted">{{ $content->name }}</p>
								</div>
								<div class="col mb-5">
									<b class="text-dark">Cargo:</b>
									<p class="mb-0 text-muted">{{ $content->office }}</p>
								</div>
								<div class="col">
									<b class="text-dark">Dias de trabalho:</b>
									<div class="mt-1 d-flex">
										<div class="div-day-week {{ $content->monday ? "div-day-active" : "div-day-not-active" }}">
											<span class="day-week">S</span>
										</div>
										<div class="div-day-week {{ $content->tuesday ? "div-day-active" : "div-day-not-active" }}">
											<span class="day-week">T</span>
										</div>
										<div class="div-day-week {{ $content->wednesday ? "div-day-active" : "div-day-not-active" }}">
											<span class="day-week">Q</span>
										</div>
										<div class="div-day-week {{ $content->thursday ? "div-day-active" : "div-day-not-active" }}">
											<span class="day-week">Q</span>
										</div>
										<div class="div-day-week {{ $content->friday ? "div-day-active" : "div-day-not-active" }}">
											<span class="day-week">S</span>
										</div>
										<div class="div-day-week {{ $content->saturday ? "div-day-active" : "div-day-not-active" }}">
											<span class="day-week">S</span>
										</div>
										<div class="div-day-week {{ $content->sunday ? "div-day-active" : "div-day-not-active" }}">
											<span class="day-week">D</span>
										</div>
									</div>
								</div>
								<div class="col">
									<b class="text-dark">Horário de atendimento:</b>
									<p class="mb-0 text-muted">{{ $content->hour_start . ' - ' . $content->hour_end }}</p>
								</div>
								<div class="col">
									<b class="text-dark">Cadastrado em:</b>
									<p class="mb-0 text-muted">{{ $content->created_at->format('d/m/Y H:i') }}</p>
								</div>
							</div>
								
							
							<p class="text-justify text-muted mt-4 mb-0 description-post">
								{{ $content->observation }}
							</p>
						</p>
					</div>
				</div>
			</div>
			<!-- CONTENT PAGE -->
		</div>
	</div>
</div>
<!-- POST -->
@endsection
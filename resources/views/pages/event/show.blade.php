@extends('layouts.app')

{{-- TITLE OF PAGE --}}
@section('title', $content->title)

{{-- TITLE OF POST --}}
@section('title-post', $content->title) 

{{-- LINK FOR RETURN --}}
@section('link-back', route('events.index'))

@section('custom-head')
<link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
@endsection

{{-- CONTENT --}}
@section('content')
<!-- POST -->
<div class="content flex-column-fluid">
	@include('includes.pageTitle')
	<div class="container">
		<div class="row gy-5 g-xl-8">
			<!-- CONTENT PAGE -->
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						@include('includes.imagePost')
						@include('includes.infosPost')
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						<p class="text-justify text-muted mb-0 description-post">
							<div>
								<b>Início: </b><span class="text-muted">{{ date_create($content->start)->format('d/m/Y') }}</span> - <b>Fim: </b><span class="text-muted mr-4">{{ date_create($content->end)->format('d/m/Y') }}</span>
								<b>Horário das: </b><span class="text-muted">{{ date_create($content->hour_start)->format('H:i') }}</span> <b>Até </b><span class="text-muted mr-4">{{ date_create($content->hour_end)->format('H:i') }}</span>
								@if($content->place)
								<b>Local: </b><span class="text-muted">{{ $content->place }}</span>
								@endif
								@if($content->document)
								<b class="ms-4">Arquivo: </b><a href="{{ url("storage/condominios/$content->condominium/eventos/$content->document") }}" target="_blank" class="text-muted">{{ $content->document }}</a>
								@endif
							</div>
							
							<p class="text-justify text-muted mb-0 description-post">
								{!! nl2br(e($content->description)) !!}
							</p>
							@if($content->presence()->where('event', $content->id)->where('user', Auth::user()->id)->count())
							<div>
								<b>Opção selecionada: </b><span class="text-muted">@if($content->presence()->first()->option == 1) Participarei @else Não participarei @endif</span>
							</div>
							@endif
							@if(date_create($content->end) > now())
							@if($content->presence()->where('event', $content->id)->where('user', Auth::user()->id)->count()) 
							<form action="{{ route('presences.update', $content->id) }}" class="form" method="post" enctype="multipart/form-data">
							@method('PUT')
							@else
							<form action="{{ route('presences.store', $content->id) }}" class="form" method="post" enctype="multipart/form-data">
							@endif
								@csrf
								<div class="col p-0 mb-3">
									<!--begin::Input group-->
									<div class="d-flex flex-column mb-4 fv-row mt-3">
										<div class="mb-0">
											<div class="row">
												<div class="col-12">
													<label for="" class="form-label">Selecione uma opção</label>
												</div>
												<div class="form-check form-check-custom form-check-solid ml-3 mb-3">
													<input name="option" value="1" id="option_01" class="form-check-input" type="radio" @if(!$content->presence()->first() || $content->presence()->first()->option == 1) checked @endif/>
													<label class="form-check-label" for="option_01">
														Confirmar presença
													</label>
												</div>
												<div class="form-check form-check-custom form-check-solid ml-3 mb-3">
													<input name="option" value="2" id="option_02" class="form-check-input" type="radio" @if($content->presence()->first() && $content->presence()->first()->option == 2) checked @endif/>
													<label class="form-check-label" for="option_02">
														Não irei comparecer
													</label>
												</div>
											</div>
										</div>
									</div>
									@if($content->presence()->where('event', $content->id)->where('user', Auth::user()->id)->count()) 
									<div class="mt-4">
										<button type="submit" class="btn btn-success">
											<span class="indicator-label" >Atualizar minha opção</span>
										</button>
									@else
									<div class="mt-4">
										<button type="submit" class="btn btn-success">
											<span class="indicator-label" >Registrar opção</span>
										</button>
									@endif
										<a class="btn btn-primary" href="{{ route('events.edit', $content->id) }}">Editar</a>
										<a class="btn btn-danger" href="{{ route('events.destroy', $content->id) }}">Deletar</a>
									</div>
								</div>
							</form>	
							@else
							<div class="alert alert-danger mt-5" role="alert">
								O evento já encerrou.
							</div>
							@endif
						</p>
					</div>
				</div>
			</div>
			<!-- CONTENT PAGE -->
			<!-- RESULTS -->
			@if(Auth::user()->role == 'administrador')
				<div class="col-xxl-12">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col">
									<table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
										<thead>
											<tr class="fw-bolder fs-6 text-gray-800 px-7">
												<th>ID</th>
												<th>Nome morador</th>
												<th>Status</th>
											</tr>
										</thead>
										<tbody>
											@foreach ($presences as $presence)
											<tr>
												<td>{{ $presence->id }}</td>
												<td>{{ $presence->author()->first()->name }}</td>
												@if($content->presence()->first()->option == 1) 
												<td><span class="badge badge-light-success fs-8 fw-bolder">Participarei</span></td> 
												@else 
												<td><span class="badge badge-light-danger fs-8 fw-bolder">Não participarei</span></td> 
												@endif
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			@endif
			<!-- RESULTS -->
			<!-- COMENTS -->
			<div class="col-xxl-12">
				<!--begin::Feeds Widget 3-->
				<div class="card mb-5 mb-xxl-8">
					<!--begin::Body-->
					<div class="card-body pb-0">
						<h2 class="fw-bolder mb-5">Comentários</h2>
						<div id="load-comments">
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
						</div>
					</div>
					<!--end::Body-->
				</div>
				<!--end::Feeds Widget 3-->
			</div>
			<!-- COMENTS -->
		</div>
	</div>
</div>
<!-- POST -->
@endsection


@section('custom-js')
@include('layouts.tables')
<script>
	// LOAD AJAX
	sistemAjax(3, {{ $content->id }}, {{ $content->condominium }});
</script>
@endsection
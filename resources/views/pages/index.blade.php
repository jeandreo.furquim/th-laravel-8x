@extends('layouts.app')

@section('title', 'Bem vindo ao seu condomínio')

@section('custom-head')
    <!--begin::Custom Stylesheets Nosidebar (Dreamake)-->
    <link href={{asset('assets/css/custom.nosidebar.css')}} rel="stylesheet" type="text/css" />
    <!--end::Custom Stylesheets Nosidebar (Dreamake)-->
@endsection

@section('content')
<div class="header-init d-flex align-items-center justify-content-center" style="background: url('{{ asset('assets/images/background-home-3.jpg') }}');"> 
    <div>
        <p class="page-title-dark text-center mb-1">Seja bem vindo(a),<br class="d-md-none"> {{ Auth::user()->name }}!</p>
        <p class="page-subtitle-dark text-center">Tudo sobre o seu condomínio você encontra aqui!</p>
    </div>
</div>
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0" id="kt_content">
    <div id="content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <!--begin::Col-->
            <div class="col-xxl-12 mb-9 options-section">
                <!--begin::Mixed Widget 2-->
                <div class="card">
                    <div class="row py-8 px-4">
                        <div class="col-6 col-md mb-5 mb-md-0">
                            <a href="{{ route('occurrences.create') }}">
                                <img src="{{asset('assets/images/icon-01.png')}}" class="d-flex m-auto icons-home">
                            </a>
                            <div class="mt-5 mb-0 mx-9 d-flex justify-content-center align-items-center">
                                <a href="{{ route('occurrences.create') }}" class="text-center text-uppercase font-weight-bold header-title">Abrir chamado</a>
                            </div>
                        </div>
                        <div class="col-6 col-md mb-5 mb-md-0">
                            <a href="{{ route('topics.create') }}">
                                <img src="{{asset('assets/images/icon-02.png')}}" class="d-flex m-auto icons-home">
                            </a>
                            <div class="mt-5 mb-0 d-flex justify-content-center align-items-center">
                                <a href="{{ route('topics.create') }}" class="text-center text-uppercase font-weight-bold header-title">Participar do mural</a>
                            </div>
                        </div>
                        <div class="col-6 col-md mb-5 mb-md-0">
                            <a href="{{ route('documents.index') }}">
                                <img src="{{asset('assets/images/icon-03.png')}}" class="d-flex m-auto icons-home">
                            </a>
                            <div class="mt-5 mb-0 d-flex justify-content-center align-items-center">
                                <a href="{{ route('documents.index') }}" class="text-center text-uppercase font-weight-bold header-title">Visualizar documentos</a>
                            </div>
                        </div>
                        <div class="col-6 col-md mb-5 mb-md-0">
                            <a href="{{ route('rooms.results') }}">
                                <img src="{{asset('assets/images/icon-06.png')}}" class="d-flex m-auto icons-home">
                            </a>
                            <div class="mt-5 mb-0 mx-9 d-flex justify-content-center align-items-center">
                                <a href="{{ route('rooms.results') }}" class="text-center text-uppercase font-weight-bold header-title">Reservar espaço</a>
                            </div>
                        </div>
                        <div class="col-12 col-md mb-5 mb-md-0">
                            <a href="{{ route('messages.index') }}">
                                <img src="{{asset('assets/images/icon-05.png')}}" class="d-flex m-auto icons-home">
                            </a>
                            <div class="mt-5 mb-0 d-flex justify-content-center align-items-center">
                                <a href="{{ route('messages.index') }}" class="text-center text-uppercase font-weight-bold header-title">Enviar mensagem</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row gy-5 g-xl-8">
            <!--begin::Col-->
            <div class="col-xxl-4">
                <!--begin::List Widget 5-->
                <div class="card mb-9">
                    <!--begin::Header-->
                    <div class="card-header align-items-center border-0 mt-0" style="background: url('{{ asset('assets/images/background-home-3.jpg') }}');">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="fw-bolder mb-0 text-light">Comunicados</span>
                        </h3>
                        <div class="card-toolbar">
                        </div>
                    </div>
                    <!--end::Header-->
                    <div class="height-cards">
                        @if($relases->count())
                        @foreach($relases as $relase)
                        <div class="divs-border">
                            <a href="{{ route('relases.show', $relase->id) }}" class="text-dark divs-home div-post-home">
                                <!--begin::Body-->
                                <div class="card-body p-5 posts-cards">
                                    <div class="">
                                        <div class="d-flex justify-content-between mb-2">
                                            <p class="fw-bolder mb-2 text-uppercase">Comunicado</p>
                                            <p class="text-muted mb-2" title="{{ $relase->created_at->format('d/m/Y H:i') }}">{{ $relase->created_at->format('d/m/Y') }}</p>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                                <img src="{{ imageUser($relase->user) }}" alt="user" class="rounded-circle">
                                            </div>
                                            <div class="d-block">
                                                <p class="fw-bolder mb-0">{{mb_strimwidth($relase->title, 0, 58, "...")}}</p>
                                                <p class="text-muted mb-0">{{mb_strimwidth($relase->author()->first()->name, 0, 35, "...")}}</p>
                                            </div>
                                        </div>
                                        <p class="text-justify text-muted mb-0">
                                        {{mb_strimwidth($relase->description, 0, 186, "...")}}
                                        </p>
                                    </div>
                                </div>
                                <!--end: Card Body-->
                            </a>
                        </div>
                        @endforeach
                        @else
                        <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                            <div class="">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com012.svg-->
                                <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M20 3H4C2.89543 3 2 3.89543 2 5V16C2 17.1046 2.89543 18 4 18H4.5C5.05228 18 5.5 18.4477 5.5 19V21.5052C5.5 22.1441 6.21212 22.5253 6.74376 22.1708L11.4885 19.0077C12.4741 18.3506 13.6321 18 14.8167 18H20C21.1046 18 22 17.1046 22 16V5C22 3.89543 21.1046 3 20 3Z" fill="black"/>
                                    <rect x="6" y="12" width="7" height="2" rx="1" fill="black"/>
                                    <rect x="6" y="7" width="12" height="2" rx="1" fill="black"/>
                                    </svg></p>
                                    <!--end::Svg Icon-->
                                <!--end::Svg Icon-->
                                <span class="text-muted fw-bold fs-5 ml-2">Não existem comunicados a serem exibidos</span>
                            </div>
                        </div>
                        @endif
                    </div>
                    <a href="{{ route('relases.results') }}" class="btn-posts-footer indicator-label d-flex align-items-center justify-content-center text-light">
                        Ver todos os comunicados
                    </a>
                </div>
                <!--end: List Widget 5-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-xxl-4">
                <!--begin::List Widget 5-->
                <div class="card mb-9">
                    <!--begin::Header-->
                    <div class="card-header align-items-center border-0 mt-0" style="background: url('{{ asset('assets/images/background-home-3.jpg') }}');">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="fw-bolder mb-0 text-light">Mural dos condômicos</span>
                        </h3>
                        <div class="card-toolbar">
                        </div>
                    </div>
                    <!--end::Header-->
                    <div class="height-cards">
                        @if($topics->count())
                        @foreach($topics as $topic)
                        <a href="{{ route('topics.show', $topic->id) }}" class="text-dark">
                            <!--begin::Body-->
                            <div class="card-body p-5 divs-home posts-cards">
                                <div class="">
                                    <div class="d-flex justify-content-between mb-2">
                                        <p class="fw-bolder mb-2 text-uppercase">{{ $topic->type }}</p>
                                        <p class="text-muted mb-2" title="{{ $topic->created_at->format('d/m/Y H:i') }}">{{ $topic->created_at->format('d/m/Y') }}</p>
                                    </div>
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                            <img src="{{ imageUser($topic->user) }}" alt="user" class="rounded-circle">
                                        </div>
                                        <div class="d-block">
                                            <p class="fw-bolder mb-0">{{mb_strimwidth($topic->title, 0, 58, "...")}}</p>
                                            <p class="text-muted mb-0">{{mb_strimwidth($topic->author()->first()->name, 0, 35, "...")}}</p>
                                        </div>
                                    </div>
                                    <p class="text-justify text-muted mb-0">
                                    {{mb_strimwidth($topic->description, 0, 125, "...")}}
                                    </p>
                                    @if($topic->image)
                                    <img src="{{ url("storage/condominios/$topic->condominium/topicos/$topic->image") }}" class="mt-5 mb-2 d-flex m-auto img-post-home rounded">
                                    @else
                                    <img src="{{ asset('assets/images/condominium-example-01.jpg') }}" class="mt-5 mb-2 d-flex m-auto img-post-home rounded">
                                    @endif
                                </div>
                            </div>
                            <!--end: Card Body-->
                        </a>
                        @endforeach
                        @else
                        <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                            <div class="">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/communication/com009.svg-->
                                <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M5.78001 21.115L3.28001 21.949C3.10897 22.0059 2.92548 22.0141 2.75004 21.9727C2.57461 21.9312 2.41416 21.8418 2.28669 21.7144C2.15923 21.5869 2.06975 21.4264 2.0283 21.251C1.98685 21.0755 1.99507 20.892 2.05201 20.7209L2.886 18.2209L7.22801 13.879L10.128 16.774L5.78001 21.115Z" fill="black"/>
                                    <path d="M21.7 8.08899L15.911 2.30005C15.8161 2.2049 15.7033 2.12939 15.5792 2.07788C15.455 2.02637 15.3219 1.99988 15.1875 1.99988C15.0531 1.99988 14.92 2.02637 14.7958 2.07788C14.6717 2.12939 14.5589 2.2049 14.464 2.30005L13.74 3.02295C13.548 3.21498 13.4402 3.4754 13.4402 3.74695C13.4402 4.01849 13.548 4.27892 13.74 4.47095L14.464 5.19397L11.303 8.35498C10.1615 7.80702 8.87825 7.62639 7.62985 7.83789C6.38145 8.04939 5.2293 8.64265 4.332 9.53601C4.14026 9.72817 4.03256 9.98855 4.03256 10.26C4.03256 10.5315 4.14026 10.7918 4.332 10.984L13.016 19.667C13.208 19.859 13.4684 19.9668 13.74 19.9668C14.0115 19.9668 14.272 19.859 14.464 19.667C15.3575 18.77 15.9509 17.618 16.1624 16.3698C16.374 15.1215 16.1932 13.8383 15.645 12.697L18.806 9.53601L19.529 10.26C19.721 10.452 19.9814 10.5598 20.253 10.5598C20.5245 10.5598 20.785 10.452 20.977 10.26L21.7 9.53601C21.7952 9.44108 21.8706 9.32825 21.9221 9.2041C21.9737 9.07995 22.0002 8.94691 22.0002 8.8125C22.0002 8.67809 21.9737 8.54505 21.9221 8.4209C21.8706 8.29675 21.7952 8.18392 21.7 8.08899Z" fill="black"/>
                                </svg></p>
                                <!--end::Svg Icon-->
                                <span class="text-muted fw-bold fs-5 ml-2">Não existem informações a serem exibidas</span>
                            </div>
                        </div>
                        @endif
                    </div>
                    <a href="{{ route('topics.results') }}" class="btn-posts-footer indicator-label d-flex align-items-center justify-content-center text-light">
                        Ver mural completo
                    </a>
                </div>
                <!--end: List Widget 5-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-xxl-4">
                <!--begin::List Widget 5-->
                <div class="card mb-9">
                    <!--begin::Header-->
                    <div class="card-header align-items-center border-0 mt-0" style="background: url('{{ asset('assets/images/background-home-3.jpg') }}');">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="fw-bolder mb-0 text-light">Chamados</span>
                        </h3>
                        <div class="card-toolbar">
                        </div>
                    </div>
                    <!--end::Header-->
                    <div class="height-cards-occurrence">
                        @if($occurrences->count())
                        @foreach($occurrences as $occurrence)
                        <a href="{{ route('occurrences.show', $occurrence->id) }}" class="text-dark divs-home div-post-home-ocorrence">
                            <!--begin::Body-->
                            <div class="card-body p-5 posts-cards">
                                <div class="">
                                    <div class="d-flex justify-content-between mb-2">
                                        <p class="fw-bolder mb-2 text-uppercase">Chamados</p>
                                        <p class="text-muted mb-2" title="{{ $occurrence->created_at->format('d/m/Y H:i') }}">{{ $occurrence->created_at->format('d/m/Y') }}</p>
                                    </div>
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                            <img src="{{ imageUser($occurrence->user) }}" alt="user" class="rounded-circle">
                                        </div>
                                        <div class="d-block">
                                            <p class="fw-bolder mb-0">{{mb_strimwidth($occurrence->title, 0, 58, "...")}}</p>
                                            <p class="text-muted mb-0">{{mb_strimwidth($occurrence->author()->first()->name, 0, 35, "...")}}</p>
                                        </div>
                                    </div>
                                    <p class="text-justify text-muted mb-0">
                                    {{mb_strimwidth($occurrence->description, 0, 126, "...")}}
                                    </p>
                                </div>
                            </div>
                            <!--end: Card Body-->
                        </a>
                        @endforeach
                        @else
                        <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                            <div class="">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen004.svg-->
                                <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M21.7 18.9L18.6 15.8C17.9 16.9 16.9 17.9 15.8 18.6L18.9 21.7C19.3 22.1 19.9 22.1 20.3 21.7L21.7 20.3C22.1 19.9 22.1 19.3 21.7 18.9Z" fill="black"/>
                                <path opacity="0.3" d="M11 20C6 20 2 16 2 11C2 6 6 2 11 2C16 2 20 6 20 11C20 16 16 20 11 20ZM11 4C7.1 4 4 7.1 4 11C4 14.9 7.1 18 11 18C14.9 18 18 14.9 18 11C18 7.1 14.9 4 11 4ZM8 11C8 9.3 9.3 8 11 8C11.6 8 12 7.6 12 7C12 6.4 11.6 6 11 6C8.2 6 6 8.2 6 11C6 11.6 6.4 12 7 12C7.6 12 8 11.6 8 11Z" fill="black"/>
                                </svg></p>
                                <!--end::Svg Icon-->
                                <span class="text-muted fw-bold fs-5 ml-2">Não existem chamados a serem exibidos</span>
                            </div>
                        </div>
                        @endif
                    </div>
                    <a href="{{ route('occurrences.results') }}" class="btn-posts-footer indicator-label d-flex align-items-center justify-content-center text-light">
                        Ver todos os chamados
                    </a>
                </div>
                <!--end: List Widget 5-->
                <!--begin::List Widget 5-->
                <div class="card mb-9">
                    <!--begin::Header-->
                    <div class="card-header align-items-center border-0 mt-0" style="background: url('{{ asset('assets/images/background-home-3.jpg') }}');">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="fw-bolder mb-0 text-light">Documentos</span>
                        </h3>
                        <div class="card-toolbar">
                        </div>
                    </div>
                    <!--end::Header-->
                    <div class="height-cards-document">
                        @if($documents->count())
                        @foreach ($documents as $document)
                        <a href="{{ route('documents.show', $document->id) }}" class="text-dark divs-home div-post-home-documents">
                            <!--begin::Body-->
                            <div class="card-body p-5 posts-cards">
                                <div class="">
                                    <div class="d-flex justify-content-between mb-2">
                                        <p class="fw-bolder mb-2 text-uppercase">Documento</p>
                                        <p class="text-muted mb-2" title="{{ $document->created_at->format('d/m/Y H:i') }}">{{ $document->created_at->format('d/m/Y') }}</p>
                                    </div>
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                            <img src="{{ imageUser($document->user) }}" alt="user" class="rounded-circle">
                                        </div>
                                        <div class="d-block">
                                            <p class="fw-bolder mb-0">{{mb_strimwidth($document->name, 0, 58, "...")}}</p>
                                            <p class="text-muted mb-0">{{mb_strimwidth($document->author()->first()->name, 0, 35, "...")}}</p>
                                        </div>
                                    </div>
                                    <p class="text-justify text-muted mb-0">
                                    {{mb_strimwidth($document->description, 0, 126, "...")}}
                                    </p>
                                </div>
                            </div>
                            <!--end: Card Body-->
                        </a>
                        @endforeach
                        @else
                        <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                            <div class="">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen005.svg-->
                                <span class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM15 17C15 16.4 14.6 16 14 16H8C7.4 16 7 16.4 7 17C7 17.6 7.4 18 8 18H14C14.6 18 15 17.6 15 17ZM17 12C17 11.4 16.6 11 16 11H8C7.4 11 7 11.4 7 12C7 12.6 7.4 13 8 13H16C16.6 13 17 12.6 17 12ZM17 7C17 6.4 16.6 6 16 6H8C7.4 6 7 6.4 7 7C7 7.6 7.4 8 8 8H16C16.6 8 17 7.6 17 7Z" fill="black"/>
                                <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black"/>
                                </svg></span>
                                <!--end::Svg Icon-->
                                <span class="text-muted fw-bold fs-5 ml-2">Não existem documentos a serem exibidos</span>
                            </div>
                        </div>
                        @endif
                    </div>
                    <a href="{{ route('documents.index') }}" class="btn-posts-footer indicator-label d-flex align-items-center justify-content-center text-light">
                        Ver todos os documentos
                    </a>
                </div>
                <!--end: List Widget 5-->
            </div>
            <!--end::Col-->
        </div>
        <!--end::Row-->
        <!--begin::Row-->
        <div class="row gy-5 g-xl-8">
            <!--begin::Col-->
            <div class="col-xxl-4">
                <!--begin::List Widget 5-->
                <div class="card mb-9">
                    <!--begin::Header-->
                    <div class="card-header align-items-center border-0 mt-0" style="background: url('{{ asset('assets/images/background-home-3.jpg') }}');">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="fw-bolder mb-0 text-light">Votações / Enquetes</span>
                        </h3>
                        <div class="card-toolbar">
                        </div>
                    </div>
                    <!--end::Header-->
                    <div class="height-cards">
                        @if($surveys->count())
                        @foreach ($surveys as $survey)
                        <div class="divs-border">
                            <a href="{{ route('surveys.show', $survey->id) }}" class="text-dark divs-home div-post-home">
                                <!--begin::Body-->
                                <div class="card-body p-5 posts-cards">
                                    <div class="">
                                        <div class="d-flex justify-content-between mb-2">
                                            <p class="fw-bolder mb-2 text-uppercase">Enquete</p>
                                            <p class="text-muted mb-2" title="{{ $survey->created_at->format('d/m/Y H:i') }}">{{ $survey->created_at->format('d/m/Y') }}</p>
                                        </div>
                                        <div class="d-flex align-items-center mb-3">
                                            <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                                <img src="{{ imageUser($survey->user) }}" alt="user" class="rounded-circle">
                                            </div>
                                            <div class="d-block">
                                                <p class="fw-bolder mb-0">{{mb_strimwidth($survey->title, 0, 58, "...")}}</p>
                                                <p class="text-muted mb-0">{{mb_strimwidth($survey->author()->first()->name, 0, 35, "...")}}</p>
                                            </div>
                                        </div>
                                        <p class="text-justify text-muted mb-0">
                                        {{mb_strimwidth($survey->description, 0, 186, "...")}}
                                        </p>
                                    </div>
                                </div>
                                <!--end: Card Body-->
                            </a>
                        </div>
                        @endforeach
                        @else
                        <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                            <div class="">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/graphs/gra001.svg-->
                                <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M14 3V21H10V3C10 2.4 10.4 2 11 2H13C13.6 2 14 2.4 14 3ZM7 14H5C4.4 14 4 14.4 4 15V21H8V15C8 14.4 7.6 14 7 14Z" fill="black"/>
                                <path d="M21 20H20V8C20 7.4 19.6 7 19 7H17C16.4 7 16 7.4 16 8V20H3C2.4 20 2 20.4 2 21C2 21.6 2.4 22 3 22H21C21.6 22 22 21.6 22 21C22 20.4 21.6 20 21 20Z" fill="black"/>
                                </svg></p>
                                <!--end::Svg Icon-->
                                <span class="text-muted fw-bold fs-5 ml-2">Não existem enquetes a serem exibidas</span>
                            </div>
                        </div>
                        @endif
                    </div>
                    <a href="{{ route('surveys.results') }}" class="btn-posts-footer indicator-label d-flex align-items-center justify-content-center text-light">
                        Ver todas as enquetes
                    </a>
                </div>
                <!--end: List Widget 5-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-xxl-4">
                <!--begin::List Widget 5-->
                <div class="card mb-9">
                    <!--begin::Header-->
                    <div class="card-header align-items-center border-0 mt-0" style="background: url('{{ asset('assets/images/background-home-3.jpg') }}');">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="fw-bolder mb-0 text-light">Eventos</span>
                        </h3>
                        <div class="card-toolbar">
                        </div>
                    </div>
                    <!--end::Header-->
                    <div class="height-cards">
                        @if($events->count())
                        @foreach($events as $event)
                        <a href="{{ route('events.show', $event->id) }}" class="text-dark">
                            <!--begin::Body-->
                            <div class="card-body p-5 divs-home posts-cards">
                                <div class="">
                                    <div class="d-flex justify-content-between mb-2">
                                        <p class="fw-bolder mb-2 text-uppercase">Eventos</p>
                                        <p class="text-muted mb-2" title="{{ $event->created_at->format('d/m/Y H:i') }}">{{ $event->created_at->format('d/m/Y') }}</p>
                                    </div>
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                            <img src="{{ imageUser($event->user) }}" alt="user" class="rounded-circle">
                                        </div>
                                        <div class="d-block">
                                            <p class="fw-bolder mb-0">{{mb_strimwidth($event->title, 0, 58, "...")}}</p>
                                            <p class="text-muted mb-0">{{mb_strimwidth($event->author()->first()->name, 0, 35, "...")}}</p>
                                        </div>
                                    </div>
                                    <p class="text-justify text-muted mb-0">
                                    {{mb_strimwidth($event->description, 0, 125, "...")}}
                                    </p>
                                    @if($event->image)
                                    <img src="{{ url("storage/condominios/$event->condominium/eventos/$event->image") }}" class="mt-5 mb-2 d-flex m-auto img-post-home rounded">
                                    @else
                                    <img src="{{ asset('assets/images/condominium-example-01.jpg') }}" class="mt-5 mb-2 d-flex m-auto img-post-home rounded">
                                    @endif
                                </div>
                            </div>
                            <!--end: Card Body-->
                        </a>
                        @endforeach
                        @else
                        <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                            <div class="">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen014.svg-->
                                <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="black"/>
                                <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="black"/>
                                <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="black"/>
                                </svg></p>
                                <!--end::Svg Icon-->
                                <span class="text-muted fw-bold fs-5 ml-2">Não existem eventos a serem exibidos</span>
                            </div>
                        </div>
                        @endif
                    </div>
                    <a href="{{ route('events.results') }}" class="btn-posts-footer indicator-label d-flex align-items-center justify-content-center text-light">
                        Ver todos os eventos
                    </a>
                </div>
                <!--end: List Widget 5-->
            </div>
            <!--end::Col-->
            <!--begin::Col-->
            <div class="col-xxl-4">
                <!--begin::List Widget 5-->
                <div class="card mb-9">
                    <!--begin::Header-->
                    <div class="card-header align-items-center border-0 mt-0" style="background: url('{{ asset('assets/images/background-home-3.jpg') }}');">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="fw-bolder mb-0 text-light">Álbuns</span>
                        </h3>
                        <div class="card-toolbar">
                        </div>
                    </div>
                    <!--end::Header-->
                    <div class="height-cards">
                        @if($albums->count())
                        @foreach($albums as $album)
                        <a href="{{ route('albums.show', $album->id) }}" class="text-dark">
                            <!--begin::Body-->
                            <div class="card-body p-5 divs-home posts-cards">
                                <div class="">
                                    <div class="d-flex justify-content-between mb-2">
                                        <p class="fw-bolder mb-2 text-uppercase">Eventos</p>
                                        <p class="text-muted mb-2" title="{{ $album->created_at->format('d/m/Y H:i') }}">{{ $album->created_at->format('d/m/Y') }}</p>
                                    </div>
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                            <img src="{{ imageUser($album->user) }}" alt="user" class="rounded-circle">
                                        </div>
                                        <div class="d-block">
                                            <p class="fw-bolder mb-0">{{mb_strimwidth($album->name, 0, 58, "...")}}</p>
                                            <p class="text-muted mb-0">{{mb_strimwidth($album->author()->first()->name, 0, 35, "...")}}</p>
                                        </div>
                                    </div>
                                    <p class="text-justify text-muted mb-0">
                                    {{mb_strimwidth($album->description, 0, 125, "...")}}
                                    </p>
                                    @if($album->image)
                                    <img src="{{ url("storage/condominios/" . $album->condominium . "/albuns/" . $album->id . "/" . $album->image) }}" class="mt-5 mb-2 d-flex m-auto img-post-home rounded">
                                    @else
                                    <img src="{{ asset('assets/images/condominium-example-01.jpg') }}" class="mt-5 mb-2 d-flex m-auto img-post-home rounded">
                                    @endif
                                </div>
                            </div>
                            <!--end: Card Body-->
                        </a>
                        @endforeach
                        @else
                        <div class="w-100 h-100 d-flex align-items-center justify-content-center">
                            <div class="">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen006.svg-->
                                <p class="svg-icon svg-icon-muted svg-icon-3hx text-center"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path opacity="0.3" d="M22 5V19C22 19.6 21.6 20 21 20H19.5L11.9 12.4C11.5 12 10.9 12 10.5 12.4L3 20C2.5 20 2 19.5 2 19V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5ZM7.5 7C6.7 7 6 7.7 6 8.5C6 9.3 6.7 10 7.5 10C8.3 10 9 9.3 9 8.5C9 7.7 8.3 7 7.5 7Z" fill="black"/>
                                <path d="M19.1 10C18.7 9.60001 18.1 9.60001 17.7 10L10.7 17H2V19C2 19.6 2.4 20 3 20H21C21.6 20 22 19.6 22 19V12.9L19.1 10Z" fill="black"/>
                                </svg></p>
                                <!--end::Svg Icon-->
                                <span class="text-muted fw-bold fs-5 ml-2">Não existem álbuns a serem exibidos</span>
                            </div>
                        </div>
                        @endif
                    </div>
                    <a href="{{ route('albums.results') }}" class="btn-posts-footer indicator-label d-flex align-items-center justify-content-center text-light">
                        Ver todos os álbuns
                    </a>
                </div>
                <!--end: List Widget 5-->
            </div>
            <!--end::Col-->
        </div>
        <!--end::Row-->
    </div>
</div>
@endsection
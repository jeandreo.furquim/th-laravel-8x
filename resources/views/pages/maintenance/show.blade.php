@extends('layouts.app')

{{-- TITLE OF PAGE --}}
@section('title', $content->name)

{{-- TITLE OF POST --}}
@section('title-post', $content->name) 

{{-- LINK FOR RETURN --}}
@section('link-back', route('maintenances.index'))

{{-- CONTENT --}}
@section('content')
<!-- POST -->
<div class="content flex-column-fluid">
	@include('includes.pageTitle')
	<div class="container">
		<div class="row gy-5 g-xl-8">
			<!-- CONTENT PAGE -->
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<!-- INFOS-POST -->
						@include('includes.infosPost')
						<!-- INFOS-POST -->
						<p class="text-justify text-muted mb-0 description-post">
							<div class="row">
								<div class="col mb-5">
									<b class="text-dark">Início:</b>
									<p class="mb-0 text-muted">{{ date_create($content->start)->format('d/m/Y') }}</p>
								</div>
								<div class="col mb-5">
									<b class="text-dark">Encerramento:</b>
									<p class="mb-0 text-muted">{{ date_create($content->start)->format('d/m/Y') }}</p>
								</div>
								<div class="col">
									<b class="text-dark">Horário de início:</b>
									<p class="mb-0 text-muted">{{ $content->hour_start }}</p>
								</div>
								<div class="col">
									<b class="text-dark">Horário de termino:</b>
									<p class="mb-0 text-muted">{{ $content->hour_end }}</p>
								</div>
								<div class="col"></div>
								<div class="col"></div>
								<div class="col"></div>
							</div>
							
							<p class="text-justify text-muted mb-0 description-post">
								{!! nl2br(e($content->description)) !!}
							</p>
						</p>
					</div>
				</div>
			</div>
			<!-- COMENTS -->
			<div class="col-xxl-12">
				<!--begin::Feeds Widget 3-->
				<div class="card mb-5 mb-xxl-8">
					<!--begin::Body-->
					<div class="card-body pb-0">
						<h2 class="fw-bolder mb-5">Comentários</h2>
						<div id="load-comments">
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
						</div>
					</div>
					<!--end::Body-->
				</div>
				<!--end::Feeds Widget 3-->
			</div>
			<!-- COMENTS -->
			<!-- CONTENT PAGE -->
		</div>
	</div>
</div>
<!-- POST -->
@endsection

@section('custom-js')
<script>
	// LOAD AJAX
	sistemAjax(4, {{ $content->id }}, {{ $content->condominium }});
</script>
@endsection
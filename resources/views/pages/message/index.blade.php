@extends('layouts.app')

{{-- Title of page --}}
@section('title', 'Chat')

{{-- Title of post --}}
@section('title-post', 'Chat') 

{{-- Content --}}
@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <!--begin::Col-->
            <div class="col-xxl-12">
                <!--begin::Container-->
                <div id="" class="container-xxl">
                    <!--begin::Layout-->
                    <div class="d-flex flex-column flex-lg-row">
                        <!--begin::Sidebar-->
                        <div class="flex-column flex-lg-row-auto w-100 w-lg-300px w-xl-400px mb-10 mb-lg-0">
                            <!--begin::Contacts-->
                            <div class="card card-flush">
                                <!--begin::Card header-->
                                <div class="card-header pt-7" id="kt_chat_contacts_header">
                                    <!--begin::Form-->
                                    <form class="w-100 position-relative" autocomplete="off">
                                        <!--begin::Icon-->
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                                        <span class="svg-icon svg-icon-2 svg-icon-lg-1 svg-icon-gray-500 position-absolute top-50 ms-5 translate-middle-y">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                                <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                        <!--end::Icon-->
                                        <!--begin::Input-->
                                        <input type="text" class="form-control form-control-solid px-15" id="search-here" name="search" value="" placeholder="Pesquise o usuário aqui..." />
                                        <!--end::Input-->
                                    </form>
                                    <!--end::Form-->
                                </div>
                                <!--end::Card header-->
                                <!--begin::Card body-->
                                <div class="card-body pt-5" id="kt_chat_contacts_body">
                                    <!--begin::List-->
                                    <div class="scroll-y me-n5 pe-5 h-200px h-lg-auto" id="search-users" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="485px" data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_contacts_header" data-kt-scroll-wrappers="#kt_content, #kt_chat_contacts_body" data-kt-scroll-offset="0px">
                                        @foreach($peoples as $people)
                                        <!--begin::User-->
                                        <div class="d-flex flex-stack py-4">
                                            <!--begin::Details-->
                                            <div class="d-flex align-items-center">
                                                
                                                @if($people->msgsUser()->where('to', Auth::id())->whereNull('see')->count() >0)
                                                <span class="badge badge-sm badge-circle badge-light-primary position-absolute" style="margin-top: -30px;z-index: 9999;margin-left: 30px;">{{$people->msgsUser()->whereNull('see')->where('to', Auth::id())->count()}}</span>
                                                @endif
                                                <!--begin::Avatar-->
                                                <a href="{{ route('messages.index', $people->id) }}" class="symbol symbol-45px symbol-circle">
                                                    <img alt="Pic" src="{{ imageUser($people->id) }}" />
                                                </a>
                                                <!--end::Avatar-->
                                                <!--begin::Details-->
                                                <div class="ms-5">
                                                    <a href="{{ route('messages.index', $people->id) }}" class="fs-5 fw-bolder text-gray-800 text-hover-primary mb-2">@if(strlen($people->name) > 23){{ substr($people->name, 0, 20) . '...';}}@else {{ $people->name }} @endif</a>
                                                    <div><a href="{{ route('messages.index', $people->id) }}" class="fw-bold text-muted">@if(strlen($people->email) > 23){{ substr($people->email, 0, 20) . '...';}}@else {{ $people->email }} @endif</a></div>
                                                </div>
                                                <!--end::Details-->
                                            </div>
                                            <!--end::Details-->
                                            <!--begin::Lat seen-->
                                            <div class="d-flex flex-column align-items-end ms-2">
                                                <a href="{{ route('messages.index', $people->id) }}" class="text-muted fs-7 mb-1">{{ days($people->last_login_at) }}</a>
                                            </div>
                                            <!--end::Lat seen-->
                                        </div>
                                        <!--end::User-->
                                        <!--begin::Separator-->
                                        <div class="separator separator-dashed d-none"></div>
                                        <!--end::Separator-->
                                        @endforeach
                                    </div>
                                    <!--end::List-->
                                </div>
                                <!--end::Card body-->
                            </div>
                            <!--end::Contacts-->
                        </div>
                        <!--end::Sidebar-->
                        <!--begin::Content-->
                        <div class="flex-lg-row-fluid ms-lg-7 ms-xl-10">
                            @if($user)
                            <!--begin::Messenger-->
                            <div class="card" id="kt_chat_messenger">
                                <!--begin::Card header-->
                                <div class="card-header" id="kt_chat_messenger_header">
                                    <!--begin::Title-->
                                    <div class="card-title">
                                        <!--begin::User-->
                                        <div class="d-flex justify-content-center flex-column me-3">
                                            <span href="" class="fs-4 fw-bolder text-gray-800 me-1 mb-2 lh-1">{{ $user->name }}</span>
                                        </div>
                                        <!--end::User-->
                                    </div>
                                    <!--end::Title-->
                                </div>
                                <!--end::Card header-->
                                <!--begin::Card body-->
                                <div class="card-body" id="kt_chat_messenger_body">
                                    <!--begin::Messages-->
                                    <div class="scroll-y me-n5 pe-5 h-300px h-lg-auto" id="msgs_chat" data-kt-element="messages" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="339px" style="min-height: 339px" data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer" data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body" data-kt-scroll-offset="-2px">
                                        <div class="d-flex align-items-center justify-content-center">
                                            <div class="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-center" data-kt-element="message-text">Carregando...</div>
                                        </div>
                                    </div>
                                    <!--end::Messages-->
                                </div>
                                <!--end::Card body-->
                                <!--begin::Card footer-->
                                <div class="card-footer pt-4" id="">
                                    <form action="{{ route('messages.store', $user->id) }}" class="form" method="post" data-url="{{ route('messages.show', $user->id) }}" enctype="multipart/form-data" id="send-message">
                                        @csrf
                                        <!--begin::Input-->
                                        <input class="form-control form-control-flush mb-3 rounded" rows="1" name="message" id="placetxt" placeholder="Digite sua mensagem" style="background: #fbfbfb;"></input>
                                        <!--end::Input-->
                                        <!--begin:Toolbar-->
                                        <div class="d-flex flex-stack">
                                            <!--begin::Actions-->
                                            <div class="d-flex align-items-center me-2">
                                                <button class="btn btn-sm btn-icon btn-active-light-primary me-1" type="button" data-bs-toggle="tooltip" title="Coming soon" style="display: none !important">
                                                    <i class="bi bi-paperclip fs-3"></i>
                                                </button>
                                                <button class="btn btn-sm btn-icon btn-active-light-primary me-1" type="button" data-bs-toggle="tooltip" title="Coming soon" style="display: none !important">
                                                    <i class="bi bi-upload fs-3"></i>
                                                </button>
                                            </div>
                                            <!--end::Actions-->
                                            <!--begin::Send-->
                                            <button class="btn btn-primary" type="send" id="send-msg-chat">Enviar</button>
                                            <!--end::Send-->
                                        </div>
                                        <!--end::Toolbar-->
                                    </form>
                                </div>
                                <!--end::Card footer-->
                            </div>
                            <!--end::Messenger-->
                            @else
                            <!--begin::Messenger-->
                            <div class="card" id="kt_chat_messenger">
                                <!--begin::Card header-->
                                <div class="card-header" id="kt_chat_messenger_header">
                                    <!--begin::Title-->
                                    <div class="card-title">
                                        <!--begin::User-->
                                        <div class="d-flex justify-content-center flex-column me-3">
                                            <span href="" class="fs-4 fw-bolder text-gray-800 me-1 mb-2 lh-1">Selecione um usuário para conversar</span>
                                        </div>
                                        <!--end::User-->
                                    </div>
                                    <!--end::Title-->
                                </div>
                                <!--end::Card header-->
                                <!--begin::Card body-->
                                <div class="card-body" id="kt_chat_messenger_body">
                                    <!--begin::Messages-->
                                    <div class="scroll-y me-n5 pe-5 h-300px h-lg-auto" id="msgs_chat" data-kt-element="messages" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="339px" style="min-height: 339px" data-kt-scroll-dependencies="#kt_header, #kt_toolbar, #kt_footer, #kt_chat_messenger_header, #kt_chat_messenger_footer" data-kt-scroll-wrappers="#kt_content, #kt_chat_messenger_body" data-kt-scroll-offset="-2px">
                                        <div class="d-flex align-items-center justify-content-center">
                                            <div class="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-center" data-kt-element="message-text">Carregando...</div>
                                        </div>
                                    </div>
                                    <!--end::Messages-->
                                </div>
                                <!--end::Card body-->
                                <!--begin::Card footer-->
                                <div class="card-footer pt-4" id="">
                                    <form action="{{ route('messages.store', 0) }}" class="form" method="post" data-url="{{ route('messages.show', 0) }}" enctype="multipart/form-data" id="send-message">
                                        @csrf
                                        <!--begin::Input-->
                                        <input class="form-control form-control-flush mb-3 rounded" rows="1" disabled name="message" id="placetxt" placeholder="Digite sua mensagem" style="background: #fbfbfb;"></input>
                                        <!--end::Input-->
                                        <!--begin:Toolbar-->
                                        <div class="d-flex justify-content-end">
                                            <!--begin::Send-->
                                            <button class="btn btn-primary" disabled type="send" id="send-msg-chat">Enviar</button>
                                            <!--end::Send-->
                                        </div>
                                        <!--end::Toolbar-->
                                    </form>
                                </div>
                                <!--end::Card footer-->
                            </div>
                            <!--end::Messenger-->
                            @endif
                        </div>
                        <!--end::Content-->
                    </div>
                    <!--end::Layout-->
                </div>
                <!--end::Container-->
            </div>
            <!--end::Col-->
        </div>
    </div>
</div>
@endsection

@section('custom-js')

<script>

    $(document).ready(function(){

        ajaxCall();

        function ajaxCall() {
            var url = $("#send-message").data('url');
            $.ajax({
                type: "GET",
                url: url,
                success: function(data){
                    $('#msgs_chat').html(data);
                    console.log(url);
                }
            });
        }

        setInterval(ajaxCall, 1000 * 10); //15 second

        $("#send-msg-chat").click(function(e){ 
            e.preventDefault();
            var form = $('#send-message');
            var actionUrl = form.attr('action');
            console.log(actionUrl);
            $.ajax({
                type: "GET",
                url: actionUrl,
                data: form.serialize(), // serializes the form's elements.
                success: function(data){
                    $('#placetxt').val('');
                    ajaxCall();
                    console.log('Enviou mensagem');
                }
            });
        });

        $("#search-here").keyup(function(e){

            var val = $(this).val();

            if(val != ''){
                var url = "{{route('messages.search', '')}}"+"/"+val;
            } else {
                var url = "{{route('messages.search', '')}}"+"/all";
            }

            $.ajax({
                type: "GET",
                url: url,
                success: function(data){
                    $('#search-users').html(data);
                }
            });
        });
        
    });    

</script>
@endsection
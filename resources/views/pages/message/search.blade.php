@if($contents->count())
@foreach($contents as $people)
<!--begin::User-->
<div class="d-flex flex-stack py-4">
	<!--begin::Details-->
	<div class="d-flex align-items-center">
		<!--begin::Avatar-->
		<a href="{{ route('messages.index', $people->id) }}" class="symbol symbol-45px symbol-circle">
			<img alt="Pic" src="{{ imageUser($people->id) }}" />
		</a>
		<!--end::Avatar-->
		<!--begin::Details-->
		<div class="ms-5">
			<a href="{{ route('messages.index', $people->id) }}" class="fs-5 fw-bolder text-gray-800 text-hover-primary mb-2">@if(strlen($people->name) > 23){{ substr($people->name, 0, 20) . '...';}}@else {{ $people->name }} @endif</a>
			<div><a href="{{ route('messages.index', $people->id) }}" class="fw-bold text-muted">@if(strlen($people->email) > 23){{ substr($people->email, 0, 20) . '...';}}@else {{ $people->email }} @endif</a></div>
		</div>
		<!--end::Details-->
	</div>
	<!--end::Details-->
	<!--begin::Lat seen-->
	<div class="d-flex flex-column align-items-end ms-2">
		<a href="{{ route('messages.index', $people->id) }}" class="text-muted fs-7 mb-1">{{ days($people->last_login_at) }}</a>
	</div>
	<!--end::Lat seen-->
</div>
<!--end::User-->
<!--begin::Separator-->
<div class="separator separator-dashed d-none"></div>
<!--end::Separator-->
@endforeach
@else
<div class="d-flex align-items-center justify-content-center">
	<div class="p-5 rounded bg-light text-dark fw-bold mw-lg-400px text-center" data-kt-element="message-text">Nenhum resultado encontrado.</div>
</div>
@endif
@if($contents->count())
@foreach ($contents as $msg)
	@if($msg->from == Auth::user()->id)
	<!--begin::Message(out)-->
	<div class="d-flex justify-content-end mb-10">
		<!--begin::Wrapper-->
		<div class="d-flex flex-column align-items-end">
			<!--begin::User-->
			<div class="d-flex align-items-center mb-2">
				<!--begin::Details-->
				<div class="me-3">
					<span class="text-muted fs-7 mb-1">{{ date_format($msg->created_at, 'H:i') }}</span>
					<span class="fs-5 fw-bolder text-gray-800 ms-1">Você</span>
				</div>
				<!--end::Details-->
				<!--begin::Avatar-->
				<div class="symbol symbol-35px symbol-circle">
					<img alt="Pic" src="{{ imageUser(Auth::id()) }}" />
				</div>
				<!--end::Avatar-->
			</div>
			<!--end::User-->
			<!--begin::Text-->
			<div class="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-end" data-kt-element="message-text">{{ $msg->message }}</div>
			<!--end::Text-->
		</div>
		<!--end::Wrapper-->
	</div>
	<!--end::Message(out)-->
	@else
	<!--begin::Message(in)-->
	<div class="d-flex justify-content-start mb-10">
		<!--begin::Wrapper-->
		<div class="d-flex flex-column align-items-start">
			<!--begin::User-->
			<div class="d-flex align-items-center mb-2">
				<!--begin::Avatar-->
				<div class="symbol symbol-35px symbol-circle">
					<img alt="Pic" src="{{ imageUser($msg->from) }}" />
				</div>
				<!--end::Avatar-->
				<!--begin::Details-->
				<div class="ms-3">
					<span class="fs-5 fw-bolder text-gray-800 me-1">{{ $msg->from()->first()->name }}</span>
					<span class="text-muted fs-7 mb-1">{{ date_format($msg->created_at, 'H:i') }}</span>
				</div>
				<!--end::Details-->
			</div>
			<!--end::User-->
			<!--begin::Text-->
			<div class="p-5 rounded bg-light-info text-dark fw-bold mw-lg-400px text-start" data-kt-element="message-text">{{ $msg->message }}</div>
			<!--end::Text-->
		</div>
		<!--end::Wrapper-->
	</div>
	<!--end::Message(in)-->
	@endif
@endforeach
@else
<div class="d-flex align-items-center justify-content-center">
	<div class="p-5 rounded bg-light-primary text-dark fw-bold mw-lg-400px text-center" data-kt-element="message-text">Sem mensagens ainda</div>
</div>
@endif
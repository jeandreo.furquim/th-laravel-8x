@extends('layouts.app')

@section('title', 'Abrir chamado')

@section('custom-head')

@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card p-5">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-8 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-4">Como podemos te ajudar?</h1>
                        <div class="d-flex align-items-center justify-content-center">
                            <div class="">
                                <p class="text-muted fs-6 fw-bold text-center">
                                    A TH Condomínios preza por uma comunicação eficiente com todos os condôminos e moradores. Desta forma, firmamos AQUI com você o compromisso de darmos retorno a sua solicitação em até 24h. Ainda que não tenhamos uma solução definitiva à sua solicitação já nas primeiras 24h, comunicaremos quanto ao prazo necessário para darmos conclusão ao seu pedido.<br><br>
                                    Por favor, se for algo URGENTE ou EMERGENCIAL não hesite em fazer contato direto com a síndica pelo telefone: (41) 99676-0610. Disponível 24h/dia.
                                </p>
                            </div>
                        </div> <!--end::Title-->
                    </div>
                    <!--end::Heading-->

                    <form action="{{ route('occurrences.store') }}" class="form" method="post" enctype="multipart/form-data">
                
                       @include('pages._forms.occurrence')

                        <!--begin::Actions-->
                        <div class="text-center">
                            <a href="{{ route('occurrences.index') }}">
                                <label class="btn btn-light me-3 mb-0">Voltar</label>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label" >Abrir chamado</span>
                            </button>
                        </div>
                        <!--end::Actions-->

                    </form>
                    
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
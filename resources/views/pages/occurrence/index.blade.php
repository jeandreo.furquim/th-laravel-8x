@extends('layouts.app')

@section('title', 'Todos chamados')

@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-7 mt-7 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Todos os chamados</h1>
                        <!--end::Title-->
                        
                    </div>
                    <!--end::Heading-->

                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>ID</th>
                                <th>Título</th>
                                <th>Tipo</th>
                                <th>Status</th>
                                <th>Estado</th>
                                <th>Data de criação</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $content)
                                <tr>
                                    <td>{{ $content->id }}</td>
                                    <td>{{ $content->title }}</td>
                                    <td><b>{{ $content->type }}</b></td>
                                    <td>
                                        @if($content->status == 'Aprovada') 
                                        <span class="badge badge-light-success">Aprovada</span>
                                        @elseif($content->status == 'Bloqueada')
                                        <span class="badge badge-light-danger">Bloqueada</span>
                                        @else
                                        <span class="badge badge-light-warning">Pendente</span>
                                        @endif
                                    </td>
                                    <td>
                                        @if($content->condition == 'Em andamento') 
                                        <span class="badge badge-light-primary">Em andamento</span>
                                        @else
                                        <span class="badge badge-light-danger">Encerrada</span>
                                        @endif
                                    </td>
                                    <td>{{ $content->created_at->format('d/m/Y H:i') }}</td>
                                    <td class="actions">
                                        <a href="{{ route('occurrences.toApprove', $content->id) }}"><i class="fas fa-check" title="Atualizar Status"></i></a>
                                        <a href="{{ route('occurrences.upCondition', $content->id) }}"><i class="fas fa-exclamation-circle" title="Atualizar Estado"></i></a>
                                        <a href="{{ route('occurrences.show', $content->id) }}"><i class="fas fa-eye" title="Visualizar"></i></a>
                                        @if(session('sessionCondominium'))
                                        <a href="{{ route('occurrences.edit', $content->id) }}"><i class="fas fa-pencil-alt" title="Editar"></i></a>
                                        <a href="{{ route('occurrences.destroy', $content->id) }}"><i class="fas fa-trash" title="Excluir"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @if(session('sessionCondominium'))
            <a href="{{ route('occurrences.create') }}">
                <button type="submit" class="btn btn-primary w-auto">
                    <span class="indicator-label" >Adicionar chamado</span>
                </button>
            </a>
            @endif
        </div>
    </div>
</div>


@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
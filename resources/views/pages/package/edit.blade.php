@extends('layouts.app')

@section('title', 'Editar aviso de encomenda')

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card p-5">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-8 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Editar aviso de encomenda: {{ $content->title }}</h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Heading-->

                    <form action="{{ route('packages.update', $content->id) }}" class="form" method="post" enctype="multipart/form-data">
                
                        @method('PUT')
                        @include('pages._forms.package')

                        <!--begin::Actions-->
                        <div class="text-center">
                            <a href="{{ route('packages.index') }}">
                                <label class="btn btn-light me-3 mb-0">Voltar</label>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label" >Editar aviso</span>
                            </button>
                        </div>
                        <!--end::Actions-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('title', 'Todas encomendas')

@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-7 mt-7 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Todos as encomendas</h1>
                        <!--end::Title-->
                        
                    </div>
                    <!--end::Heading-->

                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>ID</th>
                                <th>Título</th>
                                <th>Remetente</th>
                                <th>Status</th>
                                <th>Data de aviso</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $content)
                                <tr>
                                    <td>{{ $content->id }}</td>
                                    <td>{{ $content->title }}</td>
                                    <td>{{ $content->sender }}</td>
                                    @if($content->status == 'aguardando')
                                    <td><span class="badge badge-light-warning fs-8 fw-bolder">Aguardando</span></td>
                                    @elseif($content->status == 'retirado')
                                    <td><span class="badge badge-light-success fs-8 fw-bolder">Retirado</span></td>
                                    @else
                                    <td><span class="badge badge-light-danger fs-8 fw-bolder">Não retirado</span></td>
                                    @endif
                                    <td>{{ $content->created_at->format('d/m/Y H:i') }}</td>
                                    <td class="actions">
                                        <a href="{{ route('packages.show', $content->id) }}"><i class="fas fa-eye" title="Visualizar"></i></a>
                                        @if(session('sessionCondominium'))
                                        <a href="{{ route('packages.updateStatus', [$content->id, 'retirado']) }}"><i class="fas fa-check-circle" title="Retirado"></i></a>
                                        <a href="{{ route('packages.updateStatus', [$content->id, 'não retirado']) }}"><i class="fas fa-exclamation-circle" title="Retirado"></i></a>
                                        <a href="{{ route('packages.edit', $content->id) }}"><i class="fas fa-pencil-alt" title="Editar"></i></a>
                                        <a href="{{ route('packages.destroy', $content->id) }}"><i class="fas fa-trash" title="Excluir"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @if(session('sessionCondominium'))
            <a href="{{ route('admin.users.index') }}">
                <button type="submit" class="btn btn-primary w-auto">
                    <span class="indicator-label" >Adicionar novo pacote</span>
                </button>
            </a>
            @endif
        </div>
    </div>
</div>


@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
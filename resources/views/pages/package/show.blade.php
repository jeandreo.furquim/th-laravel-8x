@extends('layouts.app')

{{-- TITLE OF PAGE --}}
@section('title', $content->title)

{{-- TITLE OF POST --}}
@section('title-post', $content->title) 

{{-- LINK FOR RETURN --}}
@section('link-back', route('packages.index'))

{{-- CONTENT --}}
@section('content')
<!-- POST -->
<div class="content flex-column-fluid">
	@include('includes.pageTitle')
	<div class="container">
		<div class="row gy-5 g-xl-8">
			<!-- CONTENT PAGE -->
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						@include('includes.imagePost')
						@include('includes.infosPost')
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						<p class="text-justify text-muted mb-0 description-post">
							{{ $content->observation }}
						</p>
					</div>
				</div>
			</div>
			<!-- CONTENT PAGE -->
		</div>
	</div>
</div>
<!-- POST -->
@endsection
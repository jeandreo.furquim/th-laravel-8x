@extends('layouts.app')

{{-- Title of page --}}
@section('title', 'Fornecedores')

{{-- Title of post --}}
@section('title-post', 'Fornecedores') 

{{-- Content --}}
@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div class="container-xxl mb-7 header-page">
        <div class="container p-0">
            <div class="row">
                <div class="col-10">
                    <h1>@yield('title-post')</h1>
                </div>
                <div class="col-2 mt-auto mb-auto text-right">
                    <a href="@yield('link-back', route("feed"))"><span>Voltar</span></a>
                </div>
            </div>
        </div>
    </div>
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <!--begin::Col-->
            <div class="col-xxl-12">
                @if ($contents->count())
                @foreach ($contents as $content)
                <!--begin::Col-->
                <div class="col-xxl-12">
                    <!--begin::List Widget 5-->
                    <div class="card mb-9">
                        <!--begin::Header-->
                        <div class="card-header align-items-center border-0 mt-0 bg-dark">
                            <a href="{{ route('providers.show', $content->id) }}">
                                <h3 class="card-title align-items-start flex-column">
                                    <span class="fw-bolder mb-0 text-light">{{$content->name}}</span>
                                </h3>
                            </a>
                            <div class="card-toolbar">
                                <!--begin::Menu-->
                                <button type="button" class="btn btn-sm btn-icon btn-color-light btn-options-header" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
                                            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000"></rect>
                                                <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                            </g>
                                        </svg>
                                    </span>
                                    <!--end::Svg Icon-->
                                </button>
                                <!--begin::Menu 1-->
                                <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true" id="kt_menu_61484c207b42d">
                                    <!--begin::Form-->
                                    <div class="px-7 py-5">
                                        <!--begin::Input group-->
                                        <div class="">
                                            <!--begin::Label-->
                                            <label class="form-label fw-bold mb-5">O que deseja fazer com esse item?</label>
                                            <!--end::Label-->
                                            <!--begin::Input-->
                                            <div class="d-flex justify-content-around">
                                                <a href="{{ route('providers.edit', $content->id) }}#" class="btn btn-sm btn-light-success">Editar Dados</a>
                                                <a href="{{ route('providers.destroy', $content->id) }}" class="btn btn-sm btn-light-danger">Deletar</a>
                                            </div>
                                            <!--end::Input-->
                                        </div>
                                        <!--end::Input group-->
                                    </div>
                                    <!--end::Form-->
                                </div>
                                <!--end::Menu 1-->
                                <!--end::Menu-->
                            </div>
                        </div>
                        <!--end::Header-->
                        <a href="{{ route('providers.show', $content->id) }}" class="text-dark divs-home div-post-home">
                            <!--begin::Body-->
                            <div class="card-body p-5 posts-cards">
                                <div class="">
                                    <div class="d-flex align-items-center mb-3">
                                        <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                            <img src="{{imageUser($content->user)}}" alt="user" class="rounded-circle">
                                        </div>
                                        <div class="d-block">
                                            <p class="fw-bolder mb-0">{{ $content->author->name }}</p>
                                            <p class="text-muted mb-0">{{ $content->author->cond->name }} - {{ $content->created_at->format('d/m/Y H:i')}}</p>
                                        </div>
                                    </div>
                                    @if($content->image)
                                    <div class="mb-15"> 
                                        <img src="{{ url("storage/condominios/" . $content->condominium . "/fornecedores/" . $content->image) }}" class="img-featured rounded mx-auto d-block">
                                    </div>
                                    @endif 
                                    <p class="text-justify text-muted mb-0 description-post">
                                        {{$content->description}}
                                    </p>
                                    <a href="{{ route('providers.show', $content->id) }}" class="btn btn-bg-light btn-active-color-primary text-muted" style="float: right;font-size: 14px;">Saiba mais</a>
                                </div>
                            </div>
                            <!--end: Card Body-->
                        </a>
                    </div>
                    <!--end: List Widget 5-->
                </div>
                <!--end::Col-->
                @endforeach
            @else
            <div class="col-xxl-12">
                <!--begin::List Widget 5-->
                <div class="card mb-9 p-10 d-flex align-items-center justify-content-center">
                    <i class="las la-sad-cry fs-5x text-danger mb-5"></i>
                    <h3 class="m-0">Nenhum resultado encontrado</h3>
                </div>
                <!--end::List Widget 5-->
            </div>
            @endif
            </div>
            <!--end::Col-->
        </div>
    </div>
</div>
@endsection
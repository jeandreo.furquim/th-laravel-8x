@extends('layouts.app')

{{-- TITLE OF PAGE --}}
@section('title', $content->name)

{{-- TITLE OF POST --}}
@section('title-post', $content->name) 

{{-- LINK FOR RETURN --}}
@section('link-back', route('providers.index'))

{{-- CONTENT --}}
@section('content')
<!-- POST -->
<div class="content flex-column-fluid">
	@include('includes.pageTitle')
	<div class="container">
		<div class="row gy-5 g-xl-8">
			<!-- CONTENT PAGE -->
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						@include('includes.imagePost')
						@include('includes.infosPost')
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						<p class="text-justify text-muted mb-0 description-post">
							<div class="row">
								@if($content->start)
								<div class="col mb-5">
									<b class="text-dark">Início:</b>
									<p class="mb-0 text-muted">{{ date_create($content->start)->format('d/m/Y') }}</p>
								</div>
								@endif
								@if($content->end)
								<div class="col mb-5">
									<b class="text-dark">Encerramento:</b>
									<p class="mb-0 text-muted">{{ date_create($content->end)->format('d/m/Y') }}</p>
								</div>
								@endif
								@if($content->phone)
								<div class="col">
									<b class="text-dark">Telefone:</b>
									<p class="mb-0 text-muted">{{ $content->phone }}</p>
								</div>
								@endif
								@if($content->email)
								<div class="col">
									<b class="text-dark">Email:</b>
									<p class="mb-0 text-muted">{{ $content->email }}</p>
								</div>
								@endif
								@if($content->cnpj)
								<div class="col">
									<b class="text-dark">CNPJ:</b>
									<p class="mb-0 text-muted">{{ $content->cnpj }}</p>
								</div>
								@endif
								@if($content->website)
								<div class="col">
									<b class="text-dark">Website:</b>
									<p class="mb-0 text-muted">{{ $content->website }}</p>
								</div>
								@endif
								@if($content->document)
								<div class="col">
									<b class="text-dark">Documento:</b>
									<p>
										<a href="{{ url("storage/condominios/$content->condominium/fornecedores/$content->document") }}" target="_blank" class="mb-0 text-muted text-hover-primary">{{ $content->document }}</a>
									</p>
								</div>
								@endif
							</div>
							<p class="text-justify text-muted mb-0 description-post">
								{!! nl2br(e($content->description)) !!}
							</p>
						</p>
					</div>
				</div>
			</div>
			<!-- COMENTS -->
			<div class="col-xxl-12">
				<!--begin::Feeds Widget 3-->
				<div class="card mb-5 mb-xxl-8">
					<!--begin::Body-->
					<div class="card-body pb-0">
						<h2 class="fw-bolder mb-5">Comentários</h2>
						<div id="load-comments">
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
						</div>
					</div>
					<!--end::Body-->
				</div>
				<!--end::Feeds Widget 3-->
			</div>
			<!-- COMENTS -->
			<!-- CONTENT PAGE -->
		</div>
	</div>
</div>
<!-- POST -->
@endsection

@section('custom-js')
<script>
	// LOAD AJAX
	sistemAjax(6, {{ $content->id }}, {{ $content->condominium }});
</script>
@endsection
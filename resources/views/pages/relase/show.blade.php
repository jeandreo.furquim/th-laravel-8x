@extends('layouts.app')

{{-- TITLE OF PAGE --}}
@section('title', $content->title)

{{-- TITLE OF POST --}}
@section('title-post', $content->title) 

{{-- LINK FOR RETURN --}}
@section('link-back', route('relases.index'))

{{-- CONTENT --}}
@section('content')
<!-- POST -->
<div class="content flex-column-fluid">
	@include('includes.pageTitle')
	<div class="container">
		<div class="row gy-5 g-xl-8">
			<!-- CONTENT PAGE -->
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						@include('includes.imagePost')
						@include('includes.infosPost')
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						<p class="text-justify text-muted mb-0 description-post">
							{!! nl2br(e($content->description)) !!}
						</p>
					</div>
				</div>
			</div>
			<!-- COMENTS -->
			<div class="col-xxl-12">
				<!--begin::Feeds Widget 3-->
				<div class="card mb-5 mb-xxl-8">
					<!--begin::Body-->
					<div class="card-body pb-0">
						<h2 class="fw-bolder mb-5">Comentários</h2>
						<div id="load-comments">
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
						</div>
					</div>
					<!--end::Body-->
				</div>
				<!--end::Feeds Widget 3-->
			</div>
			<!-- COMENTS -->
			<!-- CONTENT PAGE -->
		</div>
	</div>
</div>
<!-- POST -->
@endsection

@section('custom-js')
<script>
	// LOAD AJAX
	sistemAjax(7, {{ $content->id }}, {{ $content->condominium }});
</script>
@endsection
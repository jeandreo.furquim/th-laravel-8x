@extends('layouts.app')

{{-- Title of page --}}
@section('title', $titleCategory)

{{-- Title of post --}}
@section('title-post', $titleCategory) 

{{-- Content --}}
@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <!--begin::Toolbar-->
        <div class="d-flex flex-wrap flex-stack pb-5">
            <!--begin::Title-->
            <div class="d-flex flex-wrap align-items-center my-1">
                <h3 class="fw-bolder me-5 my-1">{{$titleCategory}}
                <span class="text-gray-400 fs-6">@if($contents->count() > 1) {{ $contents->count() }} itens encontrados @elseif($contents->count() == 1) {{ $contents->count() }} item encontrado @else Nenhum item encontrado @endif</span></h3>
            </div>
            <!--end::Title-->
            @if(!isset($disableActions))

            <!--begin::Controls-->
            <div class="d-flex flex-wrap my-1">
                <!--begin::Tab nav-->
                <ul class="nav nav-pills mb-2 mb-sm-0">
                    <li class="nav-item m-0">
                        <a class="btn btn-sm btn-icon btn-light btn-color-muted btn-active-primary me-3 active" data-bs-toggle="tab" href="#tab_01">
                            <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black" />
                                    <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </a>
                    </li>
                    <li class="nav-item m-0">
                        <a class="btn btn-sm btn-icon btn-light btn-color-muted btn-active-primary" data-bs-toggle="tab" href="#tab_02">
                            <!--begin::Svg Icon | path: icons/duotune/abstract/abs015.svg-->
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000" />
                                        <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
                                        <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
                                        <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3" />
                                    </g>
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                        </a>
                    </li>
                </ul>
                <!--end::Tab nav-->
                <!--begin::Actions-->
                <div class="d-flex my-0">
                    <div class="ml-3">
                        <!--begin::Trigger-->
                        <button type="button" class="form-select form-select-white form-select-sm w-150px text-muted text-left" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start">
                            @if(str_contains(Request::url(), 'semana'))
                            Últimos 7 dias
                            @elseif(str_contains(Request::url(), 'mes'))
                            Últimos 30 dias
                            @elseif(str_contains(Request::url(), 'ano'))
                            Este ano
                            @elseif(str_contains(Request::url(), 'all'))
                            Todos
                            @else
                            Filtrar por
                            @endif                        
                        </button>
                        <!--end::Trigger-->

                        <!--begin::Menu-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-200px py-4" data-kt-menu="true">
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="{{ route($routeResults) }}@if(isset($typeSelected)){{ '/semana/' . $typeSelected}}@else{{ '/semana' }}@endif" class="menu-link px-3">
                                    Últimos 7 dias
                                </a>
                            </div>
                            <!--end::Menu item-->

                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="{{ route($routeResults) }}@if(isset($typeSelected)){{ '/mes/' . $typeSelected}}@else{{ '/mes' }}@endif" class="menu-link px-3">
                                    Últimos 30 dias
                                </a>
                            </div>
                            <!--end::Menu item-->

                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="{{ route($routeResults) }}@if(isset($typeSelected)){{ '/ano/' . $typeSelected}}@else{{ '/ano' }}@endif" class="menu-link px-3">
                                    Este ano
                                </a>
                            </div>
                            <!--end::Menu item-->

                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="{{ route($routeResults) }}@if(isset($typeSelected)){{ '/all/' . $typeSelected}}@else{{ '/all' }}@endif" class="menu-link px-3">
                                    Todos
                                </a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu-->
                    </div>
                    <div class="ml-3">
                        <!--begin::Trigger-->
                        <button type="button" class="form-select form-select-white form-select-sm w-150px text-muted text-left" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-start">
                            @if(request()->segment(count(request()->segments())) == 'recentes')
                                Mais recentes
                            @elseif(request()->segment(count(request()->segments())) == 'antigos')
                                Mais antigos
                            @else 
                                Ordernar por
                            @endif
                        </button>
                        <!--end::Trigger-->
                        <!--begin::Menu-->
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-200px py-4" data-kt-menu="true">
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="{{route($routeResults)}}@if(isset($typeSelected)){{'/' . $typeSelected}}@endif/recentes" class="menu-link px-3">
                                    Mais recentes
                                </a>
                            </div>
                            <!--end::Menu item-->
                            <!--begin::Menu item-->
                            <div class="menu-item px-3">
                                <a href="{{route($routeResults)}}@if(isset($typeSelected)){{'/' . $typeSelected}}@endif/antigos" class="menu-link px-3">
                                    Mais antigos
                                </a>
                            </div>
                            <!--end::Menu item-->
                        </div>
                        <!--end::Menu-->
                    </div>
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Controls-->
            @endif
        </div>
        <!--end::Toolbar-->
        <!--begin::Tab Content-->
        <div class="tab-content" id="tab-results">
            <!--begin::Tab pane-->
            <div id="tab_01" class="tab-pane fade show active">
                <!--begin::Col-->
                @if($contents->count())
                <div class="row">
                    @foreach ($contents as $content)
                        @if(isset($customStyle))
                        @if($customStyle == 'encomendas')
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <!--begin::List Widget 5-->
                            <div class="card mb-9">
                                <!--begin::Header-->
                                <div class="card-header align-items-center border-0 mt-0 bg-dark bg-dark">
                                    <a href="{{ route('packages.show', $content->id) }}">
                                        <h3 class="card-title align-items-start flex-column">
                                            <span class="fw-bolder mb-0 text-light">{{ $content->title ?? $content->name }}</span>
                                        </h3>
                                    </a>
                                    <div class="card-toolbar">
                                        <!--begin::Menu-->
                                        <button type="button" class="btn btn-sm btn-icon btn-color-light btn-options-header" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                            <span class="svg-icon svg-icon-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000"></rect>
                                                        <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                        <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                        <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                    </g>
                                                </svg>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </button>
                                        <!--begin::Menu 1-->
                                        <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true" id="kt_menu_61484c207b42d">
                                            <!--begin::Form-->
                                            <div class="px-7 py-5">
                                                <!--begin::Input group-->
                                                <div class="">
                                                    <!--begin::Label-->
                                                    <label class="form-label fw-bold mb-5">O que deseja fazer com esse item?</label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <div class="d-flex justify-content-around">
                                                        <a href="{{ route('packages.edit', $content->id) }}" class="btn btn-sm btn-light-success">Editar Dados</a>
                                                        <a href="{{ route('packages.destroy', $content->id) }}" class="btn btn-sm btn-light-danger">Deletar</a>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Input group-->
                                            </div>
                                            <!--end::Form-->
                                        </div>
                                        <!--end::Menu 1-->
                                        <!--end::Menu-->
                                    </div>
                                </div>
                                <!--end::Header-->
                                <a href="{{ route('packages.show', $content->id) }}" class="text-dark divs-home div-post-home">
                                    <!--begin::Body-->
                                    <div class="card-body p-5 posts-cards">
                                        <div class="">
                                            <div class="d-flex align-items-center mb-3">
                                                <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                                    <img src="{{imageUser($content->user)}}" alt="user" class="rounded-circle">
                                                </div>
                                                <div class="d-block">
                                                    <p class="fw-bolder mb-0">{{ $content->author->name }}</p>
                                                    <p class="text-muted mb-0">{{ $content->author->cond->name }} - {{ $content->created_at->format('d/m/Y H:i')}}</p>
                                                </div>
                                            </div>
                                            @if($content->image)
                                            <div class="mb-15"> 
                                                <img src="{{ url("storage/condominios/" . $content->condominium . "/usuarios/" . $content->owner . "/" . $content->image) }}" class="img-featured rounded mx-auto d-block">
                                            </div>
                                            @endif 
                                            <p class="text-justify text-muted mb-0 description-post">
                                                {!! Str::limit(nl2br(e($content->observation)), 350) !!}
                                            </p>
                                            <a href="{{ route('packages.show', $content->id) }}" class="btn btn-bg-light btn-active-color-primary text-muted" style="float: right;font-size: 14px;">Saiba mais</a>
                                        </div>
                                    </div>
                                    <!--end: Card Body-->
                                </a>
                            </div>
                            <!--end: List Widget 5-->
                        </div>
                        <!--end::Col-->
                        @else
                        <!--begin::Col-->
                        <div class="col-lg-6"> 
                            <a href="{{ route($routeShow, $content->id) }}">
                            <!--begin::List Posts-->
                                <div class="card mb-8">
                                    <div class="card-header align-items-center border-0 mt-0 bg-dark bg-dark">
                                        <h3 class="card-title align-items-start flex-column">
                                            <span class="fw-bolder mb-0 text-light">{{ $content->name }}</span>
                                        </h3>
                                    </div>
                                    <div class="row mw-100 ml-0">
                                        <div class="col-sm-5 p-0">
                                            @if($content->image)
                                                <img src="{{ url("storage/condominios/" . $content->condominium . "/" . $routePath . "/" . $content->image) }}" class="w-100 no-radius-mobile" style="height: 180px; object-fit: cover;border-radius: 0px 0px 0px 1em;" >
                                            @else 
                                                <img src="{{ asset('assets/images/sem_imagem.jpg') }}" class="w-100 no-radius-mobile" style="height: 180px; object-fit: cover;border-radius: 0px 0px 0px 1em;" >
                                            @endif
                                        </div>
                                        <div class="col-sm p-5 m-auto">
                                            @if($customStyle == 'eventos')
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Cargo</p>
                                            <p class="timeline-content fw-mormal text-muted mb-2">{{ $content->office }}</p>
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Horários</p>
                                            <p class="timeline-content fw-mormal text-muted mb-2">{{ $content->hour_start . ' - ' . $content->hour_end }}</p>
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Dias de funcionamento</p>
                                            @else
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Capacidade de pessoas</p>
                                            <p class="timeline-content fw-mormal text-muted mb-2">{{ $content->capacity }} pessoas</p>
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Valor da reserva</p>
                                            <p class="timeline-content fw-mormal text-muted mb-2">
                                                @if($content->tax_money == 0) 
                                                Gratuito
                                                @else
                                                {{ $content->tax_money }}
                                                @endif
                                            </p>
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Dias de funcionamento</p>
                                            @endif
                                            <div class="mt-1 d-flex">
                                                <div class="div-day-week {{ $content->monday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">S</span>
                                                </div>
                                                <div class="div-day-week {{ $content->tuesday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">T</span>
                                                </div>
                                                <div class="div-day-week {{ $content->wednesday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">Q</span>
                                                </div>
                                                <div class="div-day-week {{ $content->thursday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">Q</span>
                                                </div>
                                                <div class="div-day-week {{ $content->friday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">S</span>
                                                </div>
                                                <div class="div-day-week {{ $content->saturday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">S</span>
                                                </div>
                                                <div class="div-day-week {{ $content->sunday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">D</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!--end: List Posts-->
                            </a>
                        </div>
                        <!--end::Col-->
                        @endif
                        @else
                        <!--begin::Col-->
                        <div class="col-lg-12">
                            <!--begin::List Widget 5-->
                            <div class="card mb-8">
                                <!--begin::Header-->
                                <div class="card-header align-items-center border-0 mt-0 bg-dark">
                                    <a href="{{ route($routeShow, $content->id) }}">
                                        <h3 class="card-title align-items-start flex-column">
                                            <span class="fw-bolder mb-0 text-light">{{ $content->title ?? $content->name }}</span>
                                        </h3>
                                    </a>
                                    <div class="card-toolbar">
                                        <!--begin::Menu-->
                                        <button type="button" class="btn btn-sm btn-icon btn-color-light btn-options-header" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                            <span class="svg-icon svg-icon-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000"></rect>
                                                        <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                        <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                        <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                    </g>
                                                </svg>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </button>
                                        <!--begin::Menu 1-->
                                        <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true" id="kt_menu_61484c207b42d">
                                            <!--begin::Form-->
                                            <div class="px-7 py-5">
                                                <!--begin::Input group-->
                                                <div class="">
                                                    <!--begin::Label-->
                                                    <label class="form-label fw-bold mb-5">O que deseja fazer com esse item?</label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <div class="d-flex justify-content-around">
                                                        <a href="{{ route($routeEdit, $content->id) }}" class="btn btn-sm btn-light-success">Editar Dados</a>
                                                        <a href="{{ route($routeDestroy, $content->id) }}" class="btn btn-sm btn-light-danger">Deletar</a>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Input group-->
                                            </div>
                                            <!--end::Form-->
                                        </div>
                                        <!--end::Menu 1-->
                                        <!--end::Menu-->
                                    </div>
                                </div>
                                <!--end::Header-->
                                <a href="{{ route($routeShow, $content->id) }}" class="text-dark divs-home div-post-home">
                                    <!--begin::Body-->
                                    <div class="card-body p-5 posts-cards">
                                        <div class="">
                                            <div class="d-flex align-items-center mb-3">
                                                <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                                    <img src="{{imageUser($content->user)}}" alt="user" class="rounded-circle">
                                                </div>
                                                <div class="d-block">
                                                    <p class="fw-bolder mb-0">{{ $content->author->name }}</p>
                                                    <p class="text-muted mb-0">{{ $content->author->cond->name }} - {{ $content->created_at->format('d/m/Y H:i')}}</p>
                                                </div>
                                            </div>
                                            @if($content->image)
                                            <div class="mb-15"> 
                                                @if(!isset($subFolder))
                                                <img src="{{ url("storage/condominios/" . $content->condominium . "/" . $routePath . "/" . $content->image) }}" class="img-featured rounded mx-auto d-block">
                                                @else
                                                <img src="{{ url("storage/condominios/" . $content->condominium . "/" . $routePath . "/" . $content->id . "/" . $content->image) }}" class="img-featured rounded mx-auto d-block">
                                                @endif
                                            </div>
                                            @endif 
                                            <p class="text-justify text-muted mb-0 description-post">
                                                {!! Str::limit(nl2br(e($content->description)), 350) !!}
                                            </p>
                                            <a href="{{ route($routeShow, $content->id) }}" class="btn btn-bg-light btn-active-color-primary text-muted" style="float: right;font-size: 14px;">Saiba mais</a>
                                        </div>
                                    </div>
                                    <!--end: Card Body-->
                                </a>
                            </div>
                            <!--end: List Widget 5-->
                        </div>
                        <!--end::Col-->
                        @endif
                    @endforeach
                </div>
                @else
                <div class="row mb-9">
                    <div class="col-lg-12">
                        <!--begin::List Widget 5-->
                        <div class="card p-10 d-flex align-items-center justify-content-center">
                            <i class="las la-sad-cry fs-5x text-danger mb-5"></i>
                            <h3 class="m-0">Nenhum resultado encontrado</h3>
                        </div>
                        <!--end::List Widget 5-->
                    </div>
                </div>
                @endif
            </div>
            <!--end::Tab pane-->
            <!--begin::Tab pane-->
            <div id="tab_02" class="tab-pane fade">
                <!--begin::Card-->
                <!--begin::Col-->
                @if($contents->count())
                <div class="row">
                    @foreach ($contents as $content)
                        @if(isset($customStyle))
                        @if($customStyle == 'encomendas')
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <!--begin::List Widget 5-->
                            <div class="card mb-9">
                                <!--begin::Header-->
                                <div class="card-header align-items-center border-0 mt-0 bg-dark bg-dark">
                                    <a href="{{ route('packages.show', $content->id) }}">
                                        <h3 class="card-title align-items-start flex-column">
                                            <span class="fw-bolder mb-0 text-light">{{ $content->title ?? $content->name }}</span>
                                        </h3>
                                    </a>
                                    <div class="card-toolbar">
                                        <!--begin::Menu-->
                                        <button type="button" class="btn btn-sm btn-icon btn-color-light btn-options-header" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                            <span class="svg-icon svg-icon-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000"></rect>
                                                        <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                        <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                        <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                    </g>
                                                </svg>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </button>
                                        <!--begin::Menu 1-->
                                        <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true" id="kt_menu_61484c207b42d">
                                            <!--begin::Form-->
                                            <div class="px-7 py-5">
                                                <!--begin::Input group-->
                                                <div class="">
                                                    <!--begin::Label-->
                                                    <label class="form-label fw-bold mb-5">O que deseja fazer com esse item?</label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <div class="d-flex justify-content-around">
                                                        <a href="{{ route('packages.edit', $content->id) }}" class="btn btn-sm btn-light-success">Editar Dados</a>
                                                        <a href="{{ route('packages.destroy', $content->id) }}" class="btn btn-sm btn-light-danger">Deletar</a>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Input group-->
                                            </div>
                                            <!--end::Form-->
                                        </div>
                                        <!--end::Menu 1-->
                                        <!--end::Menu-->
                                    </div>
                                </div>
                                <!--end::Header-->
                                <a href="{{ route('packages.show', $content->id) }}" class="text-dark divs-home div-post-home">
                                    <!--begin::Body-->
                                    <div class="card-body p-5 posts-cards">
                                        <div class="">
                                            <div class="d-flex align-items-center mb-3">
                                                <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                                    <img src="{{imageUser($content->user)}}" alt="user" class="rounded-circle">
                                                </div>
                                                <div class="d-block">
                                                    <p class="fw-bolder mb-0">{{ $content->author->name }}</p>
                                                    <p class="text-muted mb-0">{{ $content->author->cond->name }} - {{ $content->created_at->format('d/m/Y H:i')}}</p>
                                                </div>
                                            </div>
                                            @if($content->image)
                                            <div class="mb-15"> 
                                                <img src="{{ url("storage/condominios/" . $content->condominium . "/usuarios/" . $content->owner . "/" . $content->image) }}" class="img-featured rounded mx-auto d-block">
                                            </div>
                                            @endif 
                                            <p class="text-justify text-muted mb-0 description-post">
                                                {!! Str::limit(nl2br(e($content->observation)), 350) !!}
                                            </p>
                                            <a href="{{ route('packages.show', $content->id) }}" class="btn btn-bg-light btn-active-color-primary text-muted" style="float: right;font-size: 14px;">Saiba mais</a>
                                        </div>
                                    </div>
                                    <!--end: Card Body-->
                                </a>
                            </div>
                            <!--end: List Widget 5-->
                        </div>
                        <!--end::Col-->
                        @else
                        <!--begin::Col-->
                        <div class="col-lg-6"> 
                            <a href="{{ route($routeShow, $content->id) }}">
                            <!--begin::List Posts-->
                                <div class="card mb-8">
                                    <div class="card-header align-items-center border-0 mt-0 bg-dark bg-dark">
                                        <h3 class="card-title align-items-start flex-column">
                                            <span class="fw-bolder mb-0 text-light">{{ $content->name }}</span>
                                        </h3>
                                    </div>
                                    <div class="row mw-100 ml-0">
                                        <div class="col-sm-5 p-0">
                                            @if($content->image)
                                                <img src="{{ url("storage/condominios/" . $content->condominium . "/" . $routePath . "/" . $content->image) }}" class="w-100 no-radius-mobile" style="height: 180px; object-fit: cover;border-radius: 0px 0px 0px 1em;" >
                                            @else 
                                                <img src="{{ asset('assets/images/sem_imagem.jpg') }}" class="w-100 no-radius-mobile" style="height: 180px; object-fit: cover;border-radius: 0px 0px 0px 1em;" >
                                            @endif
                                        </div>
                                        <div class="col-sm p-5 m-auto">
                                            @if($customStyle == 'eventos')
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Cargo</p>
                                            <p class="timeline-content fw-mormal text-muted mb-2">{{ $content->office }}</p>
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Horários</p>
                                            <p class="timeline-content fw-mormal text-muted mb-2">{{ $content->hour_start . ' - ' . $content->hour_end }}</p>
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Dias de funcionamento</p>
                                            @else
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Capacidade de pessoas</p>
                                            <p class="timeline-content fw-mormal text-muted mb-2">{{ $content->capacity }} pessoas</p>
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Valor da reserva</p>
                                            <p class="timeline-content fw-mormal text-muted mb-2">
                                                @if($content->tax_money == 'R$ 0,00') 
                                                Gratuito
                                                @else
                                                {{ $content->tax_money }}
                                                @endif
                                            </p>
                                            <p class="text-gray-800 text-hover-primary fw-bolder fs-6 mb-0">Dias de funcionamento</p>
                                            @endif
                                            <div class="mt-1 d-flex">
                                                <div class="div-day-week {{ $content->monday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">S</span>
                                                </div>
                                                <div class="div-day-week {{ $content->tuesday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">T</span>
                                                </div>
                                                <div class="div-day-week {{ $content->wednesday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">Q</span>
                                                </div>
                                                <div class="div-day-week {{ $content->thursday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">Q</span>
                                                </div>
                                                <div class="div-day-week {{ $content->friday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">S</span>
                                                </div>
                                                <div class="div-day-week {{ $content->saturday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">S</span>
                                                </div>
                                                <div class="div-day-week {{ $content->sunday ? "div-day-active" : "div-day-not-active" }}">
                                                    <span class="day-week">D</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!--end: List Posts-->
                            </a>
                        </div>
                        <!--end::Col-->
                        @endif
                        @else
                        <!--begin::Col-->
                        <div class="col-lg-6">
                            <!--begin::List Widget 5-->
                            <div class="card mb-8">
                                <!--begin::Header-->
                                <div class="card-header align-items-center border-0 mt-0 bg-dark">
                                    <a href="{{ route($routeShow, $content->id) }}">
                                        <h3 class="card-title align-items-start flex-column">
                                            <span class="fw-bolder mb-0 text-light">{{ $content->title ?? $content->name }}</span>
                                        </h3>
                                    </a>
                                    <div class="card-toolbar">
                                        <!--begin::Menu-->
                                        <button type="button" class="btn btn-sm btn-icon btn-color-light btn-options-header" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                            <span class="svg-icon svg-icon-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
                                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                        <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000"></rect>
                                                        <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                        <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                        <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"></rect>
                                                    </g>
                                                </svg>
                                            </span>
                                            <!--end::Svg Icon-->
                                        </button>
                                        <!--begin::Menu 1-->
                                        <div class="menu menu-sub menu-sub-dropdown w-250px w-md-300px" data-kt-menu="true" id="kt_menu_61484c207b42d">
                                            <!--begin::Form-->
                                            <div class="px-7 py-5">
                                                <!--begin::Input group-->
                                                <div class="">
                                                    <!--begin::Label-->
                                                    <label class="form-label fw-bold mb-5">O que deseja fazer com esse item?</label>
                                                    <!--end::Label-->
                                                    <!--begin::Input-->
                                                    <div class="d-flex justify-content-around">
                                                        <a href="{{ route($routeEdit, $content->id) }}" class="btn btn-sm btn-light-success">Editar Dados</a>
                                                        <a href="{{ route($routeDestroy, $content->id) }}" class="btn btn-sm btn-light-danger">Deletar</a>
                                                    </div>
                                                    <!--end::Input-->
                                                </div>
                                                <!--end::Input group-->
                                            </div>
                                            <!--end::Form-->
                                        </div>
                                        <!--end::Menu 1-->
                                        <!--end::Menu-->
                                    </div>
                                </div>
                                <!--end::Header-->
                                <a href="{{ route($routeShow, $content->id) }}" class="text-dark divs-home div-post-home">
                                    <!--begin::Body-->
                                    <div class="card-body p-5 posts-cards">
                                        <div class="">
                                            <div class="d-flex align-items-center mb-3">
                                                <div class="cursor-pointer symbol symbol-30px symbol-md-45px object-cover mr-3">
                                                    <img src="{{imageUser($content->user)}}" alt="user" class="rounded-circle">
                                                </div>
                                                <div class="d-block">
                                                    <p class="fw-bolder mb-0">{{ $content->author->name }}</p>
                                                    <p class="text-muted mb-0">{{ $content->author->cond->name }} - {{ $content->created_at->format('d/m/Y H:i')}}</p>
                                                </div>
                                            </div>
                                            @if($content->image)
                                            <div class="mb-10"> 
                                                @if(!isset($subFolder))
                                                <img src="{{ url("storage/condominios/" . $content->condominium . "/" . $routePath . "/" . $content->image) }}" class="img-featured rounded mx-auto d-block">
                                                @else
                                                <img src="{{ url("storage/condominios/" . $content->condominium . "/" . $routePath . "/" . $content->id . "/" . $content->image) }}" class="img-featured rounded mx-auto d-block">
                                                @endif
                                            </div>
                                            @endif 
                                            <p class="text-justify text-muted mb-0 description-post mb-4">
                                                {{mb_strimwidth($content->description, 0, 165, "...")}}
                                            </p>
                                            <a href="{{ route($routeShow, $content->id) }}" class="btn btn-bg-light btn-active-color-primary text-muted" style="float: right;font-size: 14px;">Saiba mais</a>
                                        </div>
                                    </div>
                                    <!--end: Card Body-->
                                </a>
                            </div>
                            <!--end: List Widget 5-->
                        </div>
                        <!--end::Col-->
                        @endif
                    @endforeach
                </div>
                @else
                <div class="row mb-9">
                    <div class="col-lg-12">
                        <!--begin::List Widget 5-->
                        <div class="card p-10 d-flex align-items-center justify-content-center">
                            <i class="las la-sad-cry fs-5x text-danger mb-5"></i>
                            <h3 class="m-0">Nenhum resultado encontrado</h3>
                        </div>
                        <!--end::List Widget 5-->
                    </div>
                </div>
                @endif
                <!--end::Card-->
            </div>
            <!--end::Tab pane-->
        </div>
        <!--end::Tab Content-->
        <!--begin::Content-->
        <div class="row gy-5 g-xl-8">
        <!--begin::Col-->

        </div>
        <!--end::Col-->
        <!--end::Content-->
        <!--begin::Pagination-->
        <div class="pt-5">
            {{$contents->links()}}
        </div>
        <!--end::Pagination-->
    </div>

</div>
@endsection
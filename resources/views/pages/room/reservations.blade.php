@extends('layouts.app')

@section('title', 'Todas as Reservas')

@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">

                <div class="card-body">
                    <!--begin::Heading-->
                    <div class="mb-7 mt-7 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Todos as Reservas</h1>
                        <!--end::Title-->
                        
                    </div>
                    <!--end::Heading-->

                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>ID</th>
                                <th>Nome</th>
                                <th>Espaço</th>
                                <th>Condomínio</th>
                                <th>Status</th>
                                <th>Data de criação</th>
                                @if(in_array(Auth::user()->role, ['administrador', 'sindico']))
                                <th>Ações</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $content)
                                <tr>
                                    <td>{{ $content->id }}</td>
                                    <td>{{ $content->author()->first()->name }}</td>
                                    <td>{{ $content->room()->first()->name }}</td>
                                    <td>{{ $content->cond()->first()->name }}</td>
                                    @if($content->status == 'pendente')
                                    <td><span class="badge badge-light-warning fs-8 fw-bolder">Pendente</span></td>
                                    @elseif($content->status == 'aprovado')
                                    <td><span class="badge badge-light-success fs-8 fw-bolder">Aprovado</span></td>
                                    @elseif($content->status == 'manutencao')
                                    <td><span class="badge badge-light-primary fs-8 fw-bolder">Manutenção</span></td>
                                    @else
                                    <td><span class="badge badge-light-danger fs-8 fw-bolder">Recusado</span></td>
                                    @endif
                                    <td>{{ $content->created_at->format('d/m/Y H:i') }}</td>
                                    @if(in_array(Auth::user()->role, ['administrador', 'sindico']))
                                    <td class="actions">
                                        <a href="{{ route('admin.users.show', $content->room()->first()->id) }}"><i class="fas fa-eye" title="Visualizar"></i></a>
                                        <a href="{{ route('reservations.update', ['id' => $content->id, 'status' => 'aprovado']) }}"><i class="fas fa-check-circle" title="Aprovar"></i></a>
                                        <a href="{{ route('reservations.update', ['id' => $content->id, 'status' => 'recusado']) }}"><i class="fas fa-times-circle" title="Recusar"></i></a>
                                        <a href="{{ route('reservations.update', ['id' => $content->id, 'status' => 'manutencao']) }}"><i class="fas fa-cogs" title="Manutenção"></i>
                                    </td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
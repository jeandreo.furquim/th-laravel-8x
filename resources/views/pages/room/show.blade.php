@extends('layouts.app')

@section('title', 'Faça sua reserva')

@section('custom-head')
    <!--begin::Custom Stylesheets Nosidebar (Dreamake)-->
    <link href={{asset('assets/css/custom.nosidebar.css')}} rel="stylesheet" type="text/css" />
    <!--end::Custom Stylesheets Nosidebar (Dreamake)-->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/color-calendar/dist/css/theme-basic.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/color-calendar/dist/css/theme-glass.css" />
    <script src="https://cdn.jsdelivr.net/npm/color-calendar/dist/bundle.min.js"></script>
@endsection

@section('content')
<div class="header-init d-flex align-items-center justify-content-center"  style="background: url('{{ asset('assets/images/background-home-3.jpg') }}');"> 
    <div>
        <p class="page-title-dark text-center mb-1">Reservar espaço!</p>
    </div>
</div>
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0" id="kt_content">
    <div id="content_container" class="container-xxl">
        <!--begin::Row-->
        <div class="row gy-5 g-xl-8 margin-start-page">
            <div class="col-xxl-8 my-4">
                <div class="card card-xxl p-8">
                    <div class="d-sflex">
                        <div class="w-100 d-flex justify-content-center align-items-center rounded object-cover img-room-size">
                            <h1 class="m-0 text-light" style="z-index: 10;text-shadow: 0px 0px 20px #000000bd;text-transform: uppercase; font-weight: 800;">{{ $content->name }}</h1>
                        </div>
                        <div class="w-100 img-room-size img-room-image" style="background-image:url(' @if($content->image) {{ url('storage/condominios/' . $content->condominium . '/espacos/' . $content->image) }} @else {{asset('assets/images/sem_imagem.jpg')}} @endif '); margin-top: -150px; background-position: center">
                        </div>
                    </div>
                    <div class="row mt-7 mb-3">
                        <div class="col-xxl-6">
                            <p class="text-gray-800 fw-bolder fs-6 mb-0">Dias de funcionamento</p>
                            <div class="mt-1 d-flex">
                                <div class="div-day-week {{ $content->monday ? "div-day-active" : "div-day-not-active" }}">
                                    <span class="day-week">S</span>
                                </div>
                                <div class="div-day-week {{ $content->tuesday ? "div-day-active" : "div-day-not-active" }}">
                                    <span class="day-week">T</span>
                                </div>
                                <div class="div-day-week {{ $content->wednesday ? "div-day-active" : "div-day-not-active" }}">
                                    <span class="day-week">Q</span>
                                </div>
                                <div class="div-day-week {{ $content->thursday ? "div-day-active" : "div-day-not-active" }}">
                                    <span class="day-week">Q</span>
                                </div>
                                <div class="div-day-week {{ $content->friday ? "div-day-active" : "div-day-not-active" }}">
                                    <span class="day-week">S</span>
                                </div>
                                <div class="div-day-week {{ $content->saturday ? "div-day-active" : "div-day-not-active" }}">
                                    <span class="day-week">S</span>
                                </div>
                                <div class="div-day-week {{ $content->sunday ? "div-day-active" : "div-day-not-active" }}">
                                    <span class="day-week">D</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xxl-6">
                            <p class="text-gray-800 fw-bolder fs-6 mb-0">Horário de funcionamento</p>
                            <b class="text-muted">{{ $content->hour_start . ' - ' . $content->hour_end }}</b>
                        </div>
                    </div>
                    <div class="row mt-3 mb-1">
                        <div class="col-xxl-12">
                            <p class="text-gray-800 fs-6 mb-0">-Reserve esse espaço com <b>{{ $content->antecedence }} dias</b> de antecedência.</p>
                            <p class="text-gray-800 fs-6 mb-0">-Para cancelar essa reserva efetue isso com <b>{{ $content->cancellation }} dias</b> de antecedência.</p>
                        </div>
                    </div>
                    <p class="text-gray-800 fw-bolder fs-6 mt-5 mb-0">Infraestutura</p>
                    <p class="text-muted">{!! $content->infrastructure !!}</p>
                    <p class="text-gray-800 fw-bolder fs-6 mt-0 mb-0">Regras</p>
                    <p class="text-muted">{!! $content->rules !!}</p>
                </div>
            </div>
            <div class="col-xxl-4 my-4">
                <div class="card card-xxl p-8">
                    <div class="mb-4">
                        <input class="form-control form-control-solid" placeholder="Pick a date" id="resevar"/>
                    </div>
                    <div id="color-calendar"></div>
                    @if(!in_array(Auth::user()->role, ['administrador', 'sindico']))
                    <form action="{{ route('reservations.store', $content->id) }}" class="form" method="post" enctype="multipart/form-data" id="form_reservation">
                        @csrf
                        <div class="col">
                            <!--begin::Input group-->
                            <div class="d-flex flex-column mb-4 fv-row mt-7">
                                <div class="mb-0">
                                    <div class="row">
                                        <div class="col-7">
                                            <input type="hidden" id="dataToForm" name="day" value="">
                                            <label for="" class="form-label">Data selecionada</label>
                                            <input class="form-control form-control-solid" disabled="disabled" placeholder="Selecione uma data" required id="calendario"/>
                                        </div>
                                        <div class="col-5">
                                            <label for="" class="form-label">Horário</label>
                                            <input name="hour" class="form-control form-control-solid" type="time" id="hour_selected" required/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p class="text-danger" id="notification"></p>
                            <!--end::Input group-->
                            <button type="submit" class="btn btn-primary" id="submit_form">
                                <span class="indicator-label" >@if($content->approval) Solicitar reserva @else Reservar espaço @endif</span>
                            </button>
                        </div>
                    </form>
                    @endif
                </div>
            </div>
        </div>
        <!--end::Row-->
    </div>
</div>


@endsection

@section('custom-js')
<style>
    .flatpickr-calendar {
        width: 410px !important;
    }
    .flatpickr-day {
        max-width: 50px;
    }
    .flatpickr-disabled {
        background: #80808017 !important;
    }
    .flatpickr-day {
        margin-bottom: 5px;
    }
</style>
<!-- Calendar DataPicker -->
<script src="https://npmcdn.com/flatpickr/dist/l10n/pt.js"></script>
<script>

   $(document).ready(function () {
        // Calendar DataPicker
        $("#resevar").flatpickr({
            minDate: "today",
            allowInput: true,
            altInput: true,
            inline: true,
            altFormat: "d/m/Y",
            dateFormat: "Y-m-d",
            "locale": "pt",
            "disable": [
                function(date) {
                    // console.log(date.getDay());
                    
                    var monday = {!! $content->monday !!} == 0 ? date.getDay() === 0 : null;
                    var tuesday = {!! $content->tuesday !!} == 0 ? date.getDay() === 1 : null;
                    var wednesday = {!! $content->wednesday !!} == 0 ? date.getDay() === 2 : null;
                    var thursday = {!! $content->thursday !!} == 0 ? date.getDay() === 3 : null;
                    var friday = {!! $content->friday !!} == 0 ? date.getDay() === 4 : null;
                    var saturday = {!! $content->saturday !!} == 0 ? date.getDay() === 5 : null;
                    var sunday = {!! $content->sunday !!} == 0 ? date.getDay() === 6 : null;
                    // return true to disable
                    return (monday + tuesday + wednesday + thursday + friday + saturday + sunday);
                }
            ],
        }); 

        // ANTECEDENCE
        var antecedence =  {!! $content->antecedence !!};

        // HIDE INPUT CALENDAR
        $('#resevar').next().hide();

        // INSERT VALUE IN FIELDS
        $('#resevar').change(function(){
            var dataSelected = $(this).val();
            $("#dataToForm").val(dataSelected);
            $("#calendario").val($(this).next().val());
            

            // VERIFY DATES ANTENCEDENCE
            var date_selected = $('#dataToForm').val();
            var data_selected_separed = date_selected.split('-');
            var dateNew = data_selected_separed[1] + '/' + data_selected_separed[2] + '/' + data_selected_separed[0];

            // GET DAY AND HOUR CURRENT IN SECONDS
            const today = new Date().setHours(0, 0, 0, 0);

            // GET DAY AND HOUR IN SECONDS OF DAY SELECTED
            const date_s = new Date(dateNew).setHours(0, 0, 0, 0);

            // DIFERENCE OF DAYS IN TIME
            const diffTime = Math.abs(date_s - today);
            
            // DIFERENCE OF DAYS IN DAYS
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 

            // 
            if(diffDays < antecedence){
                $('#notification').html('Esse espaço solicita recomenda que você abra sua solicitação com  <b>' + antecedence + '</b> você pode solicitar a reserva agora, mas ela precisará ser aprovada manualmente pelo sindico.');
            } else {
                $('#notification').html('');
            }
            console.log(diffDays, antecedence)


        });

        // VEFIRY HOURS AND DATE

        $('#submit_form').click(function(e){
            // DISABLE SEND FORM
            e.preventDefault();
            
            // GET HOUR SELECTED
            var hour_selected = $("#hour_selected").val();

            if(hour_selected == ''){
                $('#notification').html('Selecione um horário.');
            } else {
                
                // CHECKS IF THE SELECTED DATE IS ALLOWED
                var hour_start = '{!! $content->hour_start !!}';
                var hour_close = '{!! $content->hour_end !!}';

                const hour_s = new Date('2020-01-01 ' + hour_start);
                const hour_e = new Date('2020-01-01 ' + hour_close);
                const hour_sl = new Date('2020-01-01 ' + hour_selected);

                if(hour_s.getTime() < hour_sl.getTime() && hour_e.getTime() > hour_sl.getTime()) {
                    $('#form_reservation').submit();
                } else {
                    $('#notification').html('Você precisa selecionar um horário válido. Os horários validos neste espaço são entre <b>' + hour_start + '</b> e <b>' + hour_close + '</b>.');
                }

            }

        });


   });
    

</script>
@endsection
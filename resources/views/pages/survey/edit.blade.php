@extends('layouts.app')

@section('title', 'Editar enquete')

@section('custom-head')

@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card p-5">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-8 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Editar enquete: {{ $content->title }}</h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Heading-->

                    <form action="{{ route('surveys.update', $content->id) }}" class="form" method="post" enctype="multipart/form-data">
                
                        @method('PUT')
                        @include('pages._forms.survey')

                        <!--begin::Actions-->
                        <div class="text-center">
                            <a href="{{ route('surveys.index') }}">
                                <label class="btn btn-light me-3 mb-0">Voltar</label>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label" >Editar enquete</span>
                            </button>
                        </div>
                        <!--end::Actions-->

                    </form>
                    
                </div>

            </div>
        </div>
    </div>
</div>
@endsection


@section('custom-js')
<!-- Calendar DataPicker -->
<script src="https://npmcdn.com/flatpickr/dist/l10n/pt.js"></script>
<script>
    // Calendar DataPicker
    $(".calendario").flatpickr({
        allowInput: true,
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        "locale": "pt",
    });
</script>
@endsection
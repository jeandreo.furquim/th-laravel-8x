@extends('layouts.app')

{{-- TITLE OF PAGE --}}
@section('title', $content->title)

{{-- TITLE OF POST --}}
@section('title-post', $content->title) 

{{-- LINK FOR RETURN --}}
@section('link-back', route('surveys.index'))

{{-- CONTENT --}}
@section('content')
<!-- POST -->
<div class="content flex-column-fluid">
	@include('includes.pageTitle')
	<div class="container">
		<div class="row gy-5 g-xl-8">
			<!-- CONTENT PAGE -->
			<div class="col-xxl-12">
				<div class="card">
					<div class="card-body">
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						@include('includes.imagePost')
						@include('includes.infosPost')
						<!-- IMAGE-POST -->
						<!-- INFOS-POST -->
						<p class="text-justify text-muted mb-0 description-post">
							<div>
								<b>Início: </b><span class="text-muted">{{ date_create($content->start)->format('d/m/Y') }}</span> - <b>Fim: </b><span class="text-muted">{{ date_create($content->end)->format('d/m/Y') }}</span>
							</div>
							
							<p class="text-justify text-muted mb-0 description-post">
								{!! nl2br(e($content->description)) !!}
							</p>
							
							@if($content->votes()->where('survey', $content->id)->where('user', Auth::user()->id)->count()) 
							<div class="alert alert-success mt-5" role="alert">
								Você já votou, você escolheu a opção: <b>
								@if($content->votes()->where('survey', $content->id)->where('user', Auth::user()->id)->first()->option == 1){{ $content->option_01 }} 
								@elseif($content->votes()->where('survey', $content->id)->where('user', Auth::user()->id)->first()->option == 2){{ $content->option_02 }} 
								@elseif($content->votes()->where('survey', $content->id)->where('user', Auth::user()->id)->first()->option == 3){{ $content->option_03 }} 
								@elseif($content->votes()->where('survey', $content->id)->where('user', Auth::user()->id)->first()->option == 4){{ $content->option_04 }} 
								@elseif($content->votes()->where('survey', $content->id)->where('user', Auth::user()->id)->first()->option == 5){{ $content->option_05 }} 
								@endif
								</b>
							</div>
							@endif
							
							@if(date_create($content->start) <= now() && date_create($content->end) >= now())
							@if($content->votes()->where('survey', $content->id)->where('user', Auth::user()->id)->count()) 
							<form action="{{ route('votes.update', $content->id) }}" class="form" method="post" enctype="multipart/form-data">
							@method('PUT')
							@else
							<form action="{{ route('votes.store', $content->id) }}" class="form" method="post" enctype="multipart/form-data">
							@endif
							
								@csrf
								<div class="col p-0 mb-3">
									<!--begin::Input group-->
									<div class="d-flex flex-column mb-4 fv-row mt-3">
										<div class="mb-0">
											<div class="row">
												<div class="col-12">
													<label for="" class="form-label">Selecione uma opção</label>
												</div>
												<div class="form-check form-check-custom form-check-solid ml-3 mb-3">
													<input name="option" value="1" id="option_01" class="form-check-input" type="radio" checked/>
													<label class="form-check-label" for="option_01">
														{{ $content->option_01 }}
													</label>
												</div>
												<div class="form-check form-check-custom form-check-solid ml-3 mb-3">
													<input name="option" value="2" id="option_02" class="form-check-input" type="radio"/>
													<label class="form-check-label" for="option_02">
														{{ $content->option_02 }}
													</label>
												</div>
												@if($content->option_03)
												<div class="form-check form-check-custom form-check-solid ml-3 mb-3">
													<input name="option" value="3" id="option_03" class="form-check-input" type="radio"/>
													<label class="form-check-label" for="option_03">
														{{ $content->option_03 }}
													</label>
												</div>
												@endif
												@if($content->option_04)
												<div class="form-check form-check-custom form-check-solid ml-3 mb-3">
													<input name="option" value="4" id="option_04" class="form-check-input" type="radio"/>
													<label class="form-check-label" for="option_04">
														{{ $content->option_04 }}
													</label>
												</div>
												@endif
												@if($content->option_05)
												<div class="form-check form-check-custom form-check-solid ml-3 mb-3">
													<input name="option" value="5" id="option_05" class="form-check-input" type="radio"/>
													<label class="form-check-label" for="option_05">
														{{ $content->option_05 }}
													</label>
												</div>
												@endif
											</div>
										</div>
									</div>
									@if($content->votes()->where('survey', $content->id)->where('user', Auth::user()->id)->count()) 
									<div class="mt-4">
										<button type="submit" class="btn btn-success">
											<span class="indicator-label" >Atualizar meu voto</span>
										</button>
									@else
									<div class="mt-4">
										<button type="submit" class="btn btn-success">
											<span class="indicator-label" >Votar</span>
										</button>
									@endif
										<a class="btn btn-primary" href="{{ route('surveys.edit', $content->id) }}">Editar</a>
										<a class="btn btn-danger" href="{{ route('surveys.destroy', $content->id) }}">Deletar</a>
									</div>
								</div>
							</form>	
							@elseif(date_create($content->start) > now() && date_create($content->end) > now())
							<div class="alert alert-warning mt-5" role="alert">
								A votação será iniciada em breve.
							</div>
							@elseif(date_create($content->start) < now() && date_create($content->end) < now())
							<div class="alert alert-success mt-5" role="alert">
								Votação encerrada.
							</div>
							@endif
							
							@if(Auth::user()->role == 'administrador')
							<div class="row p-0 rounded mt-7">
								<div class="col-5">
									<div class="p-7 rounded">
										<p class="form-label mb-4 text-center" style="font-weight: 700;">Resultado atual da campanha</p>
										<div class="w-100 mb-4 d-flex justify-content-center align-items-center">
											<p class="percentage d-flex justify-content-center align-items-center">{{ $countVotes }}%</p>
										</div>
										<div class="row">
											<div class="col-4">
												<p class="form-label text-center mb-0" style="font-weight: 700;">Condomínios</p>
												<p class="text-center m-0">{{ $content->usersCond()->count() }}</p>
											</div>
											<div class="col-4">
												<p class="form-label text-center mb-0" style="font-weight: 700;">Total de votos</p>
												<p class="text-center m-0">{{ $content->votes()->where('survey', $content->id)->count() }}</p>
											</div>
											<div class="col-4">
												<p class="form-label text-center mb-0" style="font-weight: 700;">% de votos</p>
												<p class="text-center m-0">{{ $countVotes }}%</p>
											</div>
										</div>
									</div>
								</div>
								<div class="col-7">
									<div class="p-7 rounded">
										<p class="form-label mb-4 text-center" style="font-weight: 700;">Resultado total das opções</p>
										<p class="form-label mb-0" style="font-weight: 700; font-size: 12px;">{{ $content->option_01 }}</p>
										<div class="progress mb-2" style="height: 20px">
											<div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="min-width: 8%; width: {{$countVotes_01 }}%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">{{ $countVotes_01 }}% - {{ $content->votes()->where('survey', $content->id)->where('option', 1)->count() }}</div>
										</div>
										<p class="form-label mb-0" style="font-weight: 700; font-size: 12px;">{{ $content->option_02 }}</p>
										<div class="progress mb-2" style="height: 20px">
											<div class="progress-bar progress-bar-striped progress-bar-animated bg-primary" role="progressbar" style="min-width: 8%; width: {{$countVotes_02 }}%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">{{ $countVotes_02 }}% - {{ $content->votes()->where('survey', $content->id)->where('option', 2)->count() }}</div>
										</div>
										@if($content->option_03)
										<p class="form-label mb-0" style="font-weight: 700; font-size: 12px;">{{ $content->option_03 }}</p>
										<div class="progress mb-2" style="height: 20px">
											<div class="progress-bar progress-bar-striped progress-bar-animated bg-danger" role="progressbar" style="min-width: 8%; width: {{$countVotes_03 }}%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">{{ $countVotes_03 }}% - {{ $content->votes()->where('survey', $content->id)->where('option', 3)->count() }}</div>
										</div>
										@endif
										@if($content->option_04)
										<p class="form-label mb-0" style="font-weight: 700; font-size: 12px;">{{ $content->option_04 }}</p>
										<div class="progress mb-2" style="height: 20px">
											<div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="min-width: 8%; width: {{ $countVotes_04 }}%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">{{ $countVotes_04 }}% - {{ $content->votes()->where('survey', $content->id)->where('option', 4)->count() }}</div>
										</div>
										@endif
										@if($content->option_05)
										<p class="form-label mb-0" style="font-weight: 700; font-size: 12px;">{{ $content->option_05 }}</p>
										<div class="progress mb-2" style="height: 20px">
											<div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar" style="min-width: 8%; width: {{$countVotes_05 }}%;" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">{{ $countVotes_05 }}% - {{ $content->votes()->where('survey', $content->id)->where('option', 5)->count() }}</div>
										</div>
										@endif
									</div>
								</div>
							</div>
							@endif
						</p>
					</div>
				</div>
			</div>
			<!-- COMENTS -->
			<div class="col-xxl-12">
				<!--begin::Feeds Widget 3-->
				<div class="card mb-5 mb-xxl-8">
					<!--begin::Body-->
					<div class="card-body pb-0">
						<h2 class="fw-bolder mb-5">Comentários</h2>
						<div id="load-comments">
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
							<!-- AJAX COMMENTS -->
						</div>
					</div>
					<!--end::Body-->
				</div>
				<!--end::Feeds Widget 3-->
			</div>
			<!-- COMENTS -->
			<!-- CONTENT PAGE -->
		</div>
	</div>
</div>
<!-- POST -->
@endsection

@section('custom-js')
<script>
	// LOAD AJAX
	sistemAjax(8, {{ $content->id }}, {{ $content->condominium }});
</script>
@endsection
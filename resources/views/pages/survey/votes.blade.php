@extends('layouts.app')

@section('title', 'Todas enquetes')

@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-7 mt-7 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Todos as enquetes</h1>
                        <!--end::Title-->
                        
                    </div>
                    <!--end::Heading-->

                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>ID</th>
                                <th>Título</th>
                                <th>Status</th>
                                <th>Votos</th>
                                <th>Mostrar</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $content)
                                <tr>
                                    <td>{{ $content->id }}</td>
                                    <td>{{ $content->title }}</td>
                                    <td>
                                        @if(date_create($content->start) <= now() && date_create($content->end) >= now())<span class="badge badge-light-primary fs-8 fw-bolder">Em andamento</span> 
                                        @elseif(date_create($content->start) > now() && date_create($content->end) > now())<span class="badge badge-light-warning fs-8 fw-bolder">Em breve</span></td>
                                        @elseif(date_create($content->start) < now() && date_create($content->end) < now())<span class="badge badge-light-success fs-8 fw-bolder">Encerrada</span>
                                        @endif
                                    </td>
                                    <td>0 / {{ $content->usersCond()->where('condominium', $content->condominium)->count() }}</td>
                                    <td>{{ $content->show }}</td>
                                    <td class="actions">
                                        <a href="{{ route('surveys.show', $content->id) }}"><i class="fas fa-eye" title="Visualizar"></i></a>
                                        @if(session('sessionCondominium'))
                                        <a href="{{ route('surveys.edit', $content->id) }}"><i class="fas fa-pencil-alt" title="Editar"></i></a>
                                        <a href="{{ route('surveys.destroy', $content->id) }}"><i class="fas fa-trash" title="Excluir"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @if(session('sessionCondominium'))
            <a href="{{ route('surveys.create') }}">
                <button type="submit" class="btn btn-primary w-auto">
                    <span class="indicator-label" >Criar nova enquete</span>
                </button>
            </a>
            @endif
        </div>
    </div>
</div>


@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
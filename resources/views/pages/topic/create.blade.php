@extends('layouts.app')

@section('title', 'Mural do condomínio')

@section('custom-head')

@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card p-5">
                <div class="card-body">
                    <!--begin::Heading-->
                    <div class="mb-8 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-4">Mural do condomínio</h1>
                        <!--end::Title-->
                        <div class="d-flex align-items-center justify-content-center">
                            <div class="">
                                <p class="text-muted fs-6 fw-bold text-center">
                                    Este espaço tem a finalidade de criar interação entre os condôminos. Desde compartilhar com seus vizinhos as melhores indicações do bairro (ou aquelas não recomendáveis), comunicar sobre algum objeto perdido, oferecer ou solicitar carona para levar as crianças na escola etc...<br>
                                    Este é um mural público e todos os moradores conseguirão visualizar.<br>
                                    Façam bom uso!!
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--end::Heading-->
                    <form action="{{ route('topics.store') }}" class="form" method="post" enctype="multipart/form-data">
                       @include('pages._forms.topic')
                        <!--begin::Actions-->
                        <div class="text-center">
                            <a href="{{ route('topics.index') }}">
                                <label class="btn btn-light me-3 mb-0">Voltar</label>
                            </a>
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label" >Adicionar ao mural</span>
                            </button>
                        </div>
                        <!--end::Actions-->
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
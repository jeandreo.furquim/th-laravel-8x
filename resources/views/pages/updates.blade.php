@extends('layouts.app')

@section('title', 'Atualizações')

@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">

                <div class="card-body">

                    <!--begin::Heading-->
                    <div class="mb-7 mt-7 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Últimas Atualizações</h1>
                        <!--end::Title-->
                        
                    </div>
                    <!--end::Heading-->

                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>ID</th>
                                <th>Título</th>
                                <th>Categoria</th>
                                <th>Data de criação</th>
                                <th>Ação</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($contents as $content)
                                <tr>
                                        <td>{{ $content->id }}</td>
                                    <td><a href="{{urlUpdates($content->type, $content->content, $content->user)}}">{{ $content->title }}</a></td>
                                    <td>{{ msgUpdate($content->type, $content->action) }}</td>
                                    <td><span class="badge badge-light fs-8 fw-bolder" title="{{ $content->created_at->format('d/m/Y H:i') }}">{{days($content->created_at)}}</span></td>
                                    <td>
                                        @if($content->action == 0)
                                            <span class="badge badge-light-success">Adicionado</span>
                                        @elseif($content->action == 1)
                                            <span class="badge badge-light-warning">Alterado</span>
                                        @elseif($content->action == 2)
                                            <span class="badge badge-light-danger">Excluído</span>
                                        @else
                                            <span class="badge badge-light-primary">Sistema</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
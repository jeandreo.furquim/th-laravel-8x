@csrf
                <div class="row fv-row mb-7">
                    <!--begin::Col-->
                    <div class="col-xl-6">
                        <label class="form-label fw-bolder text-dark fs-6">Espécie</label>
                        <input class="form-control form-control-lg form-control-solid" type="text" name="species" placeholder="Ex.: cachorro, gato, pássaro etc." value="{{ $pet->species ?? old('species') }}" required="">
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-xl-6">
                        <label class="form-label fw-bolder text-dark fs-6">Nome</label>
                        <input class="form-control form-control-lg form-control-solid" type="text" name="name" placeholder="Ex.: Bob, Gracinha, Max, etc." value="{{ $pet->name ?? old('name') }}" required="">
                    </div>
                    <!--end::Col-->
                </div>
                <div class="row fv-row mb-7">
                    <!--begin::Col-->
                    <div class="col-xl-6">
                        <label class="form-label fw-bolder text-dark fs-6">Raça</label>
                        <input class="form-control form-control-lg form-control-solid" type="text" name="breed" placeholder="Ex.: siamês, labrador, shih-tzu, etc." value="{{ $pet->breed ?? old('breed') }}" required="">
                    </div>
                    <!--end::Col-->
                    <!--begin::Col-->
                    <div class="col-xl-6">
                        <label class="form-label fw-bolder text-dark fs-6">Cor</label>
                        <input class="form-control form-control-lg form-control-solid" type="text" name="color" placeholder="Ex.: preto e branco, caramelo etc." value="{{ $pet->color ?? old('color') }}" required="">
                    </div>
                    <!--end::Col-->
                </div>

                <div class="d-flex flex-column mb-8">
                    <label class="form-label fw-bolder text-dark fs-6">Observação (opicional)</label>
                    <textarea class="form-control form-control-solid" rows="3" name="observation" placeholder="Ex.: ele(a) é dócil? Possui coleira de identificação? Possui controle vacinal em dia? É castrado(a)?
                    ">{{ $pet->observation ?? old('observation') }}</textarea>
                </div>
                <div class="flex-stack mb-12">
                    <!--begin::Label-->
                    <div class="me-5 mb-2">
                        <label class="form-label fw-bolder text-dark fs-6">Imagem do pet (opicional)</label>
                        <div class="fs-7 fw-bold text-muted">Selecione uma imagem para exibir esse pet.</div>
                    </div>
                    <!--end::Label-->
                    <!--begin::Switch-->
                    <label class="form-check form-switch form-check-custom form-check-solid">
                        <input class="form-control form-control-solid" name="image" type="file" id="formFile" accept=".png, .jpg, .jpeg">
                    </label>
                    <!--end::Switch-->
                </div>
@csrf

<!--begin::Input group-->
<div class="row fv-row mb-7">
    <!--begin::Col-->
    <div class="col-xl-6">
        <label class="form-label fw-bolder text-dark fs-6">Tipo de véiculo</label>
        <input class="form-control form-control-lg form-control-solid" type="text" name="type" placeholder="Ex.: Ex.: carro, moto, caminhonete etc." value="{{ $vehicle->type ?? old('type') }}" required="">
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-6">
        <label class="form-label fw-bolder text-dark fs-6">Placa</label>
        <input class="form-control form-control-lg form-control-solid" type="text" name="board" placeholder="Ex.: ABC1D23." value="{{ $vehicle->board ?? old('board') }}" required="">
    </div>
    <!--end::Col-->
</div>
<div class="row fv-row mb-7">
    <!--begin::Col-->
    <div class="col-xl-4">
        <label class="form-label fw-bolder text-dark fs-6">Marca e modelo</label>
        <input class="form-control form-control-lg form-control-solid" type="text" name="brand" placeholder="Ex.: Honda Civic." value="{{ $vehicle->brand ?? old('brand') }}" required="">
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-4">
        <label class="form-label fw-bolder text-dark fs-6">Ano</label>
        <input class="form-control form-control-lg form-control-solid" type="text" name="year" placeholder="Ex.: 2022" value="{{ $vehicle->year ?? old('year') }}" required="">
    </div>
    <!--end::Col-->
    <!--begin::Col-->
    <div class="col-xl-4">
        <label class="form-label fw-bolder text-dark fs-6">Cor</label>
        <input class="form-control form-control-lg form-control-solid" type="text" name="color" placeholder="Ex.: cinza." value="{{ $vehicle->color ?? old('color') }}" required="">
    </div>
    <!--end::Col-->
</div>
<!--end::Input group-->

<!--begin::Input group-->
<div class="d-flex flex-column mb-8">
    <label class="form-label fw-bolder text-dark fs-6">Observação</label>
    <textarea class="form-control form-control-solid" rows="3" name="observation" placeholder="Deixe sua observação aqui">{{ $vehicle->observation ?? old('observation') }}</textarea>
</div>
<div class="flex-stack mb-12">
    <!--begin::Label-->
    <div class="me-5 mb-2">
        <label class="form-label fw-bolder text-dark fs-6">Imagem do veículo</label>
        <div class="fs-7 fw-bold text-muted">Selecione uma imagem para exibir esse veículo.</div>
    </div>
    <!--end::Label-->
    <!--begin::Switch-->
    <label class="form-check form-switch form-check-custom form-check-solid">
        <input class="form-control form-control-solid" name="image" type="file" id="formFile" accept=".png, .jpg, .jpeg">
    </label>
    <!--end::Switch-->
</div>
<!--end::Input group-->
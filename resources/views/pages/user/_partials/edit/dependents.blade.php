<div class="modal-header">
    <h5 class="modal-title fs-2">Editar Morador</h5>

    <!--begin::Close-->
    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
        <span class="svg-icon svg-icon-2x"></span>
    </div>
    <!--end::Close-->
</div>

<div class="modal-body">
    <div class="container">
        <div class="row">
            <form action="{{ route('account.edit.update.dependents', $dependent->id)}}" class="form" method="post" enctype="multipart/form-data">
                @method('PUT')
                @include('pages.user._partials._forms.dependents')
                <!--begin::Actions-->
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <label class="btn btn-light me-3 mb-0"  data-bs-dismiss="modal" aria-label="Close">Cancelar</label>
                    <button type="submit" class="btn btn-primary">
                        <span class="indicator-label" >Editar Morador</span>
                    </button>
                </div>
                <!--end::Actions-->
            </form>
        </div>
      </div>
</div>
<div class="modal-header">
    <h5 class="modal-title fs-2">Funcionário: {{ $employee->name }}</h5>

    <!--begin::Close-->
    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
        <span class="svg-icon svg-icon-2x"></span>
    </div>
    <!--end::Close-->
</div>

<div class="modal-body">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="{{ itemImageUser($employee) }}" class="img-post rounded mx-auto d-block margin-bottom-feature">
            </div>
            <div class="col-md-7 m-auto">
                <p class="text-gray-800 fs-2 fw-bolder me-1">{{ $employee->name }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">RG:</b> {{ $employee->identifier }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Cargo:</b> {{ $employee->office }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Celular:</b> {{ $employee->cellphone }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Telefone:</b> {{ $employee->phone }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Liberado de:</b> {{ $employee->from }} <b class="fw-normal text-gray-800 fw-boldest">até: </b>@if($employee->until) {{ Str::limit($employee->until, 17) }} @else Indeterminado @endif</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Dias liberados:</b> {{ $employee->week }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Observação:</b> {{ $employee->observation }}</p>
            </div>
        </div>
      </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Fechar</button>
</div>

<div class="modal-header">
    <h5 class="modal-title fs-2">Veículo: {{ $vehicle->type }}</h5>

    <!--begin::Close-->
    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
        <span class="svg-icon svg-icon-2x"></span>
    </div>
    <!--end::Close-->
</div>

<div class="modal-body">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="{{ itemImageUser($vehicle) }}" class="img-post rounded mx-auto d-block margin-bottom-feature">
            </div>
            <div class="col-md-7 m-auto">
                <p class="text-gray-800 fs-2 fw-bolder me-1">{{ $vehicle->type }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Marca e modelo:</b> {{ $vehicle->brand }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Cor:</b> {{ $vehicle->color }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Placa:</b> {{ $vehicle->board }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Ano:</b> {{ $vehicle->year }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Observação:</b> {{ $vehicle->observation }}</p>
            </div>
        </div>
      </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Fechar</button>
</div>

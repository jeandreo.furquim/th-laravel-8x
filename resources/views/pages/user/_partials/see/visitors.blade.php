<div class="modal-header">
    <h5 class="modal-title fs-2">Visitante: {{ $visitor->name }}</h5>

    <!--begin::Close-->
    <div class="btn btn-icon btn-sm btn-active-light-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
        <span class="svg-icon svg-icon-2x"></span>
    </div>
    <!--end::Close-->
</div>

<div class="modal-body">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <img src="{{ itemImageUser($visitor) }}" class="img-post rounded mx-auto d-block margin-bottom-feature">
            </div>
            <div class="col-md-7 m-auto">
                <p class="text-gray-800 fs-2 fw-bolder me-1">{{ $visitor->name }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">RG:</b> {{ $visitor->identifier }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Tipo:</b> {{ $visitor->type }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Avisar:</b> {{ $visitor->warn }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Permitir entrada:</b> {{ $visitor->empty }}</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Liberado de:</b> {{ $visitor->from }} <b class="fw-normal text-gray-800 fw-boldest">até: </b>@if($visitor->until) {{ Str::limit($visitor->until, 17) }} @else Indeterminado @endif</p>
                <p class="fw-normal text-muted mb-1"><b class="fw-normal text-gray-800 fw-boldest">Observação:</b> {{ $visitor->observation }}</p>
            </div>
        </div>
      </div>
</div>

<div class="modal-footer">
    <button type="button" class="btn btn-light" data-bs-dismiss="modal">Fechar</button>
</div>

@extends('layouts.app')

@section('title', 'Minhas Publicações')

@section('custom-head')
    <!--begin::Page Vendor Stylesheets(used by this page)-->
    <link href={{asset('assets/plugins/custom/datatables/datatables.bundle.css')}} rel="stylesheet" type="text/css" />
    <!--end::Page Vendor Stylesheets-->
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <div id="kt_content_container" class="container-xxl">
        <div class="row gy-5 g-xl-8">
            <div class="card">
                <div class="card-body">
                    <!--begin::Heading-->
                    <div class="mb-7 mt-7 text-center">
                        <!--begin::Title-->
                        <h1 class="mb-0">Minhas Publicações</h1>
                        <!--end::Title-->
                    </div>
                    <!--end::Heading-->
                    <table id="datatable_default" class="table table-striped table-row-bordered gy-5 gs-7 border rounded">
                        <thead>
                            <tr class="fw-bolder fs-6 text-gray-800 px-7">
                                <th>Título</th>
                                <th>Categoria</th>
                                <th>Tipo</th>
                                <th>Status</th>
                                <th>Data de criação</th>
                                <th>Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($occurrences as $occurrence)
                                <tr>
                                    <td>{{ $occurrence->title }}</td>
                                    <td>Chamados</td>
                                    <td><b>{{ $occurrence->type }}</b></td>
                                    <td>
                                        @if($occurrence->status == 'Aprovado') 
                                        <span class="badge badge-light-success">Aprovada</span>
                                        @elseif($occurrence->status == 'Pendente') 
                                        <span class="badge badge-light-info">Em aprovação</span>
                                        @else
                                        <span class="badge badge-light-danger">Bloqueada</span>
                                        @endif
                                    </td>
                                    <td>{{ $occurrence->created_at->format('d/m/Y H:i') }}</td>
                                    <td class="actions">
                                        <a href="{{ route('occurrences.show', $occurrence->id) }}"><i class="fas fa-eye" title="Visualizar"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            @foreach ($topics as $topic)
                            <tr>
                                <td>{{ $topic->title }}</td>
                                <td>Tópicos do Mural</td>
                                <td><b>{{ $topic->type }}</b></td>
                                <td>
                                    @if($topic->status == 'Aprovado') 
                                    <span class="badge badge-light-success">Aprovada</span>
                                    @elseif($topic->status == 'Pendente') 
                                    <span class="badge badge-light-info">Em aprovação</span>
                                    @else
                                    <span class="badge badge-light-danger">Bloqueada</span>
                                    @endif
                                </td>
                                <td>{{ $topic->created_at->format('d/m/Y H:i') }}</td>
                                <td class="actions">
                                    <a href="{{ route('topics.show', $topic->id) }}"><i class="fas fa-eye" title="Visualizar"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('custom-js')
    @include('layouts.tables')
@endsection
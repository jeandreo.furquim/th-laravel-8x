@extends('layouts.app')

@section('title', $titlePage)

@section('btn-account')
@if(!in_array(Auth::user()->role, ['administrador', 'sindico']))
<a href="{{ route('account.index') }}" class="btn btn-sm btn-primary me-3">Ver minha conta</a>
@else
<a href="{{ route('admin.users.show', $user->id) }}" class="btn btn-sm btn-primary me-3">Visualizar usuário</a>
@endif
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Navbar-->
            <div class="card mb-3 mb-xl-5">
                <div class="card-body pt-9 pb-0">
                    <!--begin::Details-->
                    @include('pages.user._partials.infos')
                    <!--end::Details-->
                </div>
            </div>
            <!--end::Navbar-->
            <!--begin::Basic info-->
            <div class="card mb-5 mb-xl-10">
                <!--begin::Card header-->
                <div class="card-header border-0">
                    <!--begin::Card title-->
                    <div class="card-title m-0">
                        <h3 class="fw-bolder m-0">Editar meus dados</h3>
                    </div>
                    <!--end::Card title-->
                </div>
                <!--begin::Card header-->
                <!--begin::Content-->
                <div id="kt_account_profile_details" class="collapse show">
                    <!--begin::Form-->
                    <form action="{{ $route }}" class="form" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <!--begin::Card body-->
                        <div class="card-body border-top p-9">
                            <!--begin::Input group-->
                            <div class="row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label fw-bold fs-6">Imagem de Perfil</label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-8">
                                    <!--begin::Image input-->
                                    <div class="image-input image-input-outline" data-kt-image-input="true" style="background-image: url(../assets/media/avatars/blank.png)">
                                        <!--begin::Preview existing avatar-->
                                        <div class="image-input-wrapper w-125px h-125px" style="background-image: url( {{ imageUser($user->id) }}); background-position:center;"></div>
                                        <!--end::Preview existing avatar-->
                                        <!--begin::Label-->
                                        <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Escolher imagem">
                                            <i class="bi bi-pencil-fill fs-7"></i>
                                            <!--begin::Inputs-->
                                            <input type="file" name="image" accept=".png, .jpg, .jpeg" />
                                            <input type="hidden" name="avatar_remove" />
                                            <!--end::Inputs-->
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Cancel-->
                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                            <i class="bi bi-x fs-2"></i>
                                        </span>
                                        <!--end::Cancel-->
                                        <!--begin::Remove-->
                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remover imagem de pefil">
                                            <i class="bi bi-x fs-2"></i>
                                        </span>
                                        <!--end::Remove-->
                                    </div>
                                    <!--end::Image input-->
                                    <!--begin::Hint-->
                                    <div class="form-text">Arquivos permitidos: png, jpg, jpeg.</div>
                                    <!--end::Hint-->
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Input group-->
                             <!--begin::Input group-->
                             <div class="row mb-6">
                                <!--begin::Label-->
                                <label class="col-lg-4 col-form-label required fw-bold fs-6">Nome Completo</label>
                                <!--end::Label-->
                                <!--begin::Col-->
                                <div class="col-lg-8 fv-row">
                                    <input type="text" name="name" class="form-control form-control-lg form-control-solid" placeholder="Nome completo" value="{{ $user->name }}" maxlength="255" />
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Input group-->
                            <div class="row fv-row mb-7">
                                <!--begin::Col-->
                                <div class="col-xl-4">
                                    <label class="col-form-label fw-bold fs-6">E-mail</label>
                                    <input class="form-control form-control-lg form-control-solid" type="email" name="email" value="{{ $user->email }}" required="" autocomplete="off" maxlength="255" >
                                </div>
                                <!--end::Col-->
                                <!--begin::Col-->
                                <div class="col-xl-4">
                                    <label class="col-form-label fw-bold fs-6">Sexo</label>
                                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" name="sex" data-placeholder="Selecione uma opção" required>
                                        <option value="">Selecione uma opção...</option>
                                        <option value="masculino" @if ( $user->sex == 'masculino') selected @endif>Masculino</option>
                                        <option value="feminino" @if ( $user->sex == 'feminino') selected @endif>Feminino</option>
                                    </select> 
                                </div>
                                <!--end::Col-->
                               <!--begin::Col-->
                                <div class="col-xl-4">
                                    <label class="col-form-label fw-bold fs-6">Casa</label>
                                    <input class="form-control form-control-lg form-control-solid" type="text" @if(Auth::user()->role != 'administrador')" disabled style="cursor: not-allowed" @else name="house" @endif value="{{ $user->house }}" placeholder="Número do imóvel" @if(Auth::user()->role != 'administrador') title="Você não pode editar o número do seu imóvel." @endif value="{{ $user->house }}" required="" autocomplete="off" inputmode="text" maxlength="255" >
                                </div>
                                <!--end::Col-->
                                <!--begin::Col-->
                                <div class="col-xl-4">
                                    <label class="col-form-label fw-bold fs-6">Celular</label>
                                    <input class="form-control form-control-lg form-control-solid" type="text" id="cellphone" name="cellphone" value="{{ $user->cellphone }}" required="" autocomplete="off" inputmode="text" maxlength="255" >
                                </div>
                                <!--end::Col-->
                                <!--begin::Col-->
                                <div class="col-xl-4">
                                    <label class="col-form-label fw-bold fs-6">CPF (últimos 3 digitos)</label>
                                    <input class="form-control form-control-lg form-control-solid" type="number" max="999" @if(Auth::user()->role != 'administrador') disabled style="cursor: not-allowed" @else name="identifier" @endif value="{{ $user->identifier }}" @if(Auth::user()->role != 'administrador') title="Você não pode editar seu CPF." @endif>
                                </div>
                                <!--end::Col-->
                               <!--begin::Col-->
                                <div class="col-xl-4">
                                    <label class="col-form-label fw-bold fs-6">Data de nascimento</label>
                                    <!--begin::Icon-->
                                    <!--begin::Input-->
                                    <div class="position-relative d-flex align-items-center">
                                        <!--begin::Icon-->
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen014.svg-->
                                        <span class="svg-icon svg-icon-2 position-absolute mx-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="black"></path>
                                                <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="black"></path>
                                                <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="black"></path>
                                            </svg>
                                        </span>
                                        <!--end::Svg Icon-->
                                        <!--end::Icon-->
                                        <!--begin::Datepicker-->
                                        <input class="form-control form-control-solid ps-12 @if(Auth::user()->role != 'administrador')" disabled style="cursor: not-allowed"@else flatpickr" name="birth" @endif value="{{date("d/m/Y", strtotime($user->birth))}}" placeholder="Selecione uma data" @if(Auth::user()->role != 'administrador') title="Você não pode editar a data de nascimento." @endif>
                                        <!--end::Datepicker-->
                                    </div> 
                                    <!--end::Input-->
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card body-->
                        <!--begin::Actions-->
                        <div class="card-footer d-flex justify-content-end py-6 px-9">
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label" >Salvar</span>
                            </button>
                        </div>
                        <!--end::Actions-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Basic info-->
            @if(!in_array(Auth::user()->role, ['administrador', 'portaria']))
            <!--begin::Basic info-->
            <div class="card mb-5 mb-xl-10">
                <!--begin::Card header-->
                <div class="card-header border-0">
                    <!--begin::Card title-->
                    <div class="card-title m-0">
                        <h3 class="fw-bolder m-0">De que forma deseja ser contatado(a)?</h3>
                    </div>
                    <!--end::Card title-->
                </div>
                <!--begin::Card header-->
                <!--begin::Content-->
                <div id="contatos" class="collapse show">
                    <!--begin::Form-->
                    <form action="{{ route('account.contacts')}}" class="form" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <!--begin::Card body-->
                        <div class="card-body border-top p-9">
                                <p class="mt-1 mb-0"><b style="color: red;">Atenção:</b> TODA informação/comunicação será feita, sem exceções, via site da TH Condomínios – Área do Condômino, podendo ser consultado a qualquer tempo</p>
                                <div class="row fv-row mb-7">
                                <!--begin::Col-->
                                <div class="col-xl-3">
                                    <label class="col-form-label fw-bold fs-6 required">Whatsapp</label>
                                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" name="contacts[]" data-placeholder="Selecione uma opção" required>
                                        <option value="">Selecione uma opção...</option>
                                        <option value="0" @if($user->contacts && json_decode($user->contacts)[0] == 0) selected @endif>Sempre me chame aqui</option>
                                        <option value="1" @if($user->contacts && json_decode($user->contacts)[0] == 1) selected @endif>Me chame aqui em emergências</option>
                                        <option value="2" @if($user->contacts && json_decode($user->contacts)[0] == 2) selected @endif>Nunca me chame por aqui</option>
                                    </select> 
                                </div>
                                <!--end::Col-->
                                <!--begin::Col-->
                                <div class="col-xl-3">
                                    <label class="col-form-label fw-bold fs-6 required">E-mail</label>
                                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" name="contacts[]" data-placeholder="Selecione uma opção" required>
                                        <option value="">Selecione uma opção...</option>
                                        <option value="0" @if($user->contacts && json_decode($user->contacts)[1] == 0) selected @endif>Sempre me chame aqui</option>
                                        <option value="1" @if($user->contacts && json_decode($user->contacts)[1] == 1) selected @endif>Me chame aqui em emergências</option>
                                        <option value="2" @if($user->contacts && json_decode($user->contacts)[1] == 2) selected @endif>Nunca me chame por aqui</option>
                                    </select> 
                                </div>
                                <!--end::Col-->
                                <!--begin::Col-->
                                <div class="col-xl-3">
                                    <label class="col-form-label fw-bold fs-6 required">Telegram</label>
                                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" name="contacts[]" data-placeholder="Selecione uma opção" required>
                                        <option value="">Selecione uma opção...</option>
                                        <option value="0" @if($user->contacts && json_decode($user->contacts)[2] == 0) selected @endif>Sempre me chame aqui</option>
                                        <option value="1" @if($user->contacts && json_decode($user->contacts)[2] == 1) selected @endif>Me chame aqui em emergências</option>
                                        <option value="2" @if($user->contacts && json_decode($user->contacts)[2] == 2) selected @endif>Nunca me chame por aqui</option>
                                    </select> 
                                </div>
                                <!--end::Col-->
                                <!--begin::Col-->
                                <div class="col-xl-3">
                                    <label class="col-form-label fw-bold fs-6 required">Documento Impresso</label>
                                    <select class="form-select form-select-solid" data-control="select2" data-hide-search="true" name="contacts[]" data-placeholder="Selecione uma opção" required>
                                        <option value="">Selecione uma opção...</option>
                                        <option value="0" @if($user->contacts && json_decode($user->contacts)[3] == 0) selected @endif>Sempre me chame aqui</option>
                                        <option value="1" @if($user->contacts && json_decode($user->contacts)[3] == 1) selected @endif>Me chame aqui em emergências</option>
                                        <option value="2" @if($user->contacts && json_decode($user->contacts)[3] == 2) selected @endif>Nunca me chame por aqui</option>
                                    </select> 
                                </div>
                                <!--end::Col-->
                            </div>
                            <!--end::Input group-->
                        </div>
                        <!--end::Card body-->
                        <!--begin::Actions-->
                        <div class="card-footer d-flex justify-content-end py-6 px-9">
                            <button type="submit" class="btn btn-primary">
                                <span class="indicator-label" >Atualizar meus dados</span>
                            </button>
                        </div>
                        <!--end::Actions-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Basic info-->
            <!--begin::Deactivate Account-->
            <div class="card" id="desativar">
                <!--begin::Card header-->
                <div class="card-header border-0">
                    <div class="card-title m-0">
                        <h3 class="fw-bolder m-0">Desativar Minha Conta</h3>
                    </div>
                </div>
                <!--end::Card header-->
                <!--begin::Content-->
                <div class="collapse show">
                    <!--begin::Form-->
                    <form class="form" method="POST" action="{{ route('account.destroy') }}">
                        @csrf
                        @method('delete')
                        <!--begin::Card body-->
                        <div class="card-body border-top p-9">
                            <!--begin::Notice-->
                            <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-9 p-6">
                                <!--begin::Icon-->
                                <!--begin::Svg Icon | path: icons/duotune/general/gen044.svg-->
                                <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                                        <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                                        <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                                <!--end::Icon-->
                                <!--begin::Wrapper-->
                                <div class="d-flex flex-stack flex-grow-1">
                                    <!--begin::Content-->
                                    <div class="fw-bold">
                                        <h4 class="text-gray-800 fw-bolder">Você Está Desativando Sua Conta</h4>
                                        <div class="fs-6 text-gray-800 mb-4">Você está de mudança de endereço? Deixará de ser condômino/morador do <b>{{ Auth::user()->cond()->first()->name }}</b>? Faça a solicitação da exclusão do seu cadastro abaixo...</div>
                                        <p class="fs-7 text-gray-800">O cadastramento do condômino (proprietário) é item contratual obrigatório para o exercício da sindicatura pela TH Condomínios, portanto, a exclusão dos dados cadastrais do atual proprietário do imóvel, desta plataforma, será realizada concomitantemente à confirmação do cadastramento do novo condômino, seguindo as diretrizes da LGPD e metodologia de trabalho da síndica Thays Hoeffling.</p>
                                    </div>
                                    <!--end::Content-->
                                </div>
                                <!--end::Wrapper-->
                            </div>
                            <!--end::Notice-->
                            <!--begin::Form input row-->
                            <div class="form-check form-check-solid fv-row">
                                <input name="deactivate" class="form-check-input" type="checkbox" value="" required id="deactivate" />
                                <label class="form-check-label fw-bold ps-2 fs-6" for="deactivate">Eu confirmo a desativação da minha conta</label>
                            </div>
                            <!--end::Form input row-->
                        </div>
                        <!--end::Card body-->
                        <!--begin::Card footer-->
                        <div class="card-footer d-flex justify-content-end py-6 px-9">
                            <a href="{{ route('account.index') }}">
                                <label class="btn btn-light me-3 mb-0">Voltar</label>
                            </a>
                            <button type="submit" class="btn btn-danger fw-bold">Solicitar desativação de conta</button>
                        </div>
                        <!--end::Card footer-->
                    </form>
                    <!--end::Form-->
                </div>
                <!--end::Content-->
            </div>
            <!--end::Deactivate Account-->
            @endif
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
</div>
            
@endsection


@section('custom-js')
<!-- Calendar DataPicker -->
<script src="https://npmcdn.com/flatpickr/dist/l10n/pt.js"></script>
<script type="text/javascript">
    
    // Calendar DataPicker
    $(".flatpickr").flatpickr({
        allowInput: true,
        altInput: true,
        altFormat: "d/m/Y",
        dateFormat: "Y-m-d",
        "locale": "pt",
    });

</script>
@endsection
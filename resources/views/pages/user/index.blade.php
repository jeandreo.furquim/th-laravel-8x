@extends('layouts.app')

@section('title', $titlePage)

@section('btn-account')
@if(!in_array(Auth::user()->role, ['administrador', 'sindico']))
<a href="{{ route('account.edit') }}" class="btn btn-sm btn-primary me-3">Editar conta</a>
@else
<a href="{{ route('admin.users.edit', $user->id) }}" class="btn btn-sm btn-primary me-3">Editar este usuário</a>
@endif
@endsection

@section('content')
<div class="content d-flex flex-column flex-column-fluid pt-7 pd-md-0">
    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Navbar-->
            <div class="card mb-3 mb-xl-1">
                <div class="card-body pt-9 pb-0">
                    <!--begin::Details-->
                    @include('pages.user._partials.infos')
                    <!--end::Details-->
                </div>
            </div>
            <!--end::Navbar-->
            <!--begin::Row-->
            <div class="row gy-5 gx-xl-10 p-3 pb-0 mb-0">
                <!--begin::Col-->
                <div class="col-xl-4 col-md-6 p-2 pt-0 pb-0">
                    <!--begin::List Widget 5-->
                    <div class="card card-xl-stretch mb-xl-5">
                        <!--begin::Header-->
                        <div class="card-header align-items-center border-0 mt-4">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="fw-bolder mb-2 text-dark">Veículos</span>
                                @if ($vehicles->count())
                                <span class="text-muted fw-bold fs-7">Última atualização {{ date_format(date_create($vehicles->first()->updated_at), 'd/m/Y - H:i') }}</span>
                                @else
                                <span class="text-muted fw-bold fs-7">Nenhuma atualização recente</span>
                                @endif                                
                            </h3>
                            <div class="card-toolbar">
                                <!--begin::Menu-->
                                <span class="bg-light p-1 rounded" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                    <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                    <i class="las la-car-alt fs-2qx text-muted"></i>
                                    <!--end::Svg Icon-->
                                </span>
                                <!--end::Menu-->
                            </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-5">
                            <!-- BEGIN CARS -->
                            @if ($vehicles->count())
                                @foreach ($vehicles as $vehicle)
                                    <div class="mySlides-vehicle">
                                        <!--begin::SliderDiv-->
                                        <div class="row">
                                            <div class="col-xl-6 p-2 pt-0 pb-0 mb-4">
                                                <img src="{{ itemImageUser($vehicle) }}" alt="" class="img-fluid rounded img-cards-user">
                                            </div>
                                            <div class="col-xl-6 p-2 pt-0 pb-0 d-flex justify-details">
                                                <div class="ml-0 mt-auto mb-auto">
                                                    <p class="fw-boldest text-muted mb-1 text-gray-800 font-weight-bold title-boxes-account">{{ Str::limit($vehicle->type, 17) }}</p>
                                                    <p class="fw-normal text-muted mb-1">Cor: {{ Str::limit($vehicle->color, 16) }}</p>
                                                    <p class="fw-normal text-muted mb-1">Placa: {{ Str::limit($vehicle->board, 13) }}</p>
                                                    <p class="fw-normal text-muted mb-1">Tipo: {{ Str::limit($vehicle->color, 15) }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="fw-light text-muted mt-6 text-center">
                                            <i>
                                                <a href="{{ route('account.edit.vehicles', $vehicle->id) }}" class="ajax-see text-muted"  data-bs-toggle="modal" data-bs-target="#modal_details">Editar</a> /
                                                <a href="{{ route('account.see.vehicles', $vehicle->id) }}" class="ajax-see text-muted"  data-bs-toggle="modal" data-bs-target="#modal_details">+informações</a> /
                                                <a href="{{ route('account.destroy.vehicles', $vehicle->id) }}" class="text-muted ajax-confirm"  data-bs-toggle="modal" data-bs-target="#modal_confirm">Deletar</a>
                                            </i>
                                        </p>
                                    </div>
                                @endforeach
                            @else
                                <div class="w-100 d-flex align-items-center justify-content-center div-none-items">
                                    <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen044.svg-->
                                        <span class="svg-icon svg-icon-muted svg-icon-3hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                        <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black"/>
                                        <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                        </svg></span>
                                    <!--end::Svg Icon-->
                                    <span class="text-muted fw-bold fs-5 ml-2">Sem carros adicionados</span>
                                </div>
                            @endif
                            <!-- BEGIN DOTS -->
                            <div class="d-flex justify-content-center mb-3">
                                @for ($i = 1; $i < ($vehicles->count() + 1); $i++)
                                    <span class='dot-vehicle' onclick="currentSlide({{$i}}, 'vehicle')"></span>
                                @endfor
                            </div>
                            <!-- END DOTS -->
                            <!-- END CARS -->
                            <a href="{{ route('account.add.vehicles', $user->id) }}" class="ajax-see">
                                <button type="button" class="btn btn-light w-100 mt-5" data-bs-toggle="modal" data-bs-target="#modal_details">
                                    Adicionar novo veículo
                                </button>
                            </a>
                            <!--end::SliderDiv-->
                        </div>
                        <!--end: Card Body-->
                    </div>
                    <!--end: List Widget 5-->
                </div>
                <!--end::Col-->
                <!--begin::Col-->
                <div class="col-xl-4 col-md-6 p-2 pt-0 pb-0">
                    <!--begin::List Widget 5-->
                    <div class="card card-xl-stretch mb-xl-5">
                        <!--begin::Header-->
                        <div class="card-header align-items-center border-0 mt-4">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="fw-bolder mb-2 text-dark">Animais / Pets</span>
                                @if ($pets->count())
                                <span class="text-muted fw-bold fs-7">Última atualização {{ date_format(date_create($pets->first()->updated_at), 'd/m/Y - H:i') }}</span>
                                @else
                                <span class="text-muted fw-bold fs-7">Nenhuma atualização recente</span>
                                @endif
                            </h3>
                            <div class="card-toolbar">
                                <div class="card-toolbar">
                                    <!--begin::Menu-->
                                    <span class="bg-light p-1 rounded" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                        <i class="las la-dog fs-2qx text-muted"></i>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <!--end::Menu-->
                                </div>
                            </div>
                        </div>
                        <!--end::Header-->
                         <!--begin::Body-->
                         <div class="card-body pt-5">
                            <!-- BEGIN PETS -->
                            @if ($pets->count())
                                @foreach ($pets as $pet)
                                    <div class="mySlides-pet">
                                        <!--begin::SliderDiv-->
                                        <div class="row">
                                            <div class="col-xl-6 p-2 pt-0 pb-0 mb-4">
                                                <img src="{{ itemImageUser($pet) }}" alt="" class="img-fluid rounded img-cards-user">
                                            </div>
                                            <div class="col-xl-6 p-2 pt-0 pb-0 d-flex justify-details">
                                                <div class="ml-0 mt-auto mb-auto">
                                                    <p class="fw-boldest text-muted mb-1 text-gray-800 font-weight-bold title-boxes-account">{{ Str::limit($pet->name, 17) }}</p>
                                                    <p class="fw-normal text-muted mb-1">Espécie: {{ Str::limit($pet->species, 14) }}</p>
                                                    <p class="fw-normal text-muted mb-1">Raça: {{ Str::limit($pet->breed, 16) }}</p>
                                                    <p class="fw-normal text-muted mb-1">Cor: {{ Str::limit($pet->color, 17) }}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="fw-light text-muted mt-6 text-center">
                                            <i>
                                                <a href="{{ route('account.edit.pets', $pet->id) }}" class="ajax-see text-muted"  data-bs-toggle="modal" data-bs-target="#modal_details">Editar</a> /
                                                <a href="{{ route('account.see.pets', $pet->id) }}" class="ajax-see text-muted"  data-bs-toggle="modal" data-bs-target="#modal_details">+informações</a> /
                                                <a href="{{ route('account.destroy.pets', $pet->id) }}" class="text-muted ajax-confirm"  data-bs-toggle="modal" data-bs-target="#modal_confirm">Deletar</a>
                                            </i>
                                        </p>
                                    </div>
                                @endforeach
                            @else
                                <div class="w-100 d-flex align-items-center justify-content-center div-none-items">
                                    <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen044.svg-->
                                        <span class="svg-icon svg-icon-muted svg-icon-3hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                        <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black"/>
                                        <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                        </svg></span>
                                    <!--end::Svg Icon-->
                                    <span class="text-muted fw-bold fs-5 ml-2">Sem pets adicionais</span>
                                </div>
                            @endif
                            <!-- BEGIN DOTS -->
                            <div class="d-flex justify-content-center mb-3">
                                @for ($i = 1; $i < ($pets->count() + 1); $i++)
                                    <span class='dot-pet' onclick="currentSlide({{$i}}, 'pet')"></span>
                                @endfor
                            </div>
                            <!-- END DOTS -->
                            <!-- END PETS -->
                            <a href="{{ route('account.add.pets', $user->id) }}" class="ajax-see">
                                <button type="button" class="btn btn-light w-100 mt-5" data-bs-toggle="modal" data-bs-target="#modal_details">
                                    Adicionar novo animal
                                </button>
                            </a>
                            <!--end::SliderDiv-->
                        </div>
                        <!--end: Card Body-->
                    </div>
                    <!--end: List Widget 5-->
                </div>
                <!--end::Col-->
                <!--begin::Col-->
                <div class="col-xl-4 col-md-6 p-2 pt-0 pb-0">
                    <!--begin::List Widget 5-->
                    <div class="card card-xl-stretch mb-xl-5">
                        <!--begin::Header-->
                        <div class="card-header align-items-center border-0 mt-4">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="fw-bolder mb-2 text-dark">Funcionários</span>
                                @if ($employees->count())
                                <span class="text-muted fw-bold fs-7">Última atualização {{ date_format(date_create($employees->first()->updated_at), 'd/m/Y - H:i') }}</span>
                                @else
                                <span class="text-muted fw-bold fs-7">Nenhuma atualização recente</span>
                                @endif
                            </h3>
                            <div class="card-toolbar">
                                <div class="card-toolbar">
                                    <!--begin::Menu-->
                                    <span class="bg-light p-1 rounded" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                        <i class="las la-user-nurse fs-2qx text-muted"></i>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <!--end::Menu-->
                                </div>
                            </div>
                        </div>
                        <!--end::Header-->
                         <!--begin::Body-->
                         <div class="card-body pt-5">
                            <!-- BEGIN EMPLOYEES -->
                            @if ($employees->count())
                                @foreach ($employees as $employee)
                                    <div class="mySlides-employees">
                                        <!--begin::SliderDiv-->
                                        <div class="row">
                                            <div class="col-xl-6 p-2 pt-0 pb-0 mb-4">
                                                <img src="{{ itemImageUser($employee) }}" alt="" class="img-fluid rounded img-cards-user">
                                            </div>
                                            <div class="col-xl-6 p-2 pt-0 pb-0 d-flex justify-details">
                                                <div class="ml-0 mt-auto mb-auto">
                                                    <p class="fw-boldest text-muted mb-1 text-gray-800 font-weight-bold title-boxes-account">{{ Str::limit($employee->name, 17) }}</p>
                                                    <p class="fw-normal text-muted mb-1">Cargo: {{ Str::limit($employee->office, 14) }}</p>
                                                    <p class="fw-normal text-muted mb-1">Celular: {{ Str::limit($employee->cellphone, 16) }}</p>
                                                    <p class="fw-normal text-muted mb-1">Liber. até: @if($employee->until) {{ Str::limit($employee->until, 17) }} @else Indeterminado @endif</p>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="fw-light text-muted mt-6 text-center">
                                            <i>
                                                <a href="{{ route('account.edit.employees', $employee->id) }}" class="ajax-see text-muted"  data-bs-toggle="modal" data-bs-target="#modal_details">Editar</a> /
                                                <a href="{{ route('account.see.employees', $employee->id) }}" class="ajax-see text-muted"  data-bs-toggle="modal" data-bs-target="#modal_details">+informações</a> /
                                                <a href="{{ route('account.destroy.employees', $employee->id) }}" class="text-muted ajax-confirm"  data-bs-toggle="modal" data-bs-target="#modal_confirm">Deletar</a>
                                            </i>
                                        </p>
                                    </div>
                                @endforeach
                            @else
                                <div class="w-100 d-flex align-items-center justify-content-center div-none-items">
                                    <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen044.svg-->
                                        <span class="svg-icon svg-icon-muted svg-icon-3hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                        <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black"/>
                                        <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                        </svg></span>
                                    <!--end::Svg Icon-->
                                    <span class="text-muted fw-bold fs-5 ml-2">Sem funcionários adicionados</span>
                                </div>
                            @endif
                            <!-- BEGIN DOTS -->
                            <div class="d-flex justify-content-center mb-3">
                                @for ($i = 1; $i < ($employees->count() + 1); $i++)
                                    <span class='dot-employees' onclick="currentSlide({{$i}}, 'employees')"></span>
                                @endfor
                            </div>
                            <!-- END DOTS -->
                            <!-- END EMPLOYEES -->
                            <a href="{{ route('account.add.employees', $user->id) }}" class="ajax-see">
                                <button type="button" class="btn btn-light w-100 mt-5" data-bs-toggle="modal" data-bs-target="#modal_details">
                                    Adicionar novo funcionário
                                </button>
                            </a>
                            <!--end::SliderDiv-->
                        </div>
                        <!--end: Card Body-->
                    </div>
                    <!--end: List Widget 5-->
                </div>
                <!--end::Col-->
                <!--begin::Col-->
                <div class="col-xl-4 col-md-6 p-2 pt-0 pb-0 desk-margin-top">
                    <!--begin::List Widget 5-->
                    <div class="card card-xl-stretch mb-xl-5">
                        <!--begin::Header-->
                        <div class="card-header align-items-center border-0 mt-4">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="fw-bolder mb-2 text-dark">Visitantes</span>
                                @if ($visitors->count())
                                <span class="text-muted fw-bold fs-7">Última atualização {{ date_format(date_create($visitors->first()->updated_at), 'd/m/Y - H:i') }}</span>
                                @else
                                <span class="text-muted fw-bold fs-7">Nenhuma atualização recente</span>
                                @endif     
                            </h3>
                            <div class="card-toolbar">
                                <div class="card-toolbar">
                                    <!--begin::Menu-->
                                    <span class="bg-light p-1 rounded" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                        <i class="las la-users fs-2qx text-muted"></i>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <!--end::Menu-->
                                </div>
                            </div>
                        </div>
                        <!--end::Header-->
                         <!--begin::Body-->
                         <div class="card-body pt-5">
                            <!-- BEGIN VISITORS -->
                            @if ($visitors->count())
                            @foreach ($visitors as $visitor)
                                <div class="mySlides-visitors">
                                    <!--begin::SliderDiv-->
                                    <div class="row">
                                        <div class="col-xl-6 p-2 pt-0 pb-0 mb-4">
                                            <img src="{{ itemImageUser($visitor) }}" alt="" class="img-fluid rounded img-cards-user">
                                        </div>
                                        <div class="col-xl-6 p-2 pt-0 pb-0 d-flex justify-details">
                                            <div class="ml-0 mt-auto mb-auto">
                                                <p class="fw-boldest text-muted mb-1 text-gray-800 font-weight-bold title-boxes-account">{{ Str::limit($visitor->name, 17) }}</p>
                                                <p class="fw-normal text-muted mb-1">RG: {{ Str::limit($visitor->identifier, 14) }}</p>
                                                <p class="fw-normal text-muted mb-1">Interfonar: {{ Str::limit($visitor->warn, 16) }}</p>
                                                <p class="fw-normal text-muted mb-1">Esta autorizado: {{ Str::limit($visitor->empty, 17) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="fw-light text-muted mt-6 text-center">
                                        <i>
                                            <a href="{{ route('account.edit.visitors', $visitor->id) }}" class="ajax-see text-muted"  data-bs-toggle="modal" data-bs-target="#modal_details">Editar</a> /
                                            <a href="{{ route('account.see.visitors', $visitor->id) }}" class="ajax-see text-muted"  data-bs-toggle="modal" data-bs-target="#modal_details">+informações</a> /
                                            <a href="{{ route('account.destroy.visitors', $visitor->id) }}" class="text-muted ajax-confirm"  data-bs-toggle="modal" data-bs-target="#modal_confirm">Deletar</a>
                                        </i>
                                    </p>
                                </div>
                            @endforeach
                        @else
                            <div class="w-100 d-flex align-items-center justify-content-center div-none-items">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen044.svg-->
                                    <span class="svg-icon svg-icon-muted svg-icon-3hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                    <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black"/>
                                    <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                    </svg></span>
                                <!--end::Svg Icon-->
                                <span class="text-muted fw-bold fs-5 ml-2">Sem visitantes adicionados</span>
                            </div>
                        @endif
                        <!-- BEGIN DOTS -->
                        <div class="d-flex justify-content-center mb-3">
                            @for ($i = 1; $i <($visitors->count() + 1); $i++)
                                <span class='dot-visitors' onclick="currentSlide({{$i}}, 'visitors')"></span>
                            @endfor
                        </div>
                        <!-- END DOTS -->
                        <!-- END VISITORS -->
                            <a href="{{ route('account.add.visitors', $user->id) }}" class="ajax-see">
                                <button type="button" class="btn btn-light w-100 mt-5" data-bs-toggle="modal" data-bs-target="#modal_details">
                                    Adicionar novo visitante
                                </button>
                            </a>
                            <!--end::SliderDiv-->
                        </div>
                        <!--end: Card Body-->
                    </div>
                    <!--end: List Widget 5-->
                </div>
                <!--end::Col-->
                <!--begin::Col-->
                <div class="col-xl-4 col-md-6 p-2 pt-0 pb-0 desk-margin-top">
                    <!--begin::List Widget 5-->
                    <div class="card card-xl-stretch mb-xl-5">
                        <!--begin::Header-->
                        <div class="card-header align-items-center border-0 mt-4">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="fw-bolder mb-2 text-dark">Moradores</span>
                                @if ($dependents->count())
                                <span class="text-muted fw-bold fs-7">Última atualização {{ date_format(date_create($dependents->first()->updated_at), 'd/m/Y - H:i') }}</span>
                                @else
                                <span class="text-muted fw-bold fs-7">Nenhuma atualização recente</span>
                                @endif     
                            </h3>
                            <div class="card-toolbar">
                                <div class="card-toolbar">
                                    <!--begin::Menu-->
                                    <span class="bg-light p-1 rounded" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                        <i class="las la-user-alt fs-2qx text-muted"></i>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <!--end::Menu-->
                                </div>
                            </div>
                        </div>
                        <!--end::Header-->
                         <!--begin::Body-->
                         <div class="card-body pt-5">
                            <!-- BEGIN DEPENDENTS -->
                            @if ($dependents->count())
                            @foreach ($dependents as $dependent)
                                <div class="mySlides-dependents">
                                    <!--begin::SliderDiv-->
                                    <div class="row">
                                        <div class="col-xl-6 p-2 pt-0 pb-0 mb-4">
                                            <img src="{{ itemImageUser($dependent) }}" alt="" class="img-fluid rounded img-cards-user">
                                        </div>
                                        <div class="col-xl-6 p-2 pt-0 pb-0 d-flex justify-details">
                                            <div class="ml-0 mt-auto mb-auto">
                                                <p class="fw-boldest text-muted mb-1 text-gray-800 font-weight-bold title-boxes-account">{{ Str::limit($dependent->name, 17) }}</p>
                                                <p class="fw-normal text-muted mb-1">CPF: {{ Str::limit($dependent->identifier, 16) }}</p>
                                                <p class="fw-normal text-muted mb-1">Nasci.: {{ Str::limit($dependent->birth, 17) }}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <p class="fw-light text-muted mt-6 text-center">
                                        <i>
                                            <a href="{{ route('account.edit.dependents', $dependent->id) }}" class="ajax-see text-muted"  data-bs-toggle="modal" data-bs-target="#modal_details">Editar</a> /
                                            <a href="{{ route('account.see.dependents', $dependent->id) }}" class="ajax-see text-muted"  data-bs-toggle="modal" data-bs-target="#modal_details">+informações</a> /
                                            <a href="{{ route('account.destroy.dependents', $dependent->id) }}" class="text-muted ajax-confirm"  data-bs-toggle="modal" data-bs-target="#modal_confirm">Deletar</a>
                                        </i>
                                    </p>
                                </div>
                            @endforeach
                        @else
                            <div class="w-100 d-flex align-items-center justify-content-center div-none-items">
                                <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen044.svg-->
                                    <span class="svg-icon svg-icon-muted svg-icon-3hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                    <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black"/>
                                    <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                    </svg></span>
                                <!--end::Svg Icon-->
                                <span class="text-muted fw-bold fs-5 ml-2">Sem moradores adicionados</span>
                            </div>
                        @endif
                        <!-- BEGIN DOTS -->
                        <div class="d-flex justify-content-center mb-3">
                            @for ($i = 1; $i < ($dependents->count() + 1); $i++)
                                <span class='dot-dependents' onclick="currentSlide({{$i}}, 'dependents')"></span>
                            @endfor
                        </div>
                        <!-- END DOTS -->
                        <!-- END DEPENDENTS -->
                            <a href="{{ route('account.add.dependents', $user->id) }}" class="ajax-see">
                                <button type="button" class="btn btn-light w-100 mt-5" data-bs-toggle="modal" data-bs-target="#modal_details">
                                    Adicionar novo morador
                                </button>
                            </a>
                            <!--end::SliderDiv-->
                        </div>
                        <!--end: Card Body-->
                    </div>
                    <!--end: List Widget 5-->
                </div>
                <!--end::Col-->
                <!--begin::Col-->
                <div class="col-xl-4 col-md-6 p-2 pt-0 pb-0 desk-margin-top">
                    <!--begin::List Widget 5-->
                    <div class="card card-xl-stretch mb-xl-5">
                        <!--begin::Header-->
                        <div class="card-header align-items-center border-0 mt-4">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="fw-bolder mb-2 text-dark">Minhas reservas</span>
                                @if ($reservations->count())
                                <span class="text-muted fw-bold fs-7">Última atualização {{ date_format(date_create($reservations->first()->updated_at), 'd/m/Y - H:i') }}</span>
                                @else
                                <span class="text-muted fw-bold fs-7">Nenhuma atualização recente</span>
                                @endif
                            </h3>
                            <div class="card-toolbar">
                                <div class="card-toolbar">
                                    <!--begin::Menu-->
                                    <span class="bg-light p-1 rounded" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                        <!--begin::Svg Icon | path: icons/duotune/general/gen024.svg-->
                                        <i class="las la-calendar-check fs-2qx text-muted"></i>
                                        <!--end::Svg Icon-->
                                    </span>
                                    <!--end::Menu-->
                                </div>
                            </div>
                        </div>
                        <!--end::Header-->
                        <!--begin::Body-->
                        <div class="card-body pt-5">
                            <!--begin::Timeline-->
                            @if($reservations->count())
                            <div class="timeline-label">
                                @foreach($reservations as $reservation)
                                <!--begin::Item-->
                                <div class="timeline-item">
                                    <!--begin::Label-->
                                    <div class="timeline-label fw-normal text-gray-800 fs-6">08:42</div>
                                    <!--end::Label-->
                                    <!--begin::Badge-->
                                    <div class="timeline-badge">
                                        @if($reservation->status == 'pendente')
                                        <i class="fa fa-genderless text-warning fs-1"></i>
                                        @elseif($reservation->status == 'aprovado')
                                        <i class="fa fa-genderless text-success fs-1"></i>
                                        @elseif($reservation->status == 'manutencao')
                                        <i class="fa fa-genderless text-primary fs-1"></i>
                                        @else
                                        <i class="fa fa-genderless text-danger fs-1"></i>
                                        @endif
                                    </div>
                                    <!--end::Badge-->
                                    <!--begin::Text-->
                                    <div class="timeline-content fw-bold text-gray-800 ps-3">{{ $reservation->room()->first()->name }}
                                    <p class="m-0 p-0 text-muted">{{ date_format(date_create($reservation->day), 'd/m/Y') . ' ' . date_format(date_create($reservation->hour), 'H:i') }}</p></div>
                                    @if($reservation->status == 'pendente')
                                    <span class="badge badge-light-warning fs-8 fw-bolder">Pendente</span>
                                    @elseif($reservation->status == 'aprovado')
                                    <span class="badge badge-light-success fs-8 fw-bolder">Aprovado</span>
                                    @elseif($reservation->status == 'manutencao')
                                    <span class="badge badge-light-primary fs-8 fw-bolder">Manutenção</span>
                                    @else
                                    <span class="badge badge-light-danger fs-8 fw-bolder">Recusado</span>
                                    @endif
                                    <!--end::Text-->
                                </div>
                                <!--end::Item-->
                                @endforeach
                            </div>
                            @else
                                <div class="w-100 d-flex align-items-center justify-content-center div-none-items">
                                    <!--begin::Svg Icon | path: assets/media/icons/duotune/general/gen044.svg-->
                                        <span class="svg-icon svg-icon-muted svg-icon-3hx"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                        <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black"/>
                                        <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                        </svg></span>
                                    <!--end::Svg Icon-->
                                    <span class="text-muted fw-bold fs-5 ml-2">Sem reservas no momento</span>
                                </div>
                            @endif
                            <div class="d-flex justify-content-center mb-3">
                            </div>
                            <!--end::Timeline-->
                            <a href="{{ route('rooms.index') }}" class="">
                                <button type="button" class="btn btn-light w-100 mt-5">
                                    Reservar um espaço
                                </button>
                            </a>
                        </div>
                        <!--end: Card Body-->
                    </div>
                    <!--end: List Widget 5-->
                </div>
                <!--end::Col-->
            </div>
            <!--end::Row-->
            <!-- Modal -->
            <div class="modal fade" tabindex="-1" id="modal_confirm">
                <div class="modal-dialog mw-600px">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Atenção</h5>
            
                            <!--begin::Close-->
                            <div class="btn btn-icon btn-sm btn-light text-primary ms-2" data-bs-dismiss="modal" aria-label="Close">
                                <i class="fas fa-times"></i>
                            </div>
                            <!--end::Close-->
                        </div>
            
                        <div class="modal-body">
                            <p>Você tem certeza que deseja mesmo deletar isso?</p>
                        </div>
            
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light" data-bs-dismiss="modal">Fechar</button>
                            <a type="button" class="btn btn-primary" id="btn-confirm">Sim, vou remover!</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- MODAL DE BUSCA -->
            <div class="modal fade" tabindex="-1" id="modal_details">  
                <div class="modal-dialog">
                    <div class="modal-content" id="modal-content">
                        
                    </div>
                </div>
            </div>
            <!-- END MODAL DE BUSCA -->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->
</div>


@endsection


@section('custom-js')

<!-- Calendar DataPicker -->
<script src="https://npmcdn.com/flatpickr/dist/l10n/pt.js"></script>

<!-- POP UP -->
<script type="text/javascript">

    $(".ajax-see").click(function(e) { //clicou no botão editar 

        e.preventDefault();
        let href = $(this).attr('href');
        $.get(href, function(response) {
            $('#modal-content').html(response); //Local em que vai ser aparecer
        });
        
    });

    // LOAD AJAX POST

    $(document).ready(function(){
        $(document).on('click', '.ajax-confirm', function(e) { //clicou no botão editar 

            e.preventDefault();
            let url = $(this).attr('href');
            var btn = $("#btn-confirm").attr('href', url);

        });
    });

    // WHICH SLIDER DOES IT START
    var slideIndex = 1;

    // CALL SECTIONS IN SLIDER
    showSlides(slideIndex, 'vehicle');
    showSlides(slideIndex, 'pet');
    showSlides(slideIndex, 'employees');
    showSlides(slideIndex, 'visitors');
    showSlides(slideIndex, 'dependents');
    
    // CHECK WHICH SLIDER IS CURRENT 
    function currentSlide(numberDot, classElement) {
      showSlides(slideIndex = numberDot, classElement);
    }
    
    // FUNCTION
    function showSlides(numberDot, classElement) {
      var i;
      var slides = document.getElementsByClassName("mySlides-" + classElement);
      var dots = document.getElementsByClassName("dot-" + classElement);
      if (numberDot > slides.length) {slideIndex = 1}
      if (numberDot < 1) {slideIndex = slides.length}
      for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";
      }
      for (i = 0; i < dots.length; i++) {
          dots[i].className = dots[i].className.replace(" dot-active", "");
      }
      if (typeof slides[slideIndex-1] != "undefined") {
        slides[slideIndex-1].style.display = "block";
        dots[slideIndex-1].className += " dot-active";
      }
    }
    
</script>

@endsection
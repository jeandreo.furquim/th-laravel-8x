@if ($paginator->hasPages())
    <div class="d-flex flex-stack flex-wrap pt-5">
        <div class="fs-6 fw-bold text-gray-800">Mostrando {{ $paginator->firstItem() }} a {{ $paginator->lastItem() }} de {{ $paginator->total() }} resultados</div>
        <!--begin::Pages-->
        <ul class="pagination">

            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
            <li class="page-item previous" aria-disabled="true" aria-label="{{ __('pagination.previous') }}">
                <span href="#" class="page-link">
                    <i class="previous"></i>
                </span>
            </li>
            @else
            <li class="page-item previous">
                <a href="{{ $paginator->previousPageUrl() }}" class="page-link">
                    <i class="previous"></i>
                </a>
            </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                <span aria-disabled="true">
                    <span class="relative inline-flex items-center px-4 py-2 -ml-px text-sm font-medium text-gray-800 bg-white border border-gray-300 cursor-default leading-5">{{ $element }}</span>
                </span>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="page-item active" aria-current="page">
                                <a href="#" class="page-link">{{ $page }}</a>
                            </li>
                        @else
                            <li class="page-item">
                                <a href="{{ $url }}" class="page-link">{{ $page }}</a>
                            </li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
            <li class="page-item next">
                <a href="{{ $paginator->nextPageUrl() }}" class="page-link">
                    <i class="next"></i>
                </a>
            </li>
            @else
                <li class="page-item next" aria-disabled="true" aria-label="{{ __('pagination.next') }}">
                    <a href="{{ $paginator->nextPageUrl() }}" class="page-link">
                        <i class="next"></i>
                    </a>
                </li>
            @endif
        </ul>
        <!--end::Pages-->
    </div>
@endif
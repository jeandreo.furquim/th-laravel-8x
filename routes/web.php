<?php

use App\Http\Controllers\admin\UsersController;
use App\Http\Controllers\admin\DashboardController;
use App\Http\Controllers\AccountController;
use App\Http\Controllers\AlbumController;
use App\Http\Controllers\CondominiumController;
use App\Http\Controllers\ConstructionController;
use App\Http\Controllers\DependentsController;
use App\Http\Controllers\EmployeesCondController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\EventController;
use App\Http\Controllers\OccurrenceController;
use App\Http\Controllers\PetsController;
use App\Http\Controllers\PresenceController;
use App\Http\Controllers\RelaseController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\RoomController;
use App\Http\Controllers\SurveyController;
use App\Http\Controllers\TopicController;
use App\Http\Controllers\VehiclesController;
use App\Http\Controllers\VisitorsController;
use App\Http\Controllers\VotesController;
use App\Http\Controllers\BranchController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\ConciergeController;
use App\Http\Controllers\ConfigurationsController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\MaintenanceController;
use App\Http\Controllers\MessagesController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\NotificationsController;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\ProviderController;
use App\Http\Controllers\SubdivisionController;
use App\Http\Controllers\UpdatesController;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth'])->group(function () {  

    // ADMINISTRAÇÃO //

    Route::prefix('administracao')->middleware('access:admin')->group(function () {
        Route::name('admin.')->group(function () {
            
            Route::get('/', [DashboardController::class, 'index'])->name('index');

            // CONDOMNÍNIOS //

             Route::prefix('condominios')->group(function () {
                Route::name('condominiums.')->group(function () {
                    Route::get('/', [CondominiumController::class, 'index'])->name('index');
                    Route::get('/criar', [CondominiumController::class, 'create'])->name('create');
                    Route::post('/criar', [CondominiumController::class, 'store'])->name('store');
                    Route::get('/visualizando/{id}', [CondominiumController::class, 'show'])->name('show');
                    Route::get('/ver-condominio/{id}', [CondominiumController::class, 'session'])->name('session');
                    Route::get('/deletar/{id}', [CondominiumController::class, 'destroy'])->name('destroy');
                    Route::get('/editar/{id}/', [CondominiumController::class, 'edit'])->name('edit');
                    Route::put('/editar/{id}/', [CondominiumController::class, 'update'])->name('update');
                });
            });

            // CONDOMNÍNIOS //

            Route::prefix('subdivisoes')->group(function () {
                Route::name('subdivisions.')->group(function () {
                    Route::get('/', [SubdivisionController::class, 'index'])->name('index');
                    Route::get('/criar', [SubdivisionController::class, 'create'])->name('create');
                    Route::post('/criar', [SubdivisionController::class, 'store'])->name('store');
                    Route::get('/deletar/{id}', [SubdivisionController::class, 'destroy'])->name('destroy');
                    Route::get('/editar/{id}/', [SubdivisionController::class, 'edit'])->name('edit');
                    Route::put('/editar/{id}/', [SubdivisionController::class, 'update'])->name('update');
                });
            });

            // USUÁRIOS //

            Route::prefix('usuarios')->group(function () {
                Route::name('users.')->group(function () {
                    Route::get('/', [UsersController::class, 'index'])->name('index');
                    Route::get('/veiculos', [VehiclesController::class, 'index'])->name('vehicle');
                    Route::get('/pets', [PetsController::class, 'index'])->name('pet');
                    Route::get('/funcionarios', [EmployeesController::class, 'index'])->name('employee');
                    Route::get('/visitantes', [VisitorsController::class, 'index'])->name('visitor');
                    Route::get('/moradores', [DependentsController::class, 'index'])->name('dependent');
                    Route::get('/visualizando/{id}', [UsersController::class, 'show'])->name('show');            
                    Route::get('/editar/{id}', [UsersController::class, 'edit'])->name('edit');
                    Route::put('/editar/{id}', [UsersController::class, 'update'])->name('update');
                    Route::get('/deletar/{id}', [UsersController::class, 'destroy'])->name('destroy');
                    Route::get('/atualizar-status/{id}/{status}', [UsersController::class, 'approve'])->name('approve');
                });
            });

            // NOTIFICATIONS

            Route::prefix('notificar')->group(function () {
                Route::name('notifications.')->group(function () {
                    Route::get('/', [NotificationsController::class, 'index'])->name('index');
                    Route::get('/criar/{id}', [NotificationsController::class, 'create'])->name('create');
                    Route::post('/criar/{id}', [NotificationsController::class, 'store'])->name('store');
                    Route::get('/deletar/{id}', [NotificationsController::class, 'destroy'])->name('destroy');
                    Route::get('/editar/{id}/', [NotificationsController::class, 'edit'])->name('edit');
                    Route::put('/editar/{id}/', [NotificationsController::class, 'update'])->name('update');
                });
            });

            // CONCIERGE //

            Route::prefix('portarias')->group(function () {
                Route::name('concierges.')->group(function () {
                    Route::get('/', [ConciergeController::class, 'index'])->name('index');
                    Route::get('/criar', [ConciergeController::class, 'create'])->name('create');
                    Route::post('/criar', [ConciergeController::class, 'store'])->name('store');
                    Route::get('/deletar/{id}', [ConciergeController::class, 'destroy'])->name('destroy');
                    Route::get('/editar/{id}/', [ConciergeController::class, 'edit'])->name('edit');
                    Route::put('/editar/{id}/', [ConciergeController::class, 'update'])->name('update');
                });
            });

            // CONFIGURATIONS

            Route::prefix('configuracoes')->group(function () {
                Route::name('configurations.')->group(function () {
                    Route::get('/', [ConfigurationsController::class, 'index'])->name('index');
                    Route::post('/criar', [ConfigurationsController::class, 'store'])->name('store');
                    Route::get('/deletar', [ConfigurationsController::class, 'destroy'])->name('destroy');
                    Route::put('/editar', [ConfigurationsController::class, 'update'])->name('update');
                });
            });

        });
    });
    
    // PÁGINA INICIAL //

    Route::get('/', [AccountController::class, 'feed'])->name('feed');
    Route::get('/ficha-tecnica', [UsersController::class, 'condominium'])->name('condominium');
    Route::get('/atualizacoes', [UpdatesController::class, 'updates'])->name('updates');

    // MINHA CONTA //
    
    Route::prefix('minha-conta')->group(function () {
        Route::name('account.')->group(function () {
            Route::get('/', [AccountController::class, 'index'])->name('index');
            Route::get('/editar', [AccountController::class, 'edit'])->name('edit');
            Route::put('/editar', [AccountController::class, 'update'])->name('update');
            Route::put('/contatos', [AccountController::class, 'contacts'])->name('contacts');
            Route::delete('/deletar', [AccountController::class, 'destroy'])->name('destroy');
            Route::get('/publicacoes', [AccountController::class, 'approval'])->name('approval');
            Route::name('see.')->group(function () {
                Route::get('/buscar-veiculo/{id}/', [VehiclesController::class, 'show'])->name('vehicles');
                Route::get('/buscar-pet/{id}/', [PetsController::class, 'show'])->name('pets');
                Route::get('/buscar-funcionario/{id}/', [EmployeesController::class, 'show'])->name('employees');
                Route::get('/buscar-visitante/{id}/', [VisitorsController::class, 'show'])->name('visitors');
                Route::get('/buscar-morador/{id}/', [DependentsController::class, 'show'])->name('dependents');
            });
            Route::name('add.')->group(function () {
                Route::get('/adicionar-veiculo/{id}/', [VehiclesController::class, 'create'])->name('vehicles');
                Route::post('/adicionar-veiculo/{id}/', [VehiclesController::class, 'store'])->name('store.vehicles');
                Route::get('/adicionar-pet/{id}/', [PetsController::class, 'create'])->name('pets');
                Route::post('/adicionar-pet/{id}/', [PetsController::class, 'store'])->name('store.pets');
                Route::get('/adicionar-funcionario/{id}/', [EmployeesController::class, 'create'])->name('employees');
                Route::post('/adicionar-funcionario/{id}/', [EmployeesController::class, 'store'])->name('store.employees');
                Route::get('/adicionar-visitante/{id}/', [VisitorsController::class, 'create'])->name('visitors');
                Route::post('/adicionar-visitante/{id}/', [VisitorsController::class, 'store'])->name('store.visitors');
                Route::get('/adicionar-morador/{id}/', [DependentsController::class, 'create'])->name('dependents');
                Route::post('/adicionar-morador/{id}/', [DependentsController::class, 'store'])->name('store.dependents');
            });
            Route::name('edit.')->group(function () {
                Route::get('/editar-veiculo/{id}/', [VehiclesController::class, 'edit'])->name('vehicles');
                Route::put('/editar-veiculo/{id}/', [VehiclesController::class, 'update'])->name('update.vehicles');
                Route::get('/editar-pet/{id}/', [PetsController::class, 'edit'])->name('pets');
                Route::put('/editar-pet/{id}/', [PetsController::class, 'update'])->name('update.pets');
                Route::get('/editar-funcionario/{id}/', [EmployeesController::class, 'edit'])->name('employees');
                Route::put('/editar-funcionario/{id}/', [EmployeesController::class, 'update'])->name('update.employees');
                Route::get('/editar-visitante/{id}/', [VisitorsController::class, 'edit'])->name('visitors');
                Route::put('/editar-visitante/{id}/', [VisitorsController::class, 'update'])->name('update.visitors');
                Route::get('/editar-morador/{id}/', [DependentsController::class, 'edit'])->name('dependents');
                Route::put('/editar-morador/{id}/', [DependentsController::class, 'update'])->name('update.dependents');
            });
            Route::name('destroy.')->group(function () {
                Route::get('/excluir-veiculo/{id}/', [VehiclesController::class, 'destroy'])->name('vehicles');
                Route::get('/excluir-pet/{id}/', [PetsController::class, 'destroy'])->name('pets');
                Route::get('/excluir-funcionario/{id}/', [EmployeesController::class, 'destroy'])->name('employees');
                Route::get('/excluir-visitante/{id}/', [VisitorsController::class, 'destroy'])->name('visitors');
                Route::get('/excluir-morador/{id}/', [DependentsController::class, 'destroy'])->name('dependents');
            });
        });
    });


    // MENSAGENS //

    Route::prefix('chat')->group(function () {
        Route::name('messages.')->group(function () {
            Route::get('/{id?}', [MessagesController::class, 'index'])->name('index');
            Route::get('/criar/{id}', [MessagesController::class, 'store'])->name('store');
            Route::get('/mensagens/{id}', [MessagesController::class, 'show'])->name('show');
            Route::get('/deletar/{id}/', [MessagesController::class, 'destroy'])->name('destroy');
            Route::get('/procurar/{value}', [MessagesController::class, 'search'])->name('search');
        });
    });


    // COMUNICADOS //

    Route::prefix('comunicados')->group(function () {
        Route::name('relases.')->group(function () {
            Route::get('/todos', [RelaseController::class, 'index'])->name('index');
            Route::get('/visualizando/{id}', [RelaseController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/criar', [RelaseController::class, 'create'])->name('create');
                Route::post('/criar', [RelaseController::class, 'store'])->name('store');
                Route::get('/deletar/{id}/', [RelaseController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [RelaseController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [RelaseController::class, 'update'])->name('update');
            });
            Route::get('/{filter?}/{order?}', [RelaseController::class, 'results'])->name('results');
        });
    });

    // ENQUETES E VOTAÇÕES //

    Route::prefix('enquetes')->group(function () {
        Route::name('surveys.')->group(function () {
            Route::get('/todos', [SurveyController::class, 'index'])->name('index');
            Route::get('/visualizando/{id}', [SurveyController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/criar', [SurveyController::class, 'create'])->name('create');
                Route::post('/criar', [SurveyController::class, 'store'])->name('store');
                Route::get('/deletar/{id}/', [SurveyController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [SurveyController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [SurveyController::class, 'update'])->name('update');
            });
            Route::get('/{filter?}/{order?}', [SurveyController::class, 'results'])->name('results');
        });
    });
    
    // VOTOS //

    Route::prefix('votos')->group(function () {
        Route::name('votes.')->group(function () {
            Route::post('/votar/{id}', [VotesController::class, 'store'])->name('store');
            Route::put('/atualizar/{id}', [VotesController::class, 'update'])->name('update');
        });
    });


    // ESPAÇOS //

    Route::prefix('espacos')->group(function () {
        Route::name('rooms.')->group(function () {
            Route::get('/todos', [RoomController::class, 'index'])->name('index');
            Route::get('/visualizando/{id}', [RoomController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/criar', [RoomController::class, 'create'])->name('create');
                Route::post('/criar', [RoomController::class, 'store'])->name('store');
                Route::get('/deletar/{id}/', [RoomController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [RoomController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [RoomController::class, 'update'])->name('update');
            });
            Route::get('/{filter?}/{order?}', [RoomController::class, 'results'])->name('results');
        });
    });

    // RESERVAS //

    Route::prefix('reservas')->group(function () {
        Route::name('reservations.')->group(function () {
            Route::get('/todas', [ReservationController::class, 'index'])->name('index');
            Route::post('/reservar/{id}', [ReservationController::class, 'store'])->name('store');
            Route::get('/atualizar/{id}/{status}', [ReservationController::class, 'update'])->name('update');
            Route::get('/cancelar/{id}', [ReservationController::class, 'requestCancel'])->name('requestCancel');
        });
    });

    
    // EVENTOS //

    Route::prefix('eventos')->group(function () {
        Route::name('events.')->group(function () {
            Route::get('/todos', [EventController::class, 'index'])->name('index');
            Route::get('/visualizando/{id}', [EventController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/criar', [EventController::class, 'create'])->name('create');
                Route::post('/criar', [EventController::class, 'store'])->name('store');
                Route::get('/deletar/{id}', [EventController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [EventController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [EventController::class, 'update'])->name('update');
            });
            Route::get('/{filter?}/{order?}', [EventController::class, 'results'])->name('results');
        });
    });

    // CHAMADOS //

    Route::prefix('chamados')->group(function () {
        Route::name('occurrences.')->group(function () {
            Route::get('/todos', [OccurrenceController::class, 'index'])->name('index');
            Route::get('/criar', [OccurrenceController::class, 'create'])->name('create');
            Route::post('/criar', [OccurrenceController::class, 'store'])->name('store');
            Route::get('/visualizando/{id}', [OccurrenceController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){
                Route::get('/deletar/{id}', [OccurrenceController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [OccurrenceController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [OccurrenceController::class, 'update'])->name('update');
                Route::get('/status/{id}/', [OccurrenceController::class, 'toApprove'])->name('toApprove');
                Route::get('/estado/{id}/', [OccurrenceController::class, 'upCondition'])->name('upCondition');
            });
            Route::get('/{filter?}/{order?}/{type?}', [OccurrenceController::class, 'results'])->name('results');
        });
    });

    // TÓPICOS //

    Route::prefix('topicos')->group(function () {
        Route::name('topics.')->group(function () {
            Route::get('/todos', [TopicController::class, 'index'])->name('index');
            Route::get('/criar', [TopicController::class, 'create'])->name('create');
            Route::post('/criar', [TopicController::class, 'store'])->name('store');
            Route::get('/visualizando/{id}', [TopicController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/deletar/{id}', [TopicController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [TopicController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [TopicController::class, 'update'])->name('update');
                Route::get('/status/{id}/', [TopicController::class, 'toApprove'])->name('toApprove');
            });
            Route::get('/{filter?}/{order?}/{type?}', [TopicController::class, 'results'])->name('results');
        });
    });

    // FUNCIONÁRIOS //

    Route::prefix('funcionarios')->group(function () {
        Route::name('employeescond.')->group(function () {
            Route::get('/todos', [EmployeesCondController::class, 'index'])->name('index');
            Route::get('/visualizando/{id}', [EmployeesCondController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/criar', [EmployeesCondController::class, 'create'])->name('create');
                Route::post('/criar', [EmployeesCondController::class, 'store'])->name('store');
                Route::get('/deletar/{id}', [EmployeesCondController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [EmployeesCondController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [EmployeesCondController::class, 'update'])->name('update');
            });
            Route::get('/{filter?}/{order?}', [EmployeesCondController::class, 'results'])->name('results');
        });
    });

    // OBRAS //

    Route::prefix('obras')->group(function () {
        Route::name('constructions.')->group(function () {
            Route::get('/todas', [ConstructionController::class, 'index'])->name('index');
            Route::get('/visualizando/{id}', [ConstructionController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/criar', [ConstructionController::class, 'create'])->name('create');
                Route::post('/criar', [ConstructionController::class, 'store'])->name('store');
                Route::get('/deletar{id}', [ConstructionController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [ConstructionController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [ConstructionController::class, 'update'])->name('update');
                Route::post('/adicionar-imagens/{id}/', [ConstructionController::class, 'storeGallery'])->name('storeGallery');
                Route::get('/remover-imagem/{id}/', [ConstructionController::class, 'removeImage'])->name('removeImage');
                Route::post('/adicionar-atualizacao/{id}/', [ConstructionController::class, 'storeUpdate'])->name('storeUpdate');
                Route::get('/excluir-atualizacao/{id}/', [ConstructionController::class, 'destroyUpdate'])->name('destroyUpdate'); 
            });
            Route::get('/{filter?}/{order?}', [ConstructionController::class, 'results'])->name('results');
        });
    });

    // PRESENÇAS //

    Route::prefix('presenca')->group(function () {
        Route::name('presences.')->group(function () {
            Route::post('/registrar/{id}', [PresenceController::class, 'store'])->name('store');
            Route::put('/atualizar/{id}', [PresenceController::class, 'update'])->name('update');
        });
    });


    // BRANCHES //

    Route::prefix('ramais')->group(function () {
        Route::name('branches.')->group(function () {
            Route::get('/todos', [BranchController::class, 'index'])->name('index');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/adicionar', [BranchController::class, 'create'])->name('create');
                Route::post('/adicionar', [BranchController::class, 'store'])->name('store');
                Route::get('/editar/{id}/', [BranchController::class, 'edit'])->name('edit');
                Route::put('/atualizar/{id}', [BranchController::class, 'update'])->name('update');
                Route::get('/excluir/{id}/', [BranchController::class, 'destroy'])->name('destroy');
            });
            Route::get('/', [BranchController::class, 'results'])->name('results');
        });
    });
    

    // PROVIDERS //

    Route::prefix('fornecedores')->group(function () {
        Route::name('providers.')->group(function () {
            Route::get('/todos', [ProviderController::class, 'index'])->name('index');
            Route::get('/visualizando/{id}', [ProviderController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/criar', [ProviderController::class, 'create'])->name('create');
                Route::post('/criar', [ProviderController::class, 'store'])->name('store');
                Route::get('/excluir/{id}/', [ProviderController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [ProviderController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [ProviderController::class, 'update'])->name('update');
            });
            Route::get('/{filter?}/{order?}', [ProviderController::class, 'results'])->name('results');
        });
    });


    // ALBUMS //

    Route::prefix('albuns')->group(function () {
        Route::name('albums.')->group(function () {
            Route::get('/todos', [AlbumController::class, 'index'])->name('index');
            Route::get('/visualizando/{id}', [AlbumController::class, 'show'])->name('show');
                Route::middleware('access:admin')->group(function(){ 
                Route::get('/criar', [AlbumController::class, 'create'])->name('create');
                Route::post('/criar', [AlbumController::class, 'store'])->name('store');
                Route::get('/excluir/{id}/', [AlbumController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [AlbumController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [AlbumController::class, 'update'])->name('update');
                Route::post('/adicionar-imagens/{id}/', [AlbumController::class, 'storeGallery'])->name('storeGallery');
                Route::get('/remover-imagem/{id}/', [AlbumController::class, 'removeImage'])->name('removeImage');
            });
            Route::get('/{filter?}/{order?}', [AlbumController::class, 'results'])->name('results');
        });
    });

    // ENCOMENDAS //

    Route::prefix('encomendas')->group(function () {
        Route::name('packages.')->group(function () {
            Route::get('/todas', [PackageController::class, 'index'])->name('index');
            Route::get('/visualizando/{id}', [PackageController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/criar/{id}/', [PackageController::class, 'create'])->name('create');
                Route::post('/criar/{id}/', [PackageController::class, 'store'])->name('store');
                Route::get('/excluir/{id}/', [PackageController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [PackageController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [PackageController::class, 'update'])->name('update');
                Route::get('/atualizar/{id}/{status}', [PackageController::class, 'updateStatus'])->name('updateStatus');
            });
            Route::get('/{filter?}/{order?}', [PackageController::class, 'results'])->name('results');
        });
    });

    // OBSERVAÇÕES //

    Route::prefix('anotacoes')->middleware('access:admin')->group(function () {
        Route::name('notes.')->group(function () {
            Route::get('/todas', [NoteController::class, 'index'])->name('index');
            Route::get('/criar', [NoteController::class, 'create'])->name('create');
            Route::post('/criar', [NoteController::class, 'store'])->name('store');
            Route::get('/excluir/{id}/', [NoteController::class, 'destroy'])->name('destroy');
            Route::get('/editar/{id}/', [NoteController::class, 'edit'])->name('edit');
            Route::put('/editar/{id}/', [NoteController::class, 'update'])->name('update');
        });
    });

    // MANUTENÇÕES //

    Route::prefix('manutencoes')->group(function () {
        Route::name('maintenances.')->group(function () {
            Route::get('/todas', [MaintenanceController::class, 'index'])->name('index');
            Route::get('/visualizando/{id}', [MaintenanceController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/criar', [MaintenanceController::class, 'create'])->name('create');
                Route::post('/criar', [MaintenanceController::class, 'store'])->name('store');
                Route::get('/excluir/{id}/', [MaintenanceController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [MaintenanceController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [MaintenanceController::class, 'update'])->name('update');
            });
            Route::get('/', [MaintenanceController::class, 'results'])->name('results');
        });
    });

    // DOCUMENTOS //

    Route::prefix('documentos')->group(function () {
        Route::name('documents.')->group(function () {
            Route::get('/', [DocumentController::class, 'index'])->name('index');
            Route::get('/visualizando/{id}', [DocumentController::class, 'show'])->name('show');
            Route::middleware('access:admin')->group(function(){ 
                Route::get('/criar', [DocumentController::class, 'create'])->name('create');
                Route::post('/criar', [DocumentController::class, 'store'])->name('store');
                Route::get('/excluir/{id}/', [DocumentController::class, 'destroy'])->name('destroy');
                Route::get('/editar/{id}/', [DocumentController::class, 'edit'])->name('edit');
                Route::put('/editar/{id}/', [DocumentController::class, 'update'])->name('update');
            });
        });
    });

    // COMENTÁRIOS //

    Route::prefix('comentarios')->group(function () {
        Route::name('comments.')->group(function () {
            Route::get('/', [CommentsController::class, 'index'])->name('index');
            Route::post('/criar', [CommentsController::class, 'store'])->name('store');
            Route::post('/comentarios', [CommentsController::class, 'show'])->name('show');
            Route::get('/deletar/{id}/', [CommentsController::class, 'destroy'])->name('destroy');
            Route::get('/procurar/{value}', [CommentsController::class, 'search'])->name('search');
        });
    });

});

Route::get('subdivisoes/{condominium}/', [SubdivisionController::class, 'load'])->name('subdivisions.load');
Route::get('/atencao', [NotificationsController::class, 'show'])->name('notifications.show');

require __DIR__.'/auth.php';
